
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
// require('@fancyapps/dist/jquery.fancybox.js');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));

// const app = new Vue({
//     el: '#app'
// });

$(document).ready(function(){
  // Trigger button
  $('#trigger-overlay').on('click',function() {
    $('#overlay-search').toggleClass('open');
    // Form input handler
    $('#form-search input[type=text]').focus();
  });
  // Close button
  $('.overlay-close').on('click',function() {
    $('#overlay-search').removeClass('open');
  });
  $("a[data-fancybox].nav-link").fancybox({
    // Custom CSS class for layout
    onInit:function () {
      // Init login first
      $('.login-handler').show();
      $('.register-handler').hide();
    }
  });
  // Holder.addTheme('thumb', {
  //   bg: '#55595c',
  //   fg: '#eceeef',
  //   text: 'Thumbnail'
  // });
  // Account form handler
  $('.form-handler').on('click',function( ) {
      $('.login-handler, .register-handler').find('.message').empty();
      $('.login-handler, .register-handler').find('input').val('');
      // console.log( $('.login-handler, .register-handler').find('message').html() );
      // e.preventDefault();
  });
  // Login Handler
    $('#form-login').submit(function(e) {
      e.preventDefault();
      var inputs = $(this).find('input[class=form-control], button[type=submit]');
      var email = $(this).find('[name=email]').val();
      var password = $(this).find('[name=password]').val();
      var box = $(this).find('.message');
      var $this = $(this);
      var message = 'Not match credentials';
      // Set input on disable first
      inputs.prop('disabled',true).css({'opacity':0.75});
      // Send a POST request
      axios.post(base_URL + 'auth/login', {
          email: email,
          password: password
      },
      // Set axios config
      {
          // `onUploadProgress` allows handling of progress events for uploads
          onUploadProgress: function (progressEvent) {
              // Do whatever you want with the native progress event
              // $(this).find('.message').html('Checking..');
          }

      }).then(function (response) {
          var message = (typeof response.data.messages !== 'undefined') ? response.data.messages : '';
          // Close current fancybox instance
          if (response.data.authenticated == 1) {
              parent.jQuery.fancybox.getInstance().close();
              window.location.href = base_URL + 'activity/upload';
          } else if(message) {
              var display = '<div class="alert alert-warning" role="alert"><span>'+message+'</span></div>';
              $this.find('.message').html(display);
              //setInterval(window.location.href = base_URL,3000);
          } else {
              $this.find('.message').empty();
          }
      })
      .catch(function (error) {
        console.log(error);
      });
      inputs.prop('disabled',false).css({'opacity':1});
    });
    $('.register-trig').on('click', function(){
      $('.login-handler').hide();
      $('.register-handler').show();
      $('form.message').html();
    });
    $('.login-trig').on('click', function() {
      $('.login-handler').show();
      $('.register-handler').hide();
      $('form.message').html();
    });
    // User Form Register Handler
    $('#form-register').submit(function(e) {
        e.preventDefault();
        var $this = $(this);
        var inputs = $this.find('input[class=form-control], button[type=submit]');
        // ---- Inputs Start
        var first_name = $this.find('[name=first_name]').val();
        var last_name = $this.find('[name=last_name]').val();
        var email = $this.find('[name=email]').val();
        var password = $this.find('[name=password]').val();
        var password_confirmation = $this.find('[name=password_confirmation]').val();
        // ---- Inputs End
        var box = $(this).find('.message');
        var $this = $(this);
        var message = 'Not match credentials';
        // Set input on disable first
        inputs.prop('disabled',true).css({'opacity':0.75});
        // Send a POST request
        axios.post(base_URL + 'auth/register', {
            first_name: first_name,
            last_name: last_name,
            email: email,
            password: password,
            password_confirmation: password_confirmation
        },
        // Set axios config
        {
            // `onUploadProgress` allows handling of progress events for uploads
            onUploadProgress: function (progressEvent) {
                // Do whatever you want with the native progress event
                //$this.find('.message').html('Checking..');
            }

        }).then(function (response) {
            // Close current fancybox instance
            var message = ( typeof response.data.messages !== 'undefined' ) ? response.data.messages : '';
            if (response.data.authenticated == 1) {
                // Close fancybox
                parent.jQuery.fancybox.getInstance().close();
                // Redirect
                window.location.href = base_URL + 'activity/upload';
            } else if (typeof response.data.errors !== 'undefined') {
                // Set errors variable
                var errors = response.data.errors ? response.data.errors : '';
                var display = '<div class="alert alert-warning" role="alert"><span>'+errors+'</span></div>';
                $this.find('.message').html(display);
            } else if (message) {
                var display = '<div class="alert alert-warning" role="alert"><span>'+message+'</span></div>';
                $this.find('.message').html(display);
                //setInterval(window.location.href = base_URL,3000);
            } else {
                $this.find('.message').html();
            }
        })
        .catch(function (error) {
          console.log(error);
        });
        inputs.prop('disabled',false).css({'opacity':1});
    });
    // Fancybox Handler
    $('[data-fancybox="gallery"]').fancybox({ 
        toolbar : false
    });
  // $('#app').html('hee hoo');
          // Handle gallery category
          $('select[name=gallery-category]').on('change',function () {
            var url = $(this).val(); // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
            return false;
        });
        // Handle data text
        $('[data-src="#hidden-content-hide"]').on('click',function(){
            var $html = $(this).find('.mx-auto.d-none').text();
            $('#hidden-content-hide').find('.fancybox-container').html($html);
        })   
        $('[data-src="#hidden-content3"]').on('click',function() {
            var $html = $(this).find('.mx-auto.d-none').text();
            var $title = $(this).attr('title');
            var $img = $(this).find('.img-fluid').attr('src');
            var $content = '<h4 class="popper-title">' + $title + '</h4>';
                $content += '<img src="' + $img + '" class="popper-img img-fluid"/>';
                $content += '<div class="popper-description fancybox-description"><p>';
                $content += $title;
                $content += '</p></div>';                
            $('#hidden-content3 .content-popper').empty().html($content);
        });

        $('[data-fancybox="videos"]').fancybox({
            margin : [44,0,22,0],
            showinfo : 0,
            controls : 0,
            infobar : false,
            youtube : {
                controls : 1,
                showinfo : 0
            },
            thumbs : {
                autoStart : true,
                axis      : 'x'
            },
            baseTpl:
            '<div class="fancybox-container" role="dialog" tabindex="-1">' +
            '<div class="fancybox-bg"></div>' +
            '<div class="fancybox-inner">' +
            '<div class="fancybox-infobar">' +
            "<span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span>" +
            "</div>" +
            //'<div class="fancybox-toolbar">{{buttons}}</div>' +
            '<div class="fancybox-navigation">{{arrows}}</div>' +
            '<div class="fancybox-stage"></div>' +
            '<div class="fancybox-caption my-0 py-1"></div>' +
            "</div>" +
            "</div>",
        });

        $(".fancybox-iframe").fancybox({
            margin : [0,0,0,0],
            // Custom CSS class for layout
            baseClass: "content-popper-gallery",
            iframe : {
                margin : [0,0,0,0],
                controls : 1,
                showinfo : 0,
                css : {
                    width : '100%'//,
                    //height : '690px',
                    //padding : '0'
                }
            }
        });
  
  
  
  // Handle wrapper control
  var $imageControl = $('.image-editor');
  // Handle image loader
  var $imageCropper = $('.image-editor').cropit({
      'width':'460',
      'smallImage':'allow',
      'height':'460',
      'maxZoom':'1.25',
      'minZoom':'fit',
      'initialZoom':'image',
      'export': {
          type: 'image/png',
          quality: 1,
          originalSize: true
      },
      //'freeMove':true,
      onFileChange:function(obj) {
          var $msg = $imageControl.find('.msg');
          $msg.empty();
          // Validate type of file on client side
          if(['image/jpeg', 'image/jpg','image/png'].indexOf(obj.target.files[0].type) == -1) {
              if($body.hasClass('device-xxs')) {
                  $msg.html('Sorry : Only JPG and PNG allowed');
              } else {
                  alert('Sorry : Only JPG and PNG allowed');
              }
              return;
          }
          if(obj.target.value) {
              $imageControl.find('.btn-handler').removeClass('d-none');
          } else {
              $imageControl.find('.btn-handler').hideClass('d-none');
          }

          // var input = obj.target;
          // var reader = new FileReader();
          // reader.onload = function(){
          //     var dataURL = reader.result;
          //     $('input[name=image-data-real]').val(dataURL);
          // };
          // reader.readAsDataURL(input.files[0]);

      },
      onOffsetChange:function(obj) {                                
          // console.log($imageControl.cropit('exportZoom'));
          // console.log($imageControl.cropit('zoom'));
          // console.log($imageControl.cropit('imageSize'));
          // console.log($imageControl.cropit('offset'));
          // var $offset = $imageControl.cropit('offset');                
          // $('input[name=image-data-offset-x]').val($offset.x);
          // $('input[name=image-data-offset-y]').val($offset.y);
      }
  });

  $imageControl.find('.handler-edit').on('click', function() {
    var $text = $(this).text();
    if ($text == 'Edit') {
        $(this).html('Done');
        $imageControl.find('.form-submit-handler button[type="submit"]').toggle();
    } else {                
        $(this).html('Edit');
        $imageControl.find('.form-submit-handler button[type="submit"]').toggle();
    }
    $('.cropit-preview').find('.image-frame').toggle();
    $('.fileUpload').toggle();
  });

  // Handle image rotation
  // $('.rotate-cw-btn').click(function() {
      // $imageCropper.cropit('rotateCW');
  // });

  // Handle image rotation
  $('.rotate-ccw-btn').click(function() {
    $imageCropper.cropit('rotateCCW');
  });

  // Handle form submit
  $('#playground form').submit(function() {
      // Find inputs
      var submitBtn = $(this).find('button[type="submit"]');
      var textarea  = $(this).find('textarea[name="caption"]');
      // Find message box 
      var msgBox = $('.msg');
      // Move cropped image data to hidden input
      var imageData = $imageCropper.cropit('export');
      var formData = $(this);
      $('input[name=image-data]').val(imageData);
      // Print HTTP request params
      var formValue = $(this).serialize();
      // $('#result-data').text(formValue);
      // Set returned data
      var returned = '';
      // Disabled all inputs after click before HTTP request
      submitBtn.prop('disabled',true);
      textarea.prop('disabled',true);            
      // Set post ajax request
      axios.post(formData.attr('action'), formValue)
      .then(function (response) {
          var message = (typeof response.data.result !== 'undefined') ? response.data.result : '';
          if (message.code == 1) {
              // Close current fancybox instance                
              // parent.jQuery.fancybox.getInstance().close();
              window.location.href = base_URL + 'activity';
          } else if(message.code == 2) {
              msgBox.empty();
              returned = '<div class="alert alert-warning" role="alert"><ul class="list-unstyled p-0 m-0">';
              $.each(message.errors,function (l, obj) {
                  returned += '<li><i class="fas fa-asterisk"></i> ' + obj + '</li>';
              });
              returned += '</ul></div>';
              msgBox.html(returned);
          } else if(message) {
              var display = '<div class="alert alert-warning" role="alert"><span>'+message.text+'</span></div>';
              msgBox.html(display);
              //setInterval(window.location.href = base_URL,3000);
          } else {
              msgBox.empty();
          }
          submitBtn.prop('disabled',false);
          textarea.prop('disabled',false);
      })
      .catch(function (error) {
        console.log(error);
      });
      // Prevent the form from actually submitting
      return false;
  });
  $('.download-btn').click(function() {
      var imageData = $imageCropper.cropit('export');
      window.open(imageData);
  });
  // Handle frame image click 
  $('.frame a').on('click',function() {
      // Remove active classe for other ahref
      $('.frame a').removeClass('active').addClass('ilist');
      // Set current ahref to active
      $(this).addClass('active').removeClass('ilist');
      var frame = $(this).data('src');
      // Remove any frame class
      $('.image-frame').removeClass('frame1-main frame2-main frame3-main');
      $('.image-frame').addClass(frame);
      // Set hidden input data
      $('input[name=frame-data]').val(frame);
  });
  // Set first frame to click active
  $( ".frame a" ).eq(0).click();
  // $('.frame a').addClass('ilist');    

});
