@extends('layouts.master')

@section('content')

<div class="bg-light py-5">
    <div class="page-header">
        <h1 class="text-center"><span>Upload</span></h1>
    </div>
    <div class="row-fluid">
        <div class="mx-auto">            
            <div id="playground" class="row py-0 m-0 m-md-3 py-md-4">
                <div class="col-12 frame-holder m-0 p-0 pl-sm-3">
                    <div class="text-center text-sm-left clearfix py-2"><h5>Choose Frame</h5></div>
                    <div class="d-flex">
                      <div class="frame text-left">
                        <a href="javascript:;" class="frame-one ilist" data-src="frame1-main">
                          <img src="{{asset('images/frame/frame1.png')}}" class="img-fluid" alt="Frame 1">
                        </a>
                      </div>
                      <div class="frame text-left">
                        <a href="javascript:;" class="frame-two ilist" data-src="frame2-main">
                          <img src="{{asset('images/frame/frame2.png')}}" class="img-fluid" alt="Frame 2">
                        </a>
                      </div>
                      <div class="frame text-left">
                        <a href="javascript:;" class="frame-three ilist" data-src="frame3-main">
                          <img src="{{asset('images/frame/frame3.png')}}" class="img-fluid" alt="Frame 3">
                        </a>
                      </div>
                    </div>
                    <div class="col-12 col-sm-12 m-0 p-0 mx-auto">
                        {!! Form::open(['route' => 'activity.response']) !!}
                            <div class="image-editor my-1 my-sm-4 mr-sm-3">
                                <div class="form-group">
                                    <div class="box-handler">
                                        <div class="cropit-preview">
                                            <div class="image-frame"></div>
                                        </div>
                                    </div>
                                    <div class="form-input-handler">
                                        <div class="msg pb-2"></div>
                                        <div class="image-control">
                                            <div class="fileUpload btn btn-rounded btn-warning btn-handler">
                                                <label for="fileBrowser">Browse</label>
                                                {{-- {!! Form::file('', ['id'=>'fileBrowser','class'=>'cropit-image-input upload','onchange'=>'previewFile()']); !!} --}}
                                                {!! Form::file('', ['id'=>'fileBrowser','class'=>'cropit-image-input upload']); !!}
                                            </div>
                                            <div class="btn-handler d-none mx-1">
                                                <a href="javascript:;" class="rotate-ccw-btn mx-2 text-brown" data-toggle="button" aria-pressed="false" autocomplete="off"><i class="fas fa-sync"></i></a>
                                                <input type="range" class="cropit-image-zoom-input align-middle mr-3" style="width:76px">
                                            </div>
                                            <a href="javascript:;" class="btn btn-rounded btn-brown btn-handler handler-edit d-none" data-toggle="button" aria-pressed="false" autocomplete="off">Edit</a>                                    
                                        </div>
                                    </div>
                                    <div class="form-submit-handler">
                                        <div class="btn-handler d-none">
                                            <div class="form-group mt-4">
                                                <div class="mx-auto col-9 col-md-10 p-0">
                                                    {!! Form::textarea('caption','',['class'=>'form-control','rows'=>2,'cols'=>64,'maxlength'=>144,'placeholder'=>'Caption Image']) !!}
                                                </div>           
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-rounded btn-brown">Submit</button>
                                            </div>
                                        </div>
                                    </div>    
                                </div>                        
                            </div>
                            {!! Form::hidden('image-data', '') !!}
                            {!! Form::hidden('frame-data', '') !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        {{-- <div id="result" class="container py-2">
            <code>$form.serialize() =</code>
            <code id="result-data"></code>
        </div> --}}
    </div>
</div>
@if(!Auth::user())
{{-- <div class="container-fluid">
	<h4>Sign In to upload</h4>
	<div class="form-horizontal">
		{!! Form::open(['route' => 'auth.login','method'=>'POST']) !!}
			<div class="form-group{{ $errors->first('email', ' has-error') }}">
				{!! Form::label('email', 'Email',['class'=>'col-md-4 control-label']) !!}
				<div class="col-md-6">
				 	{!! Form::text('email',Input::old('email', ''),[
						'placeholder'=>'Email',
						'name'=>'email',
						'id'=>'email',
						'class' => 'email form-control']) !!}
				</div>
			</div>
			<div class="form-group">				
				{!! Form::label('password', 'Password',['class'=>'col-md-4 control-label']) !!}
				<div class="col-md-6">
					{!! Form::password('password',[
						'placeholder'=>'Password',
						'name'=>'password',
						'id'=>'password',
						'class' => 'password form-control']); !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-md-offset-4">
					<div class="checkbox">
						<label for="remember">
							{!! Form::checkbox('remember', ''); !!} Remember Me
						</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-md-offset-4">
					{!! Form::button('Submit',['type'=>'submit', 'class'=>'btn btn-primary']); !!}
					<a class="btn btn-link" href="{{ url('/auth/password/email') }}">Forgot Your Password?</a>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
	<hr class="clear"/>
	<h4>Social Sign In</h4>	
	<div class="form-group">
		<div class="col-lg-12 col-md-12 col-xs-12">
			<a href="{{ url('auth/social/twitter') }}" title="Login with Twitter" class="btn btn-info btn-md"><span class="fab fa-twitter"></span>&nbsp; Login with Twitter</a>
			<a href="{{ url('auth/social/facebook') }}" title="Login with Facebook" class="btn btn-primary btn-md"><span class="fab fa-facebook"></span>&nbsp; Login with Facebook</a>
			<a href="{{ url('auth/social/linkedin') }}" title="Login with Linkedin" class="btn btn-success btn-md"><span class="fab fa-linkedin"></span>&nbsp; Login with LinkedIn</a>
			<a href="{{ url('auth/social/google') }}" title="Login with Google" class="btn btn-warning btn-md"><span class="fab fa-google-plus"></span>&nbsp; Login with Google</a>
		</div>
	</div>
</div> --}}
@else

@endif

@stop

@section('js')
<script src="{{ asset('js/jquery.min.js') }}"></script>
@stop