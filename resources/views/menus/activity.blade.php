@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="mx-auto">
            @if ($canvas)
                <img src="{{asset('storage/uploads/images/'.$canvas->file_name)}}" class="text-center rounded rounded-circle" />
            @endif    
        </div>
    </div>
</div>
<div class="bg-brown-alt">
    <div class="container py-3">
        <h1 class="text-gold text-center h1-gallery">
          <span>Gallery</span>
        </h1>
        <div class="row mx-auto">
            @if($images)
                @foreach($images as $image)
                    <div class="col-12 col-md-3 mb-3 text-center">
                        <figure class="gray imghvr-zoom-in">
                            <img src="{{ asset('storage/uploads/images/'.$image->file_name) }}" class="img-fluid ilist-c">
                            <figcaption class="text-center">
                                <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">
                                    <?php 
                                    // dd($image->participant);
                                    $participated = '';
                                    if($image->participant['name']) {
                                        $participated = $image->participant['name'];
                                    } 
                                    if (!$participated && $image->participant['first_name'] && $image->participant['last_name']) {
                                        $participated = $image->participant['first_name'] .' '.$image->participant['last_name'];
                                    } else {
                                        $participated = $image->participant['email'];
                                    }
                                    ?>
                                    <?php echo $participated; ?>
                                </h5>
                                <p class="ih-zoom-in ih-delay-sm">{{ $image->title }}</p>
                                <div class="ih-zoom-in ih-delay-md">
                                    <a href="{{ asset('storage/uploads/images/'.$image->file_name) }}" data-fancybox="gallery" data-caption="{{$image->title}} @ <?php echo $participated;?>" class="btn btn-warning btn-rounded btn-sm btn-popup text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                                    <div class="mx-auto">
                                                                        
                                    </div>
                                </div>
                            </figcaption>
                        </figure>	
                    </div>
                @endforeach
            @else
            <div class="col-12 col-md-3 mb-3 text-center">
                <figure class="gray imghvr-zoom-in">
                    <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                    <figcaption class="text-center">
                        <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                        <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                        <div class="ih-zoom-in ih-delay-md">
                            <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                            <div class="mx-auto">
                                                                
                            </div>
                        </div>
                    </figcaption>
                </figure>	
            </div>
            <div class="col-12 col-md-3 mb-3 text-center">
                <figure class="gray imghvr-zoom-in">
                    <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                    <figcaption class="text-center">
                        <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                        <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                        <div class="ih-zoom-in ih-delay-md">
                            <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                            <div class="mx-auto">
                                                                
                            </div>
                        </div>
                    </figcaption>
                </figure>   
            </div>
            <div class="col-12 col-md-3 mb-3 text-center">
                <figure class="gray imghvr-zoom-in">
                    <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                    <figcaption class="text-center">
                        <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                        <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                        <div class="ih-zoom-in ih-delay-md">
                            <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                            <div class="mx-auto">
                                                                
                            </div>
                        </div>
                    </figcaption>
                </figure>   
            </div>
            <div class="col-12 col-md-3 mb-3 text-center">
                <figure class="gray imghvr-zoom-in">
                    <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                    <figcaption class="text-center">
                        <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                        <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                        <div class="ih-zoom-in ih-delay-md">
                            <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                            <div class="mx-auto">
                                                                
                            </div>
                        </div>
                    </figcaption>
                </figure>   
            </div>
            <div class="col-12 col-md-3 mb-3 text-center">
                <figure class="gray imghvr-zoom-in">
                    <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                    <figcaption class="text-center">
                        <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                        <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                        <div class="ih-zoom-in ih-delay-md">
                            <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                            <div class="mx-auto">
                                                                
                            </div>
                        </div>
                    </figcaption>
                </figure>  
            </div>
            <div class="col-12 col-md-3 mb-3 text-center">
                <figure class="gray imghvr-zoom-in">
                    <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                    <figcaption class="text-center">
                        <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                        <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                        <div class="ih-zoom-in ih-delay-md">
                            <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                            <div class="mx-auto">
                                                                
                            </div>
                        </div>
                    </figcaption>
                </figure>   
            </div>
            <div class="col-12 col-md-3 mb-3 text-center">
                <figure class="gray imghvr-zoom-in">
                    <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                    <figcaption class="text-center">
                        <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                        <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                        <div class="ih-zoom-in ih-delay-md">
                            <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                            <div class="mx-auto">
                                                                
                            </div>
                        </div>
                    </figcaption>
                </figure>  
            </div>
            <div class="col-12 col-md-3 mb-3 text-center">
                <figure class="gray imghvr-zoom-in">
                    <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                    <figcaption class="text-center">
                        <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                        <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                        <div class="ih-zoom-in ih-delay-md">
                            <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                            <div class="mx-auto">
                                                                
                            </div>
                        </div>
                    </figcaption>
                </figure> 
            </div>
            @endif
        </div>
    </div>

    <div class="container py-4 text-uppercase text-center">
        <a href="{{route('activity.gallery')}}" class="btn btn-rounded bg-button-alt-warning">Lihat Galeri</a>
    </div>
    
    @if(!Auth::user())
    {{-- <div class="container">
        <h4>Sign In to upload</h4>
        <div class="form-horizontal">
            {!! Form::open(['route' => 'auth.login','method'=>'POST']) !!}
                <div class="form-group{{ $errors->first('email', ' has-error') }}">
                    {!! Form::label('email', 'Email',['class'=>'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('email',Input::old('email', ''),[
                            'placeholder'=>'Email',
                            'name'=>'email',
                            'id'=>'email',
                            'class' => 'email form-control']) !!}
                    </div>
                </div>
                <div class="form-group">				
                    {!! Form::label('password', 'Password',['class'=>'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::password('password',[
                            'placeholder'=>'Password',
                            'name'=>'password',
                            'id'=>'password',
                            'class' => 'password form-control']); !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label for="remember">
                                {!! Form::checkbox('remember', ''); !!} Remember Me
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        {!! Form::button('Submit',['type'=>'submit', 'class'=>'btn btn-primary']); !!}
                        <a class="btn btn-link" href="{{ url('/auth/password/email') }}">Forgot Your Password?</a>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
        <hr class="clear"/>
        <h4>Social Sign In</h4>	
        <div class="form-group">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <a href="{{ url('auth/social/twitter') }}" title="Login with Twitter" class="btn btn-info btn-md"><span class="fab fa-twitter"></span>&nbsp; Login with Twitter</a>
                <a href="{{ url('auth/social/facebook') }}" title="Login with Facebook" class="btn btn-primary btn-md"><span class="fab fa-facebook"></span>&nbsp; Login with Facebook</a>
                <a href="{{ url('auth/social/linkedin') }}" title="Login with Linkedin" class="btn btn-success btn-md"><span class="fab fa-linkedin"></span>&nbsp; Login with LinkedIn</a>
                <a href="{{ url('auth/social/google') }}" title="Login with Google" class="btn btn-warning btn-md"><span class="fab fa-google-plus"></span>&nbsp; Login with Google</a>
            </div>
        </div>
    </div> --}}
    @else
    <!--div class="container">
        <div id="playground" class="mx-auto">
            <form action="#" class="form-horizontal">
                <div class="image-editor">
                    <input type="file" class="cropit-image-input">
                    <div class="cropit-preview"></div>
                    <div class="image-size-label">
                        Resize image
                    </div>
                    <input type="range" class="cropit-image-zoom-input">
                    <input type="hidden" name="image-data" class="hidden-image-data" />
                    <button type="submit" class="btn btn-primary btn-block">Submit</button>
                </div>
            </form>
        </div>
        <div id="result" class="py-5">
            <code>$form.serialize() =</code>
            <code id="result-data"></code>
        </div>
    </div-->
    @endif
    <div class="bg-ramadhan-gold-down"></div>
</div>    
@stop

@section('js')
<script src="{{ asset('js/jquery.min.js') }}"></script>
@stop