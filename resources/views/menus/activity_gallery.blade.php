@extends('layouts.master')

@section('content')

    <div class="bg-light py-5" id="activity">
        <div class="container">
            <h1 class="text-gold text-center h1-gallery">
              <span>Gallery</span>
            </h1>
            <div class="row mx-auto py-3">
                @if($images->count() > 0)
                    @foreach($images as $image)
                        <div class="col-12 col-md-3 mb-3 text-center">
                            <figure class="gray imghvr-zoom-in">
                                <img src="{{ asset('storage/uploads/images/'.$image->file_name) }}" class="ilist-c">
                                <figcaption class="text-center">
                                    <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">
                                        <?php 
                                        $participated = '';
                                        if($image->participant['name']) {
                                            $participated = $image->participant['name'];
                                        } 
                                        if (!$participated || $image->participant['first_name'] && $image->participant['last_name']) {
                                            $participated = $image->participant['first_name'] .' '.$image->participant['last_name'];
                                        } else {
                                            $participated = $image->participant['email'];
                                        }
                                        ?>
                                        <?php echo @$participated;?>
                                    </h5>
                                    <p class="ih-zoom-in ih-delay-sm">{{ $image->title }}</p>
                                    <div class="ih-zoom-in ih-delay-md">
                                        <a href="{{ asset('storage/uploads/images/'.$image->file_name) }}" data-zoom="false" data-fancybox="images" data-caption="{{$image->title}} @ <?php echo @$participated;?>" data-fancybox class="btn btn-warning btn-rounded btn-sm btn-popup text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                                        <div class="mx-auto">
                                                                            
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>	
                        </div>
                    @endforeach
                    <div class="col-sm-12">
                        <div class="text-center">
                            {!! $images->render('pagination::bootstrap-4') !!}
                        </div>
                    </div>    
                @else
                <div class="col-12 col-md-3 mb-3 text-center">
                    <figure class="gray imghvr-zoom-in">
                        <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                        <figcaption class="text-center">
                            <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                            <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                            <div class="ih-zoom-in ih-delay-md">
                                <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                                <div class="mx-auto">
                                                                    
                                </div>
                            </div>
                        </figcaption>
                    </figure>	
                </div>
                <div class="col-12 col-md-3 mb-3 text-center">
                    <figure class="gray imghvr-zoom-in">
                        <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                        <figcaption class="text-center">
                            <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                            <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                            <div class="ih-zoom-in ih-delay-md">
                                <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                                <div class="mx-auto">
                                                                    
                                </div>
                            </div>
                        </figcaption>
                    </figure>   
                </div>
                <div class="col-12 col-md-3 mb-3 text-center">
                    <figure class="gray imghvr-zoom-in">
                        <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                        <figcaption class="text-center">
                            <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                            <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                            <div class="ih-zoom-in ih-delay-md">
                                <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                                <div class="mx-auto">
                                                                    
                                </div>
                            </div>
                        </figcaption>
                    </figure>   
                </div>
                <div class="col-12 col-md-3 mb-3 text-center">
                    <figure class="gray imghvr-zoom-in">
                        <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                        <figcaption class="text-center">
                            <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                            <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                            <div class="ih-zoom-in ih-delay-md">
                                <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                                <div class="mx-auto">
                                                                    
                                </div>
                            </div>
                        </figcaption>
                    </figure>   
                </div>
                <div class="col-12 col-md-3 mb-3 text-center">
                    <figure class="gray imghvr-zoom-in">
                        <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                        <figcaption class="text-center">
                            <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                            <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                            <div class="ih-zoom-in ih-delay-md">
                                <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                                <div class="mx-auto">
                                                                    
                                </div>
                            </div>
                        </figcaption>
                    </figure>  
                </div>
                <div class="col-12 col-md-3 mb-3 text-center">
                    <figure class="gray imghvr-zoom-in">
                        <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                        <figcaption class="text-center">
                            <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                            <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                            <div class="ih-zoom-in ih-delay-md">
                                <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                                <div class="mx-auto">
                                                                    
                                </div>
                            </div>
                        </figcaption>
                    </figure>   
                </div>
                <div class="col-12 col-md-3 mb-3 text-center">
                    <figure class="gray imghvr-zoom-in">
                        <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                        <figcaption class="text-center">
                            <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                            <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                            <div class="ih-zoom-in ih-delay-md">
                                <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                                <div class="mx-auto">
                                                                    
                                </div>
                            </div>
                        </figcaption>
                    </figure>  
                </div>
                <div class="col-12 col-md-3 mb-3 text-center">
                    <figure class="gray imghvr-zoom-in">
                        <img src="{{ asset('images/ramadhan/ramadhan-thumb-gallery.jpg') }}" class="img-fluid ilist-c">
                        <figcaption class="text-center">
                            <h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ 'Foobar' }}</h5>
                            <p class="ih-zoom-in ih-delay-sm">{{ 'Foobar' }}</p>
                            <div class="ih-zoom-in ih-delay-md">
                                <a href="javascript:;" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                                <div class="mx-auto">
                                                                    
                                </div>
                            </div>
                        </figcaption>
                    </figure> 
                </div>
                @endif
            </div>
        </div>
        <div class="container py-3 text-uppercase text-center">
            @if($user)
                <a href="{{ route('activity.upload') }}" class="btn btn-rounded bg-button-alt-warning form-handler">Upload Photo</a>      
            @else    
                <a data-fancybox data-src="#hidden-content" href="javascript:;" class="btn btn-rounded bg-button-alt-warning form-handler">Upload Photo</a>
            @endif
        </div>
    </div>

@stop

@section('js')
<script src="{{ asset('js/jquery.min.js') }}"></script>
@stop