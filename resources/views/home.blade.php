@extends('layouts.app')

@section('content')
<section class="section-divider c-intended" style="margin-top: 0 !important;background: #ffe9eb;">
        <div class="container">
          <div class="row">
            <div class="col-md-12" style="text-align: center;">
              <img src="assets/img/lfp.png" style="width: 80%;margin: auto;text-align: center;">
              <br><br>
              <p>Pilih berbagai frame menarik yang kamu suka, lalu upload dan share melalui social media dan dapatkan berbagai hadiah menarik</p>
              {{-- <a href="javascript:void(0);" onclick="Popup.show('authentication');">
                <img src="assets/img/buttonupload.png"  style="width: 80%;margin: auto;text-align: center;">
              </a> --}}
              @if(!$user) 
                <a data-fancybox data-src="#hidden-content" href="javascript:;" data-options='{"touch" : false}'>
                  <img src="{{asset('assets/img/buttonupload.png')}}" style="width: 80%;margin: auto;text-align: center;">
                </a> 
              @else
                <a href="{{route('activity.upload')}}" data-options='{"touch" : false}'>
                  <img src="{{asset('assets/img/buttonupload.png')}}" style="width: 80%;margin: auto;text-align: center;">
                </a>
              @endif
               <br><br>
            </div>

            <div class="col-md-12 intended-content-wrapper">

              <div id="myparallaxslider" class="parallaxslider col-lg-12">
                <!-- 1. Bekerja -->
                <div class="slide" data-pagination="Sign Up">

                  <div class="bgoverlay" data-bgimage="{{asset('assets/img/pslider/bubble.png')}}"></div>
                  <div class="col-lg-12 mobile-image">
                    <div class="top-mobile-image">
                      <img src="{{asset('assets/img/signup.png')}}" alt="Sign UP"/>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="c-profile-box shadowed">
                      <div class="content">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 desktop-image">
                    <div class="desc">
                      <img src="{{asset('assets/img/signup.png')}}" alt="signup"/>
                    </div>
                  </div>
                </div>
                <div class="slide" data-pagination="Upload">
                  <div class="col-lg-12 mobile-image">
                    <div class="top-mobile-image">
                      <img src="{{asset('assets/img/upload.png')}}" alt="Sign UP"/>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="c-profile-box shadowed">
                      <div class="content">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 desktop-image">
                    <div class="desc">
                      <img src="{{asset('assets/img/upload.png')}}" alt="signup"/>
                    </div>
                  </div>
                </div>
                <div class="slide" data-pagination="Share">
                  <div class="col-lg-12 mobile-image">
                    <div class="top-mobile-image">
                      <img src="{{asset('assets/img/post.png')}}" alt="Sign UP"/>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="c-profile-box shadowed">
                      <div class="content">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 desktop-image">
                    <div class="desc">
                      <img src="{{asset('assets/img/post.png')}}" alt="signup"/>
                    </div>
                  </div>
                </div>

              </div>

              <div class="pagination col-lg-12">

                <p>Geser untuk melihat ilustrasi lainnya</p>
                <input class="parallaxsliderNavigation" />
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="section-divider c-intended" style="margin-top: 0 !important;background: #ffe9eb;">
        <div class="container">
          <div class="row">
            <div class="col-md-12" style="text-align: center;"><div class="col-md-12" style="text-align: center;">
              <img src="{{asset('assets/img/gallery.png')}}" style="width: 80%;margin: auto;text-align: center;">
<img src="{{asset('assets/img/1.png')}}" style="/* width: 80%; */margin: auto;text-align: center;">
<img src="{{asset('assets/img/2.png')}}" style="/* width: 80%; */margin: auto;text-align: center;">
             </div>
             </div>


          </div>
        </div>
      </section>
@endsection
