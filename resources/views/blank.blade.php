@extends('layouts.app')

@section('content')
<style>    

    .owl-nav.disabled {
        display: block !important;
        position: absolute;
        top: 50%;
        width: 100%;
    }

    .owl-carousel .owl-nav button.owl-prev, .owl-carousel .owl-nav button.owl-next, .owl-carousel button.owl-dot {
        font-size: 40px !important;
        position: absolute;
        left: 0;
        background: #ffffff8a !important;
        padding: 0 15px !important;
    }

    .owl-carousel .owl-nav button.owl-next {
        left: auto;
        right: 0;
    }

    .blank .fancybox-content, .blank .fancybox-content::after, .blank .fancybox-content::before {
        background: none !important;        
    }
</style>
<div class="">
@if($rows->count())
    <div class="main-slider owl-carousel">
        @foreach($rows as $row)
            @if(File::exists(public_path('uploads/activity/'.$row->filename)))
                <div class="item" data-hash="{{$row->id}}">
                    <h4 class="ff-merriweather font-weight-bold text-brown text-center my-2">{{$row->caption}}</h4>
                    <img src="{{asset('uploads/activity/'.rawurlencode($row->filename))}}" alt="{{ $row->caption }}">
                </div>
            @endif
        @endforeach
    </div>
    <div class="text-center">
        <h5 class="ff-merriweather font-weight-bold text-brown text-center header-ams my-2">{{$campaign->name}}</h5>
    </div>
    <ul class="nav-main-slider">
    <?php $l=0;?>
    @foreach($rows as $row)
        @if(File::exists(public_path('uploads/activity/'.$row->filename)))
            <li style="background-image:url('{{asset('uploads/activity/'.rawurlencode($row->filename))}}')">
                <a href="#{{$row->id}}" class="m-0 p-0{{$l==0 ? ' active':''}}"></a>
            </li>
        @endif
    <?php $l++;?>    
    @endforeach
    </ul>
@elseif ($activity)
{{-- {{ dd($activity) }} --}}
    <div class="main-slider owl-carousel">
        @foreach($activity as $key)
            @if(File::exists(public_path('uploads/'.$key->image)))
                <div class="item mb-3" data-hash="{{$key->id}}">
                    {{-- <h4 class="ff-merriweather font-weight-bold text-brown text-center my-2">{{$key->name}}</h4> --}}
                    <div class="video-thumb mx-auto" style="background-image: url('{{asset('uploads/512x490px_'.$key->image)}}'); background-size:cover">
                        <h5 class="text-white uppercase pt-3 header-ams-white">{{str_limit($key->name,20)}}</h5>
                        <div class="position-absolute" style="top: 9%;right: 0%;left:0%; text-align:center">
                            <a data-fancybox href="{{$key->url}}" class="btn-play-video mx-auto d-none"></a>
                        </div>
                        <div class="position-absolute" style="bottom: 10%;right: 0%;left:0%; text-align:center;text-shadow: 1px 1px 1px rgba(0,0,0,0.6);">
                            <div class="text-center mt-3 mb-2">
                                <h6 class="vote_count">
                                    <i class="fas fa-heart text-ambass"></i> {{ $key->votes->count() }} Votes
                                </h6>
                            </div>
                            {{-- <a data-fancybox data-src="#hidden-content1" href="#{{$key->slug}}" class="btn btn-warning btn-rounded open-modal d-none">Vote</a> --}}
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
    <div class="text-center">
        <h5 class="ff-merriweather font-weight-bold text-brown text-center header-ams my-2">{{$campaign->name}}</h5>
    </div>
    <ul class="nav-main-slider">
    <?php $l=0;?>
    @foreach($activity as $key)
        @if(File::exists(public_path('uploads/'.$key->image)))
            <li style="background-image:url('{{asset('uploads/'.rawurlencode($key->image))}}');">
                <a href="#{{$key->id}}" class="m-0 p-0{{$l==0 ? ' active':''}}"></a>
            </li>
        @endif
    <?php $l++;?>    
    @endforeach
    </ul>
@else
<div class="main-slider owl-carousel">
    <div class="item">
        <h4 class="ff-merriweather font-weight-bold text-brown text-center my-2">{{$campaign->name}}</h4>
        <img src="{{asset('uploads/'.rawurlencode($campaign->image))}}" alt="{{ $campaign->name }}">
    </div>
</div>
@endif
</div>
@stop
