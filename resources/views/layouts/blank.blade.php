<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ @$title .' - '. @$company_name->value }}</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Merriweather" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.bundle.css') }}">
    <script type="text/javascript">var base_URL = "{{ url('/') }}/";</script>
    <style>
    body {
        width: 1px; 
        min-width: 80%;
        overflow: hidden;
    }
    @media screen and (max-width: 576px) {
        .video-thumb {
            max-height: 305px !important;
            width:320px !important;
            height:305px !important;
        }
        .nav-main-slider li {
            width: 60px !important;
        }
    }
    </style>
</head>
<body class="blank">
    @yield('content')
    <!-- Scripts -->
<script src="{{ mix('js/app.bundle.js') }}"></script>
</body>
</html>
