<!DOCTYPE html>
<html dir="ltr" lang="{{ app()->getLocale() }}">
<head>
<meta name="author" content="{{ @$company_name->value }}" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- SEO tag -->
<meta property="og:url" content="{{ url()->current() }}" />
@if(isset($ogs))
@foreach ($ogs as $og => $graph)
<meta property="{{$og}}" content="{{ $graph }}" />
@endforeach
@endif
<meta property="og:locale" content="en_US" />
<meta property="og:site_name" content="{{ @$company_name->value }}" />
<meta property="og:type" content="website" />
<meta property="fb:app_id" content="1911132052253936" />
<meta name="keyword" content="{{ @$meta_keywords->value }}" />
<meta name="description" content="{{ @$meta_description->value }}" />
<link rel="canonical" href="{{ url()->current() }}" />
<script type="application/ld+json">
{"description":"{{@$meta_description->value}}","@type":"WebPage","url":"{{url()->current()}}","publisher":{"@type":"Organization","logo":{"@type":"ImageObject","url":"{{asset('images/logo/logo.png')}}"}},"headline":"{{@$title .' - '. @$company_name->value}}","@context":"http://schema.org"}
</script>
<!-- End SEO tag -->
<title>{{ @$title .' - '. @$company_name->value }}</title>
{{-- <link href="https://fonts.googleapis.com/css?family=Montserrat|Merriweather|Material+Icons" rel="stylesheet"> --}}
@if(isset($styles)) @foreach ($styles as $style => $css) {!! Html::style($css, ['rel'=>'stylesheet']) !!} @endforeach @endif
{{-- <link rel="stylesheet" href="{{ mix('css/app.bundle.css') }}"> --}}
<script type="text/javascript">var base_URL = "{{ url('/') }}/";</script>   
<link rel="stylesheet" href="{{ asset('css/fontawesome.css') }}">
<link rel="stylesheet" href="{{ asset('css/imagehover.css') }}">
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('js/fancybox/jquery.fancybox.min.css') }}">
<link href="{{ asset('assets/css/main.css') }}" rel="stylesheet" media="screen" />
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
@yield('css')
</head>
<body class="main-body">
  <section class="main-header">
      <div class="c-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="burger-menu js-mobile-menu">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
              </div>
              <div class="header-container">
                <a href="{{route('/')}}" class="logo">
                  <img src="https://www.instax.co.uk/wp-content/uploads/2017/07/instax_logo.png" alt="Logo Instax" class="i-love-life" />
                </a>
                <div class="header-action">
                  {{-- <a href="javascript:void(0);" onClick="Popup.show('authentication');" class="btn min primary">Login</a>                                       --}}
                  @if(!$user) 
                    <a data-fancybox data-src="#hidden-content" href="javascript:;" class="btn min primary form-handler" data-options='{"touch" : false}'>Login</a> 
                  @else
                    <a href="{{route('auth.logout')}}" class="btn min primary form-handler" data-options='{"touch" : false}'>Logout</a>
                  @endif
                  <div class="bg-warning" id="hidden-content" style="display:none">
                    <div class="login-handler">
                        <h4 class="container font-weight-bold py-2">Login</h4>
                        {!! Form::open([
                            'route' => 'auth.login',
                            'class' => 'from-horizontal',
                            'id'    => 'form-login',
                            'method' => 'POST'
                        ]) !!}
                            <div class="container"><div class="message"></div></div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label sr-only">{{ trans('label.email_address') }}</label>
                                    <div class="input-group input-group-lg mb-3">
                                        <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-envelope"></i></span></div>
                                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ trans('label.email_address') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label sr-only">{{ trans('label.password') }}</label>
                                    <div class="input-group input-group-lg mb-3">
                                        <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-lock"></i></span></div>
                                        <input type="password" class="form-control" name="password" placeholder="{{ trans('label.password') }}">
                                        <div class="input-group-append"><a class="text-dark small d-none" href="{{route('auth.password')}}">Lupa password?</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <div class="col-md-12">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> {{trans('label.remember_me')}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="my-0">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-dark btn-md btn-rounded btn-login px-2">{{ trans('label.login') }}</button>
                                    <div class="mt-3">
                                        Belum punya akun?
                                        <a class="text-dark register-trig" href="javascript:;" data-rel="{{ route('auth.register') }}">
                                        Sign Up
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="container pb-3">
                                <div class="text-center">Atau</div>
                                <div class="text-center pb-3">Login dengan Sosial Media</div>
                                <div class="row">
                                    <div class="col-md-12 col-xs-12 text-center">
                                        <a href="{{ url('auth/social/facebook') }}" title="{{ trans('label.login_with') }} Facebook" class="btn btn-info text-primary btn-md btn-rounded px-2"><span class="fab fa-facebook"></span>&nbsp; Facebook</a>
                                        {{-- <a href="{{ url('auth/social/twitter') }}" title="{{ trans('label.login_with') }} Twitter" class="btn btn-info btn-md btn-rounded"><span class="fab fa-twitter"></span>&nbsp; Twitter</a>  --}}                                        
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="register-handler" style="display:none;">
                        <div class="container">
                            <h4 class="font-weight-bold py-2">Register</h4>
                            {!! Form::open([
                                'route' => 'auth.register',
                                'class' => 'from-horizontal',
                                'id'    => 'form-register',
                                'method' => 'POST'
                            ]) !!}
                            <div class="container row"><div class="message"></div></div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-lg-6">
                                        <label class="control-label">{{ trans('label.first_name') }}</label>
                                        <input type="text" class="form-control" placeholder="First Name" name="first_name" value="{{ old('first_name') }}">
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="control-label">{{ trans('label.last_name') }}</label>
                                        <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="{{ old('last_name') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-lg-12">
                                        <label class="control-label">{{ trans('label.email_address') }}</label>
                                        <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-lg-12">
                                        <label class="control-label">{{ trans('label.password') }}</label>
                                        <input type="password" class="form-control" placeholder="Password" name="password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-lg-12">
                                        <label class="control-label">{{ trans('label.confirm_password') }}</label>
                                        <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation">
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="form-group{{ $errors->first('g-recaptcha-response', ' has-error') }}">
                                <div class="form-row">
                                    <div class="col-lg-12">
                                        {!! Form::label('captcha', 'Captcha', ['class'=>'control-label']); !!}
                                        {!! app('captcha')->display(); !!}
                                        <span class="help-block">{{{ $errors->first('g-recaptcha-response', ':message') }}}</span>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn btn-warning btn-rounded btn-login">{{ trans('label.register') }}</button>
                                        <div class="align-self-center py-3">
                                            Sudah punya akun? <a href="javascript:;" class="login-trig">Login</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close(); !!}
                        </div>
                    </div>
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="mobile-logo" style="visibility: hidden;">
          <a href="#">
            <img src="https://www.instax.co.uk/wp-content/uploads/2017/07/instax_logo.png" src="https://www.instax.co.uk/wp-content/uploads/2017/07/instax_logo.png"/>
            <img src="https://www.instax.co.uk/wp-content/uploads/2017/07/instax_logo.png" alt="Instax Logo" />
          </a>
        </div>
      </div>
      <div class="container">
        <div class="main-text container">
          <div class="row">
            <div class="col-xs-12">

              <div class="title active-banner" style="visibility: hidden;">
                <div class="text">I love</div>
                <div class="outer">
                  <div class="inner">
                    <span class="active-banner" data-header="life">Life</span>
                    <span data-header="sports">Sports</span>
                    <span data-header="giving">Giving</span>
                    <div class="up-down-arrow">
                      <img src="{{ asset('assets/img/icon/arrow-up-down.svg') }}" alt="">
                      <a href="#">top</a>
                      <a href="#">bottom</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="desc-banner" style="visibility: hidden;">
                <p class="active-banner" data-header="life">
                  Perlindungan sesuai kebutuhan hidupmu.
                </p>
                <p data-header="sports">
                  Jalankan Hobi Tanpa Cemas Pada Diri Sendiri.
                </p>
                <p data-header="giving">
                  Waktunya memberi perlindungan pada yang tersayang.
                </p>
              </div>
              <div class="btn-container" style="visibility: hidden;">
                <a href="product-life.html" class="btn min primary">Lihat Detail</a>
                <a href="javascript:void(0);" class="btn min border-blue js-video-popup-trigger" data-video-url="https://www.youtube.com/embed/HfnS6la-RnE"><img src="{{ asset('assets/img/icon/play-circle.svg')}}" alt="Play Icon"> Lihat Video</a>
              </div>
            </div>
          </div>
        </div>

        <div class="main-image">
          <div class="row">
            <div class="col-md-12">
              <div class="img-container" style="visibility: hidden;">
                <img class="active-banner" data-header="life" src="{{ asset('assets/img/dummy/home-kado.html')}}" alt="">
                <img data-header="sports" src="{{ asset('assets/img/bg-sports-person.png')}}" alt="">
                <img data-header="giving" src="{{ asset('assets/img/bg-kado-person.png')}}" alt="">
              </div>
            </div>              
          </div>
        </div>
      </div>
    </section>
      @yield('content')
    <div class="c-popup with-header popup-authentication" id="authentication">
      <div class="bg"></div>
      <div class="popup-wrapper">
        <a href="javascript:void(0);" class="close"></a>
        <div class="content">

          <div class="popup-header text-center">
            <h2>
              Selamat Datang <br/>
            </h2>
          </div>

          

        </div>
      </div>
    </div>
</div>
<div class="c-back-to-top js-back-to-top">
  Kembali ke atas
</div>

<!-- js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script> --}}
@yield('js')
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/cropit/dist/jquery.cropit.js') }}"></script>
<script src="{{ asset('js/fancybox/jquery.fancybox.min.js') }}"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>
<script src="{{ asset('js/library.js')}}"></script>
@if(Session::has('flash_thankyou'))
<?php
$path = asset('uploads/ramadhan/'.$participant->images->file_name);
$type = pathinfo($path, PATHINFO_EXTENSION);
$data = file_get_contents($path);
$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
?>
@endif
@if(isset($scripts)) @foreach($scripts as $script => $js) {!! Html::script($js, ['rel'=>$script]) !!} @endforeach @endif
</body>
</html>