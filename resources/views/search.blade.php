@extends('layouts.master')

@section('content')
    <div class="container py-2 px-4">
        <div class="content py-5 px-4">
            <h1 class="main-head">Search for : 
                <small class="text-warning main-head">{{ $query }}</small>
            </h1>
            <div class="row pb-5 mb-5">
                @if($collections->count())
                    @foreach ($collections as $collection)
                    <div class="media">
                        <div class="col-lg-12">
                            <h3>{{$collection->title}}</h4>
                            <p>{{str_limit(strip_tags($collection->description),100,'...')}}</p>
                            <div class="float-right"><a href="{{route('news_event.show',$collection->slug)}}"><i class="fa fa-search"></i> See more</a></div>
                        </div>
                    </div>
                    @endforeach
                @else 
                <div class="py-5 my-5 text-center mx-auto"><h3 class="py-5 my-5 font-weight-bold text-warning">Not Found Search</h3></div>
                @endif
            </div>
            {!! $collections->render() !!}
        </div>
    </div>
@stop
