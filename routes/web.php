<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () { return view('welcome'); })->name('/');

Route::get('/', 'HomeController@index')->name('/');

// Search
Route::post('search', 'SearchController@index')->name('search.post');
Route::get('search/{query}', 'SearchController@search')->name('search.list');

// Ramadhan Activity routes
Route::get('activity', 'ActivityController@index')->name('activity');
Route::get('activity/upload', 'ActivityController@upload')->name('activity.upload');
Route::post('activity/response', 'ActivityController@response')->name('activity.response');
Route::get('activity/response', 'ActivityController@response')->name('activity.response');
Route::get('activity/gallery', 'ActivityController@gallery')->name('activity.gallery');
Route::get('activity/dashboard', 'ActivityController@dashboard')->name('activity.dashboard');

// User related routes...
Route::get('profile',['as'=>'profile','uses'=>'UsersController@profile']);
Route::get('profile/{id}', ['as'=>'profile.edit', 'uses'=>'UsersController@edit']);
Route::patch('profile/{id}', ['as'=>'profile.update', 'uses'=>'UsersController@update']);
// Socialite Routes...
Route::get('auth/social/{provider}', 'Auth\AuthSocialController@redirectToProvider');
Route::get('auth/social', 'Auth\AuthSocialController@handleProviderCallback');
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin')->name('auth.login');
Route::post('auth/login', 'Auth\AuthController@postLogin')->name('auth.login');
Route::get('auth/logout', 'Auth\AuthController@getLogout')->name('auth.logout');
// Password forgotten routes...
Route::get('auth/password/email','Auth\PasswordController@getEmail')->name('auth.password');
Route::post('auth/password/email','Auth\PasswordController@postEmail')->name('auth.password');;
// Registration routes...
Route::get('auth/register','Auth\AuthController@getRegister')->name('auth.register');
Route::post('auth/register','Auth\AuthController@postRegister')->name('auth.register');


// Disable checkpoints (throttling, activation) for demo purposes
// for now in home area does not require to auth
// Sentinel::disableCheckpoints();

Route::get('wait', function()
{
  return View::make('User::sentinel.wait');
});

Route::get('activate/{id}/{code}', function($id, $code)
{
  $user = Sentinel::findById($id);

  if ( ! Activation::complete($user, $code))
  {
      return Redirect::to("login")
          ->withErrors('Invalid or expired activation code.');
  }

  return Redirect::to('login')
        ->withSuccess('Account activated.');
})->where('id', '\d+');

Route::get('reactivate', function()
{
  if ( ! $user = Sentinel::check())
  {
      return Redirect::to('login');
  }

  $activation = Activation::exists($user) ?: Activation::create($user);

  // This is used for the demo, usually you would want
  // to activate the account through the link you
  // receive in the activation email
  Activation::complete($user, $activation->code);

  // $code = $activation->code;

  // $sent = Mail::send('sentinel.emails.activate', compact('user', 'code'), function($m) use ($user)
  // {
  //  $m->to($user->email)->subject('Activate Your Account');
  // });

  // if ($sent === 0)
  // {
  //  return Redirect::to('register')
  //      ->withErrors('Failed to send activation email.');
  // }

  return Redirect::to('account')
      ->withSuccess('Account activated.');
})->where('id', '\d+');

Route::get('deactivate', function()
{
  $user = Sentinel::check();

  Activation::remove($user);

  return Redirect::back()
      ->withSuccess('Account deactivated.');
});

Route::get('reset', function()
{
  if(Sentinel::check()) {
      return Redirect::to(route('admin.dashboard'));
  }

  return View::make('User::sentinel.reset.begin');
});

Route::post('reset', function()
{
  $rules = [
      'email' => 'required|email',
  ];

  $validator = Validator::make(Input::get(), $rules);

  if ($validator->fails())
  {
      return Redirect::back()
          ->withInput()
          ->withErrors($validator);
  }

  $email = Input::get('email');

  $user = Sentinel::findByCredentials(compact('email'));

  if ( ! $user)
  {
      return Redirect::back()
          ->withInput()
          ->withErrors('No user with that email address belongs in our system.');
  }

  $reminder = Reminder::exists($user) ?: Reminder::create($user);

  $code = $reminder->code;

  $sent = Mail::send('User::sentinel.emails.reminder', compact('user', 'code'), function($m) use ($user)
  {
    $m->to($user->email)->subject('Reset your account password.');
  });

  if ($sent === 0)
  {
    return Redirect::to('register')
        ->withErrors('Failed to send reset password email.');
  }

  return Redirect::to('wait');
});

Route::get('reset/{id}/{code}', function($id, $code)
{
  if(Sentinel::check()) {
      return Redirect::to(route('admin.dashboard'));
  }

  $user = Sentinel::findById($id);

  return View::make('User::sentinel.reset.complete');

})->where('id', '\d+');

Route::post('reset/{id}/{code}', function($id, $code)
{
  $rules = [
      'password' => 'required|confirmed',
  ];

  $validator = Validator::make(Input::get(), $rules);

  if ($validator->fails())
  {
      return Redirect::back()
          ->withInput()
          ->withErrors($validator);
  }

  $user = Sentinel::findById($id);

  if ( ! $user)
  {
      return Redirect::back()
          ->withInput()
          ->withErrors('The user no longer exists.');
  }

  if ( ! Reminder::complete($user, $code, Input::get('password')))
  {
      return Redirect::to(route('admin.login'))
          ->withErrors('Invalid or expired reset code.');
  }

  return Redirect::to(route('admin.login'))
      ->withSuccess("Password Reset.");

})->where('id', '\d+');

Route::group(['prefix' => 'account', 'before' => 'auth'], function()
{

  Route::get('/', function()
  {
      $user = Sentinel::getUser();

      $persistence = Sentinel::getPersistenceRepository();

      return View::make('User::sentinel.account.home', compact('user', 'persistence'));
  });

  Route::get('kill', function()
  {
      $user = Sentinel::getUser();

      Sentinel::getPersistenceRepository()->flush($user);

      return Redirect::back();
  });

  Route::get('kill-all', function()
  {
      $user = Sentinel::getUser();

      Sentinel::getPersistenceRepository()->flush($user, false);

      return Redirect::back();
  });

  Route::get('kill/{code}', function($code)
  {
      Sentinel::getPersistenceRepository()->remove($code);

      return Redirect::back();
  });

});

Auth::routes();

Route::get('routes',function(){
  dd(Route::getRoutes());
});

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
