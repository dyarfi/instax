<?php namespace App\Http\Controllers\Auth;

use Illuminate\Routing\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Input, Sentinel, Socialite, Request;
//use App\Modules\User\Model\User;
use App\Participant;
// or use App\Modules\User\Model\Participant;

class AuthSocialController extends Controller {

    //use AuthenticatesUsers;
    protected $redirectAfterLogout = '/';
	  protected $redirectTo = 'activity/upload';
    protected $loginPath = '/';
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {

        $this->auth = $auth;

    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();

        if ($registeredUser = Participant::where('provider_id', $user->getId())->first()) {
            Auth::login($registeredUser);
        } elseif ($registeredUser = Participant::where('email', $user->getEmail())->first()) {
            $registeredUser->provider_id = $user->getId();
            $registeredUser->save();
            Auth::login($registeredUser);
        } else {
            $register              = new Participant();
            $register->name        = $user->getName();
            $register->email       = $user->getEmail() ? $user->getEmail() : $user->getId();
            $register->provider_id = $user->getId();
            $register->password    = $user->getId();
            $register->provider    = 'facebook';
            $register->avatar      = $user->getAvatar();
            $register->status      = 1;
            $register->save();
            Auth::login($register);
        }

        return redirect()->intended('activity#videos');
    }

    /**
     * Obtain the user information from Database.
     *
     * @return Response
     */
    public function profile()
    {
        dd(Auth::user());
    }
    /**
     * Logout user social login.
     *
     * @return Response
     */
    public function logout()
    {
        Auth::logout();
        return redirect()->intended();
    }

}
