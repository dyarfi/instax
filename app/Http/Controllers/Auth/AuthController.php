<?php namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth as Auth;
use Illuminate\Contracts\Auth\Guard;
// use App\User;
use App\Participant;
use Validator;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/
	// Set call for parent trait class
	use AuthenticatesUsers;
	public $redirectAfterLogout = 'activity';
	public $redirectTo = 'activity/upload';
	public $loginPath = '/';
	// Redirect after login
	public $redirectPath = 'activity/upload';

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Support\Facades\Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth/*, Registrar $registrar*/)
	{
		
		$this->auth = $auth;
		//$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}

	/**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
		// Set validator
		$validator = $this->validator($request->all());
		
		// Check if fails
        if ($validator->fails()) {
			if ($request->ajax()) {
				// Set messages
				return response()->json(['messages'=>'', 'errors'=> $validator->errors()->first()]);
			} else {
				// Set redirect		
				return redirect($this->loginPath())
					->withInput($request->only('name', 'remember'))
					->withErrors([
						'email' => $this->getFailedLoginMessage(),
				]);
			}
        } else { 
			// Set default
			$authenticated = $this->create($request->all());
			// Logged the users
			$logged = $this->attemptLogin($request);
			// Checking
			if ($authenticated) {
				$request->session()->flash('flash_message', 'Registered!');
			}
			// Check the request type
			if($request->ajax() && $authenticated) {
				return response()->json([
				'authenticated' => $logged,
				'messages' => $this->getMessage('Registering..')
				]);
			} else {
				// Redirect to redirect path
				return redirect()->intended($this->redirectPath());
			}
		}
		
    }

	/**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {	
        // usleep(2000);
        // Set validation
        $this->validate((array) $request, [
          'email' => 'required|email',
          'password' => 'required'
        ]);
        // Set credentials
        $credentials = $request->only('email', 'password');
        // Checking attempt        
        $authenticated = $this->attemptLogin($request);
        // Checking
        if ($authenticated) {
          $request->session()->flash('flash_message', 'Logged In!');
        }

        //dd($this->redirectPath());
        // Check the request type
        if($request->ajax()) {
          return response()->json([
          'authenticated' => $authenticated,
          'messages' => $this->getMessage($this->getFailedLoginMessage())
          ]);
        } else {
          // Redirect to redirect path
          return redirect()->intended($this->redirectPath());
        }
            
        return redirect($this->loginPath())
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
              'email' => $this->getFailedLoginMessage(),
          ]);
	}    
	
	/**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
		// Logging out
        if ($this->auth->logout()) {
			// Start flashing messages
			Request::session()->flash('flash_message', 'Logged In!');
		}
 
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : URL::previous()); /** '/' **/
    }

	/**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage($messages='')
    {
        return ($messages) ? $messages : 'These credentials do not match our records.';
	}
	

	/**
     * Get the login message.
     *
     * @return string
     */
    protected function getMessage($messages='')
    {
        return ($messages) ? $messages : 'These credentials do not match our records.';
	}
	
    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return view('auth.login');
    }

	/**
     * Get the path to the login route.
     *
     * @return string
     */
    public function loginPath()
    {
        return property_exists($this, 'loginPath') ? $this->loginPath : '/auth/login';
	}
	
	/**
     * Validates a user.
     *
     * @param  array  $data
     * @param  mixed  $id
     * @return \Illuminate\Support\MessageBag
     */
    protected function validate($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }

	/**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
		
		return Validator::make($data, [
			      'first_name' => 'required|min:2|string|max:255',
            'last_name' => 'required|min:2|string|max:255',
            'email' => 'required|string|email|max:255|unique:participants',
			      'password' => 'required|min:4|confirmed'
        ]);
	}
	
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Participant
     */
    protected function create(array $data)
    {	
		// Debug
		// return response()->json($data);
		// Create
        return Participant::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'status'=>1
        ]);
    }
}
