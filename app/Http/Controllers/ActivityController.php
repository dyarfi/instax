<?php namespace App\Http\Controllers;

// Load Laravel classes
use Route, Request, Input, Validator, Redirect, Session, Storage, Image, Auth;
// Load main models
use App\Modules\Page\Model\Menu, 
App\Modules\Page\Model\Page,
// App\Modules\Participant\Model\Participant,
App\Modules\Participant\Model\Participant,
App\Modules\Participant\Model\Image as ImageParticipant;

class ActivityController extends BasePublic {

	// Participant / User set default
  public $participant = '';
  // Check uploaded canvas
  public $canvas_uploaded = '';
  // Participant canvas
  public $canvas = '';
  
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {

		// Parent constructor
		parent::__construct();

    // Image Participant Model
    $this->ImageParticipant = new ImageParticipant;

    // Set participant
    if ($this->user) {
        
        $this->participant = $this->user;
        
        $this->canvas = Participant::find($this->user->id)
        ->images()
        ->where('participant_id',$this->user->id)
        ->where('type','canvas')
        ->first();

        $this->canvas_uploaded = Participant::find($this->user->id)->images()->where('type','canvas')->count();
        
    }
    
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {

    // Set return data
    $images = ImageParticipant::notCanvas($this->participant?$this->participant->id:'');
    
    // Set pagination path
    // $images->setPath('gallery');

    // Get the page path that requested
		$path = pathinfo(Request::path(), PATHINFO_BASENAME);

    // Set data to return
    $data = [
        'images'=>$images,
        'canvas'=>$this->canvas,
        'participant'=>$this->participant,
        'menu'=>$this->menu->where('slug', $path)->first()
    ];
              
    $title  = ($this->canvas) ? '' : '';
    $image  = ($this->canvas) ? '' : asset('storage/uploads/'. '');

		// Set open graph		
		$ogs = [
			'og:title' => $title,
			// 'og:description' => strip_tags($campaign->description),
			'og:description' => '',
			'og:image' => $image
		];
		
		// Set views
		return $this->view('menus.activity')->data($data)
		// ->ogs($ogs)
    // ->scripts([
    //   'library'=>asset('js/library.js')
    // ])
		->title('Page | Activity - Homepage ');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function upload() {

    // Check existing participant and image 
    // if($this->participant !='' && $this->participant['images'] || !$this->user) {
    if($this->participant =='' && !$this->user) {

        // Redirect to redirect path
        return redirect()->intended(route('activity.gallery'));
        
    }
    
    // Get the page path that requested
    $path = pathinfo(Request::path(), PATHINFO_BASENAME);
        
		// Set data to views
    $data = [
      'menu'=>$this->menu->where('slug', $path)->first(),
    ];

		// Return view
    return $this->view('menus.activity_form')
    ->data($data)
    ->title('Page | Activity - Instax Upload Image');

	}


  // Response on uploader
	public function response() {

        // Request inputs
        $input = Input::all();
        
        // Detect if data sent by POST and is an Ajax Requested POST
        if(Request::ajax() && Request::isMethod('post')) {

            // Set validation 
            $rules = [
                // Base64image validation extended rules see @ AppServiceProvider.php
                'image-data'    => 'base64image|required',
                'caption'  	    => 'required|max:255'
            ];

			      // Set validation messages
            $messages = $this->validateImageParticipant($input, $rules);

            // Default result empty variable
            $result = [];

            // Checking data for validation
            // if ($input["image-data"] && $this->participant['images'] == '' && $messages->isEmpty()) {
            if ($input["image-data"] && $messages->isEmpty()) {

                // -------- Cropit --- start
                $frame = public_path('images/'.$input["frame-data"].'-490.png');
                // Set image filename
                $filename = date('y-m-d-').uniqid().'.jpg';
                // Stored default image first
                Storage::disk('local_upload_images')->put($filename, file_get_contents($input["image-data"]));
                // Make from file in upload directory and modified with intervention to add frame                
                $image = Image::make(public_path('storage/uploads/images/'. $filename))->insert($frame,'center')->save(public_path('storage/uploads/images/'. $filename,100));
                // --------- Cropit --- end
                
                // Check image
                if ($image) {

                    // Participant input
                    $object['participant_id'] 	= $this->user->id;
                    $object['type'] 			      = $this->canvas_uploaded ? 'second' : 'canvas';
                    $object['title']            = $input['caption'];
                    $object['file_name']		    = $filename;
                    $object['status'] 		  	  = 1;

                    // Update Participant Data
                    $this->ImageParticipant->create($object);

                    // Flash Messages
                    Request::session()->flash('flash_thankyou', 'Participated!');

                    // Send success message
                    $result['result']['code'] = 1;
                    $result['result']['text'] = 'Success';
                    $result['result']['file'] = $filename;
                    
                } else {

                    // Send failed message
                    $result['result']['code'] = 0;
                    $result['result']['text'] = 'Failed to Upload';

                }

            } else {
                
                // Send fail and validation message
                $result['result']['code'] = 2;
                $result['result']['text'] = 'Error';
                $result['result']['errors'] = $messages ? $messages : 'Unable to Upload, try again later..';
            }
            
			// Load data into json 
			return response()->json($result);
		}
	}

  /**
   * Display all gallery resources.
   *
   * @return View
   */
  public function gallery () {
      
    // Get data from database
    // $image = $this->ImageParticipant->where('slug',$slug)->first();
    // $data = ['image'=>$image];
    $data = ['images'=>ImageParticipant::active()
    ->with('participant')
    ->orderBy('created_at', 'DESC')
    ->take(200)
    ->paginate(8)];

    // Return data and view
    return $this->view('menus.activity_gallery')->data($data)
    // ->scripts([
    //   'library'=>asset('js/library.js')
    // ])
    ->title('Page | Activity - Image Gallery');
  }

  /**
   * Display participant dashboard.
   *
   * @return View
   */
  public function dashboard() {
    
  }
  /**
	 * Validates participant inputs.
	 *
	 * @param  array  $data
	 * @param  mixed  $id
	 * @return \Illuminate\Support\MessageBag
	 */
	protected function validateImageParticipant($data, $rules)
	{
		$validator = Validator::make($data, $rules);

		$validator->passes();

		return $validator->errors();
	}

}
