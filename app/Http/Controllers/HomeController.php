<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Participant\Model\Participant;
use App\Modules\Participant\Model\Image;

class HomeController extends BasePublic
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      	// Parent constructor
        parent::__construct();
        // Middleware
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      // Set open graph		
      $ogs = [
        'og:title' => 'Home',
        // 'og:description' => strip_tags($campaign->description),
        'og:description' => ''//,
        //'og:image' => $image
      ];
      // Set data
      $data = ['participant' => Image::with('participant')->active()->orderBy('created_at','DESC')->get()];
      
      // Set views
      return $this->view('home')->data($data)
      ->title('Home | Instax - '. ' ');
    }
}
