<?php namespace App\Providers;

// Load Service Provider Support
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
// Load App Setting Model
use App\Modules\User\Model\Setting;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		// Load from setting from DB
		$dbset = new Setting;

		// Load config/setting.php file
		$setting = config('setting');

		// Share a var with all views : $admin_url
		view()->share('admin_url', $setting['admin_url']);

		// Share a var with all views : $admin_app
		view()->share('admin_app', isset($dbset) ? $dbset->slug('site-name')->value : $setting['admin_app']);

		// Share a var with all views : $admin_url
		view()->share('company_name', isset($dbset) ? $dbset->slug('company-name')->value : $setting['company_name']);

		// Returning the current class name and action
		app('view')->composer('Admin::layouts.template', function($view)
	    {
	    	// $basename =  explode("@", str_replace('Controller','',class_basename(Route::getCurrentRoute()->getActionName())));
	        // $action = app('request')->route()->getAction();
			$action = app('request')->route()->getActionName();

	        // $controller = class_basename($action['controller']);
			$controller = class_basename($action);

			if (str_contains($controller, 'Controller@')) {

	        	list($controller, $action) = explode('Controller@', $controller);

	    	} else {

				list($controller, $action) = explode('@', $controller);

	    	}

        	$view->with(compact('controller', 'action'));

        });
        
        Validator::extend('base64', function ($attribute, $value, $parameters, $validator) {
            if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $value)) {
                return true;
            } else {
                return false;
            }
        });
        
        Validator::extend('base64image', function ($attribute, $value, $parameters, $validator) {
            $explode = explode(',', $value);
            $allow = ['png', 'jpg', 'svg'];
            $format = str_replace(
                [
                    'data:image/',
                    ';',
                    'base64',
                ],
                [
                    '', '', '',
                ],
                $explode[0]
            );
            // check file format
            if (!in_array($format, $allow)) {
                return false;
            }
            // check base64 format
            if (!preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $explode[1])) {
                return false;
            }
            return true;
        });
	    
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
