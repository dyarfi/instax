<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// ******************* Admin Routes ********************* { //
/*
 *
 * Administrator panel routes
 *
 */
Route::prefix(config('setting.admin_url'))->group( function()
{
    // News DataTables routes
    Route::get('newscategories/datatable', 'App\Modules\News\Controller\NewsCategories@datatable')->name('admin.newscategories.datatable');
    // News Controller routes
    Route::get('newscategories', 'App\Modules\News\Controller\NewsCategories@index')->name('admin.newscategories.index');
    Route::get('newscategories/export', 'App\Modules\News\Controller\NewsCategories@export')->name('admin.newscategories.export');
    Route::get('newscategories/create', 'App\Modules\News\Controller\NewsCategories@create')->name('admin.newscategories.create');
    Route::post('newscategories/create', 'App\Modules\News\Controller\NewsCategories@store')->name('admin.newscategories.store');
    Route::post('newscategories/change', 'App\Modules\News\Controller\NewsCategories@change')->name('admin.newscategories.change');
    // Put the method with the parameter below the static method
    Route::get('newscategories/{id}/show', 'App\Modules\News\Controller\NewsCategories@show')->name('admin.newscategories.show');
    Route::get('newscategories/{id}', 'App\Modules\News\Controller\NewsCategories@edit')->name('admin.newscategories.edit');
    Route::post('newscategories/{id}', 'App\Modules\News\Controller\NewsCategories@update')->name('admin.newscategories.update');
    Route::get('newscategories/{id}/trash', 'App\Modules\News\Controller\NewsCategories@trash')->name('admin.newscategories.trash');
    Route::get('newscategories/{id}/restored', 'App\Modules\News\Controller\NewsCategories@restored')->name('admin.newscategories.restored');
    Route::get('newscategories/{id}/delete', 'App\Modules\News\Controller\NewsCategories@delete')->name('admin.newscategories.delete');
    // News DataTables routes
    Route::get('news/datatable', 'App\Modules\News\Controller\NewsController@datatable')->name('admin.news.datatable');
    // News Controller routes
    Route::get('news', 'App\Modules\News\Controller\NewsController@index')->name('admin.news.index');
    Route::get('news/export', 'App\Modules\News\Controller\NewsController@export')->name('admin.news.export');
    Route::get('news/create', 'App\Modules\News\Controller\NewsController@create')->name('admin.news.create');
    Route::post('news/create', 'App\Modules\News\Controller\NewsController@store')->name('admin.news.store');
    Route::post('news/change', 'App\Modules\News\Controller\NewsController@change')->name('admin.news.change');
    // Put the method with the parameter below the static method
    Route::get('news/{id}/show', 'App\Modules\News\Controller\NewsController@show')->name('admin.news.show');
    Route::get('news/{id}', 'App\Modules\News\Controller\NewsController@edit')->name('admin.news.edit');
    Route::post('news/{id}', 'App\Modules\News\Controller\NewsController@update')->name('admin.news.update');
    Route::get('news/{id}/trash', 'App\Modules\News\Controller\NewsController@trash')->name('admin.news.trash');
    Route::get('news/{id}/restored', 'App\Modules\News\Controller\NewsController@restored')->name('admin.news.restored');
    Route::get('news/{id}/delete', 'App\Modules\News\Controller\NewsController@delete')->name('admin.news.delete');
    // Put other methods
    Route::get('news/tags/all', 'App\Modules\News\Controller\NewsController@tags')->name('admin.news.tags');
    Route::get('news/tags/{id}/show', 'App\Modules\News\Controller\NewsController@tagsShow')->name('admin.news.tags.show');

});
