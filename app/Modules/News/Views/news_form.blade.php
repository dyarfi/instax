@extends('Admin::layouts.template')

{{-- Page content --}}
@section('body')

<div class="page-header">
	<h1>{{ $mode == 'create' ? 'Create News' : 'Update News' }} <small>{{ $mode === 'update' ? $row->name : null }}</small></h1>
</div>

{!! Form::open([
    'route' => ($mode == 'create') ? 'admin.news.create' : ['admin.news.update', $row->id],
	'files' => true
]) !!}

	<div class="form-group{{ $errors->first('name', ' has-error') }}">
		{!! Form::label('name', 'Name'); !!}
		{!! Form::text('name',Input::old('name', $row->name),[
			'placeholder'=>'Enter the News Name.',
			'name'=>'name',
			'id'=>'name',
			'class'=>'form-control']); !!}
		<span class="help-block">{{{ $errors->first('name', ':message') }}}</span>
	</div>

	<div class="form-group{{ $errors->first('category_id', ' has-error') }}">
		<label for="category_id">Category</label>
		{!! Form::select('category_id', $categories, Input::get('category_id') ? Input::get('category_id') : Input::old('category_id', @$row->category_id),['class'=>'form-control']); !!}
		<span class="help-block">{{{ $errors->first('category_id', ':message') }}}</span>
	</div>

	<div class="form-group{{ $errors->first('excerpt', ' has-error') }}">
		{!! Form::label('excerpt', 'Excerpt'); !!}
		{!! Form::textarea('excerpt',Input::old('excerpt', $row->excerpt),[
			'placeholder'=>'Enter the News Excerpt.',
			'name'=>'excerpt',
			'id'=>'excerpt',
			'class'=>'form-control ckeditor']); !!}
		<span class="help-block">{{{ $errors->first('excerpt', ':message') }}}</span>
	</div>

	<div class="form-group{{ $errors->first('description', ' has-error') }}">
		{!! Form::label('description', 'Description'); !!}
		{!! Form::textarea('description',Input::old('description', $row->description),[
			'placeholder'=>'Enter the News Description.',
			'name'=>'description',
			'id'=>'description',
			'class'=>'form-control ckeditor']); !!}
		<span class="help-block">{{{ $errors->first('description', ':message') }}}</span>
	</div>

	<div class="form-group{{ $errors->first('publish_date', ' has-error') }}">
		<div class="row">
			<div class="col-xs-6">
				{!! Form::label('publish_date', 'Publish Date'); !!}
				<div class="input-group input-group-sm">
					{!! Form::text('slug', Input::old('publish_date', $row->publish_date),[
						'placeholder'=>'Enter the News Publish Date.',
						'name'=>'publish_date',
						'id'=>'datepicker',
						'data-date-format'=>'yyyy-mm-dd',
						'placeholder'=>'yyyy-mm-dd',
						'class'=>'form-control date-picker']); !!}
					<span class="input-group-addon">
						<i class="ace-icon fa fa-calendar"></i>
					</span>
				</div>
			</div>
		</div>
		<span class="help-block">{{{ $errors->first('publish_date', ':message') }}}</span>
	</div>

	<div class="form-group{{ $errors->first('image', ' has-error') }}">
		{!! Form::label('image', 'News Image:'); !!}
		@if ($row->image)
			{!! Form::label('image', ($row->image) ? 'Replace Image ?' : 'News Image:', ['class' => 'control-label center-block ace-file-input']) !!}
			<img src="{{ asset('uploads/'.$row->image) }}" alt="{{ $row->image }}" class="image-alt" style="max-width:300px"/>
			<div class="clearfix space-6"></div>
		@endif
		<div class="row">
			<div class="col-xs-6">
				<label class="ace-file-input">
					{!! Form::file('image',['class'=>'form-controls','id'=>'id-input-file-2']) !!}
					<span class="ace-file-container" data-title="Choose">
						<span class="ace-file-name" data-title="No File ...">
							<i class=" ace-icon fa fa-upload"></i>
						</span>
					</span>
				</label>
			</div>
		</div>
		<span class="help-block red">{{{ $errors->first('image', ':message') }}}</span>
    </div>
    
	{{-- <div class="form-group{{ $errors->first('image_mobile', ' has-error') }}">
		@if ($row->image_mobile)
			<img src="{{ asset('uploads/'.$row->image_mobile) }}" alt="{{ $row->image_mobile }}" class="image-alt img-thumbnail" style="width:300px"/>
		@endif
		<div class="row">
			<div class="col-xs-6">
				{!! Form::label('image_mobile', ($row->image_mobile) ? 'Replace Image Mobile:' : 'Image Mobile:', ['class' => '']) !!}
				<label class="ace-file-input">
					{!! Form::file('image_mobile',['class'=>'form-controls','id'=>'id-input-file-3']) !!}
					<span class="ace-file-container" data-title="Choose">
						<span class="ace-file-name" data-title="No Image Mobile File ...">
							<i class=" ace-icon fa fa-upload"></i>
						</span>
					</span>
				</label>
			</div>
		</div>
		<span class="help-block">{{{ $errors->first('image_mobile', ':message') }}}</span>
    </div> --}}
    
	<div class="form-group">
		<label class="control-label" for="form-field-tags">Tag</label>
		<div class="inline">
			<?php
			$tagged = [];
			foreach ($tags as $tag) {
				$tagged[] = $tag->name;
			}
			?>
			<input type="text" name="tags" data-rel="{{ route('admin.news.tags') }}" id="form-field-tags" value="{{ is_array($tagged) ? implode(', ', $tagged) : '' }}" placeholder="Enter tags ..." />
		</div>
	</div>

	<div class="form-group{{ $errors->first('type', ' has-error') }}">
		<label for="type">News Type</label>
		{!! Form::select('type', ['blog'=>'Standard News','blog_gallery'=>'News with Gallery','blog_video'=>'News with Video'], Input::get('type') ? Input::get('type') : Input::old('type', @$row->type),['class'=>'form-control']); !!}
		<span class="help-block">{{{ $errors->first('type', ':message') }}}</span>
	</div>

	<div class="form-group{{ $errors->first('index', ' has-error') }}">
		{!! Form::label('index', 'Index'); !!}
		{!! Form::text('index',($row->index ? Input::old('index', $row->index) : $model->max('index') + 1),[
			'placeholder'=>'Enter the News Index.',
			'name'=>'index',
			'id'=>'index',
			'class'=>'form-control']); !!}
		<span class="help-block">{{{ $errors->first('index', ':message') }}}</span>
	</div>

	<div class="form-group{{ $errors->first('status', ' has-error') }}">
		<label for="status">Status</label>
		<select id="status" name="status" class="form-control input-sm">
			<option value="">&nbsp;</option>
			@foreach (config('setting.status') as $config => $val)
				<option value="{{ $config ? $config : Input::old('status', $row->status) }}" {{ $config == $row->status ? 'selected' : '' }}>{{$val}}</option>
			@endforeach
		</select>
	</div>

	<button type="submit" class="btn btn-default">Submit</button>
{!! Form::close() !!}

@stop
