<?php namespace App\Modules\Participant\Controller;

// Load Laravel classes
use Route, Request, Session, Redirect, Input, Validator, View, Excel, File;
// Load main base controller
use App\Modules\BaseAdmin;
// Load main models
use App\Modules\Participant\Model\Image;
// Load Datatable
use Datatables;
// User Activity Logs
use Activity;

class Images extends BaseAdmin {

	/**
	 * Set participant and images data.
	 *
	 */
	public $images;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

		// Parent constructor
		parent::__construct();

		// Load Http/Middleware/Admin controller
		$this->middleware('auth.admin');

		// Load images and get repository data from Auth
		$this->images = new Image;

	}

	/**
	 * Display a listing of images.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{

        // Set return data
        $images = Input::get('path') === 'trashed' ? $this->images->onlyTrashed()->get() : $this->images->orderBy('created_at','desc')->get();

        // Get deleted count
        $deleted = $this->images->onlyTrashed()->get()->count();

        // Set data to return
        $data = ['deleted'=>$deleted,'junked'=>Input::get('path')];

		// Load needed scripts
        $scripts = [
            'dataTables' => asset('themes/ace-admin/js/jquery.dataTables.min.js'),
            'dataTableBootstrap'=> asset('themes/ace-admin/js/jquery.dataTables.bootstrap.min.js'),
         // ColorBox
            'jquery.colorbox' => asset('themes/ace-admin/js/jquery.colorbox.min.js'),
         'library' => asset("themes/ace-admin/js/library.js")
            ];

        $styles 	= [
             'jquery.colorbox' => asset('themes/ace-admin/css/colorbox.min.css'),
             ];
    
        // Set inline script or style
        $inlines = [
            // Script execution on a specific controller page
            'script' => "
            // --- datatable handler [".route('admin.images.index')."]--- //
                var datatable  = $('#datatable-table');
                var controller = datatable.attr('rel');

                $('#datatable-table').DataTable({
                    processing: true,
                    serverSide: true,
                    bAutoWidth: false,
                    ajax: '".route('admin.images.datatable')."' + ($.getURLParameter('path') ? '?path=' + $.getURLParameter('path') : ''),
                    columns: [
                        {data: 'id', name:'id', orderable: false, searchable: false},
                        {data: 'file_name', name: 'file_name'},
                        {data: 'title', name: 'title'},
                        {data: 'participant', name: 'participant'},                        
                        {data: 'status', name: 'status'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ],
                    language: {
                        processing: ''
                    },
                    fnDrawCallback : function (oSettings) {
                        $('#datatable-table > thead > tr > th:first-child')
                        .removeClass('sorting_asc')
                        .find('input[type=checkbox]')
                        .prop('checked',false);
                        $('#datatable-table > tbody > tr > td:first-child').addClass('center');
                        $('[data-rel=tooltip]').tooltip();
                    }
                });
            ",
        ];

        return $this->view('Participant::image_datatable_index')
        ->data($data)
        ->scripts($scripts)
        ->inlines($inlines)
        ->title('Images List');
    }
    
    /**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function datatable(Request $request)
	{
		$rows = Input::get('path') === 'trashed' ? $this->images->with('participant')->onlyTrashed()->get() : $this->images->with('participant')->orderBy('created_at', 'asc')->get();

		return Datatables::of($rows)
			// Set action buttons
			->editColumn('action', function ($row) {
				if (Input::get('path') !== 'trashed') {
					return '
						<a data-rel="tooltip" data-original-title="View" title="" href="'.route('admin.images.show', $row->id).'" class="btn btn-xs btn-success tooltip-default">
							<i class="ace-icon fa fa-check bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Edit"  href="'.route('admin.images.edit', $row->id).'" class="btn btn-xs btn-info tooltip-default">
							<i class="ace-icon fa fa-pencil bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Trashed"  href="'.route('admin.images.trash', $row->id).'" class="btn btn-xs btn-danger tooltip-default">
							<i class="ace-icon fa fa-trash-o bigger-120"></i>
						</a>';
				} else {
					return '
						<a data-rel="tooltip" data-original-title="Restore!" href="'.route('admin.images.restored', $row->id).'" class="btn btn-xs btn-primary tooltip-default">
							<i class="ace-icon fa fa-save bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Permanent Delete!" href="'.route('admin.images.delete', $row->id).'" class="btn btn-xs btn-danger">
							<i class="ace-icon fa fa-trash bigger-120"></i>
						</a>';
				}
			})
			// Edit column id
			->editColumn('id', function ($row) {
				return 	'
				<label class="pos-rel">
					<input type="checkbox" class="ace" name="check[]" id="check_'.$row->id.'" value="'.$row->id.'" />
					<span class="lbl"></span>
				</label>';
            })
            // Edit column file_name
            ->editColumn('file_name', function ($row) {
                $html = '<a data-rel="colorbox" href="'.asset("uploads/images/".$row->file_name).'"><img src="'.asset('storage/uploads/images/'.$row->file_name).'" class="img-responsive" alt="'.$row->file_name.'"/></a>';
				return $html;
			})
			// Set description limit
			->editColumn('participant', function ($row) {
                $participant = ($row->participant['name']) ? $row->participant['name'] : $row->participant['first_name'] .' '.$row->participant['last_name'];
                return $participant .' ('.$row->participant['email'].')';
            })
			// Set status icon and text
			->editColumn('status', function ($row) {
				return '
				<span class="label label-'.($row->status == 1 ? 'success' : 'warning').' arrowed-in arrowed-in-right">
					<span class="fa fa-'.($row->status == 1 ? 'flag' : 'exclamation-circle').' fa-sm"></span>
					'.config('setting.status')[$row->status].'
				</span>';
            })
			->rawColumns(['id','action','file_name','participant','status'])
			->make(true);
    }
    
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// Get data from database
        $image = $this->images->findOrFail($id);

		// Set data to return
	   	$data = ['row'=>$image];

	   	// Return data and view
	   	return $this->view('Participant::image_show')->data($data)->title('View Image');

	}

	/**
	 * Show the form for creating new image.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new image.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating image.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating image.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified image.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function trash($id)
	{
		if ($image = $this->images->find($id))
		{

			// Add deleted_at and not completely delete
			$image->delete();

			// Redirect with messages
			return Redirect::to(route('admin.images.index'))->with('success', 'Image Trashed!');
		}

		return Redirect::to(route('admin.images.index'))->with('error', 'Image Not Found!');
	}

	/**
	 * Restored the specified image.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function restored($id)
	{
		if ($image = $this->images->onlyTrashed()->find($id))
		{

			// Restored back from deleted_at database
			$image->restore();

			// Redirect with messages
			return Redirect::to(route('admin.images.index'))->with('success', 'Image Restored!');
		}

		return Redirect::to(route('admin.images.index'))->with('error', 'Image Not Found!');
	}
	/**
	 * Remove the specified image.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{

		// Get Image from id fetch
		if ($image = $this->images->onlyTrashed()->find($id))
		{

			// Delete from pivot table many to many
			$this->images->onlyTrashed()->find($id)->participant()->detach();

			// Delete if there is an image attached
			if(File::exists('storage/uploads/'.$image->file_name)) {
				// Delete the single file
				File::delete('storage/uploads/'.$image->file_name);

			}

			// Permanently delete
			$image->forceDelete();

			return Redirect::to(route('admin.images.index','path=trashed'))->with('success', 'Image Permanently Deleted!');
		}

		return Redirect::to(route('admin.images.index','path=trashed'))->with('error', 'Image Not Found!');
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{

		if ($id)
		{
			if ( ! $row = $this->images->find($id))
			{
				return Redirect::to(route('admin.images.index'));
			}
		}
		else
		{
			$row = $this->images;
		}

		return $this->view('Participant::image_form')->data(compact('mode', 'row'))->title('Image '.$mode);
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		$input = array_filter(Input::all());

		$rules = [
			'first_name' => 'required',
			'last_name'  => 'required',
			'role_id'  	 => 'required',
			'email'      => 'required|unique:images'
		];

		if ($id)
		{
			//$participant = Admin::getParticipantRepository()->createModel()->find($id);

			//$rules['email'] .= ",email,{$participant->email},email";

			$messages = $this->validateParticipant($input, $rules);

			if ($messages->isEmpty())
			{

				//if ( ! $participant->roles()->first() ) {

					// Syncing relationship Many To Many // Create New
					//$participant->roles()->sync(['role_id'=>$input['role_id']]);

				//} else {

					// Syncing relationship Many To Many // Update Existing
					//$participant->roles()->sync(['role_id'=>$input['role_id']]);

					// Update participant model data
					//Admin::getParticipantRepository()->update($participant, $input);

				//}

			}
		}
		else
		{

			$messages = $this->validateImage($input, $rules);

			if ($messages->isEmpty())
			{
				// Create participant into the database
				//$participant = Admin::getParticipantRepository()->create($input);

				// Syncing relationship Many To Many // Create New
				//$participant->roles()->sync(['role_id'=>$input['role_id']]);

				//$code = Activation::create($participant);

				//Activation::complete($participant, $code);
			}
		}

		if ($messages->isEmpty())
		{
			return Redirect::to(route('admin.images.show'))->with('success', 'Image Updated!');;
		}

		return Redirect::back()->withInput()->withErrors($messages);
	}

	/**
	 * Change the data status.
	 *
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function change() {

        // Log it first
        Activity::log(__FUNCTION__);

		if (Input::get('check') !='') {

		    $rows	= Input::get('check');

		    foreach ($rows as $row) {
				// Set id for load and change status
				$this->images->withTrashed()->find($row)->update(['status' => Input::get('select_action')]);
		    }

		    // Set message
		    return Redirect::to(route('admin.images.index'))->with('success', 'Image  Status Changed!');

		} else {

		    // Set message
		    return Redirect::to(route('admin.images.index'))->with('error','Data not Available!');
		}
	}

    /**
	 * Process a file to download.
	 *
	 * @return $file export
	 */
	public function export() {

		// Log it first
		Activity::log(__FUNCTION__);

		// Get type file to export
		$type = Input::get('rel');
		// Get data to export
		$data = [];
		foreach ($this->images->with('participant')->get() as $row => $images) {
            $data[$row]['id'] = $images->id;
			$data[$row]['file_name'] = asset('images/images/'.$images->file_name);
            $data[$row]['title'] = $images->title;
            $data[$row]['participant'] = ($images->participant) ? $images->participant['email'] : '';
			$data[$row]['status'] = $images->status;
			$data[$row]['created_at'] = $images->created_at;
		}
		$data = collect($data);
		// Export file to type
		//Excel::create('images', function($excel) use($images) {
		Excel::create('images', function($excel) use($data) {		
			// Set the spreadsheet title, creator, and description
	        $excel->setTitle('Export List');
	        $excel->setCreator('Laravel')->setCompany('laravel.com');
	        $excel->setDescription('export file');

		    $excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
		    });
		})->export($type);

    }
    
	/**
	 * Validates a image.
	 *
	 * @param  array  $data
	 * @param  mixed  $id
	 * @return \Illuminate\Support\MessageBag
	 */
	protected function validateImage($data, $rules)
	{
		$validator = Validator::make($data, $rules);

		$validator->passes();

		return $validator->errors();
	}

}
