<?php namespace App\Modules\Participant\Controller;

// Load Laravel classes
use Route, Request, Session, Redirect, Input, Validator, View, File;
// Load main base controller
use App\Modules\BaseAdmin;
// Load main models
use App\Modules\Participant\Model\Vote;
// Load Datatable
use Datatables;

class Votes extends BaseAdmin {

	/**
	 * Set participant and votes data.
	 *
	 */
	public $participants, $votes;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

		// Parent constructor
		parent::__construct();

		// Load Http/Middleware/Admin controller
		$this->middleware('auth.admin');

		// Load participants and get repository data from Auth
		$this->votes = new Vote;

	}

	/**
	 * Display a listing of votes.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{

	   	// Set return data
	   	$votes = Input::get('path') === 'trashed' ? $this->votes->with('participant')->onlyTrashed()->get() : $this->votes->with('participant')->orderBy('created_at','desc')->get();

	   	// Get deleted count
		$deleted = $this->votes->onlyTrashed()->get()->count();

	   	// Set data to return
	   	$data = ['rows'=>$votes,'deleted'=>$deleted,'junked'=>Input::get('path')];

  		// Set javascripts and stylesheets
	   	$scripts 	= [
	   		// Data Tables
			'dataTables'=> 'themes/ace-admin/js/jquery.dataTables.min.js',
			'dataTableBootstrap'=> 'themes/ace-admin/js/jquery.dataTables.bootstrap.min.js',
			'dataTableTools'=> 'themes/ace-admin/js/dataTables.tableTools.min.js',
			'dataTablesColVis'=> 'themes/ace-admin/js/dataTables.colVis.min.js',
			// ColorBox
	   		'jquery.colorbox' => asset('themes/ace-admin/js/jquery.colorbox.min.js'),
			'library' => asset("themes/ace-admin/js/library.js")

	   	];
	   	$styles 	= [
	   		'jquery.colorbox' => asset('themes/ace-admin/css/colorbox.min.css'),
	   	];

	   	return $this->view('Participant::vote_index')->data($data)->scripts($scripts)->styles($styles)->title('Votes List');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// Get data from database
        $vote = $this->votes->findOrFail($id);

		// Set data to return
	   	$data = ['row'=>$vote];

	   	// Return data and view
	   	return $this->view('Participant::vote_show')->data($data)->title('View Vote');

	}

	/**
	 * Show the form for creating new vote.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new vote.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating vote.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating vote.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified vote.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function trash($id)
	{
		if ($vote = $this->votes->find($id))
		{

			// Add deleted_at and not completely delete
			$vote->delete();

			// Redirect with messages
			return Redirect::to(route('admin.votes.index'))->with('success', 'Vote Trashed!');
		}

		return Redirect::to(route('admin.votes.index'))->with('error', 'Vote Not Found!');
	}

	/**
	 * Restored the specified vote.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function restored($id)
	{
		if ($vote = $this->votes->onlyTrashed()->find($id))
		{

			// Restored back from deleted_at database
			$vote->restore();

			// Redirect with messages
			return Redirect::to(route('admin.votes.index'))->with('success', 'Vote Restored!');
		}

		return Redirect::to(route('admin.votes.index'))->with('error', 'Vote Not Found!');
	}
	/**
	 * Remove the specified vote.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{

		// Get Vote from id fetch
		if ($vote = $this->votes->onlyTrashed()->find($id))
		{

			// Delete from pivot table many to many
			$this->votes->onlyTrashed()->find($id)->participant()->detach();

			// Delete if there is an vote attached
			if(File::exists('uploads/'.$vote->file_name)) {
				// Delete the single file
				File::delete('uploads/'.$vote->file_name);

			}

			// Permanently delete
			$vote->forceDelete();

			return Redirect::to(route('admin.votes.index','path=trashed'))->with('success', 'Vote Permanently Deleted!');
		}

		return Redirect::to(route('admin.votes.index','path=trashed'))->with('error', 'Vote Not Found!');
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{

		if ($id)
		{
			if ( ! $row = $this->votes->find($id))
			{
				return Redirect::to(route('admin.votes.index'));
			}
		}
		else
		{
			$row = $this->votes;
		}

		return $this->view('Participant::vote_form')->data(compact('mode', 'row'))->title('Vote '.$mode);
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		$input = array_filter(Input::all());

		$rules = [
			'first_name' => 'required',
			'last_name'  => 'required',
			'role_id'  	 => 'required',
			'email'      => 'required|unique:participants'
		];

		if ($id)
		{
			//$participant = Admin::getParticipantRepository()->createModel()->find($id);

			//$rules['email'] .= ",email,{$participant->email},email";

			$messages = $this->validateParticipant($input, $rules);

			if ($messages->isEmpty())
			{

				//if ( ! $participant->roles()->first() ) {

					// Syncing relationship Many To Many // Create New
					//$participant->roles()->sync(['role_id'=>$input['role_id']]);

				//} else {

					// Syncing relationship Many To Many // Update Existing
					//$participant->roles()->sync(['role_id'=>$input['role_id']]);

					// Update participant model data
					//Admin::getParticipantRepository()->update($participant, $input);

				//}

			}
		}
		else
		{

			$messages = $this->validateVote($input, $rules);

			if ($messages->isEmpty())
			{
				// Create participant into the database
				//$participant = Admin::getParticipantRepository()->create($input);

				// Syncing relationship Many To Many // Create New
				//$participant->roles()->sync(['role_id'=>$input['role_id']]);

				//$code = Activation::create($participant);

				//Activation::complete($participant, $code);
			}
		}

		if ($messages->isEmpty())
		{
			return Redirect::to(route('admin.votes.show'))->with('success', 'Vote Updated!');;
		}

		return Redirect::back()->withInput()->withErrors($messages);
	}

	/**
	 * Change the data status.
	 *
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function change() {

		if (Input::get('check') !='') {

		    $rows	= Input::get('check');

		    foreach ($rows as $row) {
				// Set id for load and change status
				$this->votes->withTrashed()->find($row)->update(['status' => Input::get('select_action')]);
		    }

		    // Set message
		    return Redirect::to(route('admin.votes.index'))->with('success', 'Vote  Status Changed!');

		} else {

		    // Set message
		    return Redirect::to(route('admin.votes.index'))->with('error','Data not Available!');
		}
	}

	/**
	 * Validates a participant.
	 *
	 * @param  array  $data
	 * @param  mixed  $id
	 * @return \Illuminate\Support\MessageBag
	 */
	protected function validateVote($data, $rules)
	{
		$validator = Validator::make($data, $rules);

		$validator->passes();

		return $validator->errors();
	}

}
