<?php namespace App\Modules\Campaign\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model {
	
	// Soft deleting a model, it is not actually removed from your database.
    use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'galleries';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
        'slug',
        'image',
        'type_id',
		'description',
		'gallery_date',
		'user_id',
		'is_system',
		'user_id',
		'status'
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

    // Instead, a deleted_at timestamp is set on the record.
    protected $dates = ['deleted_at'];

    /**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
	    'name'  => 'string',
	    'slug' => 'string',
	    'is_system' => 'boolean',
	    //'status' => 'boolean'
	];
	
	// Get campaign 
    public function campaign() {

        // Set relationship belongsTo
        return $this->belongsTo('App\Modules\Campaign\Model\Campaign','id','attribute');

    }
	
	// Scope query for active status field
    public function scopeActive($query) {

		// Set query status  
		return $query->where('status', 1);

    }

	// Scope query for slug field
    public function scopeSlug($query, $string) {

        return $query->where('slug', $string)->firstOrFail();

    }
    
}
