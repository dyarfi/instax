<?php namespace App\Modules\Campaign\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model {

	// Soft deleting a model, it is not actually removed from your database.
    use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'videos';

	/**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'slug',
        'name',
        'description',
        'embed',
        'url',
        'image',
        'attributes',
        'options',
        'end_date',
        'created_at',
        'status'
    ];

    // Instead, a deleted_at timestamp is set on the record.
    protected $dates = ['deleted_at'];

    // Scope query for active status field
    public function scopeActive($query) {

      return $query->where('status', 1)->orderBy('created_at','desc');

    }

    // Scope query for slug field
    public function scopeSlug($query, $string) {

        return $query->where('slug', $string)->where('status',1)->firstOrFail();

    }

    /**
	 * A video can have many ambassadors.
	 *
	 */
	public function ambassadors()
	{
		return $this->belongsTo('App\Modules\Campaign\Model\Ambassadors','id','attribute');
    }

    /**
	 * A video can have many votes.
	 *
	 */
	public function votes()
	{
		return $this->hasMany('App\Modules\Participant\Model\Vote','attribute');
    }

    /**
	 * A video can have many tasks.
	 *
	 */
	public function users()
	{
		return $this->hasMany('App\Modules\Participant\Model\Participant','participant_id');
	}
}
