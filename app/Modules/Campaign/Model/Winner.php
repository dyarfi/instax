<?php namespace App\Modules\Campaign\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Winner extends Model {

	// Soft deleting a model, it is not actually removed from your database.
    use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'winners';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
            'priority',
            'campaign_id',
			'filename',
            'caption',
            'status',
            'is_winner',
            'created_at',
            'updated_at'
			];

    // Instead, a deleted_at timestamp is set on the record.
    protected $dates = ['deleted_at'];

	/**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'verify'      => 'boolean',
        // 'completed'   => 'boolean',        
        // 'logged_in'   => 'boolean',
        // 'status'      => 'boolean'
    ];

    /**
     * Get the video associated with the ambassador.
     */
    public function campaign() {

        return $this->hasOne('App\Modules\Campaign\Model\Campaign','id','campaign_id');
        
    }

	// Scope query for active status field
    public function scopeActive($query) {

      return $query->where('status', 1);

    }

}
