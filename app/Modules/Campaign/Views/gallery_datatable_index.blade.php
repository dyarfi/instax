@extends('Admin::layouts.template')

{{-- Page content --}}
@section('body')
<div class="page-header">
    <div class="row">
        <div class="col-md-2">
            <h1><a href="{{ route('admin.galleries.index') }}">Galleries</a></h1>
        </div>
        <div class="col-md-1 dropdown">
            <button class="btn btn-warning btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <span class="glyphicon glyphicon-pencil"></span> Create
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                {{-- <li><a href="{{ route('admin.galleries.create','rel=video') }}" class=""><span class="fa fa-camera"></span> Video</a></li> --}}
                <li><a href="{{ route('admin.galleries.create','rel=key-visual') }}" class=""><span class="fa fa-image"></span> Key Visual</a></li>
                {{-- <li><a href="{{ route('admin.galleries.create','rel=campaign') }}" class=""><span class="fa fa-file-text-o"></span> Campaign</a></li> --}}
            </ul>             
            {{$junked ? ' &raquo; Trashed' :''}}
        </div>
        <div class="col-md-1 dropdown">
            <button class="btn btn-default btn-xs dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <span class="fa fa-external-link-square"></span> Export
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                <li><a href="{{route('admin.galleries.export','rel=xls')}}" class=""><span class="fa fa-file-excel-o"></span> XLS</a></li>
                <li><a href="{{route('admin.galleries.export','rel=csv')}}" class=""><span class="fa fa-file-text-o"></span> CSV</a></li>
            </ul>
        </div>
    </div>
</div>
@if($deleted)
<div class="clearfix">
	<div class="pull-right">
		<a href="{{route('admin.galleries.index','path=trashed')}}" title="Restored Deleted" class="btn btn-link btn-xs"><span class="fa fa-trash"></span> {{ $deleted }} Deleted</a>
	</div>
</div>
@endif
{!! Form::open(['route'=>'admin.galleries.change']) !!}
	<table class="table table-bordered table-hover" id="datatable-table" rel="gallery">
		<thead>
			<tr>
				<th class="center col-lg-1"><label class="pos-rel"><input type="checkbox" class="ace" /><span class="lbl"></span></label></th>
                <th class="col-lg-1">Name</th>
                <th class="col-lg-1">Image</th>
				<th class="col-lg-2">Description</th>
				<th class="col-lg-2">Gallery Date</th>
				<th class="col-lg-1">Status</th>
				<th class="col-lg-1">Created At</th>
				<th class="col-lg-2 col-md-3 col-sm-3 col-xs-3">Actions</th>
			</tr>
		</thead>
		<tbody>
			{{-- Datatable content --}}
		</tbody>
			<tr>
				<td id="corner"><span class="glyphicon glyphicon-minus"></span></td>
				<td colspan="8">
				<div id="selection" class="input-group">
					<div class="form-group form-group-sm">
						<label class="col-xs-6 control-label small grey" for="select_action">Change status :</label>
						<div class="col-xs-6" id="select_action">
						<select id="select_action" class="form-control input-sm" name="select_action">
							<option value="">&nbsp;</option>
							@foreach (config('setting.status') as $val => $config)
								<option value="{{$val}}">{{$config}}</option>
							@endforeach
						</select>
						</div>
					  </div>
				 </div>
				</td>
			</tr>
	</table>
{!! Form::close() !!}
@stop
