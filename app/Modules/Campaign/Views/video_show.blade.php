@extends('Admin::layouts.template')

@section('body')
<div class="container-fluid">
    @if($row->name)
    <h4 class="red">Name</h4>
    <div class="row-fluid">
        {{ $row->name }}
    </div>
    @endif
    @if($row->description)
    <h4 class="red">Description</h4>
    <div class="row-fluid">
        {{ $row->description }}
    </div>
    @endif
    @if($row->embed)
    <h4 class="red">Embed</h4>
    <div class="row-fluid">
        {{ $row->embed }}
    </div>
    @endif    
    @if($row->status)
    <h4 class="red">Status</h4>
    <div class="row-fluid">
        {{ config('setting.status')[$row->status] }}
    </div>
    @endif
    @if($row->created_at)
    <h4 class="red">Created At</h4>
    <div class="row-fluid">
        {{ $row->created_at }}
    </div>
    @endif
    <hr/>
    <div class="row">
        <div class="col-md-5 col-xs-6">
            <a href="{{ route('admin.videos.index') }}" class="btn btn-info btn-xs">Back to all videos</a>
            <a href="{{ route('admin.videos.edit', $row->id) }}" class="btn btn-primary btn-xs">Edit Video</a>
            <a href="{{ route('admin.videos.create') }}" class="btn btn-warning btn-xs">Create Video</a>
        </div>
        <div class="col-md-5 col-xs-6 text-right">
            {!! Form::open([
                'method' => 'DELETE',
                'route' => ['admin.videos.trash', $row->id]
            ]) !!}
                {!! Form::submit('Delete this video?', ['class' => 'btn btn-danger btn-xs']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>

@stop
