@extends('Admin::layouts.template')

{{-- Page content --}}
@section('body')

<div class="page-header">
	<h1>{{ $mode == 'create' ? 'Create Video' : 'Update Video' }} <small>{{ $mode === 'update' ? $row->name : null }}</small></h1>
</div>
<!--form method="post" action="" autocomplete="off"-->
{!! Form::open([
	'route' => ($mode == 'create') ? 'admin.videos.create' : ['admin.videos.update', $row->id],
	'files' => true
]) !!}

	<div class="form-group{{ $errors->first('name', ' has-error') }}">
		<label for="name">Name</label>
		<input type="text" class="form-control" name="name" id="name" value="{{ Input::old('name', $row->name) }}" placeholder="Enter the video First Name.">
		<span class="help-block">{{{ $errors->first('name', ':message') }}}</span>
	</div>

	<div class="form-group{{ $errors->first('description', ' has-error') }}">
		{!! Form::label('description', 'Description') !!}
		{!! Form::textarea('description',Input::old('description', $row->description),[
			'placeholder'=>'Enter the Description.',
			'name'=>'description',
			'id'=>'description',
			'class' => 'form-control ckeditor',
			'rows' => '4'
		]); !!}
		<span class="help-block">{{{ $errors->first('description', ':message') }}}</span>
	</div>

	{{-- <div class="form-group{{ $errors->first('embed', ' has-error') }}">
		{!! Form::label('embed', 'Embed') !!}
		{!! Form::textarea('embed',Input::old('embed', $row->embed),[
			'placeholder'=>'Enter the Embed.',
			'name'=>'embed',
			'id'=>'embed',
			'class' => 'form-control ckeditor',
			'rows' => '4'
		]); !!}
		<span class="help-block">{{{ $errors->first('embed', ':message') }}}</span>
	</div> --}}

	<div class="form-group{{ $errors->first('url', ' has-error') }}">
		{!! Form::label('url', 'Embed'); !!}
		{!! Form::text('title',Input::old('url', $row->url),[
			'placeholder'=>'Enter the Embed.',
			'name'=>'url',
			'id'=>'url',
			'class' => 'form-control']); !!}
		<span class="help-block">{{{ $errors->first('url', ':message') }}}</span>
	</div>

	<div class="form-group{{ $errors->first('image', ' has-error') }}">
		{!! Form::label('image', 'Video Image:'); !!}
		@if ($row->image)
			{!! Form::label('image', ($row->image) ? 'Replace Image ?' : 'Video Image:', ['class' => 'control-label center-block ace-file-input']) !!}
			<img src="{{ asset('uploads/'.$row->image) }}" alt="{{ $row->image }}" class="image-alt" style="max-width:300px"/>
			<div class="clearfix space-6"></div>
		@endif
		<div class="row">
			<div class="col-xs-6">
				<label class="ace-file-input">
					{!! Form::file('image',['class'=>'form-controls','id'=>'id-input-file-2']) !!}
					<span class="ace-file-container" data-title="Choose">
						<span class="ace-file-name" data-title="No File ...">
							<i class=" ace-icon fa fa-upload"></i>
						</span>
					</span>
				</label>
			</div>
		</div>
		<span class="help-block red">{{{ $errors->first('image', ':message') }}}</span>
	</div>

    <div class="form-group{{ $errors->first('created_at', ' has-error') }}">
        <div class="row">
            <div class="col-xs-6">
                {!! Form::label('created_at', 'Created Date'); !!}
                <div class="input-group input-group-sm">
                    {!! Form::text('slug', Input::old('created_at', $row->created_at),[
                        'placeholder'=>'Enter the Created Date.',
                        'name'=>'created_at',
                        'id'=>'datepicker',
                        'data-date-format'=>'yyyy-mm-dd',
                        'placeholder'=>'yyyy-mm-dd',
                        'class'=>'form-control date-picker']); !!}
                    <span class="input-group-addon">
                        <i class="ace-icon fa fa-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
        <span class="help-block">{{{ $errors->first('created_at', ':message') }}}</span>
    </div>

    <div class="form-group{{ $errors->first('status', ' has-error') }}">
    	<label for="status">Status</label>
    	<select id="status" name="status" class="form-control input-sm">
    		<option value="">&nbsp;</option>
    		@foreach (config('setting.status') as $config => $val)
    			<option value="{{ $config ? $config : Input::old('status', $row->status) }}" {{ $config == $row->status ? 'selected' : '' }}>{{$val}}</option>
    		@endforeach
    	</select>
    	<span class="help-block">{{{ $errors->first('status', ':message') }}}</span>
    </div>

	<button type="submit" class="btn btn-default">Submit</button>
{!! Form::close() !!}

@stop
