@extends('Admin::layouts.template')

{{-- Page content --}}
@section('body')

<div class="page-header">
	<h1>{{ $mode == 'create' ? 'Create Winner' : 'Update Winner' }} <small>{{ $mode === 'update' ? $row->name : null }}</small></h1>
</div>

{!! Form::open([
    'route' => ($mode == 'create') ? 'admin.winners.create' : ['admin.winners.update', $row->id],
    'files' => true
]) !!}

    <div class="form-group{{ $errors->first('campaign_id', ' has-error') }}">
        <label for="campaign_id">Campaign</label>
        {!! Form::select('campaign_id', $campaigns, Input::get('campaign_id') ? Input::get('campaign_id') : Input::old('campaign_id', @$row->campaign_id),['class'=>'form-control']); !!}
        <span class="help-block">{{{ $errors->first('campaign_id', ':message') }}}</span>
    </div>

    <div class="form-group{{ $errors->first('filename', ' has-error') }}">
		@if ($row->filename)
			<img src="{{ asset('uploads/activity/'.$row->filename) }}" alt="{{ $row->filename }}" class="image-alt img-thumbnail" style="width:300px"/>
		@endif
		<div class="row">
			<div class="col-xs-6">
				{!! Form::label('filename', ($row->filename) ? 'Replace Image:' : 'Image:', ['class' => '']) !!}
				<label class="ace-file-input">
					{!! Form::file('filename',['class'=>'form-controls','id'=>'id-input-file-2']) !!}
					<span class="ace-file-container" data-title="Choose">
						<span class="ace-file-name" data-title="No Image File ...">
							<i class=" ace-icon fa fa-upload"></i>
						</span>
					</span>
				</label>
			</div>
		</div>
		<span class="help-block">{{{ $errors->first('filename', ':message') }}}</span>
    </div>

	<div class="form-group{{ $errors->first('caption', ' has-error') }}">
		{!! Form::label('caption', 'Caption') !!}
		{!! Form::text('caption',Input::old('caption', $row->caption),[
			'placeholder'=>'Enter the Caption.',
			'name'=>'caption',
			'id'=>'caption',
			// 'readonly'=>true,
			'class'=>'form-control']) !!}
		<span class="help-block">{{{ $errors->first('caption', ':message') }}}</span>
	</div>

	<div class="form-group{{ $errors->first('is_winner', 'has-error') }}">
		{!! Form::label('is_winner', 'Is Winner') !!}
		{!! Form::select('is_winner', [1=>'Yes',0=>'No'], Input::get('is_winner') ? Input::get('is_winner') : Input::old('is_winner', $row->is_winner),['class'=>'form-control']) !!}
		<span class="help-block">{{{ $errors->first('is_winner', ':message') }}}</span>
	</div>

	<div class="form-group{{ $errors->first('status', ' has-error') }}">
		<label for="status">Status</label>
		<select id="status" name="status" class="form-control input-sm">
			<option value="">&nbsp;</option>
			@foreach (config('setting.status') as $config => $val)
				<option value="{{ $config ? $config : Input::old('status', $row->status) }}" {{ $config == $row->status ? 'selected' : '' }}>{{$val}}</option>
			@endforeach
		</select>
		<span class="help-block">{{{ $errors->first('status', ':message') }}}</span>
	</div>
	
	{!! Form::submit(ucfirst($mode).' New Winner', ['class' => 'btn btn-primary btn-xs']) !!}

{!! Form::close() !!}

@stop
