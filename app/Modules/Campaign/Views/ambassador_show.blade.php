@extends('Admin::layouts.template')

@section('body')
<div class="clearfix">
    <div class="col-md-12">
        <h4 class="red">Name</h4>
        {!! $row->name !!}
    </div>
    @if ($row->image != '')
    <div class="col-md-12 clearfix">
    <a href="{{ asset('uploads/'.$row->image) }}" target="_blank" title="{{ $row->image }}"/>
        <img src="{{ asset('uploads/'.$row->image) }}" class="img-fluid"/>
    </a>
    </div>
    @endif
    <div class="col-md-12">
        <h4 class="red">Description</h4>
        <div class="clearfix">{!! $row->description !!}</div>
    </div>
    @if($row->video)
    <?php
    // Set videos
    $video = $row->video ? $row->video->url : '';
    ?>
    <div class="col-md-12">
        <h4 class="red">Video</h4>
        @if($video)
        <div style="position:relative;height:0;padding-bottom:40.63%;width:50%;">
            <iframe src="{{$video}}?rel=0&controls=0&showinfo=0"style="position:absolute;left:0" width="686" height="360" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>    
        @else
        <div>
            -
        </div>
        @endif
    </div>
    @endif
    @if($row->status)
    <div class="col-md-12">
        <h4 class="red">Status</h4>
        <div class="row-fluid">
            {{ config('setting.status')[$row->status] }}
        </div>
    </div>
    @endif
    @if($row->created_at)
    <div class="col-md-12">
        <h4 class="red">Created At</h4>
        <div class="row-fluid">
            {{ $row->created_at }}
        </div>
    </div>
    @endif
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <a href="{{ route('admin.ambassadors.index') }}" class="btn btn-info btn-xs">Back to all ambassadors</a>
        <a href="{{ route('admin.ambassadors.edit', $row->id) }}" class="btn btn-primary btn-xs">Edit Ambassador</a>
        <a href="{{ route('admin.ambassadors.create') }}" class="btn btn-warning btn-xs">Create Ambassador</a>
    </div>
    <div class="col-md-6 text-right">
        {!! Form::open([
            'method' => 'DELETE',
            'route' => ['admin.ambassadors.trash', $row->id]
        ]) !!}
            {!! Form::submit('Delete this ambassador?', ['class' => 'btn btn-danger btn-xs']) !!}
        {!! Form::close() !!}
    </div>
</div>

@stop
