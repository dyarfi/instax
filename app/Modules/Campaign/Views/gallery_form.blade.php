@extends('Admin::layouts.template')

{{-- Page content --}}
@section('body')

<div class="page-header">
	<h1>{{ $mode == 'create' ? 'Create Gallery' : 'Update Gallery' }} <small>{{ $mode === 'update' ? $row->name : null }}</small></h1>
</div>

{!! Form::open([
    'route' => ($mode == 'create') ? 'admin.galleries.create' : ['admin.galleries.update', $row->id],
    'files' => true
]) !!}

    {!! Form::hidden('type_id','key-visual',[
            'name'=>'type_id',
            'id'=>'type_id',
            'class' => 'd-none']) !!}
	    
    <span class="help-block">{{{ $errors->first('type_id', ':message') }}}</span>
        
	<div class="form-group{{ $errors->first('name', 'has-error') }}">
		{!! Form::label('name', 'Name') !!}
		{!! Form::text('name',Input::old('name', $row->name),[
			'placeholder'=>'Enter your Name.',
			'name'=>'name',
			'id'=>'name',
			'class' => 'form-control']) !!}
		<span class="help-block">{{{ $errors->first('name', ':message') }}}</span>
	</div>

	<div class="form-group{{ $errors->first('description', ' has-error') }}">
		{!! Form::label('description', 'Description') !!}
		{!! Form::textarea('description',Input::old('description', $row->description),[
			'placeholder'=>'Enter the Description.',
			'name'=>'description',
			'id'=>'description',
			'class' => 'form-control ckeditor',
			'rows' => '4'
		]); !!}
		<span class="help-block">{{{ $errors->first('description', ':message') }}}</span>
    </div>
        	
	<div class="form-group{{ $errors->first('image', ' has-error') }}">
		@if ($row->image)
			<img src="{{ asset('uploads/'.$row->image) }}" alt="{{ $row->image }}" class="image-alt img-thumbnail" style="width:300px"/>
		@endif
		<div class="row">
			<div class="col-xs-6">
				{!! Form::label('image', ($row->image) ? 'Replace Image:' : 'Image:', ['class' => '']) !!}
				<label class="ace-file-input">
					{!! Form::file('image',['class'=>'form-controls','id'=>'id-input-file-2','value'=>($row->image) ? $row->image:'']) !!}
					<span class="ace-file-container" data-title="Choose">
						<span class="ace-file-name" data-title="No Image File ...">
							<i class=" ace-icon fa fa-upload"></i>
						</span>
					</span>
				</label>
			</div>
		</div>
		<span class="help-block">{{{ $errors->first('image', ':message') }}}</span>
	</div>

    <div class="form-group{{ $errors->first('gallery_date', ' has-error') }}">
        <div class="row">
            <div class="col-xs-6">
                {!! Form::label('gallery_date', 'Gallery Date'); !!}
                <div class="input-group input-group-sm">
                    {!! Form::text('slug', Input::old('gallery_date', $row->gallery_date),[
                        'placeholder'=>'Enter the News Gallery Date.',
                        'name'=>'gallery_date',
                        'id'=>'datepicker',
                        'data-date-format'=>'yyyy-mm-dd',
                        'placeholder'=>'yyyy-mm-dd',
                        'class'=>'form-control date-picker']); !!}
                    <span class="input-group-addon">
                        <i class="ace-icon fa fa-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
        <span class="help-block">{{{ $errors->first('gallery_date', ':message') }}}</span>
    </div>

	<div class="form-group{{ $errors->first('status', ' has-error') }}">
		<label for="status">Status</label>
		<select id="status" name="status" class="form-control input-sm">
			<option value="">&nbsp;</option>
			@foreach (config('setting.status') as $config => $val)
				<option value="{{ $config ? $config : Input::old('status', $row->status) }}" {{ $config == $row->status ? 'selected' : '' }}>{{$val}}</option>
			@endforeach
		</select>
		<span class="help-block">{{{ $errors->first('status', ':message') }}}</span>
	</div>
	
	{!! Form::submit(ucfirst($mode).' New Gallery', ['class' => 'btn btn-primary btn-xs']) !!}

{!! Form::close() !!}

@stop
