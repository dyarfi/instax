@extends('Admin::layouts.template')

@section('body')
<div class="container-fluid">
    @if($row->filename)
    <h4 class="red">Filename</h4>
    <div class="row-fluid">
        <img src="{{asset('uploads/activity/'.$row->filename)}}" class="img-responsive">
    </div>
    @endif
    @if($row->caption)
    <h4 class="red">Caption</h4>
    <div class="row-fluid">
        {{ $row->Caption }}
    </div>
    @endif
    @if($row->is_winner)
    <h4 class="red">Is Winner</h4>
    <div class="row-fluid">
        {{ config('setting.winner')[$row->is_winner] }}
    </div>
    @endif
    @if($row->status)
    <h4 class="red">Status</h4>
    <div class="row-fluid">
        {{ config('setting.status')[$row->status] }}
    </div>
    @endif
    @if($row->created_at)
    <h4 class="red">Created At</h4>
    <div class="row-fluid">
        {{ $row->created_at }}
    </div>
    @endif
    <hr/>
    <div class="row">
        <div class="col-md-5 col-xs-6">
            <a href="{{ route('admin.winners.index') }}" class="btn btn-info btn-xs">Back to all winners</a>
            <a href="{{ route('admin.winners.edit', $row->id) }}" class="btn btn-primary btn-xs">Edit winner</a>
            <a href="{{ route('admin.winners.create') }}" class="btn btn-warning btn-xs">Create Winner</a>
        </div>
        <div class="col-md-5 col-xs-6 text-right">
            {!! Form::open([
                'method' => 'DELETE',
                'route' => ['admin.winners.trash', $row->id]
            ]) !!}
                {!! Form::submit('Delete this gallery?', ['class' => 'btn btn-danger btn-xs']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>

@stop
