<?php namespace App\Modules\Campaign\Controller;

// Load Laravel classes
use Route, Request, Session, Redirect, Input, Validator, View, Storage, File;
// Load main base controller
use App\Modules\BaseAdmin;
// Load main models
use App\Modules\Campaign\Model\Gallery;
// Load Datatable
use Datatables;
// User Activity Logs
use Activity;

class Galleries extends BaseAdmin {

	/**
	 * Set galleries data.
	 *
	 */
	protected $galleries;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

		// Parent constructor
		parent::__construct();

		// Load Http/Middleware/Admin controller
		$this->middleware('auth.admin');

		// Load gallery and get repository data from Auth
		$this->galleries = new Gallery;

	}

	/**
	 * Display a listing of gallery.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{

		// Set return data
	   	$galleries = Input::get('path') === 'trashed' ? $this->galleries->onlyTrashed()->get() : $this->galleries->orderBy('created_at','desc')->get();

	   	// Get deleted count
		$deleted = $this->galleries->onlyTrashed()->get()->count();

	   	// Set data to return
	   	$data = ['rows'=>$galleries,'deleted'=>$deleted,'junked'=>Input::get('path')];

		// Load needed scripts
		$scripts = [
						'dataTables' => asset('themes/ace-admin/js/jquery.dataTables.min.js'),
						'dataTableBootstrap'=> asset('themes/ace-admin/js/jquery.dataTables.bootstrap.min.js'),
						'library' => asset("themes/ace-admin/js/library.js")
					];

		// Set inline script or style
		$inlines = [
		// Script execution on a specific controller page
		'script' => "
		// --- datatable handler [".route('admin.galleries.index')."]--- //
			var datatable  = $('#datatable-table');
			var controller = datatable.attr('rel');

			$('#datatable-table').DataTable({
				processing: true,
				serverSide: true,
				bAutoWidth: false,
				ajax: '".route('admin.galleries.datatable')."' + ($.getURLParameter('path') ? '?path=' + $.getURLParameter('path') : ''),
				columns: [
					{data: 'id', name:'id', orderable: false, searchable: false},
                    {data: 'name', name: 'name'},
                    {data: 'image', name: 'image'},
					{data: 'description', name: 'description'},
					{data: 'gallery_date', name: 'gallery_date'},
					{data: 'status', name: 'status'},
					{data: 'created_at', name: 'created_at'},
					{data: 'action', name: 'action', orderable: false, searchable: false}
				],
				language: {
					processing: ''
				},
				fnDrawCallback : function (oSettings) {
					$('#datatable-table > thead > tr > th:first-child')
					.removeClass('sorting_asc')
					.find('input[type=checkbox]')
					.prop('checked',false);
					$('#datatable-table > tbody > tr > td:first-child').addClass('center');
					$('[data-rel=tooltip]').tooltip();
				}
			});
		",
		];

		return $this->view('Campaign::gallery_datatable_index')
		->data($data)
		->scripts($scripts)
		->inlines($inlines)
		->title('Gallery List');
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function datatable(Request $request)
	{
		$rows = Input::get('path') === 'trashed' ? $this->galleries->onlyTrashed()->get() : $this->galleries->orderBy('created_at', 'asc')->get();

		return Datatables::of($rows)
			// Set action buttons
			->editColumn('action', function ($row) {
				if (Input::get('path') !== 'trashed') {
					return '
						<a data-rel="tooltip" data-original-title="View" title="" href="'.route('admin.galleries.show', $row->id).'" class="btn btn-xs btn-success tooltip-default">
							<i class="ace-icon fa fa-check bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Edit"  href="'.route('admin.galleries.edit', $row->id).'" class="btn btn-xs btn-info tooltip-default">
							<i class="ace-icon fa fa-pencil bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Trashed"  href="'.route('admin.galleries.trash', $row->id).'" class="btn btn-xs btn-danger tooltip-default">
							<i class="ace-icon fa fa-trash-o bigger-120"></i>
						</a>';
				} else {
					return '
						<a data-rel="tooltip" data-original-title="Restore!" href="'.route('admin.galleries.restored', $row->id).'" class="btn btn-xs btn-primary tooltip-default">
							<i class="ace-icon fa fa-save bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Permanent Delete!" href="'.route('admin.galleries.delete', $row->id).'" class="btn btn-xs btn-danger">
							<i class="ace-icon fa fa-trash bigger-120"></i>
						</a>';
				}
			})
			// Edit column name
			->editColumn('name', function ($row) {
				return $row->name;
			})
			// Edit column id
			->editColumn('id', function ($row) {
				return 	'
				<label class="pos-rel">
					<input type="checkbox" class="ace" name="check[]" id="check_'.$row->id.'" value="'.$row->id.'" />
					<span class="lbl"></span>
				</label>';
            })            
			// Set type id
			->editColumn('image', function ($row) {
				return $row->image ? '<img class="img-responsive" src="'.asset('uploads/'.$row->image).'">' : '- not set -';
			})
			// Set description limit
			->editColumn('description', function ($row) {
				return str_limit(strip_tags($row->description), 60);
			})
			// Set status icon and text
			->editColumn('status', function ($row) {
				return '
				<span class="label label-'.($row->status == 1 ? 'success' : 'warning').' arrowed-in arrowed-in-right">
					<span class="fa fa-'.($row->status == 1 ? 'flag' : 'exclamation-circle').' fa-sm"></span>
					'.config('setting.status')[$row->status].'
				</span>';
            })
			->rawColumns(['id','image','action','status'])
			->make(true);
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// Get data from database
        $gallery = $this->galleries->findOrFail($id);

        // Read ACL settings config for any permission access
        $acl = config('setting.modules');

		// Set data to return
	   	$data = ['row'=>$gallery,'acl'=>$acl];

	   	// Return data and view
	   	return $this->view('Campaign::gallery_show')->data($data)->title('View Gallery');

	}

	/**
	 * Show the form for creating new gallery.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new gallery.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating gallery.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating gallery.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified gallery.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function trash($id)
	{
		if ($gallery = $this->galleries->find($id))
		{

			// Add deleted_at and not completely delete
			$gallery->delete();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.galleries.index'))->with('success', 'Gallery Trashed!');
		}

		return Redirect::to(route('admin.galleries.index'))->with('error', 'Gallery Not Found!');
	}

	/**
	 * Restored the specified gallery.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function restored($id)
	{
		if ($gallery = $this->galleries->onlyTrashed()->find($id))
		{

			// Restored back from deleted_at database
			$gallery->restore();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.galleries.index'))->with('success', 'Gallery Restored!');
		}

		return Redirect::to(route('admin.galleries.index'))->with('error', 'Gallery Not Found!');
	}
	/**
	 * Remove the specified gallery.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{

		// Get gallery from id fetch
		if ($gallery = $this->galleries->onlyTrashed()->find($id))
		{

			// Delete from pivot table many to many
			// $this->galleries->onlyTrashed()->find($id)->roles()->detach();

			// Delete if there is an image attached
			if(File::exists('uploads/'.$gallery->image)) {
				// Delete the single file
				File::delete('uploads/'.$gallery->image);

			}

			// Permanently delete
			$gallery->forceDelete();

			// Log it first
			Activity::log(__FUNCTION__);

			return Redirect::to(route('admin.galleries.index','path=trashed'))->with('success', 'Gallery Permanently Deleted!');
		}

		return Redirect::to(route('admin.galleries.index','path=trashed'))->with('error', 'Gallery Not Found!');
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{

		if ($id)
		{
			if ( ! $row = $this->galleries->find($id))
			{
				return Redirect::to(route('admin.galleries.index'));
			}
		}
		else
		{
			$row = $this->galleries;
        }
        
		// Load needed javascripts
		$scripts = [
			'bootstrap-datepicker'=>asset('themes/ace-admin/js/bootstrap-datepicker.min.js'),
			'ckeditor'=>asset('themes/ace-admin/plugins/ckeditor/ckeditor.js'),
			'library'=>asset('themes/ace-admin/js/library.js')
		];

		// Load needed stylesheets
		$styles = [
			'stylesheet-datepicker'=> asset('themes/ace-admin/css/datepicker.min.css')
        ];
        
		return $this->view('Campaign::gallery_form')->data(compact('mode', 'row'))->scripts($scripts)->styles($styles)->title('Gallery '.$mode);
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		$input = array_filter(Input::all());
		
		// Set blog slug
		$input['slug'] = isset($input['name']) ? str_slug($input['name'],'-') : '';
				
		$rules = [
            'type_id'       => 'required',
            'name' 			=> 'required',            
			'image' 	   	=> ($mode == 'create' ? 'required|' : '').'image|max:1024',
			'slug'  		=> 'required',
			'description' 	=> 'required',
			'status' 		=> 'boolean'
		];

		// Set slug
		$input['slug'] = str_slug($input['name'],'-');

		// Set user id
		$input['user_id'] = $this->user->id;

		if ($id)
		{
			$gallery = $this->galleries->find($id);

            $messages = $this->validateGallery($input, $rules);
            
            // If user upload an image
			if (isset($input['image']) && Input::hasFile('image')) {

				// Set filename
				$filename = $this->imageUploadToDb($input['image'], 'uploads', 'galleries_');

			} else {

				// Set default image
				$filename = $gallery->image;

			}

			if ($messages->isEmpty())
			{
				// Get all request
				$result = $input;
                
                // Slip image file
                $result = isset($filename) ? array_set($input, 'image', $filename) : $result;
                
				// Slip user id
				$result = array_set($result, 'user_id', $this->user->id);

				$gallery->update($result);

			}

		}
		else
		{
			$messages = $this->validateGallery($input, $rules);

            // If user upload a file
			if (isset($input['image']) 
			&& Input::hasFile('image')) {

				// Set filename
				$filename = $this->imageUploadToDb($input['image'], 'uploads', 'galleries_');
			
            }
            
			if ($messages->isEmpty())
			{
				// Get all request
				$result = $input;

                // Slip image file
				$result = isset($input['image']) ? array_set($result, 'image', @$filename) : array_set($result, 'image', '');

				// Slip user id
				$result = array_set($result, 'user_id', $this->user->id);

                // Create new galleries
				$gallery = $this->galleries->create($result);

			}
		}
		
		// Log it first
		Activity::log(__FUNCTION__);

		if ($messages->isEmpty())
		{
			return Redirect::to(route('admin.galleries.show', $gallery->id))->with('success', 'Gallery Updated!');
		}

		return Redirect::back()->withInput()->withErrors($messages);
	}

	/**
	 * Change the data status.
	 *
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function change() {
		
		// Log it first
		Activity::log(__FUNCTION__);

		if (Input::get('check') !='') {

		    $rows	= Input::get('check');

		    foreach ($rows as $row) {
				// Set id for load and change status
				$this->galleries->withTrashed()->find($row)->update(['status' => Input::get('select_action')]);
		    }

		    // Set message
		    return Redirect::to(route('admin.galleries.index'))->with('success', 'Gallery Status Changed!');

		} else {

		    // Set message
		    return Redirect::to(route('admin.galleries.index'))->with('error','Data not Available!');
		}
	}

	/**
	 * Validates a gallery.
	 *
	 * @param  array  $data
	 * @param  mixed  $id
	 * @return \Illuminate\Support\MessageBag
	 */
	protected function validateGallery($data, $rules)
	{
		$validator = Validator::make($data, $rules);

		$validator->passes();

		return $validator->errors();
	}
    
    /**
	 * Process a file upload save the filename to DB.
	 *
	 * @param  array  $file
	 * @param  string $path
	 * @param  string $type
	 * @return $filename
	 */
	protected function imageUploadToDb($file='', $path='', $type='')
	{
		// Set filename upload
		$filename = '';
		// Check if input and upload already assigned
		if (!empty($file) && !$file->getError()) {
			// Getting image extension
			$extension = $file->getClientOriginalExtension();
			// Renaming image
			$filename = $type . rand(11111,99999) . '.' . $extension;
			// Set intervention image for image manipulation
			Storage::disk('local_uploads')->put($filename,
				file_get_contents($file)
			);
			// If image has a resize crop data in constructor
			if (!empty($this->imgFit)) {	
				$image = Image::make($path .'/'. $filename);
				// backup status
				$image->backup();
				foreach ($this->imgFit as $imgFit) {
					$size = explode('x',$imgFit);
					$image->fit($size[0],$size[1])->save($path .'/'. $imgFit.'px_'. $filename,100)->reset();
				}
			}
		}
		return $filename;
    }
    
	public function export() {

		// Log it first
		Activity::log(__FUNCTION__);
			
		// Get type file to export
		$type = Input::get('rel');
		// Get data to export
		$gallery = $this->galleries->select('id', 'name', 'description', 'created_at')->get();
		// Export file to type
		Excel::create('gallery', function($excel) use($gallery) {
		    $excel->sheet('Sheet 1', function($sheet) use($gallery) {
		        $sheet->fromArray($gallery);
		    });
		})->export($type);

	}

}
