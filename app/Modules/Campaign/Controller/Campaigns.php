<?php namespace App\Modules\Campaign\Controller;

// Load Laravel classes
use Route, Request, Session, Redirect, Input, Validator, Image, View, Excel, File, Storage;
// Load main base controller
use App\Modules\BaseAdmin;
// Load main models
use App\Modules\Campaign\Model\Campaign;
// Load Datatable
use Datatables;
// User Activity Logs
use Activity;

class Campaigns extends BaseAdmin {

	/**
	 * Set campaigns data.
	 *
	 */
	protected $campaigns;

	/**
	 * Set campaigns status.
	 *
	 */
	protected $statuses;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

		// Parent constructor
		parent::__construct();

		// Load Http/Middleware/Admin controller
		$this->middleware('auth.admin',['except'=>'profile']);

		// Load campaigns and get repository data from Auth
		$this->campaigns = new Campaign;

		// Set campaign statuses
		$this->statuses = ['current'=>'Current','ongoing'=>'Ongoing','passed'=>'Passed'];

	}

	/**
	 * Display a listing of campaigns.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{

		// Set return data
	   	$campaigns = Input::get('path') === 'trashed' ? $this->campaigns->onlyTrashed()->get() : $this->campaigns->orderBy('created_at','desc')->get();

		// Get deleted count
		$deleted = $this->campaigns->onlyTrashed()->get()->count();

	   	// Set data to return
	   	$data = ['rows'=>$campaigns,'deleted'=>$deleted,'junked'=>Input::get('path')];

		// Load needed scripts
		$scripts = [
				'dataTables' => asset('themes/ace-admin/js/jquery.dataTables.min.js'),
				'dataTableBootstrap'=> asset('themes/ace-admin/js/jquery.dataTables.bootstrap.min.js'),
				'library' => asset("themes/ace-admin/js/library.js")
					];

		// Set inline script or style
		$inlines = [
		// Script execution on a specific controller page
		'script' => "
		// --- datatable handler [".route('admin.campaigns.index')."]--- //
			var datatable  = $('#datatable-table');
			var controller = datatable.attr('rel');

			$('#datatable-table').DataTable({
				processing: true,
				serverSide: true,
				bAutoWidth: false,
				ajax: '".route('admin.campaigns.datatable')."' + ($.getURLParameter('path') ? '?path=' + $.getURLParameter('path') : ''),
				columns: [
					{data: 'id', name:'id', orderable: false, searchable: false},
					{data: 'name', name: 'name'},
					{data: 'image', name: 'image'},
					{data: 'description', name: 'description'},
					{data: 'options', name: 'options'},
					{data: 'status', name: 'status'},
					{data: 'created_at', name: 'created_at'},
					{data: 'action', name: 'action', orderable: false, searchable: false}
				],
				language: {
					processing: ''
				},
				fnDrawCallback : function (oSettings) {
					$('#datatable-table > thead > tr > th:first-child')
					.removeClass('sorting_asc')
					.find('input[type=checkbox]')
					.prop('checked',false);
					$('#datatable-table > tbody > tr > td:first-child').addClass('center');
					$('[data-rel=tooltip]').tooltip();
				}
			});
		",
		];

		return $this->view('Campaign::campaign_datatable_index')
		->data($data)
		->scripts($scripts)
		->inlines($inlines)
		->title('Campaigns List');
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function datatable(Request $request)
	{
		$rows = Input::get('path') === 'trashed' ? $this->campaigns->onlyTrashed()->get() : $this->campaigns->orderBy('created_at', 'asc')->get();

		return DataTables::of($rows)
			// Set action buttons
			->editColumn('action', function ($row) {
				if (Input::get('path') !== 'trashed') {
					return '
						<a data-rel="tooltip" data-original-title="View" title="" href="'.route('admin.campaigns.show', $row->id).'" class="btn btn-xs btn-success tooltip-default">
							<i class="ace-icon fa fa-check bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Edit"  href="'.route('admin.campaigns.edit', $row->id).'" class="btn btn-xs btn-info tooltip-default">
							<i class="ace-icon fa fa-pencil bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Trashed"  href="'.route('admin.campaigns.trash', $row->id).'" class="btn btn-xs btn-danger tooltip-default">
							<i class="ace-icon fa fa-trash-o bigger-120"></i>
						</a>';
				} else {
					return '
						<a data-rel="tooltip" data-original-title="Restore!" href="'.route('admin.campaigns.restored', $row->id).'" class="btn btn-xs btn-primary tooltip-default">
							<i class="ace-icon fa fa-save bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Permanent Delete!" href="'.route('admin.campaigns.delete', $row->id).'" class="btn btn-xs btn-danger">
							<i class="ace-icon fa fa-trash bigger-120"></i>
						</a>';
				}
			})
			// Edit column name
			->editColumn('name', function ($row) {
				return ($row->name && $row->url) ? '<b>'.$row->name .'</b><br> ('.$row->url. ')' : $row->name;
			})
			// Edit column id
			->editColumn('id', function ($row) {
				return 	'
				<label class="pos-rel">
					<input type="checkbox" class="ace" name="check[]" id="check_'.$row->id.'" value="'.$row->id.'" />
					<span class="lbl"></span>
				</label>';
			})
			// Set description limit
			->editColumn('description', function ($row) {
				return str_limit(strip_tags($row->description), 60);
			})
			// Set description limit
			->editColumn('image', function ($row) {
				
				return $row->image ? '<img src="'.asset('uploads/'.$row->image).'" class="img-responsive"/>' : '-';
			})
			// Set status icon and text
			->editColumn('options', function ($row) {
				$options = isset($this->statuses[$row->options]) ? $this->statuses[$row->options] : 'Unset';
				return '
				<span class="label label-'.($row->options == 'current' ? 'success' : 'warning').' arrowed-in arrowed-in-right">
					<span class="fa fa-'.($row->options == 'current' ? 'flag' : 'exclamation-circle').' fa-sm"></span>
					'.$options.'
				</span>';
            })
			// Set status icon and text
			->editColumn('status', function ($row) {
				return '
				<span class="label label-'.($row->status == 1 ? 'success' : 'warning').' arrowed-in arrowed-in-right">
					<span class="fa fa-'.($row->status == 1 ? 'flag' : 'exclamation-circle').' fa-sm"></span>
					'.config('setting.status')[$row->status].'
				</span>';
            })
			->rawColumns(['id','name','image','action','options','status'])
			->make(true);
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// Get data from database
        $campaign = $this->campaigns->findOrFail($id);

        // Read ACL settings config for any permission access
        $acl = config('setting.modules');

		// Set data to return
	   	$data = ['row'=>$campaign,'acl'=>$acl];

	   	// Return data and view
	   	return $this->view('Campaign::campaign_show')->data($data)->title('View Campaign');

	}

	/**
	 * Show the form for creating new campaign.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new campaign.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating campaign.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating campaign.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified campaign.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function trash($id)
	{
		if ($campaign = $this->campaigns->find($id))
		{

			// Add deleted_at and not completely delete
			$campaign->delete();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.campaigns.index'))->with('success', 'Campaign Trashed!');
		}

		return Redirect::to(route('admin.campaigns.index'))->with('error', 'Campaign Not Found!');
	}

	/**
	 * Restored the specified campaign.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function restored($id)
	{
		if ($campaign = $this->campaigns->onlyTrashed()->find($id))
		{

			// Restored back from deleted_at database
			$campaign->restore();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.campaigns.index'))->with('success', 'Campaign Restored!');
		}

		return Redirect::to(route('admin.campaigns.index'))->with('error', 'Campaign Not Found!');
	}
	/**
	 * Remove the specified campaign.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{

		// Get campaign from id fetch
		if ($campaign = $this->campaigns->onlyTrashed()->find($id))
		{

			// Delete if there is an image attached
			if(File::exists('uploads/'.$campaign->image)) {
				// Delete the single file
				File::delete('uploads/'.$campaign->image);

			}

			// Permanently delete
			$campaign->forceDelete();

			// Log it first
			Activity::log(__FUNCTION__);

			return Redirect::to(route('admin.campaigns.index','path=trashed'))->with('success', 'Campaign Permanently Deleted!');
		}

		return Redirect::to(route('admin.campaigns.index','path=trashed'))->with('error', 'Campaign Not Found!');
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{

		if ($id)
		{
			if ( ! $row = $this->campaigns->find($id))
			{
				return Redirect::to(route('admin.campaigns.index'));
			}
		}
		else
		{
			$row = $this->campaigns;
        }
        
        // Load needed javascripts
		$scripts = [
			'bootstrap-datepicker'=>asset('themes/ace-admin/js/bootstrap-datepicker.min.js'),
			'ckeditor'=>asset('themes/ace-admin/plugins/ckeditor/ckeditor.js'),
			'library'=>asset('themes/ace-admin/js/library.js')
		];

		// Load needed stylesheets
		$styles = [
			'stylesheet-datepicker'=> asset('themes/ace-admin/css/datepicker.min.css')
        ];

		// Set campaign statutes
        $options = $this->statuses;
        
		// Return view with data
		return $this->view('Campaign::campaign_form')->data(compact('mode', 'row', 'options'))->scripts($scripts)->styles($styles)->title('Campaign '.$mode);
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		// Get form request
		$input = array_filter(Input::all());
		// Set blog slug
		$input['slug'] = isset($input['name']) ? str_slug($input['name'],'-') : '';
		// Set validation rules
		$rules = [
			'name' 			=> 'required'.($id) ? '' : '|unique:campaigns,name',
			'description'  	=> 'required',
			'image' 	   	=> ($mode == 'create' ? 'required|' : '').'image|max:1024',
			'image_mobile' 	=> ($mode == 'create' ? 'required|' : '').'image|max:1024',
            'url'      		=> 'required'.($id) ? '' : '|unique:campaigns,url',
            'embed'      	=> 'url',			
			'options'      	=> 'required'.($id) ? '' : '|in['.array_keys($this->statuses).']',
			// 'status'	 	=> 'boolean'
		];
		// Check update or create
		if ($id)
		{
			// Set campaign
			$campaign = Campaign::find($id);

			// Set validation messages
			$messages = $this->validateCampaign($input, $rules);
			
			// If user upload an image
			if (isset($input['image']) && Input::hasFile('image')) {

				// Set filename
				$filename = $this->imageUploadToDb($input['image'], 'uploads', 'campaign_');

			} else {

				// Set default image
				$filename = $campaign->image;

			}
			
			// If user upload an image for mobile
			if(isset($input['image_mobile']) && Input::hasFile('image_mobile')) {
			
				// Set filename
				$filename_mobile = $this->imageUploadToDb($input['image_mobile'], 'uploads', 'campaign_mobile_');

			} else {

				// Set default image mobile
				$filename_mobile = $campaign->image_mobile;

			}

			if ($messages->isEmpty())
			{

				// Get all request
				$result = $input;

				// Slip user id
				$result = array_set($result, 'user_id', $this->user->id);

				// Slip image file
				$result = isset($filename) ? array_set($input, 'image', $filename) : $result;

				// Slip image mobile file
				$result = isset($filename_mobile) ? array_set($input, 'image_mobile', $filename_mobile) : $result;

				// Get campaign model to update other data
				$campaign->update($result);

				return Redirect::back()->withInput()->with('success', 'Campaign Updated!');

			}
		}
		else
		{

			$messages = $this->validateCampaign($input, $rules);
			
			// If user upload a file
			if (isset($input['image']) 
			&& Input::hasFile('image')
				&& isset($input['image_mobile'])
					&& Input::hasFile('image_mobile')) {

				// Set filename
				$filename = $this->imageUploadToDb($input['image'], 'uploads', 'campaign_');
				// Set filename
				$filename_mobile = $this->imageUploadToDb($input['image_mobile'], 'uploads', 'campaign_mobile_');

			}

			if ($messages->isEmpty())
			{
				// Get all request
				$result = $input;

				// Slip user id
				$result = array_set($result, 'user_id', $this->user->id);

				// Slip image file
				$result = isset($input['image']) ? array_set($result, 'image', @$filename) : array_set($result, 'image', '');

				// Slip image mobile file
				$result = isset($input['image_mobile']) ? array_set($result, 'image_mobile', @$filename_mobile) : array_set($result, 'image_mobile', '');

				// Create campaign into the database
				$campaign = Campaign::create($result);

			}
		}

		// Log it first
		Activity::log(__FUNCTION__);

		if ($messages->isEmpty())
		{
			return Redirect::to(route('admin.campaigns.show',$campaign->id))->with('success', 'Campaign Updated!');
		}

		return Redirect::back()->withInput()->withErrors($messages);
	}

	/**
	 * Change the data status.
	 *
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function change() {

		// Log it first
		Activity::log(__FUNCTION__);

		if (Input::get('check') !='') {

		    $rows	= Input::get('check');

		    foreach ($rows as $row) {
				// Set id for load and change status
				$this->campaigns->withTrashed()->find($row)->update(['status' => Input::get('select_action')]);
		    }

		    // Set message
		    return Redirect::to(route('admin.campaigns.index'))->with('success', 'Campaign Status Changed!');

		} else {

		    // Set message
		    return Redirect::to(route('admin.campaigns.index'))->with('error','Data not Available!');
		}
	}

	/**
	 * Validates a campaign.
	 *
	 * @param  array  $data
	 * @param  mixed  $id
	 * @return \Illuminate\Support\MessageBag
	 */
	protected function validateCampaign($data, $rules)
	{
		$validator = Validator::make($data, $rules);

		$validator->passes();

		return $validator->errors();
	}

    /**
	 * Process a file upload save the filename to DB.
	 *
	 * @param  array  $file
	 * @param  string $path
	 * @param  string $type
	 * @return $filename
	 */
	protected function imageUploadToDb($file='', $path='', $type='')
	{
		// Set filename upload
		$filename = '';
		// Check if input and upload already assigned
		if (!empty($file) && !$file->getError()) {
			// Getting image extension
			$extension = $file->getClientOriginalExtension();
			// Renaming image
			$filename = $type . rand(11111,99999) . '.' . $extension;
			// Set intervention image for image manipulation
			Storage::disk('local_uploads')->put($filename,
				file_get_contents($file)
			);
			// If image has a resize crop data in constructor
			if (!empty($this->imgFit)) {	
				$image = Image::make($path .'/'. $filename);
				// backup status
				$image->backup();
				foreach ($this->imgFit as $imgFit) {
					$size = explode('x',$imgFit);
					$image->fit($size[0],$size[1])->save($path .'/'. $imgFit.'px_'. $filename,100)->reset();
				}
			}
		}
		return $filename;
	}
	  
	/**
	 * Process a file to download.
	 *
	 * @return $file export
	 */
	public function export() {

		// Log it first
		Activity::log(__FUNCTION__);

		// Get type file to export
		$type = Input::get('rel');
		// Get data to export
		$campaigns = $this->campaigns->select('id','name','description','status','updated_at','created_at')->get();
		// Export file to type
		Excel::create('campaigns', function($excel) use($campaigns) {
			// Set the spreadsheet title, creator, and description
	        $excel->setTitle('Export List');
	        $excel->setCreator('Laravel')->setCompany('laravel.com');
	        $excel->setDescription('export file');

		    $excel->sheet('Sheet 1', function($sheet) use($campaigns) {
				$sheet->fromArray($campaigns);
		    });
		})->export($type);

	}

}
