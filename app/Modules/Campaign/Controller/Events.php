<?php namespace App\Modules\Campaign\Controller;

// Load Laravel classes
use Route, Request, Session, Redirect, Input, Validator, View, File;
// Load main base controller
use App\Modules\BaseAdmin;
// Load main models
use App\Modules\Campaign\Model\Event;
// Load Datatable
use Datatables;
// User Activity Logs
use Activity;

class Events extends BaseAdmin {

	/**
	 * Set events data.
	 *
	 */
	protected $events;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

		// Parent constructor
		parent::__construct();

		// Load Http/Middleware/Admin controller
		$this->middleware('auth.admin');

		// Load event and get repository data from Auth
		$this->events = new Event;

	}

	/**
	 * Display a listing of event.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{

		// Set return data
	   	$events = Input::get('path') === 'trashed' ? $this->events->onlyTrashed()->get() : $this->events->orderBy('created_at','desc')->get();

	   	// Get deleted count
		$deleted = $this->events->onlyTrashed()->get()->count();

	   	// Set data to return
	   	$data = ['rows'=>$events,'deleted'=>$deleted,'junked'=>Input::get('path')];

		// Load needed scripts
		$scripts = [
						'dataTables' => asset('themes/ace-admin/js/jquery.dataTables.min.js'),
						'dataTableBootstrap'=> asset('themes/ace-admin/js/jquery.dataTables.bootstrap.min.js'),
						'library' => asset("themes/ace-admin/js/library.js")
					];

		// Set inline script or style
		$inlines = [
		// Script execution on a specific controller page
		'script' => "
		// --- datatable handler [".route('admin.events.index')."]--- //
			var datatable  = $('#datatable-table');
			var controller = datatable.attr('rel');

			$('#datatable-table').DataTable({
				processing: true,
				serverSide: true,
				bAutoWidth: false,
				ajax: '".route('admin.events.datatable')."' + ($.getURLParameter('path') ? '?path=' + $.getURLParameter('path') : ''),
				columns: [
					{data: 'id', name:'id', orderable: false, searchable: false},
					{data: 'name', name: 'name'},
					{data: 'description', name: 'description'},
					{data: 'event_date', name: 'event_date'},
					{data: 'status', name: 'status'},
					{data: 'created_at', name: 'created_at'},
					{data: 'action', name: 'action', orderable: false, searchable: false}
				],
				language: {
					processing: ''
				},
				fnDrawCallback : function (oSettings) {
					$('#datatable-table > thead > tr > th:first-child')
					.removeClass('sorting_asc')
					.find('input[type=checkbox]')
					.prop('checked',false);
					$('#datatable-table > tbody > tr > td:first-child').addClass('center');
					$('[data-rel=tooltip]').tooltip();
				}
			});
		",
		];

		return $this->view('Campaign::event_datatable_index')
		->data($data)
		->scripts($scripts)
		->inlines($inlines)
		->title('Event List');
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function datatable(Request $request)
	{
		$rows = Input::get('path') === 'trashed' ? $this->events->onlyTrashed()->get() : $this->events->orderBy('created_at', 'asc')->get();

		return Datatables::of($rows)
			// Set action buttons
			->editColumn('action', function ($row) {
				if (Input::get('path') !== 'trashed') {
					return '
						<a data-rel="tooltip" data-original-title="View" title="" href="'.route('admin.events.show', $row->id).'" class="btn btn-xs btn-success tooltip-default">
							<i class="ace-icon fa fa-check bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Edit"  href="'.route('admin.events.edit', $row->id).'" class="btn btn-xs btn-info tooltip-default">
							<i class="ace-icon fa fa-pencil bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Trashed"  href="'.route('admin.events.trash', $row->id).'" class="btn btn-xs btn-danger tooltip-default">
							<i class="ace-icon fa fa-trash-o bigger-120"></i>
						</a>';
				} else {
					return '
						<a data-rel="tooltip" data-original-title="Restore!" href="'.route('admin.events.restored', $row->id).'" class="btn btn-xs btn-primary tooltip-default">
							<i class="ace-icon fa fa-save bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Permanent Delete!" href="'.route('admin.events.delete', $row->id).'" class="btn btn-xs btn-danger">
							<i class="ace-icon fa fa-trash bigger-120"></i>
						</a>';
				}
			})
			// Edit column name
			->editColumn('name', function ($row) {
				return $row->name;
			})
			// Edit column id
			->editColumn('id', function ($row) {
				return 	'
				<label class="pos-rel">
					<input type="checkbox" class="ace" name="check[]" id="check_'.$row->id.'" value="'.$row->id.'" />
					<span class="lbl"></span>
				</label>';
			})
			// Set description limit
			->editColumn('description', function ($row) {
				return str_limit(strip_tags($row->description), 60);
			})
			// Set status icon and text
			->editColumn('status', function ($row) {
				return '
				<span class="label label-'.($row->status == 1 ? 'success' : 'warning').' arrowed-in arrowed-in-right">
					<span class="fa fa-'.($row->status == 1 ? 'flag' : 'exclamation-circle').' fa-sm"></span>
					'.config('setting.status')[$row->status].'
				</span>';
            })
			->rawColumns(['id','action','status'])
			->make(true);
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// Get data from database
        $event = $this->events->findOrFail($id);

        // Read ACL settings config for any permission access
        $acl = config('setting.modules');

		// Set data to return
	   	$data = ['row'=>$event,'acl'=>$acl];

	   	// Return data and view
	   	return $this->view('Campaign::event_show')->data($data)->title('View Event');

	}

	/**
	 * Show the form for creating new event.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new event.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating event.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating event.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified event.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function trash($id)
	{
		if ($event = $this->events->find($id))
		{

			// Add deleted_at and not completely delete
			$event->delete();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.events.index'))->with('success', 'Event Trashed!');
		}

		return Redirect::to(route('admin.events.index'))->with('error', 'Event Not Found!');
	}

	/**
	 * Restored the specified event.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function restored($id)
	{
		if ($event = $this->events->onlyTrashed()->find($id))
		{

			// Restored back from deleted_at database
			$event->restore();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.events.index'))->with('success', 'Event Restored!');
		}

		return Redirect::to(route('admin.events.index'))->with('error', 'Event Not Found!');
	}
	/**
	 * Remove the specified event.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{

		// Get event from id fetch
		if ($event = $this->events->onlyTrashed()->find($id))
		{

			// Delete from pivot table many to many
			// $this->events->onlyTrashed()->find($id)->roles()->detach();

			// Delete if there is an image attached
			if(File::exists('uploads/'.$event->image)) {
				// Delete the single file
				File::delete('uploads/'.$event->image);

			}

			// Permanently delete
			$event->forceDelete();

			// Log it first
			Activity::log(__FUNCTION__);

			return Redirect::to(route('admin.events.index','path=trashed'))->with('success', 'Event Permanently Deleted!');
		}

		return Redirect::to(route('admin.events.index','path=trashed'))->with('error', 'Event Not Found!');
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{

		if ($id)
		{
			if ( ! $row = $this->events->find($id))
			{
				return Redirect::to(route('admin.events.index'));
			}
		}
		else
		{
			$row = $this->events;
		}

		return $this->view('Campaign::event_form')->data(compact('mode', 'row'))->title('Event '.$mode);
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		$input = array_filter(Input::all());
		
		// Set blog slug
		$input['slug'] = isset($input['name']) ? str_slug($input['name'],'-') : '';
				
		$rules = [
			'name' 			=> 'required',
			'slug'  		=> 'required',
			'description' 	=> 'required',
			'status' 		=> 'boolean',
		];

		// Set slug
		$input['slug'] = str_slug($input['name'],'-');

		// Set user id
		$input['user_id'] = $this->user->id;

		if ($id)
		{
			$event = $this->events->find($id);

			$messages = $this->validateEvent($input, $rules);

			if ($messages->isEmpty())
			{
				// Get all request
				$result = $input;

				// Slip user id
				$result = array_set($result, 'user_id', $this->user->id);

				$event->update($result);

			}

		}
		else
		{
			$messages = $this->validateEvent($input, $rules);

			if ($messages->isEmpty())
			{
				// Get all request
				$result = $input;

				// Slip user id
				$result = array_set($result, 'user_id', $this->user->id);

				//$menu = $this->menus->create($input);
				$event = $this->events->create($result);

			}
		}
		
		// Log it first
		Activity::log(__FUNCTION__);

		if ($messages->isEmpty())
		{
			return Redirect::to(route('admin.events.show', $event->id))->with('success', 'Event Updated!');
		}

		return Redirect::back()->withInput()->withErrors($messages);
	}

	/**
	 * Change the data status.
	 *
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function change() {
		
		// Log it first
		Activity::log(__FUNCTION__);

		if (Input::get('check') !='') {

		    $rows	= Input::get('check');

		    foreach ($rows as $row) {
				// Set id for load and change status
				$this->events->withTrashed()->find($row)->update(['status' => Input::get('select_action')]);
		    }

		    // Set message
		    return Redirect::to(route('admin.events.index'))->with('success', 'Event Status Changed!');

		} else {

		    // Set message
		    return Redirect::to(route('admin.events.index'))->with('error','Data not Available!');
		}
	}

	/**
	 * Validates a event.
	 *
	 * @param  array  $data
	 * @param  mixed  $id
	 * @return \Illuminate\Support\MessageBag
	 */
	protected function validateEvent($data, $rules)
	{
		$validator = Validator::make($data, $rules);

		$validator->passes();

		return $validator->errors();
	}

	public function export() {

		// Log it first
		Activity::log(__FUNCTION__);
			
		// Get type file to export
		$type = Input::get('rel');
		// Get data to export
		$event = $this->events->select('id', 'name', 'description', 'created_at')->get();
		// Export file to type
		Excel::create('event', function($excel) use($event) {
		    $excel->sheet('Sheet 1', function($sheet) use($event) {
		        $sheet->fromArray($event);
		    });
		})->export($type);

	}

}
