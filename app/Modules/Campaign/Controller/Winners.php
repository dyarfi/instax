<?php namespace App\Modules\Campaign\Controller;

// Load Laravel classes
use Route, Request, Session, Redirect, Input, Validator, View, Storage, File;
// Load main base controller
use App\Modules\BaseAdmin;
// Load main models
use App\Modules\Campaign\Model\Winner;
use App\Modules\Campaign\Model\Campaign;
// Load Datatable
use Datatables;
// User Activity Logs
use Activity;

class Winners extends BaseAdmin {

	/**
	 * Set winners data.
	 *
	 */
    protected $winners;
    
    /**
	 * Set videos data.
	 *
	 */
	protected $campaign;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

		// Parent constructor
		parent::__construct();

		// Load Http/Middleware/Admin controller
		$this->middleware('auth.admin');

		// Load winner and get repository data from Auth
        $this->winners = new Winner;
        $this->campaigns = new Campaign;

	}

	/**
	 * Display a listing of winner.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{

		// Set return data
	   	$winners = Input::get('path') === 'trashed' ? $this->winners->onlyTrashed()->get() : $this->winners->orderBy('created_at','desc')->get();

	   	// Get deleted count
		$deleted = $this->winners->onlyTrashed()->get()->count();

	   	// Set data to return
	   	$data = ['rows'=>$winners,'deleted'=>$deleted,'junked'=>Input::get('path')];

		// Load needed scripts
		$scripts = [
						'dataTables' => asset('themes/ace-admin/js/jquery.dataTables.min.js'),
						'dataTableBootstrap'=> asset('themes/ace-admin/js/jquery.dataTables.bootstrap.min.js'),
						'library' => asset("themes/ace-admin/js/library.js")
					];

		// Set inline script or style
		$inlines = [
		// Script execution on a specific controller page
		'script' => "
		// --- datatable handler [".route('admin.winners.index')."]--- //
			var datatable  = $('#datatable-table');
			var controller = datatable.attr('rel');

			$('#datatable-table').DataTable({
				processing: true,
				serverSide: true,
				bAutoWidth: false,
				ajax: '".route('admin.winners.datatable')."' + ($.getURLParameter('path') ? '?path=' + $.getURLParameter('path') : ''),
				columns: [
                    {data: 'id', name:'id', orderable: false, searchable: false},
                    {data: 'campaign_id', name: 'campaign_id'},
                    {data: 'filename', name: 'filename'},
                    {data: 'caption', name: 'caption'},
                    {data: 'status', name: 'status'},              
					{data: 'is_winner', name: 'is_winner'},
					{data: 'created_at', name: 'created_at'},
					{data: 'action', name: 'action', orderable: false, searchable: false}
				],
				language: {
					processing: ''
				},
				fnDrawCallback : function (oSettings) {
					$('#datatable-table > thead > tr > th:first-child')
					.removeClass('sorting_asc')
					.find('input[type=checkbox]')
					.prop('checked',false);
					$('#datatable-table > tbody > tr > td:first-child').addClass('center');
					$('[data-rel=tooltip]').tooltip();
				}
			});
		",
		];

		return $this->view('Campaign::winner_datatable_index')
		->data($data)
		->scripts($scripts)
		->inlines($inlines)
		->title('Winner List');
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function datatable(Request $request)
	{
		$rows = Input::get('path') === 'trashed' ? $this->winners->with('campaign')->onlyTrashed()->get() : $this->winners->with('campaign')->orderBy('created_at', 'asc')->get();

		return Datatables::of($rows)
			// Set action buttons
			->editColumn('action', function ($row) {
				if (Input::get('path') !== 'trashed') {
					return '
						<a data-rel="tooltip" data-original-title="View" title="" href="'.route('admin.winners.show', $row->id).'" class="btn btn-xs btn-success tooltip-default">
							<i class="ace-icon fa fa-check bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Edit"  href="'.route('admin.winners.edit', $row->id).'" class="btn btn-xs btn-info tooltip-default">
							<i class="ace-icon fa fa-pencil bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Trashed"  href="'.route('admin.winners.trash', $row->id).'" class="btn btn-xs btn-danger tooltip-default">
							<i class="ace-icon fa fa-trash-o bigger-120"></i>
						</a>';
				} else {
					return '
						<a data-rel="tooltip" data-original-title="Restore!" href="'.route('admin.winners.restored', $row->id).'" class="btn btn-xs btn-primary tooltip-default">
							<i class="ace-icon fa fa-save bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Permanent Delete!" href="'.route('admin.winners.delete', $row->id).'" class="btn btn-xs btn-danger">
							<i class="ace-icon fa fa-trash bigger-120"></i>
						</a>';
				}
			})
			// Edit column id
			->editColumn('id', function ($row) {
				return 	'
				<label class="pos-rel">
					<input type="checkbox" class="ace" name="check[]" id="check_'.$row->id.'" value="'.$row->id.'" />
					<span class="lbl"></span>
				</label>';
            })
			// Edit column name
			->editColumn('campaign_id', function ($row) {
				return ($row->campaign->name) ? $row->campaign->name : '-';
			})
			// Edit column name
			->editColumn('filename', function ($row) {
				return ($row->filename) ? '<img src="'.asset('uploads/activity/'.$row->filename).'" class="img-responsive">' : '';
			})
			// Set description limit
			->editColumn('is_winner', function ($row) {
				return '
				<span class="label label-'.($row->is_winner == 1 ? 'success' : 'warning').' arrowed-in arrowed-in-right">
					<span class="fa fa-'.($row->is_winner == 1 ? 'flag' : 'exclamation-circle').' fa-sm"></span>
					'.@config('setting.winner')[@$row->is_winner].'
				</span>';
			})
			// Set status icon and text
			->editColumn('status', function ($row) {
				return '
				<span class="label label-'.($row->status == 1 ? 'success' : 'warning').' arrowed-in arrowed-in-right">
					<span class="fa fa-'.($row->status == 1 ? 'flag' : 'exclamation-circle').' fa-sm"></span>
					'.config('setting.status')[$row->status].'
				</span>';
            })
			->rawColumns(['id','filename','action','is_winner','status'])
			->make(true);
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// Get data from database
        $winner = $this->winners->findOrFail($id);

        // Read ACL settings config for any permission access
        $acl = config('setting.modules');

		// Set data to return
	   	$data = ['row'=>$winner,'acl'=>$acl];

	   	// Return data and view
	   	return $this->view('Campaign::winner_show')->data($data)->title('View Winner');

	}

	/**
	 * Show the form for creating new winner.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new winner.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating winner.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating winner.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified winner.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function trash($id)
	{
		if ($winner = $this->winners->find($id))
		{

			// Add deleted_at and not completely delete
			$winner->delete();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.winners.index'))->with('success', 'Winner Trashed!');
		}

		return Redirect::to(route('admin.winners.index'))->with('error', 'Winner Not Found!');
	}

	/**
	 * Restored the specified winner.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function restored($id)
	{
		if ($winner = $this->winners->onlyTrashed()->find($id))
		{

			// Restored back from deleted_at database
			$winner->restore();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.winners.index'))->with('success', 'Winner Restored!');
		}

		return Redirect::to(route('admin.winners.index'))->with('error', 'Winner Not Found!');
	}
	/**
	 * Remove the specified winner.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{

		// Get winner from id fetch
		if ($winner = $this->winners->onlyTrashed()->find($id))
		{

			// Delete from pivot table many to many
			// $this->winners->onlyTrashed()->find($id)->roles()->detach();

			// Delete if there is an image attached
			if(File::exists('uploads/activity/'.$winner->image)) {
				// Delete the single file
				File::delete('uploads/activity/'.$winner->image);

			}

			// Permanently delete
			$winner->forceDelete();

			// Log it first
			Activity::log(__FUNCTION__);

			return Redirect::to(route('admin.winners.index','path=trashed'))->with('success', 'Winner Permanently Deleted!');
		}

		return Redirect::to(route('admin.winners.index','path=trashed'))->with('error', 'Winner Not Found!');
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{

		if ($id)
		{
			if ( ! $row = $this->winners->find($id))
			{
				return Redirect::to(route('admin.winners.index'));
			}
		}
		else
		{
			$row = $this->winners;
        }
        
        // Set this model
		$model	 	= $this->winners;
		
		// Get videos data
		$campaigns = $this->campaigns->pluck('name','id')->all();

		return $this->view('Campaign::winner_form')->data(compact('mode', 'row', 'campaigns'))->title('Winner '.$mode);
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		$input = array_filter(Input::all());
		
		// Set blog slug
		$input['slug'] = isset($input['name']) ? str_slug($input['name'],'-') : '';
				
		$rules = [
            'campaign_id'   => 'required',
            'filename' 	   	=> ($mode == 'create' ? 'required|' : '').'image|max:1024',
            'caption'  		=> 'required',
            'is_winner'     => 'boolean'
			// 'status' 		=> 'boolean'
		];

		// Set slug
		// $input['slug'] = str_slug($input['name'],'-');

		// Set user id
		$input['user_id'] = $this->user->id;

		if ($id)
		{
			$winner = $this->winners->find($id);

			$messages = $this->validateWinner($input, $rules);

            // If user upload an image
			if (isset($input['filename']) && Input::hasFile('filename')) {

				// Set filename
				$filename = $this->imageUploadToDb($input['filename'], 'activity', 'activity_');

			} else {

				// Set default image
				$filename = $winner->image;

            }
            
			if ($messages->isEmpty())
			{
				// Get all request
				$result = $input;

	            // Slip image file
				$result = isset($filename) ? array_set($input, 'filename', $filename) : $result;

				// Slip user id
				$result = array_set($result, 'user_id', $this->user->id);

                // Update winners
				$winner->update($result);

			}

		}
		else
		{
			$messages = $this->validateWinner($input, $rules);

            // If user upload a file
			if (isset($input['filename']) 
			&& Input::hasFile('filename')) {

				// Set filename
				$filename = $this->imageUploadToDb($input['filename'], 'activity', 'activity_');

            }
            
			if ($messages->isEmpty())
			{
				// Get all request
				$result = $input;

                // Slip image file
				$result = isset($filename) ? array_set($input, 'filename', $filename) : $result;

				// Slip user id
				$result = array_set($result, 'user_id', $this->user->id);

                // Create new winners
				$winner = $this->winners->create($result);

			}
		}
		
		// Log it first
		Activity::log(__FUNCTION__);

		if ($messages->isEmpty())
		{
			return Redirect::to(route('admin.winners.show', $winner->id))->with('success', 'Winner Updated!');
		}

		return Redirect::back()->withInput()->withErrors($messages);
	}

	/**
	 * Change the data status.
	 *
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function change() {
		
		// Log it first
		Activity::log(__FUNCTION__);
        //dd(Input::get('select_action') == null);

		if (Input::get('select_action') != null && Input::get('check') !='') {

		    $rows	= Input::get('check');

		    foreach ($rows as $row) {
				// Set id for load and change status
				$this->winners->withTrashed()->find($row)->update(['status' => Input::get('select_action')]);
		    }

		    // Set message
            return Redirect::to(route('admin.winners.index'))->with('success', 'Winner Status Changed!');

        } else if (Input::get('select_action') == null || Input::get('select_winner') != null && Input::get('check') !='') {
            
            $rows	= Input::get('check');

            if(is_array($rows)) {
                foreach ($rows as $row) {
                    // Set id for load and change status
                    $this->winners->withTrashed()->find($row)->update(['is_winner' => Input::get('select_winner')]);
                }
            }
            
		    // Set message
            return Redirect::to(route('admin.winners.index'))->with('success', 'Winner Status Changed!');

        } else {

		    // Set message
		    return Redirect::to(route('admin.winners.index'))->with('error','Data not Available!');
		}
	}

	/**
	 * Validates a winner.
	 *
	 * @param  array  $data
	 * @param  mixed  $id
	 * @return \Illuminate\Support\MessageBag
	 */
	protected function validateWinner($data, $rules)
	{
		$validator = Validator::make($data, $rules);

		$validator->passes();

		return $validator->errors();
	}
  
    /**
	 * Process a file upload save the filename to DB.
	 *
	 * @param  array  $file
	 * @param  string $path
	 * @param  string $type
	 * @return $filename
	 */
	protected function imageUploadToDb($file='', $path='', $type='')
	{
		// Set filename upload
		$filename = '';
		// Check if input and upload already assigned
		if (!empty($file) && !$file->getError()) {
			// Getting image extension
			$extension = $file->getClientOriginalExtension();
			// Renaming image
			$filename = $type . rand(11111,99999) . '.' . $extension;
			// Set intervention image for image manipulation
			Storage::disk('local_upload_activity')->put($filename,
				file_get_contents($file)
			);
			// If image has a resize crop data in constructor
			if (!empty($this->imgFit)) {
				$image = Image::make($path .'/'. $filename);
				// backup status
				$image->backup();
				foreach ($this->imgFit as $imgFit) {
					$size = explode('x',$imgFit);
					$image->fit($size[0],$size[1])->save($path .'/'. $imgFit.'px_'. $filename,100)->reset();
				}
			}
		}
		return $filename;
	}
	  
	public function export() {

		// Log it first
		Activity::log(__FUNCTION__);
			
		// Get type file to export
		$type = Input::get('rel');
		// Get data to export
		$winner = $this->winners->select('id', 'filename', 'caption', 'created_at')->get();
		// Export file to type
		Excel::create('winner', function($excel) use($winner) {
		    $excel->sheet('Sheet 1', function($sheet) use($winner) {
		        $sheet->fromArray($winner);
		    });
		})->export($type);

	}

}
