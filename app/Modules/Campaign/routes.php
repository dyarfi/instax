<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// ******************* Admin Routes ********************* { //
/*
 * 
 * Administrator panel routes
 *
 */
Route::prefix(config('setting.admin_url'))->group( function()
{
    
    // Campaigns Controller routes
    // Campaigns DataTables routes
    Route::get('campaigns/datatable', 'App\Modules\Campaign\Controller\Campaigns@datatable')->name('admin.campaigns.datatable');   
    Route::get('campaigns', 'App\Modules\Campaign\Controller\Campaigns@index')->name('admin.campaigns.index');
    Route::get('campaigns/export', 'App\Modules\Campaign\Controller\Campaigns@export')->name('admin.campaigns.export');   
    Route::get('campaigns/create', 'App\Modules\Campaign\Controller\Campaigns@create')->name('admin.campaigns.create');
    Route::post('campaigns/create', 'App\Modules\Campaign\Controller\Campaigns@store')->name('admin.campaigns.store');
    Route::post('campaigns/change', 'App\Modules\Campaign\Controller\Campaigns@change')->name('admin.campaigns.change');
    // Put the method with the parameter below the static method     
    Route::get('campaigns/{id}/show', 'App\Modules\Campaign\Controller\Campaigns@show')->name('admin.campaigns.show');
    Route::get('campaigns/{id}', 'App\Modules\Campaign\Controller\Campaigns@edit')->name('admin.campaigns.edit');
    Route::post('campaigns/{id}', 'App\Modules\Campaign\Controller\Campaigns@update')->name('admin.campaigns.update');
    Route::get('campaigns/{id}/trash', 'App\Modules\Campaign\Controller\Campaigns@trash')->name('admin.campaigns.trash');
    Route::get('campaigns/{id}/restored', 'App\Modules\Campaign\Controller\Campaigns@restored')->name('admin.campaigns.restored');
    Route::get('campaigns/{id}/delete', 'App\Modules\Campaign\Controller\Campaigns@delete')->name('admin.campaigns.delete');
    // Videos DataTables routes
    Route::get('video/datatable', 'App\Modules\Campaign\Controller\Videos@datatable')->name('admin.videos.datatable');   
    Route::get('video', 'App\Modules\Campaign\Controller\Videos@index')->name('admin.videos.index');
    Route::get('video/export', 'App\Modules\Campaign\Controller\Videos@export')->name('admin.videos.export');   
    Route::get('video/create', 'App\Modules\Campaign\Controller\Videos@create')->name('admin.videos.create');
    Route::post('video/create', 'App\Modules\Campaign\Controller\Videos@store')->name('admin.videos.store');
    Route::post('video/change', 'App\Modules\Campaign\Controller\Videos@change')->name('admin.videos.change');
    // Put the method with the parameter below the static method     
    Route::get('video/{id}/show', 'App\Modules\Campaign\Controller\Videos@show')->name('admin.videos.show');
    Route::get('video/{id}', 'App\Modules\Campaign\Controller\Videos@edit')->name('admin.videos.edit');
    Route::post('video/{id}', 'App\Modules\Campaign\Controller\Videos@update')->name('admin.videos.update');
    Route::get('video/{id}/trash', 'App\Modules\Campaign\Controller\Videos@trash')->name('admin.videos.trash');
    Route::get('video/{id}/restored', 'App\Modules\Campaign\Controller\Videos@restored')->name('admin.videos.restored');
    Route::get('video/{id}/delete', 'App\Modules\Campaign\Controller\Videos@delete')->name('admin.videos.delete');

    // Campaigns Event Controller routes
    // Events DataTables routes
    // Route::get('event/datatable', 'App\Modules\Campaign\Controller\Events@datatable')->name('admin.events.datatable');       
    // Route::get('event', 'App\Modules\Campaign\Controller\Events@index')->name('admin.events.index');
    // Route::get('event/export', 'App\Modules\Campaign\Controller\Events@export')->name('admin.events.export');   
    // Route::get('event/create', 'App\Modules\Campaign\Controller\Events@create')->name('admin.events.create');
    // Route::post('event/create', 'App\Modules\Campaign\Controller\Events@store')->name('admin.events.store');
    // Route::post('event/change', 'App\Modules\Campaign\Controller\Events@change')->name('admin.events.change');

    // Put the method with the parameter below the static method         
    // Route::get('event/{id}/show', 'App\Modules\Campaign\Controller\Events@show')->name('admin.events.show');
    // Route::get('event/{id}', 'App\Modules\Campaign\Controller\Events@edit')->name('admin.events.edit');
    // Route::post('event/{id}', 'App\Modules\Campaign\Controller\Events@update')->name('admin.events.update');
    // Route::get('event/{id}/trash', 'App\Modules\Campaign\Controller\Events@trash')->name('admin.events.trash');
    // Route::get('event/{id}/restored', 'App\Modules\Campaign\Controller\Events@restored')->name('admin.events.restored');
    // Route::get('event/{id}/delete', 'App\Modules\Campaign\Controller\Events@delete')->name('admin.events.delete');

    // Galleries DataTables routes
    Route::get('galleries/datatable', 'App\Modules\Campaign\Controller\Galleries@datatable')->name('admin.galleries.datatable');       
    Route::get('galleries', 'App\Modules\Campaign\Controller\Galleries@index')->name('admin.galleries.index');
    Route::get('galleries/export', 'App\Modules\Campaign\Controller\Galleries@export')->name('admin.galleries.export');   
    Route::get('galleries/create', 'App\Modules\Campaign\Controller\Galleries@create')->name('admin.galleries.create');
    Route::post('galleries/create', 'App\Modules\Campaign\Controller\Galleries@store')->name('admin.galleries.store');
    Route::post('galleries/change', 'App\Modules\Campaign\Controller\Galleries@change')->name('admin.galleries.change');
    // Put the method with the parameter below the static method         
    Route::get('galleries/{id}/show', 'App\Modules\Campaign\Controller\Galleries@show')->name('admin.galleries.show');
    Route::get('galleries/{id}', 'App\Modules\Campaign\Controller\Galleries@edit')->name('admin.galleries.edit');
    Route::post('galleries/{id}', 'App\Modules\Campaign\Controller\Galleries@update')->name('admin.galleries.update');
    Route::get('galleries/{id}/trash', 'App\Modules\Campaign\Controller\Galleries@trash')->name('admin.galleries.trash');
    Route::get('galleries/{id}/restored', 'App\Modules\Campaign\Controller\Galleries@restored')->name('admin.galleries.restored');
    Route::get('galleries/{id}/delete', 'App\Modules\Campaign\Controller\Galleries@delete')->name('admin.galleries.delete');
 
    // Galleries DataTables routes
    Route::get('winners/datatable', 'App\Modules\Campaign\Controller\Winners@datatable')->name('admin.winners.datatable');       
    Route::get('winners', 'App\Modules\Campaign\Controller\Winners@index')->name('admin.winners.index');
    Route::get('winners/export', 'App\Modules\Campaign\Controller\Winners@export')->name('admin.winners.export');   
    Route::get('winners/create', 'App\Modules\Campaign\Controller\Winners@create')->name('admin.winners.create');
    Route::post('winners/create', 'App\Modules\Campaign\Controller\Winners@store')->name('admin.winners.store');
    Route::post('winners/change', 'App\Modules\Campaign\Controller\Winners@change')->name('admin.winners.change');
    // Put the method with the parameter below the static method         
    Route::get('winners/{id}/show', 'App\Modules\Campaign\Controller\Winners@show')->name('admin.winners.show');
    Route::get('winners/{id}', 'App\Modules\Campaign\Controller\Winners@edit')->name('admin.winners.edit');
    Route::post('winners/{id}', 'App\Modules\Campaign\Controller\Winners@update')->name('admin.winners.update');
    Route::get('winners/{id}/trash', 'App\Modules\Campaign\Controller\Winners@trash')->name('admin.winners.trash');
    Route::get('winners/{id}/restored', 'App\Modules\Campaign\Controller\Winners@restored')->name('admin.winners.restored');
    Route::get('winners/{id}/delete', 'App\Modules\Campaign\Controller\Winners@delete')->name('admin.winners.delete');
 
    // Campaigns Ambassadors Controller routes
    // Ambassador DataTables routes
    Route::get('ambassador/datatable', 'App\Modules\Campaign\Controller\Ambassadors@datatable')->name('admin.ambassadors.datatable');          
    Route::get('ambassador', 'App\Modules\Campaign\Controller\Ambassadors@index')->name('admin.ambassadors.index');
    Route::get('ambassador/export', 'App\Modules\Campaign\Controller\Ambassadors@export')->name('admin.ambassadors.export');   
    Route::get('ambassador/create', 'App\Modules\Campaign\Controller\Ambassadors@create')->name('admin.ambassadors.create');
    Route::post('ambassador/create', 'App\Modules\Campaign\Controller\Ambassadors@store')->name('admin.ambassadors.store');
    Route::post('ambassador/change', 'App\Modules\Campaign\Controller\Ambassadors@change')->name('admin.ambassadors.change');
    // Put the method with the parameter below the static method         
    Route::get('ambassador/{id}/show', 'App\Modules\Campaign\Controller\Ambassadors@show')->name('admin.ambassadors.show');
    Route::get('ambassador/{id}', 'App\Modules\Campaign\Controller\Ambassadors@edit')->name('admin.ambassadors.edit');
    Route::post('ambassador/{id}', 'App\Modules\Campaign\Controller\Ambassadors@update')->name('admin.ambassadors.update');
    Route::get('ambassador/{id}/trash', 'App\Modules\Campaign\Controller\Ambassadors@trash')->name('admin.ambassadors.trash');
    Route::get('ambassador/{id}/restored', 'App\Modules\Campaign\Controller\Ambassadors@restored')->name('admin.ambassadors.restored');
    Route::get('ambassador/{id}/delete', 'App\Modules\Campaign\Controller\Ambassadors@delete')->name('admin.ambassadors.delete');
    
});