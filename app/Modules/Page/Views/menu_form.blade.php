@extends('Admin::layouts.template')

{{-- Page content --}}
@section('body')

<div class="page-header">
	<h1>{{ $mode == 'create' ? 'Create Menu' : 'Update Menu' }} <small>{{ $mode === 'update' ? $row->name : null }}</small></h1>
</div>
<!--form method="post" action="" autocomplete="off"-->
{!! Form::open([
    'route' => ($mode == 'create') ? 'admin.menus.create' : ['admin.menus.update', $row->id],
	'files' => true
]) !!}

	<div class="form-group{{ $errors->first('name', ' has-error') }}">
		{!! Form::label('name', 'Name'); !!}
		{!! Form::text('name',Input::old('name', $row->name),[
			'placeholder'=>'Enter the Menu Name.',
			'name'=>'name',
			'id'=>'name',
			'class'=>'form-control']); !!}
		<span class="help-block">{{{ $errors->first('name', ':message') }}}</span>
	</div>

	<div class="form-group{{ $errors->first('description', ' has-error') }}">
		{!! Form::label('description', 'Description'); !!}
		{!! Form::textarea('description',Input::old('description', $row->description),[
			'placeholder'=>'Enter the Menu Description.',
			'name'=>'description',
			'id'=>'description',
			'class'=>'form-control ckeditor']); !!}
		<span class="help-block">{{{ $errors->first('description', ':message') }}}</span>
	</div>

    <div class="form-group{{ $errors->first('image', ' has-error') }}">
        @if ($row->image)
            <img src="{{ asset('storage/uploads/'.$row->image) }}" alt="{{ $row->image }}" class="image-alt img-thumbnail" style="width:300px"/>
        @endif
        <div class="row">
            <div class="col-xs-6">
                {!! Form::label('image', ($row->image) ? 'Replace Image: *1622x800' : 'Image: *1622x800', ['class' => '']) !!}
                <label class="ace-file-input">
                    {!! Form::file('image',['class'=>'form-controls','id'=>'id-input-file-2']) !!}
                    <span class="ace-file-container" data-title="Choose">
                        <span class="ace-file-name" data-title="No File ...">
                            <i class=" ace-icon fa fa-upload"></i>
                        </span>
                    </span>
                </label>
                <span class="help-block">{{{ $errors->first('image', ':message') }}}</span>
            </div>
        </div>
    </div>
    
    <div class="form-group{{ $errors->first('image_mobile', ' has-error') }}">
        @if ($row->image_mobile)
            <img src="{{ asset('storage/uploads/'.$row->image_mobile) }}" alt="{{ $row->image_mobile }}" class="image-alt img-thumbnail" style="width:300px"/>
        @endif
        <div class="row">
            <div class="col-xs-6">
                {!! Form::label('image_mobile', ($row->image_mobile) ? 'Replace Image Mobile: *735x1004' : 'Image Mobile: *735x1004', ['class' => '']) !!}
                <label class="ace-file-input">
                    {!! Form::file('image_mobile',['class'=>'form-controls','id'=>'id-input-file-3']) !!}
                    <span class="ace-file-container" data-title="Choose">
                        <span class="ace-file-name" data-title="No Image Mobile File ...">
                            <i class=" ace-icon fa fa-upload"></i>
                        </span>
                    </span>
                </label>
            </div>
        </div>
        <span class="help-block">{{{ $errors->first('image_mobile', ':message') }}}</span>
    </div>

	<div class="form-group{{ $errors->first('index', ' has-error') }}">
		{!! Form::label('index', 'Index'); !!}
		{!! Form::text('index',($row->index ? Input::old('index', $row->index) : $model->max('index') + 1),[
			'placeholder'=>'Enter the Menu Index.',
			'name'=>'index',
			'id'=>'index',
			'class'=>'form-control']); !!}
		<span class="help-block">{{{ $errors->first('index', ':message') }}}</span>
	</div>

	<div class="form-group{{ $errors->first('status', ' has-error') }}">
		<label for="status">Status</label>
		<select id="status" name="status" class="form-control input-sm">
			<option value="">&nbsp;</option>
			@foreach (config('setting.status') as $config => $val)
				<option value="{{ Input::old('status', $config) }}" {{ $config == $row->status ? 'selected' : '' }}>{{$val}}</option>
			@endforeach
		</select>
	</div>

	<button type="submit" class="btn btn-default">Submit</button>
{!! Form::close() !!}

@stop
