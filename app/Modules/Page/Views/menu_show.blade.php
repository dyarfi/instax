@extends('Admin::layouts.template')

@section('body')
<div class="container-fluid">
    @if($row->name)
    <h4 class="red">Name</h4>
    <ul class="list-unstyled"><li>{{ $row->name }}</li></ul>
    @endif
    @if($row->slug)
    <h4 class="red">Slug</h4>
    <div class="row-fluid">
        {{ $row->slug }}
    </div>
    @endif
    @if($row->description)
    <h4 class="red">Description</h4>
    <div class="row-fluid">
        {!! $row->description !!}
    </div>
    @endif
    @if ($row->image != '')
    <div class="clearfix">
        <h4 class="red">Image</h4>
        <div class="row-fluid">
            <a href="{{ asset('storage/uploads/'.$row->image) }}" target="_blank" title="{{ $row->image }}"/>
                <img src="{{ asset('storage/uploads/'.$row->image) }}" class="text-left img-responsive" style="width:740px"/>
            </a>
        </div>
        <div class="space-8"></div>
    </div>
    @endif
    @if ($row->image_mobile != '')
    <div class="clearfix">
        <h4 class="red">Image Mobile</h4>
        <div class="row-fluid">
            <a href="{{ asset('storage/uploads/'.$row->image_mobile) }}" target="_blank" title="{{ $row->image_mobile }}"/>
                <img src="{{ asset('storage/uploads/'.$row->image_mobile) }}" class="text-left img-responsive" style="width:400px"/>
            </a>
        </div>
        <div class="space-8"></div>
    </div>
    @endif
    @if($row->status)
    <h4 class="red">Status</h4>
    <div class="row-fluid">
        {{ config('setting.status')[$row->status] }}
    </div>
    @endif
    @if($row->created_at)
    <h4 class="red">Created At</h4>
    <div class="row-fluid">
        {{ $row->created_at }}
    </div>
    @endif
    <hr/>
    <div class="row">
        <div class="col-md-5 col-xs-6">
            <a href="{{ route('admin.menus.index') }}" class="btn btn-info btn-xs">Back to all menus</a>
            <a href="{{ route('admin.menus.edit', $row->id) }}" class="btn btn-primary btn-xs">Edit Menu</a>
            <a href="{{ route('admin.menus.create') }}" class="btn btn-warning btn-xs">Create Menu</a>
        </div>
        <div class="col-md-5 col-xs-6 text-right">
            {!! Form::open([
                'method' => 'DELETE',
                'route' => ['admin.menus.trash', $row->id]
            ]) !!}
                {!! Form::submit('Delete this menu?', ['class' => 'btn btn-danger btn-xs']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>

@stop
