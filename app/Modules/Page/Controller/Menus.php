<?php namespace App\Modules\Page\Controller;

// Load Laravel classes
use Route, Request, Sentinel, Session, Redirect, Input, Validator, View, Image, File, Storage;
// Load main base controller
use App\Modules\BaseAdmin;
// Load main models
use App\Modules\Page\Model\Menu;
// User Activity Logs
use Activity;

class Menus extends BaseAdmin {

	/**
	 * Set menus data.
	 *
	 */
	protected $menus;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

		// Parent constructor
		parent::__construct();

		// Load Http/Middleware/Admin controller
		$this->middleware('auth.admin');

		// Load menus and get repository data from database
		$this->menus = new Menu;

		// Crop to fit image size
		$this->imgFit 		= ['735x1000'];

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {

		// Set return data
	   	$menus = Input::get('path') === 'trashed' ? $this->menus->onlyTrashed()->get() : $this->menus->orderBy('index', 'asc')->get();

	   	// Get deleted count
		$deleted = $this->menus->onlyTrashed()->get()->count();

	   	// Set data to return
	   	$data = ['rows' => $menus,'deleted' => $deleted,'junked' => Input::get('path')];

	   	// Load needed scripts
	   	$scripts = [
	   				'dataTables'=> asset('themes/ace-admin/js/jquery.dataTables.min.js'),
	   				'dataTableBootstrap'=> asset('themes/ace-admin/js/jquery.dataTables.bootstrap.min.js'),
	   				'dataTableTools'=> asset('themes/ace-admin/js/dataTables.tableTools.min.js'),
	   				'dataTablesColVis'=> asset('themes/ace-admin/js/dataTables.colVis.min.js'),
					'library' => asset('themes/ace-admin/js/library.js')
	   				];

		// Return data and view
	   	return $this->view('Page::menu_index')->data($data)->scripts($scripts)->title('Menu List');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// Get data from database
        $menu = $this->menus->find($id);

		// Set data to return
	   	$data = ['row'=>$menu];

	   	// Return data and view
	   	return $this->view('Page::menu_show')->data($data)->title('View Menu');

	}

	/**
	 * Show the form for creating new menu.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new menu.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating menu.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating menu.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified menu.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function trash($id)
	{
		if ($menu = $this->menus->find($id))
		{
			// Add deleted_at and not completely delete
			$menu->delete();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.menus.index'))->with('success', 'Menu Trashed!');
		}

		return Redirect::to(route('admin.menus.index'))->with('error', 'Menu Not Found!');
	}

	/**
	 * Restored the specified menu.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function restored($id)
	{
		if ($menu = $this->menus->onlyTrashed()->find($id))
		{

			// Restored back from deleted_at database
			$menu->restore();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.menus.index'))->with('success', 'Menu Restored!');
		}

		return Redirect::to(route('admin.menus.index'))->with('error', 'Menu Not Found!');;
	}

	/**
	 * Delete the specified menu.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		if ($menu = $this->menus->onlyTrashed()->find($id))
		{

			// Delete if there is an image attached
			if(File::exists('uploads/'.$menu->image)) {
				// Delete the single file
				File::delete('uploads/'.$menu->image);

			}

			// Completely delete from database
			$menu->forceDelete();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.menus.index','path=trashed'))->with('success', 'Menu Permanently Deleted!');
		}

		return Redirect::to(route('admin.menus.index','path=trashed'))->with('error', 'Menu Not Found!');
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{

		if ($id)
		{
			if ( ! $row = $this->menus->find($id))
			{
				return Redirect::to(route('admin.menus.index'))->withErrors('Not found data!');;
			}
		}
		else
		{
			$row = $this->menus;
		}

		$model	 	= $this->menus;

		// Load needed javascripts
		$scripts = [
			'bootstrap-tag'=>asset("themes/ace-admin/js/bootstrap-tag.min.js"),
			'bootstrap-datepicker'=>asset('themes/ace-admin/js/bootstrap-datepicker.min.js'),
			'ckeditor'=>asset('themes/ace-admin/plugins/ckeditor/ckeditor.js'),
			'library'=>asset('themes/ace-admin/js/library.js')
		];

		// Load needed stylesheets
		$styles = [
			'stylesheet-datepicker'=> asset('themes/ace-admin/css/datepicker.min.css')
		];

		return $this->view('Page::menu_form')->data(compact('mode', 'row', 'model'))->scripts($scripts)->styles($styles)->title('Menu '.$mode);
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		// Filter all input
		$input = array_filter(Input::all());

		// Set menu slug
		$input['slug'] = isset($input['name']) ? str_slug($input['name'],'_') : '';

		$rules = [
			'name' 	   	   => 'required',
			//'slug' 		   => 'required',
			'description'  => 'required',
			//'status'	   => 'boolean',
            'image'  	   => (($mode == 'create') ? 'required|' : '') . 'image|dimensions:max_width=1980,max_height=1282',
			'image_mobile' 	=> ($mode == 'create' ? 'required|' : '').'image|dimensions:max_width=735,max_height=1004|max:1024',			
			'index'	   	   => 'numeric|digits_between:1,999',
		];

		if ($id)
		{
			$menu = $this->menus->find($id);

			$messages = $this->validateMenu($input, $rules);

			// If user upload an image
			if (isset($input['image']) && Input::hasFile('image')) {
			
				// Set filename
				$filename = $this->imageUploadToDb($input['image'], 'uploads', 'menu_');
			
			} else {

				// Set default image
				$filename = $menu->image;

			}

			// If user upload an image for mobile
			if(isset($input['image_mobile']) && Input::hasFile('image_mobile')) {

				// Set filename
				$filename_mobile = $this->imageUploadToDb($input['image_mobile'], 'uploads', 'menu_mobile_');
			
			} else {
				
				// Set default image mobile
				$filename_mobile = $menu->image_mobile;

			}

			if ($messages->isEmpty())
			{
				// Get all request
				$result = $input;

				// Slip user id
				$result = array_set($result, 'user_id', $this->user->id);

                // Slip image file
                $result = isset($filename) ? array_set($input, 'image', $filename) : $result;

                // Slip image mobile file
                $result = isset($filename_mobile) ? array_set($input, 'image_mobile', $filename_mobile) : $result;

                // Update menu
				$menu->update($result);
			}

		}
		else
		{
			$messages = $this->validateMenu($input, $rules);
            // If user upload a file
            if (isset($input['image']) 
            && Input::hasFile('image')
                && isset($input['image_mobile'])
                    && Input::hasFile('image_mobile')) {

                // Set filename
                $filename = $this->imageUploadToDb($input['image'], 'uploads', 'menu_');
                // Set filename
                $filename_mobile = $this->imageUploadToDb($input['image_mobile'], 'uploads', 'menu_mobile_');

            }

			if ($messages->isEmpty())
			{
				// Get all request
				$result = $input;

				// Slip user id
				$result = array_set($result, 'user_id', $this->user->id);

				// Slip image file
				$result = isset($input['image']) ? array_set($result, 'image', @$filename) : array_set($result, 'image', '');

				// Slip image mobile file
				$result = isset($input['image_mobile']) ? array_set($result, 'image_mobile', @$filename_mobile) : array_set($result, 'image_mobile', '');

				// Create menu
				$menu = $this->menus->create($result);

			}
		}

		// Log it first
		Activity::log(__FUNCTION__);

		if ($messages->isEmpty())
		{
			return Redirect::to(route('admin.menus.show', $menu->id))->with('success', 'Menu Updated!');
		}

		return Redirect::back()->withInput()->withErrors($messages);
	}

	/**
	 * Change the data status.
	 *
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function change() {

		// Log it first
		Activity::log(__FUNCTION__);

		if (Input::get('check') !='') {

		    $rows	= Input::get('check');

		    foreach ($rows as $row) {
				// Set id for load and change status
				$this->menus->withTrashed()->find($row)->update(['status' => Input::get('select_action')]);
		    }

		    // Set message
		    return Redirect::to(route('admin.menus.index'))->with('success', 'Menu Status Changed!');

		} else {

		    // Set message
		    return Redirect::to(route('admin.menus.index'))->with('error','Data not Available!');
		}
	}

	/**
	 * Process a file upload save the filename to DB.
	 *
	 * @param  array  $file
	 * @param  string $path
	 * @param  string $type
	 * @return $filename
	 */
	protected function imageUploadToDb($file='', $path='', $type='')
	{
		// Set filename upload
		$filename = '';
		// Check if input and upload already assigned
		if (!empty($file) && !$file->getError()) {
			// Getting image extension
			$extension = $file->getClientOriginalExtension();
			// Renaming image
			$filename = $type . rand(11111,99999) . '.' . $extension;
			// Set intervention image for image manipulation
			Storage::disk('local_uploads')->put($filename,
				file_get_contents($file)
      );
			// If image has a resize crop data in constructor
			if (!empty($this->imgFit)) {
				$image = Image::make(public_path('storage/'.$path .'/'. $filename));
				// backup status
				$image->backup();
				foreach ($this->imgFit as $imgFit) {
					$size = explode('x',$imgFit);
					$image->fit($size[0],$size[1])->save(public_path('storage/'.$path .'/'. $imgFit.'px_'. $filename),100)->reset();
				}
			}
		}
		return $filename;
	}
	  
	/**
	 * Validates a menu.
	 *
	 * @param  array  $data
	 * @param  mixed  $id
	 * @return \Illuminate\Support\MessageBag
	 */
	protected function validateMenu($data, $rules)
	{
		$validator = Validator::make($data, $rules);

		$validator->passes();

		return $validator->errors();
	}


}
