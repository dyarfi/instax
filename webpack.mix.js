const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');


   mix.disableNotifications();

if (mix.inProduction()) {
    // Copy assets files
    mix.copy('node_modules/jquery/dist/jquery.min.js', 'public/js/jquery.min.js');
    mix.copy('node_modules/popper.js/dist/umd/popper.min.js', 'public/js/popper.min.js');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.min.js', 'public/js/bootstrap.min.js');
    mix.copy('node_modules/owl.carousel/dist/owl.carousel.min.js', 'public/js/owl.carousel.min.js');
    // Crop image assets
    mix.copy('node_modules/cropit/dist/jquery.cropit.js', 'public/js/jquery.cropit.js');
    // Copy modules assets directory
    mix.copyDirectory('node_modules/cropit/demo','public/js/cropit');
    mix.copyDirectory('node_modules/@fancyapps/fancybox/dist','public/js/fancybox');
    mix.copyDirectory('node_modules/bootstrap/scss','resources/assets/scss/vendors/bootstrap/scss');
    mix.copyDirectory('node_modules/owl.carousel/src/scss','resources/assets/scss/vendors/owl.carousel/scss');
    mix.copyDirectory('node_modules/owl.carousel/src/img','public/images');
    // Resources assets files
    mix.copyDirectory('resources/assets/scss/vendors/fontawesome/webfonts', 'public/webfonts');
    
    // SASS preprocess
     mix.sass(
        // SASS components
        'resources/assets/scss/main.scss',
        // CSS bundle SCSS into css app.bundle.css components bundled
        'public/css/app.bundle.css'
    ).options({
            processCssUrls: false,            
            postCss: [
                require('cssnano')({
                discardComments: {
                    removeAll: true,
                },
                }),
                //require('postcss-unprefix'),
                require('autoprefixer')({
                browsers: [
                    'last 15 versions',
                    'ie >= 10',
                ]}),
            ]            
        }).version();

    // Combine all
    mix.scripts([
        'public/js/jquery.js',
        'public/js/jquery-ias.js', 
        'public/js/popper.min.js',
        'public/js/bootstrap.min.js',
        'public/js/owl.carousel.min.js',
        'public/js/fancybox/jquery.fancybox.min.js',
        'public/js/jquery.cropit.js',
        'public/js/app.js'
        // Bundle into app.bundle.js
    ],  'public/js/app.bundle.js').version();

    // JS Apps / React
    mix.js('resources/assets/js/app.js', 'public/js');

    // Only on production 
    // Set mix version
    mix.version();

    // CSS
    // mix.combine([        
    //     'public/css/jquery.guillotine.css',
    //     'public/css/app.bundle.css'
    // ], 'public/css/app.bundle.css');
    mix.combine('public/css/app.bundle.css', 'public/css/app.bundle.css');
    
    // mix.combine(['public/css/app.bundle.css','public/js/jquery.mb.YTPlayer/css/jquery.mb.YTPlayer.min.css'], 'public/css/app.bundle.css');
    // JS
    // mix.combine('public/js/app.bundle.js', 'public/js/app.bundle.min.js');

} else {

    // SASS preprocess
    mix.sass(
        // SASS components
        'resources/assets/scss/main.scss',
        // CSS bundle SCSS into css app.bundle.css components bundled
        'public/css/app.bundle.css'
    ).options({processCssUrls: false}).version();

    // Combine all
    mix.scripts([
        'public/js/jquery.js',
        'public/js/jquery-ias.js', 
        'public/js/popper.min.js',      
        'public/js/holder.min.js',
        'public/js/bootstrap.min.js',
        'public/js/owl.carousel.min.js',
        'public/js/jquery.cropit.js',
        'public/js/fancybox/jquery.fancybox.min.js',
        'public/js/app.js'
        // Bundle into app.bundle.js
    ],  'public/js/app.bundle.js').version();
    
    // JS Apps / React
    mix.js('resources/assets/js/app.js', 'public/js');

    // Set mix version --- test mode
    // mix.version();

    // CSS combined
    // mix.combine([
    //     'public/css/jquery.guillotine.css',
    //     'public/css/app.bundle.css']
    //     , 'public/css/app.bundle.css');

    mix.combine('public/css/app.bundle.css', 'public/css/app.bundle.css');

}
