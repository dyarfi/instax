$(document).ready(function() {
  var global = Global.init();
  var fl = InputFile.init();
  var fm = Form.init();
  var cs = CircularSlider.init();
  var carousel = Carousel.init();
  var rs = RangeSlider.init();
  var ps = ParallaxSlider.init();
  var vp = VideoPopup.init();
  var mm = MobileMenu.init();
  var mh = MainHeader.init();
  var rgl = RegistrationLife.init();
  var rgi = RegistrationiSport.init();
  var ame = AktivasiMelaluiEmail.init();
  var rgg = RegistrationGift.init();
  var popup = Popup.init();
  var ed = Editable.init();
  var py = Payment.init();
  var cu = CustomUpload.init();
  var ws = WindowScroll.init();
  var bt = BackToTop.init();
  var cd = CustomDropdown.init();
  var dp = DatePicker.init();
  var accordion = Accordion.init();
  var riskmeter = RiskMeter.init();
  var sportsevents = SportsEvents.init();
  var inspirasikado = InspirasiKado.init();
  var fcsk = FlowCekStatusKado.init();
  var sc = SportCategories.init();
  var ne = NearestEvent.init();
  var pb = PolicyBenefit.init();
  var cb = Clipboard.init();
  var cpeditprofile = CPEditProfile.init();
  var seoh = SEOHider.init();
});
/*!
 * Datepicker for Bootstrap v1.7.1 (https://github.com/uxsolutions/bootstrap-datepicker)
 *
 * Licensed under the Apache License v2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a("object"==typeof exports?require("jquery"):jQuery)}(function(a,b){function c(){return new Date(Date.UTC.apply(Date,arguments))}function d(){var a=new Date;return c(a.getFullYear(),a.getMonth(),a.getDate())}function e(a,b){return a.getUTCFullYear()===b.getUTCFullYear()&&a.getUTCMonth()===b.getUTCMonth()&&a.getUTCDate()===b.getUTCDate()}function f(c,d){return function(){return d!==b&&a.fn.datepicker.deprecated(d),this[c].apply(this,arguments)}}function g(a){return a&&!isNaN(a.getTime())}function h(b,c){function d(a,b){return b.toLowerCase()}var e,f=a(b).data(),g={},h=new RegExp("^"+c.toLowerCase()+"([A-Z])");c=new RegExp("^"+c.toLowerCase());for(var i in f)c.test(i)&&(e=i.replace(h,d),g[e]=f[i]);return g}function i(b){var c={};if(q[b]||(b=b.split("-")[0],q[b])){var d=q[b];return a.each(p,function(a,b){b in d&&(c[b]=d[b])}),c}}var j=function(){var b={get:function(a){return this.slice(a)[0]},contains:function(a){for(var b=a&&a.valueOf(),c=0,d=this.length;c<d;c++)if(0<=this[c].valueOf()-b&&this[c].valueOf()-b<864e5)return c;return-1},remove:function(a){this.splice(a,1)},replace:function(b){b&&(a.isArray(b)||(b=[b]),this.clear(),this.push.apply(this,b))},clear:function(){this.length=0},copy:function(){var a=new j;return a.replace(this),a}};return function(){var c=[];return c.push.apply(c,arguments),a.extend(c,b),c}}(),k=function(b,c){a.data(b,"datepicker",this),this._process_options(c),this.dates=new j,this.viewDate=this.o.defaultViewDate,this.focusDate=null,this.element=a(b),this.isInput=this.element.is("input"),this.inputField=this.isInput?this.element:this.element.find("input"),this.component=!!this.element.hasClass("date")&&this.element.find(".add-on, .input-group-addon, .btn"),this.component&&0===this.component.length&&(this.component=!1),this.isInline=!this.component&&this.element.is("div"),this.picker=a(r.template),this._check_template(this.o.templates.leftArrow)&&this.picker.find(".prev").html(this.o.templates.leftArrow),this._check_template(this.o.templates.rightArrow)&&this.picker.find(".next").html(this.o.templates.rightArrow),this._buildEvents(),this._attachEvents(),this.isInline?this.picker.addClass("datepicker-inline").appendTo(this.element):this.picker.addClass("datepicker-dropdown dropdown-menu"),this.o.rtl&&this.picker.addClass("datepicker-rtl"),this.o.calendarWeeks&&this.picker.find(".datepicker-days .datepicker-switch, thead .datepicker-title, tfoot .today, tfoot .clear").attr("colspan",function(a,b){return Number(b)+1}),this._process_options({startDate:this._o.startDate,endDate:this._o.endDate,daysOfWeekDisabled:this.o.daysOfWeekDisabled,daysOfWeekHighlighted:this.o.daysOfWeekHighlighted,datesDisabled:this.o.datesDisabled}),this._allow_update=!1,this.setViewMode(this.o.startView),this._allow_update=!0,this.fillDow(),this.fillMonths(),this.update(),this.isInline&&this.show()};k.prototype={constructor:k,_resolveViewName:function(b){return a.each(r.viewModes,function(c,d){if(b===c||a.inArray(b,d.names)!==-1)return b=c,!1}),b},_resolveDaysOfWeek:function(b){return a.isArray(b)||(b=b.split(/[,\s]*/)),a.map(b,Number)},_check_template:function(c){try{if(c===b||""===c)return!1;if((c.match(/[<>]/g)||[]).length<=0)return!0;var d=a(c);return d.length>0}catch(a){return!1}},_process_options:function(b){this._o=a.extend({},this._o,b);var e=this.o=a.extend({},this._o),f=e.language;q[f]||(f=f.split("-")[0],q[f]||(f=o.language)),e.language=f,e.startView=this._resolveViewName(e.startView),e.minViewMode=this._resolveViewName(e.minViewMode),e.maxViewMode=this._resolveViewName(e.maxViewMode),e.startView=Math.max(this.o.minViewMode,Math.min(this.o.maxViewMode,e.startView)),e.multidate!==!0&&(e.multidate=Number(e.multidate)||!1,e.multidate!==!1&&(e.multidate=Math.max(0,e.multidate))),e.multidateSeparator=String(e.multidateSeparator),e.weekStart%=7,e.weekEnd=(e.weekStart+6)%7;var g=r.parseFormat(e.format);e.startDate!==-(1/0)&&(e.startDate?e.startDate instanceof Date?e.startDate=this._local_to_utc(this._zero_time(e.startDate)):e.startDate=r.parseDate(e.startDate,g,e.language,e.assumeNearbyYear):e.startDate=-(1/0)),e.endDate!==1/0&&(e.endDate?e.endDate instanceof Date?e.endDate=this._local_to_utc(this._zero_time(e.endDate)):e.endDate=r.parseDate(e.endDate,g,e.language,e.assumeNearbyYear):e.endDate=1/0),e.daysOfWeekDisabled=this._resolveDaysOfWeek(e.daysOfWeekDisabled||[]),e.daysOfWeekHighlighted=this._resolveDaysOfWeek(e.daysOfWeekHighlighted||[]),e.datesDisabled=e.datesDisabled||[],a.isArray(e.datesDisabled)||(e.datesDisabled=e.datesDisabled.split(",")),e.datesDisabled=a.map(e.datesDisabled,function(a){return r.parseDate(a,g,e.language,e.assumeNearbyYear)});var h=String(e.orientation).toLowerCase().split(/\s+/g),i=e.orientation.toLowerCase();if(h=a.grep(h,function(a){return/^auto|left|right|top|bottom$/.test(a)}),e.orientation={x:"auto",y:"auto"},i&&"auto"!==i)if(1===h.length)switch(h[0]){case"top":case"bottom":e.orientation.y=h[0];break;case"left":case"right":e.orientation.x=h[0]}else i=a.grep(h,function(a){return/^left|right$/.test(a)}),e.orientation.x=i[0]||"auto",i=a.grep(h,function(a){return/^top|bottom$/.test(a)}),e.orientation.y=i[0]||"auto";else;if(e.defaultViewDate instanceof Date||"string"==typeof e.defaultViewDate)e.defaultViewDate=r.parseDate(e.defaultViewDate,g,e.language,e.assumeNearbyYear);else if(e.defaultViewDate){var j=e.defaultViewDate.year||(new Date).getFullYear(),k=e.defaultViewDate.month||0,l=e.defaultViewDate.day||1;e.defaultViewDate=c(j,k,l)}else e.defaultViewDate=d()},_events:[],_secondaryEvents:[],_applyEvents:function(a){for(var c,d,e,f=0;f<a.length;f++)c=a[f][0],2===a[f].length?(d=b,e=a[f][1]):3===a[f].length&&(d=a[f][1],e=a[f][2]),c.on(e,d)},_unapplyEvents:function(a){for(var c,d,e,f=0;f<a.length;f++)c=a[f][0],2===a[f].length?(e=b,d=a[f][1]):3===a[f].length&&(e=a[f][1],d=a[f][2]),c.off(d,e)},_buildEvents:function(){var b={keyup:a.proxy(function(b){a.inArray(b.keyCode,[27,37,39,38,40,32,13,9])===-1&&this.update()},this),keydown:a.proxy(this.keydown,this),paste:a.proxy(this.paste,this)};this.o.showOnFocus===!0&&(b.focus=a.proxy(this.show,this)),this.isInput?this._events=[[this.element,b]]:this.component&&this.inputField.length?this._events=[[this.inputField,b],[this.component,{click:a.proxy(this.show,this)}]]:this._events=[[this.element,{click:a.proxy(this.show,this),keydown:a.proxy(this.keydown,this)}]],this._events.push([this.element,"*",{blur:a.proxy(function(a){this._focused_from=a.target},this)}],[this.element,{blur:a.proxy(function(a){this._focused_from=a.target},this)}]),this.o.immediateUpdates&&this._events.push([this.element,{"changeYear changeMonth":a.proxy(function(a){this.update(a.date)},this)}]),this._secondaryEvents=[[this.picker,{click:a.proxy(this.click,this)}],[this.picker,".prev, .next",{click:a.proxy(this.navArrowsClick,this)}],[this.picker,".day:not(.disabled)",{click:a.proxy(this.dayCellClick,this)}],[a(window),{resize:a.proxy(this.place,this)}],[a(document),{"mousedown touchstart":a.proxy(function(a){this.element.is(a.target)||this.element.find(a.target).length||this.picker.is(a.target)||this.picker.find(a.target).length||this.isInline||this.hide()},this)}]]},_attachEvents:function(){this._detachEvents(),this._applyEvents(this._events)},_detachEvents:function(){this._unapplyEvents(this._events)},_attachSecondaryEvents:function(){this._detachSecondaryEvents(),this._applyEvents(this._secondaryEvents)},_detachSecondaryEvents:function(){this._unapplyEvents(this._secondaryEvents)},_trigger:function(b,c){var d=c||this.dates.get(-1),e=this._utc_to_local(d);this.element.trigger({type:b,date:e,viewMode:this.viewMode,dates:a.map(this.dates,this._utc_to_local),format:a.proxy(function(a,b){0===arguments.length?(a=this.dates.length-1,b=this.o.format):"string"==typeof a&&(b=a,a=this.dates.length-1),b=b||this.o.format;var c=this.dates.get(a);return r.formatDate(c,b,this.o.language)},this)})},show:function(){if(!(this.inputField.prop("disabled")||this.inputField.prop("readonly")&&this.o.enableOnReadonly===!1))return this.isInline||this.picker.appendTo(this.o.container),this.place(),this.picker.show(),this._attachSecondaryEvents(),this._trigger("show"),(window.navigator.msMaxTouchPoints||"ontouchstart"in document)&&this.o.disableTouchKeyboard&&a(this.element).blur(),this},hide:function(){return this.isInline||!this.picker.is(":visible")?this:(this.focusDate=null,this.picker.hide().detach(),this._detachSecondaryEvents(),this.setViewMode(this.o.startView),this.o.forceParse&&this.inputField.val()&&this.setValue(),this._trigger("hide"),this)},destroy:function(){return this.hide(),this._detachEvents(),this._detachSecondaryEvents(),this.picker.remove(),delete this.element.data().datepicker,this.isInput||delete this.element.data().date,this},paste:function(b){var c;if(b.originalEvent.clipboardData&&b.originalEvent.clipboardData.types&&a.inArray("text/plain",b.originalEvent.clipboardData.types)!==-1)c=b.originalEvent.clipboardData.getData("text/plain");else{if(!window.clipboardData)return;c=window.clipboardData.getData("Text")}this.setDate(c),this.update(),b.preventDefault()},_utc_to_local:function(a){if(!a)return a;var b=new Date(a.getTime()+6e4*a.getTimezoneOffset());return b.getTimezoneOffset()!==a.getTimezoneOffset()&&(b=new Date(a.getTime()+6e4*b.getTimezoneOffset())),b},_local_to_utc:function(a){return a&&new Date(a.getTime()-6e4*a.getTimezoneOffset())},_zero_time:function(a){return a&&new Date(a.getFullYear(),a.getMonth(),a.getDate())},_zero_utc_time:function(a){return a&&c(a.getUTCFullYear(),a.getUTCMonth(),a.getUTCDate())},getDates:function(){return a.map(this.dates,this._utc_to_local)},getUTCDates:function(){return a.map(this.dates,function(a){return new Date(a)})},getDate:function(){return this._utc_to_local(this.getUTCDate())},getUTCDate:function(){var a=this.dates.get(-1);return a!==b?new Date(a):null},clearDates:function(){this.inputField.val(""),this.update(),this._trigger("changeDate"),this.o.autoclose&&this.hide()},setDates:function(){var b=a.isArray(arguments[0])?arguments[0]:arguments;return this.update.apply(this,b),this._trigger("changeDate"),this.setValue(),this},setUTCDates:function(){var b=a.isArray(arguments[0])?arguments[0]:arguments;return this.setDates.apply(this,a.map(b,this._utc_to_local)),this},setDate:f("setDates"),setUTCDate:f("setUTCDates"),remove:f("destroy","Method `remove` is deprecated and will be removed in version 2.0. Use `destroy` instead"),setValue:function(){var a=this.getFormattedDate();return this.inputField.val(a),this},getFormattedDate:function(c){c===b&&(c=this.o.format);var d=this.o.language;return a.map(this.dates,function(a){return r.formatDate(a,c,d)}).join(this.o.multidateSeparator)},getStartDate:function(){return this.o.startDate},setStartDate:function(a){return this._process_options({startDate:a}),this.update(),this.updateNavArrows(),this},getEndDate:function(){return this.o.endDate},setEndDate:function(a){return this._process_options({endDate:a}),this.update(),this.updateNavArrows(),this},setDaysOfWeekDisabled:function(a){return this._process_options({daysOfWeekDisabled:a}),this.update(),this},setDaysOfWeekHighlighted:function(a){return this._process_options({daysOfWeekHighlighted:a}),this.update(),this},setDatesDisabled:function(a){return this._process_options({datesDisabled:a}),this.update(),this},place:function(){if(this.isInline)return this;var b=this.picker.outerWidth(),c=this.picker.outerHeight(),d=10,e=a(this.o.container),f=e.width(),g="body"===this.o.container?a(document).scrollTop():e.scrollTop(),h=e.offset(),i=[0];this.element.parents().each(function(){var b=a(this).css("z-index");"auto"!==b&&0!==Number(b)&&i.push(Number(b))});var j=Math.max.apply(Math,i)+this.o.zIndexOffset,k=this.component?this.component.parent().offset():this.element.offset(),l=this.component?this.component.outerHeight(!0):this.element.outerHeight(!1),m=this.component?this.component.outerWidth(!0):this.element.outerWidth(!1),n=k.left-h.left,o=k.top-h.top;"body"!==this.o.container&&(o+=g),this.picker.removeClass("datepicker-orient-top datepicker-orient-bottom datepicker-orient-right datepicker-orient-left"),"auto"!==this.o.orientation.x?(this.picker.addClass("datepicker-orient-"+this.o.orientation.x),"right"===this.o.orientation.x&&(n-=b-m)):k.left<0?(this.picker.addClass("datepicker-orient-left"),n-=k.left-d):n+b>f?(this.picker.addClass("datepicker-orient-right"),n+=m-b):this.o.rtl?this.picker.addClass("datepicker-orient-right"):this.picker.addClass("datepicker-orient-left");var p,q=this.o.orientation.y;if("auto"===q&&(p=-g+o-c,q=p<0?"bottom":"top"),this.picker.addClass("datepicker-orient-"+q),"top"===q?o-=c+parseInt(this.picker.css("padding-top")):o+=l,this.o.rtl){var r=f-(n+m);this.picker.css({top:o,right:r,zIndex:j})}else this.picker.css({top:o,left:n,zIndex:j});return this},_allow_update:!0,update:function(){if(!this._allow_update)return this;var b=this.dates.copy(),c=[],d=!1;return arguments.length?(a.each(arguments,a.proxy(function(a,b){b instanceof Date&&(b=this._local_to_utc(b)),c.push(b)},this)),d=!0):(c=this.isInput?this.element.val():this.element.data("date")||this.inputField.val(),c=c&&this.o.multidate?c.split(this.o.multidateSeparator):[c],delete this.element.data().date),c=a.map(c,a.proxy(function(a){return r.parseDate(a,this.o.format,this.o.language,this.o.assumeNearbyYear)},this)),c=a.grep(c,a.proxy(function(a){return!this.dateWithinRange(a)||!a},this),!0),this.dates.replace(c),this.o.updateViewDate&&(this.dates.length?this.viewDate=new Date(this.dates.get(-1)):this.viewDate<this.o.startDate?this.viewDate=new Date(this.o.startDate):this.viewDate>this.o.endDate?this.viewDate=new Date(this.o.endDate):this.viewDate=this.o.defaultViewDate),d?(this.setValue(),this.element.change()):this.dates.length&&String(b)!==String(this.dates)&&d&&(this._trigger("changeDate"),this.element.change()),!this.dates.length&&b.length&&(this._trigger("clearDate"),this.element.change()),this.fill(),this},fillDow:function(){if(this.o.showWeekDays){var b=this.o.weekStart,c="<tr>";for(this.o.calendarWeeks&&(c+='<th class="cw">&#160;</th>');b<this.o.weekStart+7;)c+='<th class="dow',a.inArray(b,this.o.daysOfWeekDisabled)!==-1&&(c+=" disabled"),c+='">'+q[this.o.language].daysMin[b++%7]+"</th>";c+="</tr>",this.picker.find(".datepicker-days thead").append(c)}},fillMonths:function(){for(var a,b=this._utc_to_local(this.viewDate),c="",d=0;d<12;d++)a=b&&b.getMonth()===d?" focused":"",c+='<span class="month'+a+'">'+q[this.o.language].monthsShort[d]+"</span>";this.picker.find(".datepicker-months td").html(c)},setRange:function(b){b&&b.length?this.range=a.map(b,function(a){return a.valueOf()}):delete this.range,this.fill()},getClassNames:function(b){var c=[],f=this.viewDate.getUTCFullYear(),g=this.viewDate.getUTCMonth(),h=d();return b.getUTCFullYear()<f||b.getUTCFullYear()===f&&b.getUTCMonth()<g?c.push("old"):(b.getUTCFullYear()>f||b.getUTCFullYear()===f&&b.getUTCMonth()>g)&&c.push("new"),this.focusDate&&b.valueOf()===this.focusDate.valueOf()&&c.push("focused"),this.o.todayHighlight&&e(b,h)&&c.push("today"),this.dates.contains(b)!==-1&&c.push("active"),this.dateWithinRange(b)||c.push("disabled"),this.dateIsDisabled(b)&&c.push("disabled","disabled-date"),a.inArray(b.getUTCDay(),this.o.daysOfWeekHighlighted)!==-1&&c.push("highlighted"),this.range&&(b>this.range[0]&&b<this.range[this.range.length-1]&&c.push("range"),a.inArray(b.valueOf(),this.range)!==-1&&c.push("selected"),b.valueOf()===this.range[0]&&c.push("range-start"),b.valueOf()===this.range[this.range.length-1]&&c.push("range-end")),c},_fill_yearsView:function(c,d,e,f,g,h,i){for(var j,k,l,m="",n=e/10,o=this.picker.find(c),p=Math.floor(f/e)*e,q=p+9*n,r=Math.floor(this.viewDate.getFullYear()/n)*n,s=a.map(this.dates,function(a){return Math.floor(a.getUTCFullYear()/n)*n}),t=p-n;t<=q+n;t+=n)j=[d],k=null,t===p-n?j.push("old"):t===q+n&&j.push("new"),a.inArray(t,s)!==-1&&j.push("active"),(t<g||t>h)&&j.push("disabled"),t===r&&j.push("focused"),i!==a.noop&&(l=i(new Date(t,0,1)),l===b?l={}:"boolean"==typeof l?l={enabled:l}:"string"==typeof l&&(l={classes:l}),l.enabled===!1&&j.push("disabled"),l.classes&&(j=j.concat(l.classes.split(/\s+/))),l.tooltip&&(k=l.tooltip)),m+='<span class="'+j.join(" ")+'"'+(k?' title="'+k+'"':"")+">"+t+"</span>";o.find(".datepicker-switch").text(p+"-"+q),o.find("td").html(m)},fill:function(){var d,e,f=new Date(this.viewDate),g=f.getUTCFullYear(),h=f.getUTCMonth(),i=this.o.startDate!==-(1/0)?this.o.startDate.getUTCFullYear():-(1/0),j=this.o.startDate!==-(1/0)?this.o.startDate.getUTCMonth():-(1/0),k=this.o.endDate!==1/0?this.o.endDate.getUTCFullYear():1/0,l=this.o.endDate!==1/0?this.o.endDate.getUTCMonth():1/0,m=q[this.o.language].today||q.en.today||"",n=q[this.o.language].clear||q.en.clear||"",o=q[this.o.language].titleFormat||q.en.titleFormat;if(!isNaN(g)&&!isNaN(h)){this.picker.find(".datepicker-days .datepicker-switch").text(r.formatDate(f,o,this.o.language)),this.picker.find("tfoot .today").text(m).css("display",this.o.todayBtn===!0||"linked"===this.o.todayBtn?"table-cell":"none"),this.picker.find("tfoot .clear").text(n).css("display",this.o.clearBtn===!0?"table-cell":"none"),this.picker.find("thead .datepicker-title").text(this.o.title).css("display","string"==typeof this.o.title&&""!==this.o.title?"table-cell":"none"),this.updateNavArrows(),this.fillMonths();var p=c(g,h,0),s=p.getUTCDate();p.setUTCDate(s-(p.getUTCDay()-this.o.weekStart+7)%7);var t=new Date(p);p.getUTCFullYear()<100&&t.setUTCFullYear(p.getUTCFullYear()),t.setUTCDate(t.getUTCDate()+42),t=t.valueOf();for(var u,v,w=[];p.valueOf()<t;){if(u=p.getUTCDay(),u===this.o.weekStart&&(w.push("<tr>"),this.o.calendarWeeks)){var x=new Date(+p+(this.o.weekStart-u-7)%7*864e5),y=new Date(Number(x)+(11-x.getUTCDay())%7*864e5),z=new Date(Number(z=c(y.getUTCFullYear(),0,1))+(11-z.getUTCDay())%7*864e5),A=(y-z)/864e5/7+1;w.push('<td class="cw">'+A+"</td>")}v=this.getClassNames(p),v.push("day");var B=p.getUTCDate();this.o.beforeShowDay!==a.noop&&(e=this.o.beforeShowDay(this._utc_to_local(p)),e===b?e={}:"boolean"==typeof e?e={enabled:e}:"string"==typeof e&&(e={classes:e}),e.enabled===!1&&v.push("disabled"),e.classes&&(v=v.concat(e.classes.split(/\s+/))),e.tooltip&&(d=e.tooltip),e.content&&(B=e.content)),v=a.isFunction(a.uniqueSort)?a.uniqueSort(v):a.unique(v),w.push('<td class="'+v.join(" ")+'"'+(d?' title="'+d+'"':"")+' data-date="'+p.getTime().toString()+'">'+B+"</td>"),d=null,u===this.o.weekEnd&&w.push("</tr>"),p.setUTCDate(p.getUTCDate()+1)}this.picker.find(".datepicker-days tbody").html(w.join(""));var C=q[this.o.language].monthsTitle||q.en.monthsTitle||"Months",D=this.picker.find(".datepicker-months").find(".datepicker-switch").text(this.o.maxViewMode<2?C:g).end().find("tbody span").removeClass("active");if(a.each(this.dates,function(a,b){b.getUTCFullYear()===g&&D.eq(b.getUTCMonth()).addClass("active")}),(g<i||g>k)&&D.addClass("disabled"),g===i&&D.slice(0,j).addClass("disabled"),g===k&&D.slice(l+1).addClass("disabled"),this.o.beforeShowMonth!==a.noop){var E=this;a.each(D,function(c,d){var e=new Date(g,c,1),f=E.o.beforeShowMonth(e);f===b?f={}:"boolean"==typeof f?f={enabled:f}:"string"==typeof f&&(f={classes:f}),f.enabled!==!1||a(d).hasClass("disabled")||a(d).addClass("disabled"),f.classes&&a(d).addClass(f.classes),f.tooltip&&a(d).prop("title",f.tooltip)})}this._fill_yearsView(".datepicker-years","year",10,g,i,k,this.o.beforeShowYear),this._fill_yearsView(".datepicker-decades","decade",100,g,i,k,this.o.beforeShowDecade),this._fill_yearsView(".datepicker-centuries","century",1e3,g,i,k,this.o.beforeShowCentury)}},updateNavArrows:function(){if(this._allow_update){var a,b,c=new Date(this.viewDate),d=c.getUTCFullYear(),e=c.getUTCMonth(),f=this.o.startDate!==-(1/0)?this.o.startDate.getUTCFullYear():-(1/0),g=this.o.startDate!==-(1/0)?this.o.startDate.getUTCMonth():-(1/0),h=this.o.endDate!==1/0?this.o.endDate.getUTCFullYear():1/0,i=this.o.endDate!==1/0?this.o.endDate.getUTCMonth():1/0,j=1;switch(this.viewMode){case 0:a=d<=f&&e<=g,b=d>=h&&e>=i;break;case 4:j*=10;case 3:j*=10;case 2:j*=10;case 1:a=Math.floor(d/j)*j<=f,b=Math.floor(d/j)*j+j>=h}this.picker.find(".prev").toggleClass("disabled",a),this.picker.find(".next").toggleClass("disabled",b)}},click:function(b){b.preventDefault(),b.stopPropagation();var e,f,g,h;e=a(b.target),e.hasClass("datepicker-switch")&&this.viewMode!==this.o.maxViewMode&&this.setViewMode(this.viewMode+1),e.hasClass("today")&&!e.hasClass("day")&&(this.setViewMode(0),this._setDate(d(),"linked"===this.o.todayBtn?null:"view")),e.hasClass("clear")&&this.clearDates(),e.hasClass("disabled")||(e.hasClass("month")||e.hasClass("year")||e.hasClass("decade")||e.hasClass("century"))&&(this.viewDate.setUTCDate(1),f=1,1===this.viewMode?(h=e.parent().find("span").index(e),g=this.viewDate.getUTCFullYear(),this.viewDate.setUTCMonth(h)):(h=0,g=Number(e.text()),this.viewDate.setUTCFullYear(g)),this._trigger(r.viewModes[this.viewMode-1].e,this.viewDate),this.viewMode===this.o.minViewMode?this._setDate(c(g,h,f)):(this.setViewMode(this.viewMode-1),this.fill())),this.picker.is(":visible")&&this._focused_from&&this._focused_from.focus(),delete this._focused_from},dayCellClick:function(b){var c=a(b.currentTarget),d=c.data("date"),e=new Date(d);this.o.updateViewDate&&(e.getUTCFullYear()!==this.viewDate.getUTCFullYear()&&this._trigger("changeYear",this.viewDate),e.getUTCMonth()!==this.viewDate.getUTCMonth()&&this._trigger("changeMonth",this.viewDate)),this._setDate(e)},navArrowsClick:function(b){var c=a(b.currentTarget),d=c.hasClass("prev")?-1:1;0!==this.viewMode&&(d*=12*r.viewModes[this.viewMode].navStep),this.viewDate=this.moveMonth(this.viewDate,d),this._trigger(r.viewModes[this.viewMode].e,this.viewDate),this.fill()},_toggle_multidate:function(a){var b=this.dates.contains(a);if(a||this.dates.clear(),b!==-1?(this.o.multidate===!0||this.o.multidate>1||this.o.toggleActive)&&this.dates.remove(b):this.o.multidate===!1?(this.dates.clear(),this.dates.push(a)):this.dates.push(a),"number"==typeof this.o.multidate)for(;this.dates.length>this.o.multidate;)this.dates.remove(0)},_setDate:function(a,b){b&&"date"!==b||this._toggle_multidate(a&&new Date(a)),(!b&&this.o.updateViewDate||"view"===b)&&(this.viewDate=a&&new Date(a)),this.fill(),this.setValue(),b&&"view"===b||this._trigger("changeDate"),this.inputField.trigger("change"),!this.o.autoclose||b&&"date"!==b||this.hide()},moveDay:function(a,b){var c=new Date(a);return c.setUTCDate(a.getUTCDate()+b),c},moveWeek:function(a,b){return this.moveDay(a,7*b)},moveMonth:function(a,b){if(!g(a))return this.o.defaultViewDate;if(!b)return a;var c,d,e=new Date(a.valueOf()),f=e.getUTCDate(),h=e.getUTCMonth(),i=Math.abs(b);if(b=b>0?1:-1,1===i)d=b===-1?function(){return e.getUTCMonth()===h}:function(){return e.getUTCMonth()!==c},c=h+b,e.setUTCMonth(c),c=(c+12)%12;else{for(var j=0;j<i;j++)e=this.moveMonth(e,b);c=e.getUTCMonth(),e.setUTCDate(f),d=function(){return c!==e.getUTCMonth()}}for(;d();)e.setUTCDate(--f),e.setUTCMonth(c);return e},moveYear:function(a,b){return this.moveMonth(a,12*b)},moveAvailableDate:function(a,b,c){do{if(a=this[c](a,b),!this.dateWithinRange(a))return!1;c="moveDay"}while(this.dateIsDisabled(a));return a},weekOfDateIsDisabled:function(b){return a.inArray(b.getUTCDay(),this.o.daysOfWeekDisabled)!==-1},dateIsDisabled:function(b){return this.weekOfDateIsDisabled(b)||a.grep(this.o.datesDisabled,function(a){return e(b,a)}).length>0},dateWithinRange:function(a){return a>=this.o.startDate&&a<=this.o.endDate},keydown:function(a){if(!this.picker.is(":visible"))return void(40!==a.keyCode&&27!==a.keyCode||(this.show(),a.stopPropagation()));var b,c,d=!1,e=this.focusDate||this.viewDate;switch(a.keyCode){case 27:this.focusDate?(this.focusDate=null,this.viewDate=this.dates.get(-1)||this.viewDate,this.fill()):this.hide(),a.preventDefault(),a.stopPropagation();break;case 37:case 38:case 39:case 40:if(!this.o.keyboardNavigation||7===this.o.daysOfWeekDisabled.length)break;b=37===a.keyCode||38===a.keyCode?-1:1,0===this.viewMode?a.ctrlKey?(c=this.moveAvailableDate(e,b,"moveYear"),c&&this._trigger("changeYear",this.viewDate)):a.shiftKey?(c=this.moveAvailableDate(e,b,"moveMonth"),c&&this._trigger("changeMonth",this.viewDate)):37===a.keyCode||39===a.keyCode?c=this.moveAvailableDate(e,b,"moveDay"):this.weekOfDateIsDisabled(e)||(c=this.moveAvailableDate(e,b,"moveWeek")):1===this.viewMode?(38!==a.keyCode&&40!==a.keyCode||(b*=4),c=this.moveAvailableDate(e,b,"moveMonth")):2===this.viewMode&&(38!==a.keyCode&&40!==a.keyCode||(b*=4),c=this.moveAvailableDate(e,b,"moveYear")),c&&(this.focusDate=this.viewDate=c,this.setValue(),this.fill(),a.preventDefault());break;case 13:if(!this.o.forceParse)break;e=this.focusDate||this.dates.get(-1)||this.viewDate,this.o.keyboardNavigation&&(this._toggle_multidate(e),d=!0),this.focusDate=null,this.viewDate=this.dates.get(-1)||this.viewDate,this.setValue(),this.fill(),this.picker.is(":visible")&&(a.preventDefault(),a.stopPropagation(),this.o.autoclose&&this.hide());break;case 9:this.focusDate=null,this.viewDate=this.dates.get(-1)||this.viewDate,this.fill(),this.hide()}d&&(this.dates.length?this._trigger("changeDate"):this._trigger("clearDate"),this.inputField.trigger("change"))},setViewMode:function(a){this.viewMode=a,this.picker.children("div").hide().filter(".datepicker-"+r.viewModes[this.viewMode].clsName).show(),this.updateNavArrows(),this._trigger("changeViewMode",new Date(this.viewDate))}};var l=function(b,c){a.data(b,"datepicker",this),this.element=a(b),this.inputs=a.map(c.inputs,function(a){return a.jquery?a[0]:a}),delete c.inputs,this.keepEmptyValues=c.keepEmptyValues,delete c.keepEmptyValues,n.call(a(this.inputs),c).on("changeDate",a.proxy(this.dateUpdated,this)),this.pickers=a.map(this.inputs,function(b){return a.data(b,"datepicker")}),this.updateDates()};l.prototype={updateDates:function(){this.dates=a.map(this.pickers,function(a){return a.getUTCDate()}),this.updateRanges()},updateRanges:function(){var b=a.map(this.dates,function(a){return a.valueOf()});a.each(this.pickers,function(a,c){c.setRange(b)})},dateUpdated:function(c){if(!this.updating){this.updating=!0;var d=a.data(c.target,"datepicker");if(d!==b){var e=d.getUTCDate(),f=this.keepEmptyValues,g=a.inArray(c.target,this.inputs),h=g-1,i=g+1,j=this.inputs.length;if(g!==-1){if(a.each(this.pickers,function(a,b){b.getUTCDate()||b!==d&&f||b.setUTCDate(e)}),e<this.dates[h])for(;h>=0&&e<this.dates[h];)this.pickers[h--].setUTCDate(e);else if(e>this.dates[i])for(;i<j&&e>this.dates[i];)this.pickers[i++].setUTCDate(e);this.updateDates(),delete this.updating}}}},destroy:function(){a.map(this.pickers,function(a){a.destroy()}),a(this.inputs).off("changeDate",this.dateUpdated),delete this.element.data().datepicker},remove:f("destroy","Method `remove` is deprecated and will be removed in version 2.0. Use `destroy` instead")};var m=a.fn.datepicker,n=function(c){var d=Array.apply(null,arguments);d.shift();var e;if(this.each(function(){var b=a(this),f=b.data("datepicker"),g="object"==typeof c&&c;if(!f){var j=h(this,"date"),m=a.extend({},o,j,g),n=i(m.language),p=a.extend({},o,n,j,g);b.hasClass("input-daterange")||p.inputs?(a.extend(p,{inputs:p.inputs||b.find("input").toArray()}),f=new l(this,p)):f=new k(this,p),b.data("datepicker",f)}"string"==typeof c&&"function"==typeof f[c]&&(e=f[c].apply(f,d))}),e===b||e instanceof k||e instanceof l)return this;if(this.length>1)throw new Error("Using only allowed for the collection of a single element ("+c+" function)");return e};a.fn.datepicker=n;var o=a.fn.datepicker.defaults={assumeNearbyYear:!1,autoclose:!1,beforeShowDay:a.noop,beforeShowMonth:a.noop,beforeShowYear:a.noop,beforeShowDecade:a.noop,beforeShowCentury:a.noop,calendarWeeks:!1,clearBtn:!1,toggleActive:!1,daysOfWeekDisabled:[],daysOfWeekHighlighted:[],datesDisabled:[],endDate:1/0,forceParse:!0,format:"mm/dd/yyyy",keepEmptyValues:!1,keyboardNavigation:!0,language:"en",minViewMode:0,maxViewMode:4,multidate:!1,multidateSeparator:",",orientation:"auto",rtl:!1,startDate:-(1/0),startView:0,todayBtn:!1,todayHighlight:!1,updateViewDate:!0,weekStart:0,disableTouchKeyboard:!1,enableOnReadonly:!0,showOnFocus:!0,zIndexOffset:10,container:"body",immediateUpdates:!1,title:"",templates:{leftArrow:"&#x00AB;",rightArrow:"&#x00BB;"},showWeekDays:!0},p=a.fn.datepicker.locale_opts=["format","rtl","weekStart"];a.fn.datepicker.Constructor=k;var q=a.fn.datepicker.dates={en:{days:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],daysShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],daysMin:["Su","Mo","Tu","We","Th","Fr","Sa"],months:["January","February","March","April","May","June","July","August","September","October","November","December"],monthsShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],today:"Today",clear:"Clear",titleFormat:"MM yyyy"}},r={viewModes:[{names:["days","month"],clsName:"days",e:"changeMonth"},{names:["months","year"],clsName:"months",e:"changeYear",navStep:1},{names:["years","decade"],clsName:"years",e:"changeDecade",navStep:10},{names:["decades","century"],clsName:"decades",e:"changeCentury",navStep:100},{names:["centuries","millennium"],clsName:"centuries",e:"changeMillennium",navStep:1e3}],validParts:/dd?|DD?|mm?|MM?|yy(?:yy)?/g,nonpunctuation:/[^ -\/:-@\u5e74\u6708\u65e5\[-`{-~\t\n\r]+/g,parseFormat:function(a){if("function"==typeof a.toValue&&"function"==typeof a.toDisplay)return a;var b=a.replace(this.validParts,"\0").split("\0"),c=a.match(this.validParts);if(!b||!b.length||!c||0===c.length)throw new Error("Invalid date format.");return{separators:b,parts:c}},parseDate:function(c,e,f,g){function h(a,b){return b===!0&&(b=10),a<100&&(a+=2e3,a>(new Date).getFullYear()+b&&(a-=100)),a}function i(){var a=this.slice(0,j[n].length),b=j[n].slice(0,a.length);return a.toLowerCase()===b.toLowerCase()}if(!c)return b;if(c instanceof Date)return c;if("string"==typeof e&&(e=r.parseFormat(e)),e.toValue)return e.toValue(c,e,f);var j,l,m,n,o,p={d:"moveDay",m:"moveMonth",w:"moveWeek",y:"moveYear"},s={yesterday:"-1d",today:"+0d",tomorrow:"+1d"};if(c in s&&(c=s[c]),/^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/i.test(c)){for(j=c.match(/([\-+]\d+)([dmwy])/gi),c=new Date,n=0;n<j.length;n++)l=j[n].match(/([\-+]\d+)([dmwy])/i),m=Number(l[1]),o=p[l[2].toLowerCase()],c=k.prototype[o](c,m);return k.prototype._zero_utc_time(c)}j=c&&c.match(this.nonpunctuation)||[];var t,u,v={},w=["yyyy","yy","M","MM","m","mm","d","dd"],x={yyyy:function(a,b){return a.setUTCFullYear(g?h(b,g):b)},m:function(a,b){if(isNaN(a))return a;for(b-=1;b<0;)b+=12;for(b%=12,a.setUTCMonth(b);a.getUTCMonth()!==b;)a.setUTCDate(a.getUTCDate()-1);return a},d:function(a,b){return a.setUTCDate(b)}};x.yy=x.yyyy,x.M=x.MM=x.mm=x.m,x.dd=x.d,c=d();var y=e.parts.slice();if(j.length!==y.length&&(y=a(y).filter(function(b,c){return a.inArray(c,w)!==-1}).toArray()),j.length===y.length){var z;for(n=0,z=y.length;n<z;n++){if(t=parseInt(j[n],10),l=y[n],isNaN(t))switch(l){case"MM":u=a(q[f].months).filter(i),t=a.inArray(u[0],q[f].months)+1;break;case"M":u=a(q[f].monthsShort).filter(i),t=a.inArray(u[0],q[f].monthsShort)+1}v[l]=t}var A,B;for(n=0;n<w.length;n++)B=w[n],B in v&&!isNaN(v[B])&&(A=new Date(c),x[B](A,v[B]),isNaN(A)||(c=A))}return c},formatDate:function(b,c,d){if(!b)return"";if("string"==typeof c&&(c=r.parseFormat(c)),c.toDisplay)return c.toDisplay(b,c,d);var e={d:b.getUTCDate(),D:q[d].daysShort[b.getUTCDay()],DD:q[d].days[b.getUTCDay()],m:b.getUTCMonth()+1,M:q[d].monthsShort[b.getUTCMonth()],MM:q[d].months[b.getUTCMonth()],yy:b.getUTCFullYear().toString().substring(2),yyyy:b.getUTCFullYear()};e.dd=(e.d<10?"0":"")+e.d,e.mm=(e.m<10?"0":"")+e.m,b=[];for(var f=a.extend([],c.separators),g=0,h=c.parts.length;g<=h;g++)f.length&&b.push(f.shift()),b.push(e[c.parts[g]]);return b.join("")},headTemplate:'<thead><tr><th colspan="7" class="datepicker-title"></th></tr><tr><th class="prev">'+o.templates.leftArrow+'</th><th colspan="5" class="datepicker-switch"></th><th class="next">'+o.templates.rightArrow+"</th></tr></thead>",
contTemplate:'<tbody><tr><td colspan="7"></td></tr></tbody>',footTemplate:'<tfoot><tr><th colspan="7" class="today"></th></tr><tr><th colspan="7" class="clear"></th></tr></tfoot>'};r.template='<div class="datepicker"><div class="datepicker-days"><table class="table-condensed">'+r.headTemplate+"<tbody></tbody>"+r.footTemplate+'</table></div><div class="datepicker-months"><table class="table-condensed">'+r.headTemplate+r.contTemplate+r.footTemplate+'</table></div><div class="datepicker-years"><table class="table-condensed">'+r.headTemplate+r.contTemplate+r.footTemplate+'</table></div><div class="datepicker-decades"><table class="table-condensed">'+r.headTemplate+r.contTemplate+r.footTemplate+'</table></div><div class="datepicker-centuries"><table class="table-condensed">'+r.headTemplate+r.contTemplate+r.footTemplate+"</table></div></div>",a.fn.datepicker.DPGlobal=r,a.fn.datepicker.noConflict=function(){return a.fn.datepicker=m,this},a.fn.datepicker.version="1.7.1",a.fn.datepicker.deprecated=function(a){var b=window.console;b&&b.warn&&b.warn("DEPRECATED: "+a)},a(document).on("focus.datepicker.data-api click.datepicker.data-api",'[data-provide="datepicker"]',function(b){var c=a(this);c.data("datepicker")||(b.preventDefault(),n.call(c,"show"))}),a(function(){n.call(a('[data-provide="datepicker-inline"]'))})});
/*!
 * clipboard.js v2.0.4
 * https://zenorocha.github.io/clipboard.js
 * 
 * Licensed MIT © Zeno Rocha
 */
!function(t,e){"object"==typeof exports&&"object"==typeof module?module.exports=e():"function"==typeof define&&define.amd?define([],e):"object"==typeof exports?exports.ClipboardJS=e():t.ClipboardJS=e()}(this,function(){return function(n){var o={};function r(t){if(o[t])return o[t].exports;var e=o[t]={i:t,l:!1,exports:{}};return n[t].call(e.exports,e,e.exports,r),e.l=!0,e.exports}return r.m=n,r.c=o,r.d=function(t,e,n){r.o(t,e)||Object.defineProperty(t,e,{enumerable:!0,get:n})},r.r=function(t){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(t,"__esModule",{value:!0})},r.t=function(e,t){if(1&t&&(e=r(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var n=Object.create(null);if(r.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var o in e)r.d(n,o,function(t){return e[t]}.bind(null,o));return n},r.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return r.d(e,"a",e),e},r.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},r.p="",r(r.s=0)}([function(t,e,n){"use strict";var r="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t},i=function(){function o(t,e){for(var n=0;n<e.length;n++){var o=e[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(t,o.key,o)}}return function(t,e,n){return e&&o(t.prototype,e),n&&o(t,n),t}}(),a=o(n(1)),c=o(n(3)),u=o(n(4));function o(t){return t&&t.__esModule?t:{default:t}}var l=function(t){function o(t,e){!function(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}(this,o);var n=function(t,e){if(!t)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!e||"object"!=typeof e&&"function"!=typeof e?t:e}(this,(o.__proto__||Object.getPrototypeOf(o)).call(this));return n.resolveOptions(e),n.listenClick(t),n}return function(t,e){if("function"!=typeof e&&null!==e)throw new TypeError("Super expression must either be null or a function, not "+typeof e);t.prototype=Object.create(e&&e.prototype,{constructor:{value:t,enumerable:!1,writable:!0,configurable:!0}}),e&&(Object.setPrototypeOf?Object.setPrototypeOf(t,e):t.__proto__=e)}(o,c.default),i(o,[{key:"resolveOptions",value:function(){var t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:{};this.action="function"==typeof t.action?t.action:this.defaultAction,this.target="function"==typeof t.target?t.target:this.defaultTarget,this.text="function"==typeof t.text?t.text:this.defaultText,this.container="object"===r(t.container)?t.container:document.body}},{key:"listenClick",value:function(t){var e=this;this.listener=(0,u.default)(t,"click",function(t){return e.onClick(t)})}},{key:"onClick",value:function(t){var e=t.delegateTarget||t.currentTarget;this.clipboardAction&&(this.clipboardAction=null),this.clipboardAction=new a.default({action:this.action(e),target:this.target(e),text:this.text(e),container:this.container,trigger:e,emitter:this})}},{key:"defaultAction",value:function(t){return s("action",t)}},{key:"defaultTarget",value:function(t){var e=s("target",t);if(e)return document.querySelector(e)}},{key:"defaultText",value:function(t){return s("text",t)}},{key:"destroy",value:function(){this.listener.destroy(),this.clipboardAction&&(this.clipboardAction.destroy(),this.clipboardAction=null)}}],[{key:"isSupported",value:function(){var t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:["copy","cut"],e="string"==typeof t?[t]:t,n=!!document.queryCommandSupported;return e.forEach(function(t){n=n&&!!document.queryCommandSupported(t)}),n}}]),o}();function s(t,e){var n="data-clipboard-"+t;if(e.hasAttribute(n))return e.getAttribute(n)}t.exports=l},function(t,e,n){"use strict";var o,r="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t},i=function(){function o(t,e){for(var n=0;n<e.length;n++){var o=e[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(t,o.key,o)}}return function(t,e,n){return e&&o(t.prototype,e),n&&o(t,n),t}}(),a=n(2),c=(o=a)&&o.__esModule?o:{default:o};var u=function(){function e(t){!function(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}(this,e),this.resolveOptions(t),this.initSelection()}return i(e,[{key:"resolveOptions",value:function(){var t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:{};this.action=t.action,this.container=t.container,this.emitter=t.emitter,this.target=t.target,this.text=t.text,this.trigger=t.trigger,this.selectedText=""}},{key:"initSelection",value:function(){this.text?this.selectFake():this.target&&this.selectTarget()}},{key:"selectFake",value:function(){var t=this,e="rtl"==document.documentElement.getAttribute("dir");this.removeFake(),this.fakeHandlerCallback=function(){return t.removeFake()},this.fakeHandler=this.container.addEventListener("click",this.fakeHandlerCallback)||!0,this.fakeElem=document.createElement("textarea"),this.fakeElem.style.fontSize="12pt",this.fakeElem.style.border="0",this.fakeElem.style.padding="0",this.fakeElem.style.margin="0",this.fakeElem.style.position="absolute",this.fakeElem.style[e?"right":"left"]="-9999px";var n=window.pageYOffset||document.documentElement.scrollTop;this.fakeElem.style.top=n+"px",this.fakeElem.setAttribute("readonly",""),this.fakeElem.value=this.text,this.container.appendChild(this.fakeElem),this.selectedText=(0,c.default)(this.fakeElem),this.copyText()}},{key:"removeFake",value:function(){this.fakeHandler&&(this.container.removeEventListener("click",this.fakeHandlerCallback),this.fakeHandler=null,this.fakeHandlerCallback=null),this.fakeElem&&(this.container.removeChild(this.fakeElem),this.fakeElem=null)}},{key:"selectTarget",value:function(){this.selectedText=(0,c.default)(this.target),this.copyText()}},{key:"copyText",value:function(){var e=void 0;try{e=document.execCommand(this.action)}catch(t){e=!1}this.handleResult(e)}},{key:"handleResult",value:function(t){this.emitter.emit(t?"success":"error",{action:this.action,text:this.selectedText,trigger:this.trigger,clearSelection:this.clearSelection.bind(this)})}},{key:"clearSelection",value:function(){this.trigger&&this.trigger.focus(),window.getSelection().removeAllRanges()}},{key:"destroy",value:function(){this.removeFake()}},{key:"action",set:function(){var t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:"copy";if(this._action=t,"copy"!==this._action&&"cut"!==this._action)throw new Error('Invalid "action" value, use either "copy" or "cut"')},get:function(){return this._action}},{key:"target",set:function(t){if(void 0!==t){if(!t||"object"!==(void 0===t?"undefined":r(t))||1!==t.nodeType)throw new Error('Invalid "target" value, use a valid Element');if("copy"===this.action&&t.hasAttribute("disabled"))throw new Error('Invalid "target" attribute. Please use "readonly" instead of "disabled" attribute');if("cut"===this.action&&(t.hasAttribute("readonly")||t.hasAttribute("disabled")))throw new Error('Invalid "target" attribute. You can\'t cut text from elements with "readonly" or "disabled" attributes');this._target=t}},get:function(){return this._target}}]),e}();t.exports=u},function(t,e){t.exports=function(t){var e;if("SELECT"===t.nodeName)t.focus(),e=t.value;else if("INPUT"===t.nodeName||"TEXTAREA"===t.nodeName){var n=t.hasAttribute("readonly");n||t.setAttribute("readonly",""),t.select(),t.setSelectionRange(0,t.value.length),n||t.removeAttribute("readonly"),e=t.value}else{t.hasAttribute("contenteditable")&&t.focus();var o=window.getSelection(),r=document.createRange();r.selectNodeContents(t),o.removeAllRanges(),o.addRange(r),e=o.toString()}return e}},function(t,e){function n(){}n.prototype={on:function(t,e,n){var o=this.e||(this.e={});return(o[t]||(o[t]=[])).push({fn:e,ctx:n}),this},once:function(t,e,n){var o=this;function r(){o.off(t,r),e.apply(n,arguments)}return r._=e,this.on(t,r,n)},emit:function(t){for(var e=[].slice.call(arguments,1),n=((this.e||(this.e={}))[t]||[]).slice(),o=0,r=n.length;o<r;o++)n[o].fn.apply(n[o].ctx,e);return this},off:function(t,e){var n=this.e||(this.e={}),o=n[t],r=[];if(o&&e)for(var i=0,a=o.length;i<a;i++)o[i].fn!==e&&o[i].fn._!==e&&r.push(o[i]);return r.length?n[t]=r:delete n[t],this}},t.exports=n},function(t,e,n){var d=n(5),h=n(6);t.exports=function(t,e,n){if(!t&&!e&&!n)throw new Error("Missing required arguments");if(!d.string(e))throw new TypeError("Second argument must be a String");if(!d.fn(n))throw new TypeError("Third argument must be a Function");if(d.node(t))return s=e,f=n,(l=t).addEventListener(s,f),{destroy:function(){l.removeEventListener(s,f)}};if(d.nodeList(t))return a=t,c=e,u=n,Array.prototype.forEach.call(a,function(t){t.addEventListener(c,u)}),{destroy:function(){Array.prototype.forEach.call(a,function(t){t.removeEventListener(c,u)})}};if(d.string(t))return o=t,r=e,i=n,h(document.body,o,r,i);throw new TypeError("First argument must be a String, HTMLElement, HTMLCollection, or NodeList");var o,r,i,a,c,u,l,s,f}},function(t,n){n.node=function(t){return void 0!==t&&t instanceof HTMLElement&&1===t.nodeType},n.nodeList=function(t){var e=Object.prototype.toString.call(t);return void 0!==t&&("[object NodeList]"===e||"[object HTMLCollection]"===e)&&"length"in t&&(0===t.length||n.node(t[0]))},n.string=function(t){return"string"==typeof t||t instanceof String},n.fn=function(t){return"[object Function]"===Object.prototype.toString.call(t)}},function(t,e,n){var a=n(7);function i(t,e,n,o,r){var i=function(e,n,t,o){return function(t){t.delegateTarget=a(t.target,n),t.delegateTarget&&o.call(e,t)}}.apply(this,arguments);return t.addEventListener(n,i,r),{destroy:function(){t.removeEventListener(n,i,r)}}}t.exports=function(t,e,n,o,r){return"function"==typeof t.addEventListener?i.apply(null,arguments):"function"==typeof n?i.bind(null,document).apply(null,arguments):("string"==typeof t&&(t=document.querySelectorAll(t)),Array.prototype.map.call(t,function(t){return i(t,e,n,o,r)}))}},function(t,e){if("undefined"!=typeof Element&&!Element.prototype.matches){var n=Element.prototype;n.matches=n.matchesSelector||n.mozMatchesSelector||n.msMatchesSelector||n.oMatchesSelector||n.webkitMatchesSelector}t.exports=function(t,e){for(;t&&9!==t.nodeType;){if("function"==typeof t.matches&&t.matches(e))return t;t=t.parentNode}}}])});
// Ion.RangeSlider, 2.3.0, © Denis Ineshin, 2010 - 2018, IonDen.com, Build date: 2018-12-11 23:11:14
!function(i){!jQuery&&"function"==typeof define&&define.amd?define(["jquery"],function(t){return i(t,document,window,navigator)}):jQuery||"object"!=typeof exports?i(jQuery,document,window,navigator):i(require("jquery"),document,window,navigator)}(function(a,c,l,t,_){"use strict";var i,s,o=0,e=(i=t.userAgent,s=/msie\s\d+/i,0<i.search(s)&&s.exec(i).toString().split(" ")[1]<9&&(a("html").addClass("lt-ie9"),!0));Function.prototype.bind||(Function.prototype.bind=function(o){var e=this,h=[].slice;if("function"!=typeof e)throw new TypeError;var r=h.call(arguments,1),n=function(){if(this instanceof n){var t=function(){};t.prototype=e.prototype;var i=new t,s=e.apply(i,r.concat(h.call(arguments)));return Object(s)===s?s:i}return e.apply(o,r.concat(h.call(arguments)))};return n}),Array.prototype.indexOf||(Array.prototype.indexOf=function(t,i){var s;if(null==this)throw new TypeError('"this" is null or not defined');var o=Object(this),e=o.length>>>0;if(0===e)return-1;var h=+i||0;if(Math.abs(h)===1/0&&(h=0),e<=h)return-1;for(s=Math.max(0<=h?h:e-Math.abs(h),0);s<e;){if(s in o&&o[s]===t)return s;s++}return-1});var h=function(t,i,s){this.VERSION="2.3.0",this.input=t,this.plugin_count=s,this.current_plugin=0,this.calc_count=0,this.update_tm=0,this.old_from=0,this.old_to=0,this.old_min_interval=null,this.raf_id=null,this.dragging=!1,this.force_redraw=!1,this.no_diapason=!1,this.has_tab_index=!0,this.is_key=!1,this.is_update=!1,this.is_start=!0,this.is_finish=!1,this.is_active=!1,this.is_resize=!1,this.is_click=!1,i=i||{},this.$cache={win:a(l),body:a(c.body),input:a(t),cont:null,rs:null,min:null,max:null,from:null,to:null,single:null,bar:null,line:null,s_single:null,s_from:null,s_to:null,shad_single:null,shad_from:null,shad_to:null,edge:null,grid:null,grid_labels:[]},this.coords={x_gap:0,x_pointer:0,w_rs:0,w_rs_old:0,w_handle:0,p_gap:0,p_gap_left:0,p_gap_right:0,p_step:0,p_pointer:0,p_handle:0,p_single_fake:0,p_single_real:0,p_from_fake:0,p_from_real:0,p_to_fake:0,p_to_real:0,p_bar_x:0,p_bar_w:0,grid_gap:0,big_num:0,big:[],big_w:[],big_p:[],big_x:[]},this.labels={w_min:0,w_max:0,w_from:0,w_to:0,w_single:0,p_min:0,p_max:0,p_from_fake:0,p_from_left:0,p_to_fake:0,p_to_left:0,p_single_fake:0,p_single_left:0};var o,e,h,r=this.$cache.input,n=r.prop("value");for(h in o={skin:"flat",type:"single",min:10,max:100,from:null,to:null,step:1,min_interval:0,max_interval:0,drag_interval:!1,values:[],p_values:[],from_fixed:!1,from_min:null,from_max:null,from_shadow:!1,to_fixed:!1,to_min:null,to_max:null,to_shadow:!1,prettify_enabled:!0,prettify_separator:" ",prettify:null,force_edges:!1,keyboard:!0,grid:!1,grid_margin:!0,grid_num:4,grid_snap:!1,hide_min_max:!1,hide_from_to:!1,prefix:"",postfix:"",max_postfix:"",decorate_both:!0,values_separator:" — ",input_values_separator:";",disable:!1,block:!1,extra_classes:"",scope:null,onStart:null,onChange:null,onFinish:null,onUpdate:null},"INPUT"!==r[0].nodeName&&console&&console.warn&&console.warn("Base element should be <input>!",r[0]),(e={skin:r.data("skin"),type:r.data("type"),min:r.data("min"),max:r.data("max"),from:r.data("from"),to:r.data("to"),step:r.data("step"),min_interval:r.data("minInterval"),max_interval:r.data("maxInterval"),drag_interval:r.data("dragInterval"),values:r.data("values"),from_fixed:r.data("fromFixed"),from_min:r.data("fromMin"),from_max:r.data("fromMax"),from_shadow:r.data("fromShadow"),to_fixed:r.data("toFixed"),to_min:r.data("toMin"),to_max:r.data("toMax"),to_shadow:r.data("toShadow"),prettify_enabled:r.data("prettifyEnabled"),prettify_separator:r.data("prettifySeparator"),force_edges:r.data("forceEdges"),keyboard:r.data("keyboard"),grid:r.data("grid"),grid_margin:r.data("gridMargin"),grid_num:r.data("gridNum"),grid_snap:r.data("gridSnap"),hide_min_max:r.data("hideMinMax"),hide_from_to:r.data("hideFromTo"),prefix:r.data("prefix"),postfix:r.data("postfix"),max_postfix:r.data("maxPostfix"),decorate_both:r.data("decorateBoth"),values_separator:r.data("valuesSeparator"),input_values_separator:r.data("inputValuesSeparator"),disable:r.data("disable"),block:r.data("block"),extra_classes:r.data("extraClasses")}).values=e.values&&e.values.split(","),e)e.hasOwnProperty(h)&&(e[h]!==_&&""!==e[h]||delete e[h]);n!==_&&""!==n&&((n=n.split(e.input_values_separator||i.input_values_separator||";"))[0]&&n[0]==+n[0]&&(n[0]=+n[0]),n[1]&&n[1]==+n[1]&&(n[1]=+n[1]),i&&i.values&&i.values.length?(o.from=n[0]&&i.values.indexOf(n[0]),o.to=n[1]&&i.values.indexOf(n[1])):(o.from=n[0]&&+n[0],o.to=n[1]&&+n[1])),a.extend(o,i),a.extend(o,e),this.options=o,this.update_check={},this.validate(),this.result={input:this.$cache.input,slider:null,min:this.options.min,max:this.options.max,from:this.options.from,from_percent:0,from_value:null,to:this.options.to,to_percent:0,to_value:null},this.init()};h.prototype={init:function(t){this.no_diapason=!1,this.coords.p_step=this.convertToPercent(this.options.step,!0),this.target="base",this.toggleInput(),this.append(),this.setMinMax(),t?(this.force_redraw=!0,this.calc(!0),this.callOnUpdate()):(this.force_redraw=!0,this.calc(!0),this.callOnStart()),this.updateScene()},append:function(){var t='<span class="irs irs--'+this.options.skin+" js-irs-"+this.plugin_count+" "+this.options.extra_classes+'"></span>';this.$cache.input.before(t),this.$cache.input.prop("readonly",!0),this.$cache.cont=this.$cache.input.prev(),this.result.slider=this.$cache.cont,this.$cache.cont.html('<span class="irs"><span class="irs-line" tabindex="0"></span><span class="irs-min">0</span><span class="irs-max">1</span><span class="irs-from">0</span><span class="irs-to">0</span><span class="irs-single">0</span></span><span class="irs-grid"></span>'),this.$cache.rs=this.$cache.cont.find(".irs"),this.$cache.min=this.$cache.cont.find(".irs-min"),this.$cache.max=this.$cache.cont.find(".irs-max"),this.$cache.from=this.$cache.cont.find(".irs-from"),this.$cache.to=this.$cache.cont.find(".irs-to"),this.$cache.single=this.$cache.cont.find(".irs-single"),this.$cache.line=this.$cache.cont.find(".irs-line"),this.$cache.grid=this.$cache.cont.find(".irs-grid"),"single"===this.options.type?(this.$cache.cont.append('<span class="irs-bar irs-bar--single"></span><span class="irs-shadow shadow-single"></span><span class="irs-handle single"><i></i><i></i><i></i></span>'),this.$cache.bar=this.$cache.cont.find(".irs-bar"),this.$cache.edge=this.$cache.cont.find(".irs-bar-edge"),this.$cache.s_single=this.$cache.cont.find(".single"),this.$cache.from[0].style.visibility="hidden",this.$cache.to[0].style.visibility="hidden",this.$cache.shad_single=this.$cache.cont.find(".shadow-single")):(this.$cache.cont.append('<span class="irs-bar"></span><span class="irs-shadow shadow-from"></span><span class="irs-shadow shadow-to"></span><span class="irs-handle from"><i></i><i></i><i></i></span><span class="irs-handle to"><i></i><i></i><i></i></span>'),this.$cache.bar=this.$cache.cont.find(".irs-bar"),this.$cache.s_from=this.$cache.cont.find(".from"),this.$cache.s_to=this.$cache.cont.find(".to"),this.$cache.shad_from=this.$cache.cont.find(".shadow-from"),this.$cache.shad_to=this.$cache.cont.find(".shadow-to"),this.setTopHandler()),this.options.hide_from_to&&(this.$cache.from[0].style.display="none",this.$cache.to[0].style.display="none",this.$cache.single[0].style.display="none"),this.appendGrid(),this.options.disable?(this.appendDisableMask(),this.$cache.input[0].disabled=!0):(this.$cache.input[0].disabled=!1,this.removeDisableMask(),this.bindEvents()),this.options.disable||(this.options.block?this.appendDisableMask():this.removeDisableMask()),this.options.drag_interval&&(this.$cache.bar[0].style.cursor="ew-resize")},setTopHandler:function(){var t=this.options.min,i=this.options.max,s=this.options.from,o=this.options.to;t<s&&o===i?this.$cache.s_from.addClass("type_last"):o<i&&this.$cache.s_to.addClass("type_last")},changeLevel:function(t){switch(t){case"single":this.coords.p_gap=this.toFixed(this.coords.p_pointer-this.coords.p_single_fake),this.$cache.s_single.addClass("state_hover");break;case"from":this.coords.p_gap=this.toFixed(this.coords.p_pointer-this.coords.p_from_fake),this.$cache.s_from.addClass("state_hover"),this.$cache.s_from.addClass("type_last"),this.$cache.s_to.removeClass("type_last");break;case"to":this.coords.p_gap=this.toFixed(this.coords.p_pointer-this.coords.p_to_fake),this.$cache.s_to.addClass("state_hover"),this.$cache.s_to.addClass("type_last"),this.$cache.s_from.removeClass("type_last");break;case"both":this.coords.p_gap_left=this.toFixed(this.coords.p_pointer-this.coords.p_from_fake),this.coords.p_gap_right=this.toFixed(this.coords.p_to_fake-this.coords.p_pointer),this.$cache.s_to.removeClass("type_last"),this.$cache.s_from.removeClass("type_last")}},appendDisableMask:function(){this.$cache.cont.append('<span class="irs-disable-mask"></span>'),this.$cache.cont.addClass("irs-disabled")},removeDisableMask:function(){this.$cache.cont.remove(".irs-disable-mask"),this.$cache.cont.removeClass("irs-disabled")},remove:function(){this.$cache.cont.remove(),this.$cache.cont=null,this.$cache.line.off("keydown.irs_"+this.plugin_count),this.$cache.body.off("touchmove.irs_"+this.plugin_count),this.$cache.body.off("mousemove.irs_"+this.plugin_count),this.$cache.win.off("touchend.irs_"+this.plugin_count),this.$cache.win.off("mouseup.irs_"+this.plugin_count),e&&(this.$cache.body.off("mouseup.irs_"+this.plugin_count),this.$cache.body.off("mouseleave.irs_"+this.plugin_count)),this.$cache.grid_labels=[],this.coords.big=[],this.coords.big_w=[],this.coords.big_p=[],this.coords.big_x=[],cancelAnimationFrame(this.raf_id)},bindEvents:function(){this.no_diapason||(this.$cache.body.on("touchmove.irs_"+this.plugin_count,this.pointerMove.bind(this)),this.$cache.body.on("mousemove.irs_"+this.plugin_count,this.pointerMove.bind(this)),this.$cache.win.on("touchend.irs_"+this.plugin_count,this.pointerUp.bind(this)),this.$cache.win.on("mouseup.irs_"+this.plugin_count,this.pointerUp.bind(this)),this.$cache.line.on("touchstart.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.line.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.line.on("focus.irs_"+this.plugin_count,this.pointerFocus.bind(this)),this.options.drag_interval&&"double"===this.options.type?(this.$cache.bar.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"both")),this.$cache.bar.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"both"))):(this.$cache.bar.on("touchstart.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.bar.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click"))),"single"===this.options.type?(this.$cache.single.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"single")),this.$cache.s_single.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"single")),this.$cache.shad_single.on("touchstart.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.single.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"single")),this.$cache.s_single.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"single")),this.$cache.edge.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.shad_single.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click"))):(this.$cache.single.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,null)),this.$cache.single.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,null)),this.$cache.from.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"from")),this.$cache.s_from.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"from")),this.$cache.to.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"to")),this.$cache.s_to.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"to")),this.$cache.shad_from.on("touchstart.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.shad_to.on("touchstart.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.from.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"from")),this.$cache.s_from.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"from")),this.$cache.to.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"to")),this.$cache.s_to.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"to")),this.$cache.shad_from.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.shad_to.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click"))),this.options.keyboard&&this.$cache.line.on("keydown.irs_"+this.plugin_count,this.key.bind(this,"keyboard")),e&&(this.$cache.body.on("mouseup.irs_"+this.plugin_count,this.pointerUp.bind(this)),this.$cache.body.on("mouseleave.irs_"+this.plugin_count,this.pointerUp.bind(this))))},pointerFocus:function(t){var i,s;this.target||(i=(s="single"===this.options.type?this.$cache.single:this.$cache.from).offset().left,i+=s.width()/2-1,this.pointerClick("single",{preventDefault:function(){},pageX:i}))},pointerMove:function(t){if(this.dragging){var i=t.pageX||t.originalEvent.touches&&t.originalEvent.touches[0].pageX;this.coords.x_pointer=i-this.coords.x_gap,this.calc()}},pointerUp:function(t){this.current_plugin===this.plugin_count&&this.is_active&&(this.is_active=!1,this.$cache.cont.find(".state_hover").removeClass("state_hover"),this.force_redraw=!0,e&&a("*").prop("unselectable",!1),this.updateScene(),this.restoreOriginalMinInterval(),(a.contains(this.$cache.cont[0],t.target)||this.dragging)&&this.callOnFinish(),this.dragging=!1)},pointerDown:function(t,i){i.preventDefault();var s=i.pageX||i.originalEvent.touches&&i.originalEvent.touches[0].pageX;2!==i.button&&("both"===t&&this.setTempMinInterval(),t||(t=this.target||"from"),this.current_plugin=this.plugin_count,this.target=t,this.is_active=!0,this.dragging=!0,this.coords.x_gap=this.$cache.rs.offset().left,this.coords.x_pointer=s-this.coords.x_gap,this.calcPointerPercent(),this.changeLevel(t),e&&a("*").prop("unselectable",!0),this.$cache.line.trigger("focus"),this.updateScene())},pointerClick:function(t,i){i.preventDefault();var s=i.pageX||i.originalEvent.touches&&i.originalEvent.touches[0].pageX;2!==i.button&&(this.current_plugin=this.plugin_count,this.target=t,this.is_click=!0,this.coords.x_gap=this.$cache.rs.offset().left,this.coords.x_pointer=+(s-this.coords.x_gap).toFixed(),this.force_redraw=!0,this.calc(),this.$cache.line.trigger("focus"))},key:function(t,i){if(!(this.current_plugin!==this.plugin_count||i.altKey||i.ctrlKey||i.shiftKey||i.metaKey)){switch(i.which){case 83:case 65:case 40:case 37:i.preventDefault(),this.moveByKey(!1);break;case 87:case 68:case 38:case 39:i.preventDefault(),this.moveByKey(!0)}return!0}},moveByKey:function(t){var i=this.coords.p_pointer,s=(this.options.max-this.options.min)/100;s=this.options.step/s,t?i+=s:i-=s,this.coords.x_pointer=this.toFixed(this.coords.w_rs/100*i),this.is_key=!0,this.calc()},setMinMax:function(){if(this.options){if(this.options.hide_min_max)return this.$cache.min[0].style.display="none",void(this.$cache.max[0].style.display="none");if(this.options.values.length)this.$cache.min.html(this.decorate(this.options.p_values[this.options.min])),this.$cache.max.html(this.decorate(this.options.p_values[this.options.max]));else{var t=this._prettify(this.options.min),i=this._prettify(this.options.max);this.result.min_pretty=t,this.result.max_pretty=i,this.$cache.min.html(this.decorate(t,this.options.min)),this.$cache.max.html(this.decorate(i,this.options.max))}this.labels.w_min=this.$cache.min.outerWidth(!1),this.labels.w_max=this.$cache.max.outerWidth(!1)}},setTempMinInterval:function(){var t=this.result.to-this.result.from;null===this.old_min_interval&&(this.old_min_interval=this.options.min_interval),this.options.min_interval=t},restoreOriginalMinInterval:function(){null!==this.old_min_interval&&(this.options.min_interval=this.old_min_interval,this.old_min_interval=null)},calc:function(t){if(this.options&&(this.calc_count++,(10===this.calc_count||t)&&(this.calc_count=0,this.coords.w_rs=this.$cache.rs.outerWidth(!1),this.calcHandlePercent()),this.coords.w_rs)){this.calcPointerPercent();var i=this.getHandleX();switch("both"===this.target&&(this.coords.p_gap=0,i=this.getHandleX()),"click"===this.target&&(this.coords.p_gap=this.coords.p_handle/2,i=this.getHandleX(),this.options.drag_interval?this.target="both_one":this.target=this.chooseHandle(i)),this.target){case"base":var s=(this.options.max-this.options.min)/100,o=(this.result.from-this.options.min)/s,e=(this.result.to-this.options.min)/s;this.coords.p_single_real=this.toFixed(o),this.coords.p_from_real=this.toFixed(o),this.coords.p_to_real=this.toFixed(e),this.coords.p_single_real=this.checkDiapason(this.coords.p_single_real,this.options.from_min,this.options.from_max),this.coords.p_from_real=this.checkDiapason(this.coords.p_from_real,this.options.from_min,this.options.from_max),this.coords.p_to_real=this.checkDiapason(this.coords.p_to_real,this.options.to_min,this.options.to_max),this.coords.p_single_fake=this.convertToFakePercent(this.coords.p_single_real),this.coords.p_from_fake=this.convertToFakePercent(this.coords.p_from_real),this.coords.p_to_fake=this.convertToFakePercent(this.coords.p_to_real),this.target=null;break;case"single":if(this.options.from_fixed)break;this.coords.p_single_real=this.convertToRealPercent(i),this.coords.p_single_real=this.calcWithStep(this.coords.p_single_real),this.coords.p_single_real=this.checkDiapason(this.coords.p_single_real,this.options.from_min,this.options.from_max),this.coords.p_single_fake=this.convertToFakePercent(this.coords.p_single_real);break;case"from":if(this.options.from_fixed)break;this.coords.p_from_real=this.convertToRealPercent(i),this.coords.p_from_real=this.calcWithStep(this.coords.p_from_real),this.coords.p_from_real>this.coords.p_to_real&&(this.coords.p_from_real=this.coords.p_to_real),this.coords.p_from_real=this.checkDiapason(this.coords.p_from_real,this.options.from_min,this.options.from_max),this.coords.p_from_real=this.checkMinInterval(this.coords.p_from_real,this.coords.p_to_real,"from"),this.coords.p_from_real=this.checkMaxInterval(this.coords.p_from_real,this.coords.p_to_real,"from"),this.coords.p_from_fake=this.convertToFakePercent(this.coords.p_from_real);break;case"to":if(this.options.to_fixed)break;this.coords.p_to_real=this.convertToRealPercent(i),this.coords.p_to_real=this.calcWithStep(this.coords.p_to_real),this.coords.p_to_real<this.coords.p_from_real&&(this.coords.p_to_real=this.coords.p_from_real),this.coords.p_to_real=this.checkDiapason(this.coords.p_to_real,this.options.to_min,this.options.to_max),this.coords.p_to_real=this.checkMinInterval(this.coords.p_to_real,this.coords.p_from_real,"to"),this.coords.p_to_real=this.checkMaxInterval(this.coords.p_to_real,this.coords.p_from_real,"to"),this.coords.p_to_fake=this.convertToFakePercent(this.coords.p_to_real);break;case"both":if(this.options.from_fixed||this.options.to_fixed)break;i=this.toFixed(i+.001*this.coords.p_handle),this.coords.p_from_real=this.convertToRealPercent(i)-this.coords.p_gap_left,this.coords.p_from_real=this.calcWithStep(this.coords.p_from_real),this.coords.p_from_real=this.checkDiapason(this.coords.p_from_real,this.options.from_min,this.options.from_max),this.coords.p_from_real=this.checkMinInterval(this.coords.p_from_real,this.coords.p_to_real,"from"),this.coords.p_from_fake=this.convertToFakePercent(this.coords.p_from_real),this.coords.p_to_real=this.convertToRealPercent(i)+this.coords.p_gap_right,this.coords.p_to_real=this.calcWithStep(this.coords.p_to_real),this.coords.p_to_real=this.checkDiapason(this.coords.p_to_real,this.options.to_min,this.options.to_max),this.coords.p_to_real=this.checkMinInterval(this.coords.p_to_real,this.coords.p_from_real,"to"),this.coords.p_to_fake=this.convertToFakePercent(this.coords.p_to_real);break;case"both_one":if(this.options.from_fixed||this.options.to_fixed)break;var h=this.convertToRealPercent(i),r=this.result.from_percent,n=this.result.to_percent-r,a=n/2,c=h-a,l=h+a;c<0&&(l=(c=0)+n),100<l&&(c=(l=100)-n),this.coords.p_from_real=this.calcWithStep(c),this.coords.p_from_real=this.checkDiapason(this.coords.p_from_real,this.options.from_min,this.options.from_max),this.coords.p_from_fake=this.convertToFakePercent(this.coords.p_from_real),this.coords.p_to_real=this.calcWithStep(l),this.coords.p_to_real=this.checkDiapason(this.coords.p_to_real,this.options.to_min,this.options.to_max),this.coords.p_to_fake=this.convertToFakePercent(this.coords.p_to_real)}"single"===this.options.type?(this.coords.p_bar_x=this.coords.p_handle/2,this.coords.p_bar_w=this.coords.p_single_fake,this.result.from_percent=this.coords.p_single_real,this.result.from=this.convertToValue(this.coords.p_single_real),this.result.from_pretty=this._prettify(this.result.from),this.options.values.length&&(this.result.from_value=this.options.values[this.result.from])):(this.coords.p_bar_x=this.toFixed(this.coords.p_from_fake+this.coords.p_handle/2),this.coords.p_bar_w=this.toFixed(this.coords.p_to_fake-this.coords.p_from_fake),this.result.from_percent=this.coords.p_from_real,this.result.from=this.convertToValue(this.coords.p_from_real),this.result.from_pretty=this._prettify(this.result.from),this.result.to_percent=this.coords.p_to_real,this.result.to=this.convertToValue(this.coords.p_to_real),this.result.to_pretty=this._prettify(this.result.to),this.options.values.length&&(this.result.from_value=this.options.values[this.result.from],this.result.to_value=this.options.values[this.result.to])),this.calcMinMax(),this.calcLabels()}},calcPointerPercent:function(){this.coords.w_rs?(this.coords.x_pointer<0||isNaN(this.coords.x_pointer)?this.coords.x_pointer=0:this.coords.x_pointer>this.coords.w_rs&&(this.coords.x_pointer=this.coords.w_rs),this.coords.p_pointer=this.toFixed(this.coords.x_pointer/this.coords.w_rs*100)):this.coords.p_pointer=0},convertToRealPercent:function(t){return t/(100-this.coords.p_handle)*100},convertToFakePercent:function(t){return t/100*(100-this.coords.p_handle)},getHandleX:function(){var t=100-this.coords.p_handle,i=this.toFixed(this.coords.p_pointer-this.coords.p_gap);return i<0?i=0:t<i&&(i=t),i},calcHandlePercent:function(){"single"===this.options.type?this.coords.w_handle=this.$cache.s_single.outerWidth(!1):this.coords.w_handle=this.$cache.s_from.outerWidth(!1),this.coords.p_handle=this.toFixed(this.coords.w_handle/this.coords.w_rs*100)},chooseHandle:function(t){return"single"===this.options.type?"single":this.coords.p_from_real+(this.coords.p_to_real-this.coords.p_from_real)/2<=t?this.options.to_fixed?"from":"to":this.options.from_fixed?"to":"from"},calcMinMax:function(){this.coords.w_rs&&(this.labels.p_min=this.labels.w_min/this.coords.w_rs*100,this.labels.p_max=this.labels.w_max/this.coords.w_rs*100)},calcLabels:function(){this.coords.w_rs&&!this.options.hide_from_to&&("single"===this.options.type?(this.labels.w_single=this.$cache.single.outerWidth(!1),this.labels.p_single_fake=this.labels.w_single/this.coords.w_rs*100,this.labels.p_single_left=this.coords.p_single_fake+this.coords.p_handle/2-this.labels.p_single_fake/2):(this.labels.w_from=this.$cache.from.outerWidth(!1),this.labels.p_from_fake=this.labels.w_from/this.coords.w_rs*100,this.labels.p_from_left=this.coords.p_from_fake+this.coords.p_handle/2-this.labels.p_from_fake/2,this.labels.p_from_left=this.toFixed(this.labels.p_from_left),this.labels.p_from_left=this.checkEdges(this.labels.p_from_left,this.labels.p_from_fake),this.labels.w_to=this.$cache.to.outerWidth(!1),this.labels.p_to_fake=this.labels.w_to/this.coords.w_rs*100,this.labels.p_to_left=this.coords.p_to_fake+this.coords.p_handle/2-this.labels.p_to_fake/2,this.labels.p_to_left=this.toFixed(this.labels.p_to_left),this.labels.p_to_left=this.checkEdges(this.labels.p_to_left,this.labels.p_to_fake),this.labels.w_single=this.$cache.single.outerWidth(!1),this.labels.p_single_fake=this.labels.w_single/this.coords.w_rs*100,this.labels.p_single_left=(this.labels.p_from_left+this.labels.p_to_left+this.labels.p_to_fake)/2-this.labels.p_single_fake/2,this.labels.p_single_left=this.toFixed(this.labels.p_single_left)),this.labels.p_single_left=this.checkEdges(this.labels.p_single_left,this.labels.p_single_fake))},updateScene:function(){this.raf_id&&(cancelAnimationFrame(this.raf_id),this.raf_id=null),clearTimeout(this.update_tm),this.update_tm=null,this.options&&(this.drawHandles(),this.is_active?this.raf_id=requestAnimationFrame(this.updateScene.bind(this)):this.update_tm=setTimeout(this.updateScene.bind(this),300))},drawHandles:function(){this.coords.w_rs=this.$cache.rs.outerWidth(!1),this.coords.w_rs&&(this.coords.w_rs!==this.coords.w_rs_old&&(this.target="base",this.is_resize=!0),(this.coords.w_rs!==this.coords.w_rs_old||this.force_redraw)&&(this.setMinMax(),this.calc(!0),this.drawLabels(),this.options.grid&&(this.calcGridMargin(),this.calcGridLabels()),this.force_redraw=!0,this.coords.w_rs_old=this.coords.w_rs,this.drawShadow()),this.coords.w_rs&&(this.dragging||this.force_redraw||this.is_key)&&((this.old_from!==this.result.from||this.old_to!==this.result.to||this.force_redraw||this.is_key)&&(this.drawLabels(),this.$cache.bar[0].style.left=this.coords.p_bar_x+"%",this.$cache.bar[0].style.width=this.coords.p_bar_w+"%","single"===this.options.type?(this.$cache.bar[0].style.left=0,this.$cache.bar[0].style.width=this.coords.p_bar_w+this.coords.p_bar_x+"%",this.$cache.s_single[0].style.left=this.coords.p_single_fake+"%"):(this.$cache.s_from[0].style.left=this.coords.p_from_fake+"%",this.$cache.s_to[0].style.left=this.coords.p_to_fake+"%",(this.old_from!==this.result.from||this.force_redraw)&&(this.$cache.from[0].style.left=this.labels.p_from_left+"%"),(this.old_to!==this.result.to||this.force_redraw)&&(this.$cache.to[0].style.left=this.labels.p_to_left+"%")),this.$cache.single[0].style.left=this.labels.p_single_left+"%",this.writeToInput(),this.old_from===this.result.from&&this.old_to===this.result.to||this.is_start||(this.$cache.input.trigger("change"),this.$cache.input.trigger("input")),this.old_from=this.result.from,this.old_to=this.result.to,this.is_resize||this.is_update||this.is_start||this.is_finish||this.callOnChange(),(this.is_key||this.is_click)&&(this.is_key=!1,this.is_click=!1,this.callOnFinish()),this.is_update=!1,this.is_resize=!1,this.is_finish=!1),this.is_start=!1,this.is_key=!1,this.is_click=!1,this.force_redraw=!1))},drawLabels:function(){if(this.options){var t,i,s,o,e,h=this.options.values.length,r=this.options.p_values;if(!this.options.hide_from_to)if("single"===this.options.type)t=h?this.decorate(r[this.result.from]):(o=this._prettify(this.result.from),this.decorate(o,this.result.from)),this.$cache.single.html(t),this.calcLabels(),this.labels.p_single_left<this.labels.p_min+1?this.$cache.min[0].style.visibility="hidden":this.$cache.min[0].style.visibility="visible",this.labels.p_single_left+this.labels.p_single_fake>100-this.labels.p_max-1?this.$cache.max[0].style.visibility="hidden":this.$cache.max[0].style.visibility="visible";else{s=h?(this.options.decorate_both?(t=this.decorate(r[this.result.from]),t+=this.options.values_separator,t+=this.decorate(r[this.result.to])):t=this.decorate(r[this.result.from]+this.options.values_separator+r[this.result.to]),i=this.decorate(r[this.result.from]),this.decorate(r[this.result.to])):(o=this._prettify(this.result.from),e=this._prettify(this.result.to),this.options.decorate_both?(t=this.decorate(o,this.result.from),t+=this.options.values_separator,t+=this.decorate(e,this.result.to)):t=this.decorate(o+this.options.values_separator+e,this.result.to),i=this.decorate(o,this.result.from),this.decorate(e,this.result.to)),this.$cache.single.html(t),this.$cache.from.html(i),this.$cache.to.html(s),this.calcLabels();var n=Math.min(this.labels.p_single_left,this.labels.p_from_left),a=this.labels.p_single_left+this.labels.p_single_fake,c=this.labels.p_to_left+this.labels.p_to_fake,l=Math.max(a,c);this.labels.p_from_left+this.labels.p_from_fake>=this.labels.p_to_left?(this.$cache.from[0].style.visibility="hidden",this.$cache.to[0].style.visibility="hidden",this.$cache.single[0].style.visibility="visible",l=this.result.from===this.result.to?("from"===this.target?this.$cache.from[0].style.visibility="visible":"to"===this.target?this.$cache.to[0].style.visibility="visible":this.target||(this.$cache.from[0].style.visibility="visible"),this.$cache.single[0].style.visibility="hidden",c):(this.$cache.from[0].style.visibility="hidden",this.$cache.to[0].style.visibility="hidden",this.$cache.single[0].style.visibility="visible",Math.max(a,c))):(this.$cache.from[0].style.visibility="visible",this.$cache.to[0].style.visibility="visible",this.$cache.single[0].style.visibility="hidden"),n<this.labels.p_min+1?this.$cache.min[0].style.visibility="hidden":this.$cache.min[0].style.visibility="visible",l>100-this.labels.p_max-1?this.$cache.max[0].style.visibility="hidden":this.$cache.max[0].style.visibility="visible"}}},drawShadow:function(){var t,i,s,o,e=this.options,h=this.$cache,r="number"==typeof e.from_min&&!isNaN(e.from_min),n="number"==typeof e.from_max&&!isNaN(e.from_max),a="number"==typeof e.to_min&&!isNaN(e.to_min),c="number"==typeof e.to_max&&!isNaN(e.to_max);"single"===e.type?e.from_shadow&&(r||n)?(t=this.convertToPercent(r?e.from_min:e.min),i=this.convertToPercent(n?e.from_max:e.max)-t,t=this.toFixed(t-this.coords.p_handle/100*t),i=this.toFixed(i-this.coords.p_handle/100*i),t+=this.coords.p_handle/2,h.shad_single[0].style.display="block",h.shad_single[0].style.left=t+"%",h.shad_single[0].style.width=i+"%"):h.shad_single[0].style.display="none":(e.from_shadow&&(r||n)?(t=this.convertToPercent(r?e.from_min:e.min),i=this.convertToPercent(n?e.from_max:e.max)-t,t=this.toFixed(t-this.coords.p_handle/100*t),i=this.toFixed(i-this.coords.p_handle/100*i),t+=this.coords.p_handle/2,h.shad_from[0].style.display="block",h.shad_from[0].style.left=t+"%",h.shad_from[0].style.width=i+"%"):h.shad_from[0].style.display="none",e.to_shadow&&(a||c)?(s=this.convertToPercent(a?e.to_min:e.min),o=this.convertToPercent(c?e.to_max:e.max)-s,s=this.toFixed(s-this.coords.p_handle/100*s),o=this.toFixed(o-this.coords.p_handle/100*o),s+=this.coords.p_handle/2,h.shad_to[0].style.display="block",h.shad_to[0].style.left=s+"%",h.shad_to[0].style.width=o+"%"):h.shad_to[0].style.display="none")},writeToInput:function(){"single"===this.options.type?(this.options.values.length?this.$cache.input.prop("value",this.result.from_value):this.$cache.input.prop("value",this.result.from),this.$cache.input.data("from",this.result.from)):(this.options.values.length?this.$cache.input.prop("value",this.result.from_value+this.options.input_values_separator+this.result.to_value):this.$cache.input.prop("value",this.result.from+this.options.input_values_separator+this.result.to),this.$cache.input.data("from",this.result.from),this.$cache.input.data("to",this.result.to))},callOnStart:function(){this.writeToInput(),this.options.onStart&&"function"==typeof this.options.onStart&&(this.options.scope?this.options.onStart.call(this.options.scope,this.result):this.options.onStart(this.result))},callOnChange:function(){this.writeToInput(),this.options.onChange&&"function"==typeof this.options.onChange&&(this.options.scope?this.options.onChange.call(this.options.scope,this.result):this.options.onChange(this.result))},callOnFinish:function(){this.writeToInput(),this.options.onFinish&&"function"==typeof this.options.onFinish&&(this.options.scope?this.options.onFinish.call(this.options.scope,this.result):this.options.onFinish(this.result))},callOnUpdate:function(){this.writeToInput(),this.options.onUpdate&&"function"==typeof this.options.onUpdate&&(this.options.scope?this.options.onUpdate.call(this.options.scope,this.result):this.options.onUpdate(this.result))},toggleInput:function(){this.$cache.input.toggleClass("irs-hidden-input"),this.has_tab_index?this.$cache.input.prop("tabindex",-1):this.$cache.input.removeProp("tabindex"),this.has_tab_index=!this.has_tab_index},convertToPercent:function(t,i){var s,o=this.options.max-this.options.min,e=o/100;return o?(s=(i?t:t-this.options.min)/e,this.toFixed(s)):(this.no_diapason=!0,0)},convertToValue:function(t){var i,s,o=this.options.min,e=this.options.max,h=o.toString().split(".")[1],r=e.toString().split(".")[1],n=0,a=0;if(0===t)return this.options.min;if(100===t)return this.options.max;h&&(n=i=h.length),r&&(n=s=r.length),i&&s&&(n=s<=i?i:s),o<0&&(o=+(o+(a=Math.abs(o))).toFixed(n),e=+(e+a).toFixed(n));var c,l=(e-o)/100*t+o,_=this.options.step.toString().split(".")[1];return l=_?+l.toFixed(_.length):(l/=this.options.step,+(l*=this.options.step).toFixed(0)),a&&(l-=a),(c=_?+l.toFixed(_.length):this.toFixed(l))<this.options.min?c=this.options.min:c>this.options.max&&(c=this.options.max),c},calcWithStep:function(t){var i=Math.round(t/this.coords.p_step)*this.coords.p_step;return 100<i&&(i=100),100===t&&(i=100),this.toFixed(i)},checkMinInterval:function(t,i,s){var o,e,h=this.options;return h.min_interval?(o=this.convertToValue(t),e=this.convertToValue(i),"from"===s?e-o<h.min_interval&&(o=e-h.min_interval):o-e<h.min_interval&&(o=e+h.min_interval),this.convertToPercent(o)):t},checkMaxInterval:function(t,i,s){var o,e,h=this.options;return h.max_interval?(o=this.convertToValue(t),e=this.convertToValue(i),"from"===s?e-o>h.max_interval&&(o=e-h.max_interval):o-e>h.max_interval&&(o=e+h.max_interval),this.convertToPercent(o)):t},checkDiapason:function(t,i,s){var o=this.convertToValue(t),e=this.options;return"number"!=typeof i&&(i=e.min),"number"!=typeof s&&(s=e.max),o<i&&(o=i),s<o&&(o=s),this.convertToPercent(o)},toFixed:function(t){return+(t=t.toFixed(20))},_prettify:function(t){return this.options.prettify_enabled?this.options.prettify&&"function"==typeof this.options.prettify?this.options.prettify(t):this.prettify(t):t},prettify:function(t){return t.toString().replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g,"$1"+this.options.prettify_separator)},checkEdges:function(t,i){return this.options.force_edges&&(t<0?t=0:100-i<t&&(t=100-i)),this.toFixed(t)},validate:function(){var t,i,s=this.options,o=this.result,e=s.values,h=e.length;if("string"==typeof s.min&&(s.min=+s.min),"string"==typeof s.max&&(s.max=+s.max),"string"==typeof s.from&&(s.from=+s.from),"string"==typeof s.to&&(s.to=+s.to),"string"==typeof s.step&&(s.step=+s.step),"string"==typeof s.from_min&&(s.from_min=+s.from_min),"string"==typeof s.from_max&&(s.from_max=+s.from_max),"string"==typeof s.to_min&&(s.to_min=+s.to_min),"string"==typeof s.to_max&&(s.to_max=+s.to_max),"string"==typeof s.grid_num&&(s.grid_num=+s.grid_num),s.max<s.min&&(s.max=s.min),h)for(s.p_values=[],s.min=0,s.max=h-1,s.step=1,s.grid_num=s.max,s.grid_snap=!0,i=0;i<h;i++)t=+e[i],t=isNaN(t)?e[i]:(e[i]=t,this._prettify(t)),s.p_values.push(t);("number"!=typeof s.from||isNaN(s.from))&&(s.from=s.min),("number"!=typeof s.to||isNaN(s.to))&&(s.to=s.max),"single"===s.type?(s.from<s.min&&(s.from=s.min),s.from>s.max&&(s.from=s.max)):(s.from<s.min&&(s.from=s.min),s.from>s.max&&(s.from=s.max),s.to<s.min&&(s.to=s.min),s.to>s.max&&(s.to=s.max),this.update_check.from&&(this.update_check.from!==s.from&&s.from>s.to&&(s.from=s.to),this.update_check.to!==s.to&&s.to<s.from&&(s.to=s.from)),s.from>s.to&&(s.from=s.to),s.to<s.from&&(s.to=s.from)),("number"!=typeof s.step||isNaN(s.step)||!s.step||s.step<0)&&(s.step=1),"number"==typeof s.from_min&&s.from<s.from_min&&(s.from=s.from_min),"number"==typeof s.from_max&&s.from>s.from_max&&(s.from=s.from_max),"number"==typeof s.to_min&&s.to<s.to_min&&(s.to=s.to_min),"number"==typeof s.to_max&&s.from>s.to_max&&(s.to=s.to_max),o&&(o.min!==s.min&&(o.min=s.min),o.max!==s.max&&(o.max=s.max),(o.from<o.min||o.from>o.max)&&(o.from=s.from),(o.to<o.min||o.to>o.max)&&(o.to=s.to)),("number"!=typeof s.min_interval||isNaN(s.min_interval)||!s.min_interval||s.min_interval<0)&&(s.min_interval=0),("number"!=typeof s.max_interval||isNaN(s.max_interval)||!s.max_interval||s.max_interval<0)&&(s.max_interval=0),s.min_interval&&s.min_interval>s.max-s.min&&(s.min_interval=s.max-s.min),s.max_interval&&s.max_interval>s.max-s.min&&(s.max_interval=s.max-s.min)},decorate:function(t,i){var s="",o=this.options;return o.prefix&&(s+=o.prefix),s+=t,o.max_postfix&&(o.values.length&&t===o.p_values[o.max]?(s+=o.max_postfix,o.postfix&&(s+=" ")):i===o.max&&(s+=o.max_postfix,o.postfix&&(s+=" "))),o.postfix&&(s+=o.postfix),s},updateFrom:function(){this.result.from=this.options.from,this.result.from_percent=this.convertToPercent(this.result.from),this.result.from_pretty=this._prettify(this.result.from),this.options.values&&(this.result.from_value=this.options.values[this.result.from])},updateTo:function(){this.result.to=this.options.to,this.result.to_percent=this.convertToPercent(this.result.to),this.result.to_pretty=this._prettify(this.result.to),this.options.values&&(this.result.to_value=this.options.values[this.result.to])},updateResult:function(){this.result.min=this.options.min,this.result.max=this.options.max,this.updateFrom(),this.updateTo()},appendGrid:function(){if(this.options.grid){var t,i,s,o,e,h,r=this.options,n=r.max-r.min,a=r.grid_num,c=0,l=4,_="";for(this.calcGridMargin(),r.grid_snap&&(a=n/r.step),50<a&&(a=50),s=this.toFixed(100/a),4<a&&(l=3),7<a&&(l=2),14<a&&(l=1),28<a&&(l=0),t=0;t<a+1;t++){for(o=l,100<(c=this.toFixed(s*t))&&(c=100),e=((this.coords.big[t]=c)-s*(t-1))/(o+1),i=1;i<=o&&0!==c;i++)_+='<span class="irs-grid-pol small" style="left: '+this.toFixed(c-e*i)+'%"></span>';_+='<span class="irs-grid-pol" style="left: '+c+'%"></span>',h=this.convertToValue(c),_+='<span class="irs-grid-text js-grid-text-'+t+'" style="left: '+c+'%">'+(h=r.values.length?r.p_values[h]:this._prettify(h))+"</span>"}this.coords.big_num=Math.ceil(a+1),this.$cache.cont.addClass("irs-with-grid"),this.$cache.grid.html(_),this.cacheGridLabels()}},cacheGridLabels:function(){var t,i,s=this.coords.big_num;for(i=0;i<s;i++)t=this.$cache.grid.find(".js-grid-text-"+i),this.$cache.grid_labels.push(t);this.calcGridLabels()},calcGridLabels:function(){var t,i,s=[],o=[],e=this.coords.big_num;for(t=0;t<e;t++)this.coords.big_w[t]=this.$cache.grid_labels[t].outerWidth(!1),this.coords.big_p[t]=this.toFixed(this.coords.big_w[t]/this.coords.w_rs*100),this.coords.big_x[t]=this.toFixed(this.coords.big_p[t]/2),s[t]=this.toFixed(this.coords.big[t]-this.coords.big_x[t]),o[t]=this.toFixed(s[t]+this.coords.big_p[t]);for(this.options.force_edges&&(s[0]<-this.coords.grid_gap&&(s[0]=-this.coords.grid_gap,o[0]=this.toFixed(s[0]+this.coords.big_p[0]),this.coords.big_x[0]=this.coords.grid_gap),o[e-1]>100+this.coords.grid_gap&&(o[e-1]=100+this.coords.grid_gap,s[e-1]=this.toFixed(o[e-1]-this.coords.big_p[e-1]),this.coords.big_x[e-1]=this.toFixed(this.coords.big_p[e-1]-this.coords.grid_gap))),this.calcGridCollision(2,s,o),this.calcGridCollision(4,s,o),t=0;t<e;t++)i=this.$cache.grid_labels[t][0],this.coords.big_x[t]!==Number.POSITIVE_INFINITY&&(i.style.marginLeft=-this.coords.big_x[t]+"%")},calcGridCollision:function(t,i,s){var o,e,h,r=this.coords.big_num;for(o=0;o<r&&!(r<=(e=o+t/2));o+=t)h=this.$cache.grid_labels[e][0],s[o]<=i[e]?h.style.visibility="visible":h.style.visibility="hidden"},calcGridMargin:function(){this.options.grid_margin&&(this.coords.w_rs=this.$cache.rs.outerWidth(!1),this.coords.w_rs&&("single"===this.options.type?this.coords.w_handle=this.$cache.s_single.outerWidth(!1):this.coords.w_handle=this.$cache.s_from.outerWidth(!1),this.coords.p_handle=this.toFixed(this.coords.w_handle/this.coords.w_rs*100),this.coords.grid_gap=this.toFixed(this.coords.p_handle/2-.1),this.$cache.grid[0].style.width=this.toFixed(100-this.coords.p_handle)+"%",this.$cache.grid[0].style.left=this.coords.grid_gap+"%"))},update:function(t){this.input&&(this.is_update=!0,this.options.from=this.result.from,this.options.to=this.result.to,this.update_check.from=this.result.from,this.update_check.to=this.result.to,this.options=a.extend(this.options,t),this.validate(),this.updateResult(t),this.toggleInput(),this.remove(),this.init(!0))},reset:function(){this.input&&(this.updateResult(),this.update())},destroy:function(){this.input&&(this.toggleInput(),this.$cache.input.prop("readonly",!1),a.data(this.input,"ionRangeSlider",null),this.remove(),this.input=null,this.options=null)}},a.fn.ionRangeSlider=function(t){return this.each(function(){a.data(this,"ionRangeSlider")||a.data(this,"ionRangeSlider",new h(this,t,o++))})},function(){for(var h=0,t=["ms","moz","webkit","o"],i=0;i<t.length&&!l.requestAnimationFrame;++i)l.requestAnimationFrame=l[t[i]+"RequestAnimationFrame"],l.cancelAnimationFrame=l[t[i]+"CancelAnimationFrame"]||l[t[i]+"CancelRequestAnimationFrame"];l.requestAnimationFrame||(l.requestAnimationFrame=function(t,i){var s=(new Date).getTime(),o=Math.max(0,16-(s-h)),e=l.setTimeout(function(){t(s+o)},o);return h=s+o,e}),l.cancelAnimationFrame||(l.cancelAnimationFrame=function(t){clearTimeout(t)})}()});
(function(a){if(typeof define==="function"&&define.amd&&define.amd.jQuery){define(["jquery"],a)}else{a(jQuery)}}(function(f){var p="left",o="right",e="up",x="down",c="in",z="out",m="none",s="auto",l="swipe",t="pinch",A="tap",j="doubletap",b="longtap",y="hold",D="horizontal",u="vertical",i="all",r=10,g="start",k="move",h="end",q="cancel",a="ontouchstart" in window,v=window.navigator.msPointerEnabled&&!window.navigator.pointerEnabled,d=window.navigator.pointerEnabled||window.navigator.msPointerEnabled,B="TouchSwipe";var n={fingers:1,threshold:75,cancelThreshold:null,pinchThreshold:20,maxTimeThreshold:null,fingerReleaseThreshold:250,longTapThreshold:500,doubleTapThreshold:200,swipe:null,swipeLeft:null,swipeRight:null,swipeUp:null,swipeDown:null,swipeStatus:null,pinchIn:null,pinchOut:null,pinchStatus:null,click:null,tap:null,doubleTap:null,longTap:null,hold:null,triggerOnTouchEnd:true,triggerOnTouchLeave:false,allowPageScroll:"auto",fallbackToMouseEvents:true,excludedElements:"label, button, input, select, textarea, a, .noSwipe"};f.fn.swipe=function(G){var F=f(this),E=F.data(B);if(E&&typeof G==="string"){if(E[G]){return E[G].apply(this,Array.prototype.slice.call(arguments,1))}else{f.error("Method "+G+" does not exist on jQuery.swipe")}}else{if(!E&&(typeof G==="object"||!G)){return w.apply(this,arguments)}}return F};f.fn.swipe.defaults=n;f.fn.swipe.phases={PHASE_START:g,PHASE_MOVE:k,PHASE_END:h,PHASE_CANCEL:q};f.fn.swipe.directions={LEFT:p,RIGHT:o,UP:e,DOWN:x,IN:c,OUT:z};f.fn.swipe.pageScroll={NONE:m,HORIZONTAL:D,VERTICAL:u,AUTO:s};f.fn.swipe.fingers={ONE:1,TWO:2,THREE:3,ALL:i};function w(E){if(E&&(E.allowPageScroll===undefined&&(E.swipe!==undefined||E.swipeStatus!==undefined))){E.allowPageScroll=m}if(E.click!==undefined&&E.tap===undefined){E.tap=E.click}if(!E){E={}}E=f.extend({},f.fn.swipe.defaults,E);return this.each(function(){var G=f(this);var F=G.data(B);if(!F){F=new C(this,E);G.data(B,F)}})}function C(a4,av){var az=(a||d||!av.fallbackToMouseEvents),J=az?(d?(v?"MSPointerDown":"pointerdown"):"touchstart"):"mousedown",ay=az?(d?(v?"MSPointerMove":"pointermove"):"touchmove"):"mousemove",U=az?(d?(v?"MSPointerUp":"pointerup"):"touchend"):"mouseup",S=az?null:"mouseleave",aD=(d?(v?"MSPointerCancel":"pointercancel"):"touchcancel");var ag=0,aP=null,ab=0,a1=0,aZ=0,G=1,aq=0,aJ=0,M=null;var aR=f(a4);var Z="start";var W=0;var aQ=null;var T=0,a2=0,a5=0,ad=0,N=0;var aW=null,af=null;try{aR.bind(J,aN);aR.bind(aD,a9)}catch(ak){f.error("events not supported "+J+","+aD+" on jQuery.swipe")}this.enable=function(){aR.bind(J,aN);aR.bind(aD,a9);return aR};this.disable=function(){aK();return aR};this.destroy=function(){aK();aR.data(B,null);return aR};this.option=function(bc,bb){if(av[bc]!==undefined){if(bb===undefined){return av[bc]}else{av[bc]=bb}}else{f.error("Option "+bc+" does not exist on jQuery.swipe.options")}return null};function aN(bd){if(aB()){return}if(f(bd.target).closest(av.excludedElements,aR).length>0){return}var be=bd.originalEvent?bd.originalEvent:bd;var bc,bb=a?be.touches[0]:be;Z=g;if(a){W=be.touches.length}else{bd.preventDefault()}ag=0;aP=null;aJ=null;ab=0;a1=0;aZ=0;G=1;aq=0;aQ=aj();M=aa();R();if(!a||(W===av.fingers||av.fingers===i)||aX()){ai(0,bb);T=at();if(W==2){ai(1,be.touches[1]);a1=aZ=au(aQ[0].start,aQ[1].start)}if(av.swipeStatus||av.pinchStatus){bc=O(be,Z)}}else{bc=false}if(bc===false){Z=q;O(be,Z);return bc}else{if(av.hold){af=setTimeout(f.proxy(function(){aR.trigger("hold",[be.target]);if(av.hold){bc=av.hold.call(aR,be,be.target)}},this),av.longTapThreshold)}ao(true)}return null}function a3(be){var bh=be.originalEvent?be.originalEvent:be;if(Z===h||Z===q||am()){return}var bd,bc=a?bh.touches[0]:bh;var bf=aH(bc);a2=at();if(a){W=bh.touches.length}if(av.hold){clearTimeout(af)}Z=k;if(W==2){if(a1==0){ai(1,bh.touches[1]);a1=aZ=au(aQ[0].start,aQ[1].start)}else{aH(bh.touches[1]);aZ=au(aQ[0].end,aQ[1].end);aJ=ar(aQ[0].end,aQ[1].end)}G=a7(a1,aZ);aq=Math.abs(a1-aZ)}if((W===av.fingers||av.fingers===i)||!a||aX()){aP=aL(bf.start,bf.end);al(be,aP);ag=aS(bf.start,bf.end);ab=aM();aI(aP,ag);if(av.swipeStatus||av.pinchStatus){bd=O(bh,Z)}if(!av.triggerOnTouchEnd||av.triggerOnTouchLeave){var bb=true;if(av.triggerOnTouchLeave){var bg=aY(this);bb=E(bf.end,bg)}if(!av.triggerOnTouchEnd&&bb){Z=aC(k)}else{if(av.triggerOnTouchLeave&&!bb){Z=aC(h)}}if(Z==q||Z==h){O(bh,Z)}}}else{Z=q;O(bh,Z)}if(bd===false){Z=q;O(bh,Z)}}function L(bb){var bc=bb.originalEvent;if(a){if(bc.touches.length>0){F();return true}}if(am()){W=ad}a2=at();ab=aM();if(ba()||!an()){Z=q;O(bc,Z)}else{if(av.triggerOnTouchEnd||(av.triggerOnTouchEnd==false&&Z===k)){bb.preventDefault();Z=h;O(bc,Z)}else{if(!av.triggerOnTouchEnd&&a6()){Z=h;aF(bc,Z,A)}else{if(Z===k){Z=q;O(bc,Z)}}}}ao(false);return null}function a9(){W=0;a2=0;T=0;a1=0;aZ=0;G=1;R();ao(false)}function K(bb){var bc=bb.originalEvent;if(av.triggerOnTouchLeave){Z=aC(h);O(bc,Z)}}function aK(){aR.unbind(J,aN);aR.unbind(aD,a9);aR.unbind(ay,a3);aR.unbind(U,L);if(S){aR.unbind(S,K)}ao(false)}function aC(bf){var be=bf;var bd=aA();var bc=an();var bb=ba();if(!bd||bb){be=q}else{if(bc&&bf==k&&(!av.triggerOnTouchEnd||av.triggerOnTouchLeave)){be=h}else{if(!bc&&bf==h&&av.triggerOnTouchLeave){be=q}}}return be}function O(bd,bb){var bc=undefined;if(I()||V()){bc=aF(bd,bb,l)}else{if((P()||aX())&&bc!==false){bc=aF(bd,bb,t)}}if(aG()&&bc!==false){bc=aF(bd,bb,j)}else{if(ap()&&bc!==false){bc=aF(bd,bb,b)}else{if(ah()&&bc!==false){bc=aF(bd,bb,A)}}}if(bb===q){a9(bd)}if(bb===h){if(a){if(bd.touches.length==0){a9(bd)}}else{a9(bd)}}return bc}function aF(be,bb,bd){var bc=undefined;if(bd==l){aR.trigger("swipeStatus",[bb,aP||null,ag||0,ab||0,W,aQ]);if(av.swipeStatus){bc=av.swipeStatus.call(aR,be,bb,aP||null,ag||0,ab||0,W,aQ);if(bc===false){return false}}if(bb==h&&aV()){aR.trigger("swipe",[aP,ag,ab,W,aQ]);if(av.swipe){bc=av.swipe.call(aR,be,aP,ag,ab,W,aQ);if(bc===false){return false}}switch(aP){case p:aR.trigger("swipeLeft",[aP,ag,ab,W,aQ]);if(av.swipeLeft){bc=av.swipeLeft.call(aR,be,aP,ag,ab,W,aQ)}break;case o:aR.trigger("swipeRight",[aP,ag,ab,W,aQ]);if(av.swipeRight){bc=av.swipeRight.call(aR,be,aP,ag,ab,W,aQ)}break;case e:aR.trigger("swipeUp",[aP,ag,ab,W,aQ]);if(av.swipeUp){bc=av.swipeUp.call(aR,be,aP,ag,ab,W,aQ)}break;case x:aR.trigger("swipeDown",[aP,ag,ab,W,aQ]);if(av.swipeDown){bc=av.swipeDown.call(aR,be,aP,ag,ab,W,aQ)}break}}}if(bd==t){aR.trigger("pinchStatus",[bb,aJ||null,aq||0,ab||0,W,G,aQ]);if(av.pinchStatus){bc=av.pinchStatus.call(aR,be,bb,aJ||null,aq||0,ab||0,W,G,aQ);if(bc===false){return false}}if(bb==h&&a8()){switch(aJ){case c:aR.trigger("pinchIn",[aJ||null,aq||0,ab||0,W,G,aQ]);if(av.pinchIn){bc=av.pinchIn.call(aR,be,aJ||null,aq||0,ab||0,W,G,aQ)}break;case z:aR.trigger("pinchOut",[aJ||null,aq||0,ab||0,W,G,aQ]);if(av.pinchOut){bc=av.pinchOut.call(aR,be,aJ||null,aq||0,ab||0,W,G,aQ)}break}}}if(bd==A){if(bb===q||bb===h){clearTimeout(aW);clearTimeout(af);if(Y()&&!H()){N=at();aW=setTimeout(f.proxy(function(){N=null;aR.trigger("tap",[be.target]);if(av.tap){bc=av.tap.call(aR,be,be.target)}},this),av.doubleTapThreshold)}else{N=null;aR.trigger("tap",[be.target]);if(av.tap){bc=av.tap.call(aR,be,be.target)}}}}else{if(bd==j){if(bb===q||bb===h){clearTimeout(aW);N=null;aR.trigger("doubletap",[be.target]);if(av.doubleTap){bc=av.doubleTap.call(aR,be,be.target)}}}else{if(bd==b){if(bb===q||bb===h){clearTimeout(aW);N=null;aR.trigger("longtap",[be.target]);if(av.longTap){bc=av.longTap.call(aR,be,be.target)}}}}}return bc}function an(){var bb=true;if(av.threshold!==null){bb=ag>=av.threshold}return bb}function ba(){var bb=false;if(av.cancelThreshold!==null&&aP!==null){bb=(aT(aP)-ag)>=av.cancelThreshold}return bb}function ae(){if(av.pinchThreshold!==null){return aq>=av.pinchThreshold}return true}function aA(){var bb;if(av.maxTimeThreshold){if(ab>=av.maxTimeThreshold){bb=false}else{bb=true}}else{bb=true}return bb}function al(bb,bc){if(av.allowPageScroll===m||aX()){bb.preventDefault()}else{var bd=av.allowPageScroll===s;switch(bc){case p:if((av.swipeLeft&&bd)||(!bd&&av.allowPageScroll!=D)){bb.preventDefault()}break;case o:if((av.swipeRight&&bd)||(!bd&&av.allowPageScroll!=D)){bb.preventDefault()}break;case e:if((av.swipeUp&&bd)||(!bd&&av.allowPageScroll!=u)){bb.preventDefault()}break;case x:if((av.swipeDown&&bd)||(!bd&&av.allowPageScroll!=u)){bb.preventDefault()}break}}}function a8(){var bc=aO();var bb=X();var bd=ae();return bc&&bb&&bd}function aX(){return !!(av.pinchStatus||av.pinchIn||av.pinchOut)}function P(){return !!(a8()&&aX())}function aV(){var be=aA();var bg=an();var bd=aO();var bb=X();var bc=ba();var bf=!bc&&bb&&bd&&bg&&be;return bf}function V(){return !!(av.swipe||av.swipeStatus||av.swipeLeft||av.swipeRight||av.swipeUp||av.swipeDown)}function I(){return !!(aV()&&V())}function aO(){return((W===av.fingers||av.fingers===i)||!a)}function X(){return aQ[0].end.x!==0}function a6(){return !!(av.tap)}function Y(){return !!(av.doubleTap)}function aU(){return !!(av.longTap)}function Q(){if(N==null){return false}var bb=at();return(Y()&&((bb-N)<=av.doubleTapThreshold))}function H(){return Q()}function ax(){return((W===1||!a)&&(isNaN(ag)||ag<av.threshold))}function a0(){return((ab>av.longTapThreshold)&&(ag<r))}function ah(){return !!(ax()&&a6())}function aG(){return !!(Q()&&Y())}function ap(){return !!(a0()&&aU())}function F(){a5=at();ad=event.touches.length+1}function R(){a5=0;ad=0}function am(){var bb=false;if(a5){var bc=at()-a5;if(bc<=av.fingerReleaseThreshold){bb=true}}return bb}function aB(){return !!(aR.data(B+"_intouch")===true)}function ao(bb){if(bb===true){aR.bind(ay,a3);aR.bind(U,L);if(S){aR.bind(S,K)}}else{aR.unbind(ay,a3,false);aR.unbind(U,L,false);if(S){aR.unbind(S,K,false)}}aR.data(B+"_intouch",bb===true)}function ai(bc,bb){var bd=bb.identifier!==undefined?bb.identifier:0;aQ[bc].identifier=bd;aQ[bc].start.x=aQ[bc].end.x=bb.pageX||bb.clientX;aQ[bc].start.y=aQ[bc].end.y=bb.pageY||bb.clientY;return aQ[bc]}function aH(bb){var bd=bb.identifier!==undefined?bb.identifier:0;var bc=ac(bd);bc.end.x=bb.pageX||bb.clientX;bc.end.y=bb.pageY||bb.clientY;return bc}function ac(bc){for(var bb=0;bb<aQ.length;bb++){if(aQ[bb].identifier==bc){return aQ[bb]}}}function aj(){var bb=[];for(var bc=0;bc<=5;bc++){bb.push({start:{x:0,y:0},end:{x:0,y:0},identifier:0})}return bb}function aI(bb,bc){bc=Math.max(bc,aT(bb));M[bb].distance=bc}function aT(bb){if(M[bb]){return M[bb].distance}return undefined}function aa(){var bb={};bb[p]=aw(p);bb[o]=aw(o);bb[e]=aw(e);bb[x]=aw(x);return bb}function aw(bb){return{direction:bb,distance:0}}function aM(){return a2-T}function au(be,bd){var bc=Math.abs(be.x-bd.x);var bb=Math.abs(be.y-bd.y);return Math.round(Math.sqrt(bc*bc+bb*bb))}function a7(bb,bc){var bd=(bc/bb)*1;return bd.toFixed(2)}function ar(){if(G<1){return z}else{return c}}function aS(bc,bb){return Math.round(Math.sqrt(Math.pow(bb.x-bc.x,2)+Math.pow(bb.y-bc.y,2)))}function aE(be,bc){var bb=be.x-bc.x;var bg=bc.y-be.y;var bd=Math.atan2(bg,bb);var bf=Math.round(bd*180/Math.PI);if(bf<0){bf=360-Math.abs(bf)}return bf}function aL(bc,bb){var bd=aE(bc,bb);if((bd<=45)&&(bd>=0)){return p}else{if((bd<=360)&&(bd>=315)){return p}else{if((bd>=135)&&(bd<=225)){return o}else{if((bd>45)&&(bd<135)){return x}else{return e}}}}}function at(){var bb=new Date();return bb.getTime()}function aY(bb){bb=f(bb);var bd=bb.offset();var bc={left:bd.left,right:bd.left+bb.outerWidth(),top:bd.top,bottom:bd.top+bb.outerHeight()};return bc}function E(bb,bc){return(bb.x>bc.left&&bb.x<bc.right&&bb.y>bc.top&&bb.y<bc.bottom)}}}));
/*!
* Velocity.js: Accelerated JavaScript animation.
* @version 0.2.1
* @docs http://velocityjs.org
* @license Copyright 2014 Julian Shapiro. MIT License: http://en.wikipedia.org/wiki/MIT_License
*/
!function(a,b,c,d){function e(a){for(var b=-1,c=a?a.length:0,d=[];++b<c;){var e=a[b];e&&d.push(e)}return d}function f(a){var b=q.data(a,k);return null===b?d:b}function g(a){return function(b){return Math.round(b*a)*(1/a)}}function h(a,b){var c=a;return p.isString(a)?r.Easings[a]||(c=!1):c=p.isArray(a)&&1===a.length?g.apply(null,a):p.isArray(a)&&2===a.length?t.apply(null,a.concat([b])):p.isArray(a)&&4===a.length?s.apply(null,a):!1,c===!1&&(c=r.Easings[r.defaults.easing]?r.defaults.easing:m),c}function i(a){if(a)for(var b=(new Date).getTime(),c=0,e=r.State.calls.length;e>c;c++)if(r.State.calls[c]){var g=r.State.calls[c],h=g[0],k=g[2],l=g[3];l||(l=r.State.calls[c][3]=b-16);for(var m=Math.min((b-l)/k.duration,1),n=0,q=h.length;q>n;n++){var s=h[n],t=s.element;if(f(t)){var v=!1;k.display&&"none"!==k.display&&u.setPropertyValue(t,"display",k.display);for(var w in s)if("element"!==w){var x,y=s[w],z=p.isString(y.easing)?r.Easings[y.easing]:y.easing;if(x=1===m?y.endValue:y.startValue+(y.endValue-y.startValue)*z(m),y.currentValue=x,u.Hooks.registered[w]){var A=u.Hooks.getRoot(w),B=f(t).rootPropertyValueCache[A];B&&(y.rootPropertyValue=B)}var C=u.setPropertyValue(t,w,y.currentValue+(0===parseFloat(x)?"":y.unitType),y.rootPropertyValue,y.scrollData);u.Hooks.registered[w]&&(f(t).rootPropertyValueCache[A]=u.Normalizations.registered[A]?u.Normalizations.registered[A]("extract",null,C[1]):C[1]),"transform"===C[0]&&(v=!0)}k.mobileHA&&f(t).transformCache.translate3d===d&&(f(t).transformCache.translate3d="(0px, 0px, 0px)",v=!0),v&&u.flushTransformCache(t)}}k.display&&"none"!==k.display&&(r.State.calls[c][2].display=!1),k.progress&&k.progress.call(g[1],g[1],m,Math.max(0,l+k.duration-b),l),1===m&&j(c)}r.State.isTicking&&o(i)}function j(a,b){if(!r.State.calls[a])return!1;for(var c=r.State.calls[a][0],e=r.State.calls[a][1],g=r.State.calls[a][2],h=!1,i=0,j=c.length;j>i;i++){var k=c[i].element;if(b||"none"!==g.display||g.loop||u.setPropertyValue(k,"display",g.display),(q.queue(k)[1]===d||!/\.velocityQueueEntryFlag/i.test(q.queue(k)[1]))&&f(k)){f(k).isAnimating=!1,f(k).rootPropertyValueCache={};var l,m=["transformPerspective","translateZ","rotateX","rotateY"],n=!1;for(var o in m)l=m[o],/^\(0[^.]/.test(f(k).transformCache[l])&&(n=!0,delete f(k).transformCache[l]);g.mobileHA&&(n=!0,delete f(k).transformCache.translate3d),n&&u.flushTransformCache(k)}b||!g.complete||g.loop||i!==j-1||g.complete.call(e,e),g.queue!==!1&&q.dequeue(k,g.queue)}r.State.calls[a]=!1;for(var p=0,s=r.State.calls.length;s>p;p++)if(r.State.calls[p]!==!1){h=!0;break}h===!1&&(r.State.isTicking=!1,delete r.State.calls,r.State.calls=[])}var k="velocity",l=400,m="swing",n=function(){if(c.documentMode)return c.documentMode;for(var a=7;a>4;a--){var b=c.createElement("div");if(b.innerHTML="<!--[if IE "+a+"]><span></span><![endif]-->",b.getElementsByTagName("span").length)return b=null,a}return d}(),o=b.requestAnimationFrame||function(){var a=0;return b.webkitRequestAnimationFrame||b.mozRequestAnimationFrame||function(b){var c,d=(new Date).getTime();return c=Math.max(0,16-(d-a)),a=d+c,setTimeout(function(){b(d+c)},c)}}(),p={isString:function(a){return"string"==typeof a},isArray:Array.isArray||function(a){return"[object Array]"===Object.prototype.toString.call(a)},isFunction:function(a){return"[object Function]"===Object.prototype.toString.call(a)},isNodeList:function(a){return"object"==typeof a&&/^\[object (HTMLCollection|NodeList|Object)\]$/.test(Object.prototype.toString.call(a))&&a.length!==d&&(0===a.length||"object"==typeof a[0]&&a[0].nodeType>0)},isWrapped:function(a){return a&&(a.jquery||b.Zepto&&b.Zepto.zepto.isZ(a))}},q=b.jQuery||a.Velocity&&a.Velocity.Utilities;if(!q)throw new Error("Velocity: Either jQuery or Velocity's jQuery shim must first be loaded.");if(a.Velocity!==d&&!a.Velocity.Utilities)throw new Error("Velocity: Namespace is occupied.");if(7>=n){if(b.jQuery)return void(b.jQuery.fn.velocity=b.jQuery.fn.animate);throw new Error("Velocity: For IE<=7, Velocity falls back to jQuery, which must first be loaded.")}if(8===n&&!b.jQuery)throw new Error("Velocity: For IE8, Velocity requires jQuery to be loaded. (Velocity's jQuery shim does not work with IE8.)");var r=a.Velocity=a.velocity={State:{isMobile:/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),isAndroid:/Android/i.test(navigator.userAgent),isGingerbread:/Android 2\.3\.[3-7]/i.test(navigator.userAgent),prefixElement:c.createElement("div"),prefixMatches:{},scrollAnchor:null,scrollPropertyLeft:null,scrollPropertyTop:null,isTicking:!1,calls:[]},CSS:{},Utilities:b.jQuery?{}:q,Sequences:{},Easings:{},defaults:{queue:"",duration:l,easing:m,begin:null,complete:null,progress:null,display:null,loop:!1,delay:!1,mobileHA:!0,_cacheValues:!0},animate:function(){},mock:!1,debug:!1};b.pageYOffset!==d?(r.State.scrollAnchor=b,r.State.scrollPropertyLeft="pageXOffset",r.State.scrollPropertyTop="pageYOffset"):(r.State.scrollAnchor=c.documentElement||c.body.parentNode||c.body,r.State.scrollPropertyLeft="scrollLeft",r.State.scrollPropertyTop="scrollTop");var s=function(){function a(a,b){return 1-3*b+3*a}function b(a,b){return 3*b-6*a}function c(a){return 3*a}function d(d,e,f){return((a(e,f)*d+b(e,f))*d+c(e))*d}function e(d,e,f){return 3*a(e,f)*d*d+2*b(e,f)*d+c(e)}return function(a,b,c,f){function g(b){for(var f=b,g=0;8>g;++g){var h=e(f,a,c);if(0===h)return f;var i=d(f,a,c)-b;f-=i/h}return f}if(4!==arguments.length)return!1;for(var h=0;4>h;++h)if("number"!=typeof arguments[h]||isNaN(arguments[h])||!isFinite(arguments[h]))return!1;return a=Math.min(a,1),c=Math.min(c,1),a=Math.max(a,0),c=Math.max(c,0),function(e){return a===b&&c===f?e:d(g(e),b,f)}}}(),t=function(){function a(a){return-a.tension*a.x-a.friction*a.v}function b(b,c,d){var e={x:b.x+d.dx*c,v:b.v+d.dv*c,tension:b.tension,friction:b.friction};return{dx:e.v,dv:a(e)}}function c(c,d){var e={dx:c.v,dv:a(c)},f=b(c,.5*d,e),g=b(c,.5*d,f),h=b(c,d,g),i=1/6*(e.dx+2*(f.dx+g.dx)+h.dx),j=1/6*(e.dv+2*(f.dv+g.dv)+h.dv);return c.x=c.x+i*d,c.v=c.v+j*d,c}return function d(a,b,e){var f,g,h,i={x:-1,v:0,tension:null,friction:null},j=[0],k=0,l=1e-4,m=.016;for(a=parseFloat(a)||600,b=parseFloat(b)||20,e=e||null,i.tension=a,i.friction=b,f=null!==e,f?(k=d(a,b),g=k/e*m):g=m;;)if(h=c(h||i,g),j.push(1+h.x),k+=16,!(Math.abs(h.x)>l&&Math.abs(h.v)>l))break;return f?function(a){return j[a*(j.length-1)|0]}:k}}();!function(){r.Easings.linear=function(a){return a},r.Easings.swing=function(a){return.5-Math.cos(a*Math.PI)/2},r.Easings.ease=s(.25,.1,.25,1),r.Easings["ease-in"]=s(.42,0,1,1),r.Easings["ease-out"]=s(0,0,.58,1),r.Easings["ease-in-out"]=s(.42,0,.58,1);var a={};q.each(["Quad","Cubic","Quart","Quint","Expo"],function(b,c){a[c]=function(a){return Math.pow(a,b+2)}}),q.extend(a,{Sine:function(a){return 1-Math.cos(a*Math.PI/2)},Circ:function(a){return 1-Math.sqrt(1-a*a)},Elastic:function(a){return 0===a||1===a?a:-Math.pow(2,8*(a-1))*Math.sin((80*(a-1)-7.5)*Math.PI/15)},Back:function(a){return a*a*(3*a-2)},Bounce:function(a){for(var b,c=4;a<((b=Math.pow(2,--c))-1)/11;);return 1/Math.pow(4,3-c)-7.5625*Math.pow((3*b-2)/22-a,2)}}),q.each(a,function(a,b){r.Easings["easeIn"+a]=b,r.Easings["easeOut"+a]=function(a){return 1-b(1-a)},r.Easings["easeInOut"+a]=function(a){return.5>a?b(2*a)/2:1-b(-2*a+2)/2}}),r.Easings.spring=function(a){return 1-Math.cos(4.5*a*Math.PI)*Math.exp(6*-a)}}();var u=r.CSS={RegEx:{valueUnwrap:/^[A-z]+\((.*)\)$/i,wrappedValueAlreadyExtracted:/[0-9.]+ [0-9.]+ [0-9.]+( [0-9.]+)?/,valueSplit:/([A-z]+\(.+\))|(([A-z0-9#-.]+?)(?=\s|$))/gi},Hooks:{templates:{color:["Red Green Blue Alpha","255 255 255 1"],backgroundColor:["Red Green Blue Alpha","255 255 255 1"],borderColor:["Red Green Blue Alpha","255 255 255 1"],borderTopColor:["Red Green Blue Alpha","255 255 255 1"],borderRightColor:["Red Green Blue Alpha","255 255 255 1"],borderBottomColor:["Red Green Blue Alpha","255 255 255 1"],borderLeftColor:["Red Green Blue Alpha","255 255 255 1"],outlineColor:["Red Green Blue Alpha","255 255 255 1"],textShadow:["Color X Y Blur","black 0px 0px 0px"],boxShadow:["Color X Y Blur Spread","black 0px 0px 0px 0px"],clip:["Top Right Bottom Left","0px 0px 0px 0px"],backgroundPosition:["X Y","0% 0%"],transformOrigin:["X Y Z","50% 50% 0%"],perspectiveOrigin:["X Y","50% 50%"]},registered:{},register:function(){var a,b,c;if(n)for(a in u.Hooks.templates){b=u.Hooks.templates[a],c=b[0].split(" ");var d=b[1].match(u.RegEx.valueSplit);"Color"===c[0]&&(c.push(c.shift()),d.push(d.shift()),u.Hooks.templates[a]=[c.join(" "),d.join(" ")])}for(a in u.Hooks.templates){b=u.Hooks.templates[a],c=b[0].split(" ");for(var e in c){var f=a+c[e],g=e;u.Hooks.registered[f]=[a,g]}}},getRoot:function(a){var b=u.Hooks.registered[a];return b?b[0]:a},cleanRootPropertyValue:function(a,b){return u.RegEx.valueUnwrap.test(b)&&(b=b.match(u.Hooks.RegEx.valueUnwrap)[1]),u.Values.isCSSNullValue(b)&&(b=u.Hooks.templates[a][1]),b},extractValue:function(a,b){var c=u.Hooks.registered[a];if(c){var d=c[0],e=c[1];return b=u.Hooks.cleanRootPropertyValue(d,b),b.toString().match(u.RegEx.valueSplit)[e]}return b},injectValue:function(a,b,c){var d=u.Hooks.registered[a];if(d){var e,f,g=d[0],h=d[1];return c=u.Hooks.cleanRootPropertyValue(g,c),e=c.toString().match(u.RegEx.valueSplit),e[h]=b,f=e.join(" ")}return c}},Normalizations:{registered:{clip:function(a,b,c){switch(a){case"name":return"clip";case"extract":var d;return u.RegEx.wrappedValueAlreadyExtracted.test(c)?d=c:(d=c.toString().match(u.RegEx.valueUnwrap),d=d?d[1].replace(/,(\s+)?/g," "):c),d;case"inject":return"rect("+c+")"}},opacity:function(a,b,c){if(8>=n)switch(a){case"name":return"filter";case"extract":var d=c.toString().match(/alpha\(opacity=(.*)\)/i);return c=d?d[1]/100:1;case"inject":return b.style.zoom=1,parseFloat(c)>=1?"":"alpha(opacity="+parseInt(100*parseFloat(c),10)+")"}else switch(a){case"name":return"opacity";case"extract":return c;case"inject":return c}}},register:function(){function a(a){var b,c=/^#?([a-f\d])([a-f\d])([a-f\d])$/i,d=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i;return a=a.replace(c,function(a,b,c,d){return b+b+c+c+d+d}),b=d.exec(a),b?"rgb("+(parseInt(b[1],16)+" "+parseInt(b[2],16)+" "+parseInt(b[3],16))+")":"rgb(0 0 0)"}var b=["translateX","translateY","scale","scaleX","scaleY","skewX","skewY","rotateZ"];9>=n||r.State.isGingerbread||(b=b.concat(["transformPerspective","translateZ","scaleZ","rotateX","rotateY"]));for(var c=0,e=b.length;e>c;c++)!function(){var a=b[c];u.Normalizations.registered[a]=function(b,c,e){switch(b){case"name":return"transform";case"extract":return f(c).transformCache[a]===d?/^scale/i.test(a)?1:0:f(c).transformCache[a].replace(/[()]/g,"");case"inject":var g=!1;switch(a.substr(0,a.length-1)){case"translate":g=!/(%|px|em|rem|\d)$/i.test(e);break;case"scal":case"scale":r.State.isAndroid&&f(c).transformCache[a]===d&&(e=1),g=!/(\d)$/i.test(e);break;case"skew":g=!/(deg|\d)$/i.test(e);break;case"rotate":g=!/(deg|\d)$/i.test(e)}return g||(f(c).transformCache[a]="("+e+")"),f(c).transformCache[a]}}}();for(var g=["color","backgroundColor","borderColor","borderTopColor","borderRightColor","borderBottomColor","borderLeftColor","outlineColor"],c=0,h=g.length;h>c;c++)!function(){var b=g[c];u.Normalizations.registered[b]=function(c,e,f){switch(c){case"name":return b;case"extract":var g;if(u.RegEx.wrappedValueAlreadyExtracted.test(f))g=f;else{var h,i={aqua:"rgb(0, 255, 255);",black:"rgb(0, 0, 0)",blue:"rgb(0, 0, 255)",fuchsia:"rgb(255, 0, 255)",gray:"rgb(128, 128, 128)",green:"rgb(0, 128, 0)",lime:"rgb(0, 255, 0)",maroon:"rgb(128, 0, 0)",navy:"rgb(0, 0, 128)",olive:"rgb(128, 128, 0)",purple:"rgb(128, 0, 128)",red:"rgb(255, 0, 0)",silver:"rgb(192, 192, 192)",teal:"rgb(0, 128, 128)",white:"rgb(255, 255, 255)",yellow:"rgb(255, 255, 0)"};/^[A-z]+$/i.test(f)?h=i[f]!==d?i[f]:i.black:/^#([A-f\d]{3}){1,2}$/i.test(f)?h=a(f):/^rgba?\(/i.test(f)||(h=i.black),g=(h||f).toString().match(u.RegEx.valueUnwrap)[1].replace(/,(\s+)?/g," ")}return 8>=n||3!==g.split(" ").length||(g+=" 1"),g;case"inject":return 8>=n?4===f.split(" ").length&&(f=f.split(/\s+/).slice(0,3).join(" ")):3===f.split(" ").length&&(f+=" 1"),(8>=n?"rgb":"rgba")+"("+f.replace(/\s+/g,",").replace(/\.(\d)+(?=,)/g,"")+")"}}}()}},Names:{camelCase:function(a){return a.replace(/-(\w)/g,function(a,b){return b.toUpperCase()})},prefixCheck:function(a){if(r.State.prefixMatches[a])return[r.State.prefixMatches[a],!0];for(var b=["","Webkit","Moz","ms","O"],c=0,d=b.length;d>c;c++){var e;if(e=0===c?a:b[c]+a.replace(/^\w/,function(a){return a.toUpperCase()}),p.isString(r.State.prefixElement.style[e]))return r.State.prefixMatches[a]=e,[e,!0]}return[a,!1]}},Values:{isCSSNullValue:function(a){return 0==a||/^(none|auto|transparent|(rgba\(0, ?0, ?0, ?0\)))$/i.test(a)},getUnitType:function(a){return/^(rotate|skew)/i.test(a)?"deg":/(^(scale|scaleX|scaleY|scaleZ|opacity|alpha|fillOpacity|flexGrow|flexHeight|zIndex|fontWeight)$)|color/i.test(a)?"":"px"},getDisplayType:function(a){var b=a.tagName.toString().toLowerCase();return/^(b|big|i|small|tt|abbr|acronym|cite|code|dfn|em|kbd|strong|samp|var|a|bdo|br|img|map|object|q|script|span|sub|sup|button|input|label|select|textarea)$/i.test(b)?"inline":/^(li)$/i.test(b)?"list-item":/^(tr)$/i.test(b)?"table-row":"block"}},getPropertyValue:function(a,c,e,g){function h(a,c){var e=0;if(8>=n)e=q.css(a,c);else{if(!g){if("height"===c&&"border-box"!==u.getPropertyValue(a,"boxSizing").toString().toLowerCase())return a.offsetHeight-(parseFloat(u.getPropertyValue(a,"borderTopWidth"))||0)-(parseFloat(u.getPropertyValue(a,"borderBottomWidth"))||0)-(parseFloat(u.getPropertyValue(a,"paddingTop"))||0)-(parseFloat(u.getPropertyValue(a,"paddingBottom"))||0);if("width"===c&&"border-box"!==u.getPropertyValue(a,"boxSizing").toString().toLowerCase())return a.offsetWidth-(parseFloat(u.getPropertyValue(a,"borderLeftWidth"))||0)-(parseFloat(u.getPropertyValue(a,"borderRightWidth"))||0)-(parseFloat(u.getPropertyValue(a,"paddingLeft"))||0)-(parseFloat(u.getPropertyValue(a,"paddingRight"))||0)}var i;i=f(a)===d?b.getComputedStyle(a,null):f(a).computedStyle?f(a).computedStyle:f(a).computedStyle=b.getComputedStyle(a,null),n&&"borderColor"===c&&(c="borderTopColor"),e=9===n&&"filter"===c?i.getPropertyValue(c):i[c],(""===e||null===e)&&(e=a.style[c])}if("auto"===e&&/^(top|right|bottom|left)$/i.test(c)){var j=h(a,"position");("fixed"===j||"absolute"===j&&/top|left/i.test(c))&&(e=q(a).position()[c]+"px")}return e}var i;if(u.Hooks.registered[c]){var j=c,k=u.Hooks.getRoot(j);e===d&&(e=u.getPropertyValue(a,u.Names.prefixCheck(k)[0])),u.Normalizations.registered[k]&&(e=u.Normalizations.registered[k]("extract",a,e)),i=u.Hooks.extractValue(j,e)}else if(u.Normalizations.registered[c]){var l,m;l=u.Normalizations.registered[c]("name",a),"transform"!==l&&(m=h(a,u.Names.prefixCheck(l)[0]),u.Values.isCSSNullValue(m)&&u.Hooks.templates[c]&&(m=u.Hooks.templates[c][1])),i=u.Normalizations.registered[c]("extract",a,m)}return/^[\d-]/.test(i)||(i=h(a,u.Names.prefixCheck(c)[0])),u.Values.isCSSNullValue(i)&&(i=0),r.debug>=2&&console.log("Get "+c+": "+i),i},setPropertyValue:function(a,c,d,e,g){var h=c;if("scroll"===c)g.container?g.container["scroll"+g.direction]=d:"Left"===g.direction?b.scrollTo(d,g.alternateValue):b.scrollTo(g.alternateValue,d);else if(u.Normalizations.registered[c]&&"transform"===u.Normalizations.registered[c]("name",a))u.Normalizations.registered[c]("inject",a,d),h="transform",d=f(a).transformCache[c];else{if(u.Hooks.registered[c]){var i=c,j=u.Hooks.getRoot(c);e=e||u.getPropertyValue(a,j),d=u.Hooks.injectValue(i,d,e),c=j}if(u.Normalizations.registered[c]&&(d=u.Normalizations.registered[c]("inject",a,d),c=u.Normalizations.registered[c]("name",a)),h=u.Names.prefixCheck(c)[0],8>=n)try{a.style[h]=d}catch(k){console.log("Error setting ["+h+"] to ["+d+"]")}else a.style[h]=d;r.debug>=2&&console.log("Set "+c+" ("+h+"): "+d)}return[h,d]},flushTransformCache:function(a){var b,c,d,e="";for(b in f(a).transformCache)c=f(a).transformCache[b],"transformPerspective"!==b?(9===n&&"rotateZ"===b&&(b="rotate"),e+=b+c+" "):d=c;d&&(e="perspective"+d+" "+e),u.setPropertyValue(a,"transform",e)}};u.Hooks.register(),u.Normalizations.register(),r.animate=function(){function a(){return g||o}function b(){function a(){function a(a){var c=d,e=d,f=d;return p.isArray(a)?(c=a[0],!p.isArray(a[1])&&/^[\d-]/.test(a[1])||p.isFunction(a[1])?f=a[1]:(p.isString(a[1])||p.isArray(a[1]))&&(e=h(a[1],g.duration),a[2]&&(f=a[2]))):c=a,e=e||g.easing,p.isFunction(c)&&(c=c.call(b,x,w)),p.isFunction(f)&&(f=f.call(b,x,w)),[c||0,e,f]}function k(a,b){var c,d;return d=(b||0).toString().toLowerCase().replace(/[%A-z]+$/,function(a){return c=a,""}),c||(c=u.Values.getUnitType(a)),[d,c]}function l(){var a={parent:b.parentNode,position:u.getPropertyValue(b,"position"),fontSize:u.getPropertyValue(b,"fontSize")},d=a.position===E.lastPosition&&a.parent===E.lastParent,e=a.fontSize===E.lastFontSize&&a.parent===E.lastParent;E.lastParent=a.parent,E.lastPosition=a.position,E.lastFontSize=a.fontSize,null===E.remToPxRatio&&(E.remToPxRatio=parseFloat(u.getPropertyValue(c.body,"fontSize"))||16);var f={overflowX:null,overflowY:null,boxSizing:null,width:null,minWidth:null,maxWidth:null,height:null,minHeight:null,maxHeight:null,paddingLeft:null},g={},h=10;if(g.remToPxRatio=E.remToPxRatio,n)var i=/^auto$/i.test(b.currentStyle.width),j=/^auto$/i.test(b.currentStyle.height);d&&e||(f.overflowX=u.getPropertyValue(b,"overflowX"),f.overflowY=u.getPropertyValue(b,"overflowY"),f.boxSizing=u.getPropertyValue(b,"boxSizing"),f.width=u.getPropertyValue(b,"width",null,!0),f.minWidth=u.getPropertyValue(b,"minWidth"),f.maxWidth=u.getPropertyValue(b,"maxWidth")||"none",f.height=u.getPropertyValue(b,"height",null,!0),f.minHeight=u.getPropertyValue(b,"minHeight"),f.maxHeight=u.getPropertyValue(b,"maxHeight")||"none",f.paddingLeft=u.getPropertyValue(b,"paddingLeft")),d?(g.percentToPxRatioWidth=E.lastPercentToPxWidth,g.percentToPxRatioHeight=E.lastPercentToPxHeight):(u.setPropertyValue(b,"overflowX","hidden"),u.setPropertyValue(b,"overflowY","hidden"),u.setPropertyValue(b,"boxSizing","content-box"),u.setPropertyValue(b,"width",h+"%"),u.setPropertyValue(b,"minWidth",h+"%"),u.setPropertyValue(b,"maxWidth",h+"%"),u.setPropertyValue(b,"height",h+"%"),u.setPropertyValue(b,"minHeight",h+"%"),u.setPropertyValue(b,"maxHeight",h+"%")),e?g.emToPxRatio=E.lastEmToPx:u.setPropertyValue(b,"paddingLeft",h+"em"),d||(g.percentToPxRatioWidth=E.lastPercentToPxWidth=(parseFloat(u.getPropertyValue(b,"width",null,!0))||1)/h,g.percentToPxRatioHeight=E.lastPercentToPxHeight=(parseFloat(u.getPropertyValue(b,"height",null,!0))||1)/h),e||(g.emToPxRatio=E.lastEmToPx=(parseFloat(u.getPropertyValue(b,"paddingLeft"))||1)/h);for(var k in f)null!==f[k]&&u.setPropertyValue(b,k,f[k]);return n?(i&&u.setPropertyValue(b,"width","auto"),j&&u.setPropertyValue(b,"height","auto")):(u.setPropertyValue(b,"height","auto"),f.height!==u.getPropertyValue(b,"height",null,!0)&&u.setPropertyValue(b,"height",f.height),u.setPropertyValue(b,"width","auto"),f.width!==u.getPropertyValue(b,"width",null,!0)&&u.setPropertyValue(b,"width",f.width)),r.debug>=1&&console.log("Unit ratios: "+JSON.stringify(g),b),g}if(g.begin&&0===x&&g.begin.call(o,o),"scroll"===A){var m,v,y,z=/^x$/i.test(g.axis)?"Left":"Top",B=parseFloat(g.offset)||0;g.container?g.container.jquery||g.container.nodeType?(g.container=g.container[0]||g.container,m=g.container["scroll"+z],y=m+q(b).position()[z.toLowerCase()]+B):g.container=null:(m=r.State.scrollAnchor[r.State["scrollProperty"+z]],v=r.State.scrollAnchor[r.State["scrollProperty"+("Left"===z?"Top":"Left")]],y=q(b).offset()[z.toLowerCase()]+B),j={scroll:{rootPropertyValue:!1,startValue:m,currentValue:m,endValue:y,unitType:"",easing:g.easing,scrollData:{container:g.container,direction:z,alternateValue:v}},element:b}}else if("reverse"===A){if(!f(b).tweensContainer)return void q.dequeue(b,g.queue);"none"===f(b).opts.display&&(f(b).opts.display="block"),f(b).opts.loop=!1,f(b).opts.begin=null,f(b).opts.complete=null,t.easing||delete g.easing,t.duration||delete g.duration,g=q.extend({},f(b).opts,g);var C=q.extend(!0,{},f(b).tweensContainer);for(var D in C)if("element"!==D){var G=C[D].startValue;C[D].startValue=C[D].currentValue=C[D].endValue,C[D].endValue=G,t&&(C[D].easing=g.easing)}j=C}else if("start"===A){var C;f(b).tweensContainer&&f(b).isAnimating===!0&&(C=f(b).tweensContainer);for(var H in s){var I=a(s[H]),J=I[0],K=I[1],L=I[2];H=u.Names.camelCase(H);var M=u.Hooks.getRoot(H),N=!1;if(u.Names.prefixCheck(M)[1]!==!1||u.Normalizations.registered[M]!==d){g.display&&"none"!==g.display&&/opacity|filter/.test(H)&&!L&&0!==J&&(L=0),g._cacheValues&&C&&C[H]?(L===d&&(L=C[H].endValue+C[H].unitType),N=f(b).rootPropertyValueCache[M]):u.Hooks.registered[H]?L===d?(N=u.getPropertyValue(b,M),L=u.getPropertyValue(b,H,N)):N=u.Hooks.templates[M][1]:L===d&&(L=u.getPropertyValue(b,H));var O,P,Q,R;O=k(H,L),L=O[0],Q=O[1],O=k(H,J),J=O[0].replace(/^([+-\/*])=/,function(a,b){return R=b,""}),P=O[1],L=parseFloat(L)||0,J=parseFloat(J)||0;var S;if("%"===P&&(/^(fontSize|lineHeight)$/.test(H)?(J/=100,P="em"):/^scale/.test(H)?(J/=100,P=""):/(Red|Green|Blue)$/i.test(H)&&(J=J/100*255,P="")),/[\/*]/.test(R))P=Q;else if(Q!==P&&0!==L)if(0===J)P=Q;else{S=S||l();var T=/margin|padding|left|right|width|text|word|letter/i.test(H)||/X$/.test(H)?"x":"y";switch(Q){case"%":L*="x"===T?S.percentToPxRatioWidth:S.percentToPxRatioHeight;break;case"em":L*=S.emToPxRatio;break;case"rem":L*=S.remToPxRatio;break;case"px":}switch(P){case"%":L*=1/("x"===T?S.percentToPxRatioWidth:S.percentToPxRatioHeight);break;case"em":L*=1/S.emToPxRatio;break;case"rem":L*=1/S.remToPxRatio;break;case"px":}}switch(R){case"+":J=L+J;break;case"-":J=L-J;break;case"*":J=L*J;break;case"/":J=L/J}j[H]={rootPropertyValue:N,startValue:L,currentValue:L,endValue:J,unitType:P,easing:K},r.debug&&console.log("tweensContainer ("+H+"): "+JSON.stringify(j[H]),b)}else r.debug&&console.log("Skipping ["+M+"] due to a lack of browser support.")}j.element=b}j.element&&(F.push(j),f(b).tweensContainer=j,f(b).opts=g,f(b).isAnimating=!0,x===w-1?(r.State.calls.length>1e4&&(r.State.calls=e(r.State.calls)),r.State.calls.push([F,o,g]),r.State.isTicking===!1&&(r.State.isTicking=!0,i())):x++)}var b=this,g=q.extend({},r.defaults,t),j={};if(f(b)===d&&q.data(b,k,{isAnimating:!1,computedStyle:null,tweensContainer:null,rootPropertyValueCache:{},transformCache:{}}),/^\d/.test(g.delay)&&g.queue!==!1&&q.queue(b,g.queue,function(a){r.velocityQueueEntryFlag=!0,setTimeout(a,parseFloat(g.delay))}),r.mock===!0)g.duration=1;else switch(g.duration.toString().toLowerCase()){case"fast":g.duration=200;break;case"normal":g.duration=l;break;case"slow":g.duration=600;break;default:g.duration=parseFloat(g.duration)||1}g.easing=h(g.easing,g.duration),g.begin&&!p.isFunction(g.begin)&&(g.begin=null),g.progress&&!p.isFunction(g.progress)&&(g.progress=null),g.complete&&!p.isFunction(g.complete)&&(g.complete=null),g.display&&(g.display=g.display.toString().toLowerCase()),g.mobileHA=g.mobileHA&&r.State.isMobile&&!r.State.isGingerbread,g.queue===!1?g.delay?setTimeout(a,g.delay):a():q.queue(b,g.queue,function(b){r.velocityQueueEntryFlag=!0,a(b)}),""!==g.queue&&"fx"!==g.queue||"inprogress"===q.queue(b)[0]||q.dequeue(b)}var g,m,o,s,t,v=arguments[0]&&(q.isPlainObject(arguments[0].properties)&&!arguments[0].properties.names||p.isString(arguments[0].properties));if(p.isWrapped(this)?(m=0,o=this,g=this):(m=1,o=v?arguments[0].elements:arguments[0]),o=p.isWrapped(o)?[].slice.call(o):o){v?(s=arguments[0].properties,t=arguments[0].options):(s=arguments[m],t=arguments[m+1]);var w=p.isArray(o)||p.isNodeList(o)?o.length:1,x=0;if("stop"!==s&&!q.isPlainObject(t)){var y=m+1;t={};for(var z=y;z<arguments.length;z++)!p.isArray(arguments[z])&&/^\d/.test(arguments[z])?t.duration=parseFloat(arguments[z]):p.isString(arguments[z])||p.isArray(arguments[z])&&(1===arguments[z].length||2===arguments[z].length||4===arguments[z].length)?t.easing=arguments[z]:p.isFunction(arguments[z])&&(t.complete=arguments[z])}var A;switch(s){case"scroll":A="scroll";break;case"reverse":A="reverse";break;case"stop":var B=[];return q.each(r.State.calls,function(a,b){b!==!1&&q.each(b[1].nodeType?[b[1]]:b[1],function(b,c){q.each(o.nodeType?[o]:o,function(b,d){d===c&&(f(d)&&q.each(f(d).tweensContainer,function(a,b){b.endValue=b.currentValue}),(t===!0||p.isString(t))&&q.queue(d,p.isString(t)?t:"",[]),B.push(a))})})}),q.each(B,function(a,b){j(b,!0)}),a();default:if(!q.isPlainObject(s)||q.isEmptyObject(s)){if(p.isString(s)&&r.Sequences[s]){var C=o,D=t.duration;return t.backwards===!0&&(o=(o.jquery?[].slice.call(o):o).reverse()),q.each(o,function(a,b){parseFloat(t.stagger)&&(t.delay=parseFloat(t.stagger)*a),t.drag&&(t.duration=parseFloat(D)||(/^(callout|transition)/.test(s)?1e3:l),t.duration=Math.max(t.duration*(t.backwards?1-a/w:(a+1)/w),.75*t.duration,200)),r.Sequences[s].call(b,b,t||{},a,w)}),g||C}return console.log("First argument was not a property map, a known action, or a registered sequence. Aborting."),a()}A="start"}var E={lastParent:null,lastPosition:null,lastFontSize:null,lastPercentToPxWidth:null,lastPercentToPxHeight:null,lastEmToPx:null,remToPxRatio:null},F=[];q.each(o.nodeType?[o]:o,function(a,c){c.nodeType&&b.call(c)});var G,H=q.extend({},r.defaults,t);if(H.loop=parseInt(H.loop),G=2*H.loop-1,H.loop)for(var I=0;G>I;I++){var J={delay:H.delay};H.complete&&I===G-1&&(J.complete=H.complete),r.animate(o,"reverse",J)}return a()}};var v=b.jQuery||b.Zepto;v&&(v.fn.velocity=r.animate,v.fn.velocity.defaults=r.defaults),"undefined"!=typeof define&&define.amd?define(function(){return r}):"undefined"!=typeof module&&module.exports&&(module.exports=r),q.each(["Down","Up"],function(a,b){r.Sequences["slide"+b]=function(a,c){var d=q.extend({},c),e={height:null,marginTop:null,marginBottom:null,paddingTop:null,paddingBottom:null,overflow:null,overflowX:null,overflowY:null},f=d.begin,g=d.complete,h=!1;null!==d.display&&(d.display="Down"===b?d.display||r.CSS.Values.getDisplayType(a):d.display||"none"),d.begin=function(){function c(){a.style.display="block",e.height=r.CSS.getPropertyValue(a,"height"),a.style.height="auto",r.CSS.getPropertyValue(a,"height")===e.height&&(h=!0),r.CSS.setPropertyValue(a,"height",e.height+"px")}if("Down"===b){e.overflow=[r.CSS.getPropertyValue(a,"overflow"),0],e.overflowX=[r.CSS.getPropertyValue(a,"overflowX"),0],e.overflowY=[r.CSS.getPropertyValue(a,"overflowY"),0],a.style.overflow="hidden",a.style.overflowX="visible",a.style.overflowY="hidden",c();for(var d in e)/^overflow/.test(d)||(e[d]=[r.CSS.getPropertyValue(a,d),0]);a.style.display="none"}else{c();for(var d in e)e[d]=[0,r.CSS.getPropertyValue(a,d)];a.style.overflow="hidden",a.style.overflowX="visible",a.style.overflowY="hidden"}f&&f.call(a,a)},d.complete=function(a){var c="Down"===b?0:1;h===!0?e.height[c]="auto":e.height[c]+="px";for(var d in e)a.style[d]=e[d][c];g&&g.call(a,a)},r.animate(a,e,d)}}),q.each(["In","Out"],function(a,b){r.Sequences["fade"+b]=function(a,c,d,e){var f=q.extend({},c),g={opacity:"In"===b?1:0};d!==e-1&&(f.complete=f.begin=null),null!==f.display&&(f.display="In"===b?r.CSS.Values.getDisplayType(a):"none"),r.animate(this,g,f)}})}(window.jQuery||window.Zepto||window,window,document);
/* Parallax Slider By Dynamic Drive
* Created: Aug 19th, 2014 by DynamicDrive.com. This notice must stay intact for usage 
* Author: Dynamic Drive at http://www.dynamicdrive.com/
* Visit http://www.dynamicdrive.com/ for full source code
*/


function parallaxSlider(options){
	var $=jQuery
	this.setting={displaymode:{type:'auto', pause:2000, stoponclick:false, cycles:2, pauseonmouseover:true}, delaybtwdesc:500, activeslideclass:'selectedslide', orientation:'h', persist:true, slideduration:500, onUpdateSlide: function(currentSlide){}} //default settings
	jQuery.extend(this.setting, options) //merge default settings with options
	this.setting.displaymode.pause+=400+this.setting.slideduration // 400ms is default fade in time
	this.curslide=(this.setting.persist)? parallaxSlider.routines.getCookie("slider-"+this.setting.wrapperid) : 0
	this.curstep=0
	this.curzindex=0
	this.animation_isrunning=false //variable to indicate whether a slide is currently being slided in
	this.posprop=(this.setting.orientation=="h")? "left" : "top"
	options=null
	var slideshow=this, setting=this.setting, preloadimages=[], max=0, imagesloaded=0
	$(function(){ //on document.ready
		slideshow.$wrapperdiv=$('#'+setting.wrapperid)
		var $bgoverlays = slideshow.$wrapperdiv.find('div.bgoverlay').filter(function(i){
			var $this = $(this)
			return ($this.data('bgimage') || $this.css('backgroundImage')!='none')
		})
		var max = $bgoverlays.length
		for (var i=0; i<$bgoverlays.length; i++){ // preload bg images inside div.bgoverlays
			var $this = $bgoverlays.eq(i)
			if ($this.data('bgimage')){
				$this.css('backgroundImage', 'url(' + $this.data('bgimage') + ')')
			}
			var bgimagesrc = $this.css('backgroundImage')
			preloadimages[i]=new Image()
			$(preloadimages[i]).bind('load error', function(){
				imagesloaded++
				if (imagesloaded==max){ //when all images have preloaded
					slideshow.init($)
				}
			})
			preloadimages[i].src=bgimagesrc
		}
	})


	function positioncontrols($controls){
		var winwidth = $(window).outerWidth(),
				winheight = $(window).outerHeight(),
				controlwidth = $controls.eq(0).width(),
				controlheight = $controls.eq(0).height(),
				controltop, controlleft
				
		if (setting.orientation == 'h'){
			controltop = (setting.dimensions[1] > winheight)? winheight/2 : '50%'
			$controls.css({top: controltop, marginTop: -controlheight/2})
		}
		else if (setting.orientation == 'v'){
			controlleft = (setting.dimensions[0] > winwidth)? winwidth/2 : '50%'
			$controls.css({left: controlleft, marginLeft: -controlwidth/2})
		}
	}
	this.positioncontrols = positioncontrols

	$(window).bind('unload', function(){ //on window onload
		if (slideshow.setting.persist) //remember last shown slide's index?
			parallaxSlider.routines.setCookie("slider-"+setting.wrapperid, slideshow.curslide)
	})

	$(window).bind('resize', function(){
		if (setting.dimensions){
			setting.dimensions=[slideshow.$wrapperdiv.width(), slideshow.$wrapperdiv.height()]
			positioncontrols(slideshow.$controls)
		}
	})

}

parallaxSlider.prototype={

	slide:function(nextslide, dir){ //possible values for dir: "left", "right", "up", or "down"
		var navdir = /(left|top)/.test(dir)? 'back' : 'forth'
		if (this.curslide==nextslide)
			return
		var slider=this, setting=this.setting
		var createobj=parallaxSlider.routines.createobj
		var nextslide_initialpos=setting.dimensions[(dir=="right"||dir=="left")? 0 : 1] * ((dir=="right"||dir=="down")? -1 : 1)
		var curslide_finalpos=-nextslide_initialpos
		var posprop=this.posprop
		if (this.animation_isrunning!=null)
			this.animation_isrunning=true //indicate animation is running
		var $nextslide = this.$imageslides.eq(nextslide).show().css(createobj([posprop, nextslide_initialpos], ['zIndex', 1])) 
		var $nextdescs = $nextslide.data('$descs')
		var $curslide = this.$imageslides.eq(this.curslide)
		var $curdescs = $curslide.data('$descs')


		$nextslide //show upcoming slide: [.75,.03,0,.97] is easing "function". See http://cubic-bezier.com
			.stop().velocity(createobj([posprop, 0]), setting.slideduration, [.75,.03,0,.97], function(){
				var $this=jQuery(this)
				$this.addClass(setting.activeslideclass)
				slider.animation_isrunning=false
			})

		$nextdescs.each(function(i){ //show upcoming slide's descs
			var $desc = jQuery(this)
			$desc.stop().css(createobj([posprop, nextslide_initialpos])).velocity(createobj([posprop, 0]), [.75,.03,0,.97], $desc.data('duration'))
    })

		// $curslide //unshow current slide
		// 	.css({zIndex: 0})
		// 	.removeClass(setting.activeslideclass)
		// 	.stop().velocity(createobj([posprop, dir=='left' || dir =='up'? -100 : 100]), setting.slideduration, "ease-in-out", function(){
		// 			var $this=jQuery(this)
		// 			$this.hide()
		// 	}) //hide outgoing slide
    $curslide.fadeOut('slow');

		$curdescs.each(function(i){ //unshow current slide's descs: [.52,.25,.94,.51] is easing "function". See http://cubic-bezier.com
			var $desc = jQuery(this)
			$desc.stop().velocity(createobj([posprop, dir=='left' || dir =='up'? '-100%' : '100%']), [.52,.25,.94,.51], $desc.data('duration') * .5)
		})		

		this.curslide=nextslide
    this.setting.onUpdateSlide(nextslide)
	},

	navigate:function(keyword){ //keyword: "back" or "forth"
		var slideshow=this, setting=this.setting
		clearTimeout(this.rotatetimer)
		if (!setting.displaymode.stoponclick && setting.displaymode.type!="manual"){ //if slider should continue auto rotating after nav buttons are clicked on
			this.curstep=(keyword=="back")? this.curstep-1 : this.curstep+1 //move curstep counter explicitly back or forth depending on direction of slide
			this.rotatetimer=setTimeout(function(){slideshow.rotate()}, setting.displaymode.pause)
		}
		var dir=(keyword=="back")? (setting.orientation=="h"? "right" : "down") : (setting.orientation=="h"? "left" : "up")	
		var targetslide=(keyword=="back")? this.curslide-1 : this.curslide+1
		targetslide=(targetslide<0)? this.$imageslides.length-1 : (targetslide>this.$imageslides.length-1)? 0 : targetslide //wrap around
		if (this.animation_isrunning==false)
			this.slide(targetslide, dir)
	},

  gotoSlide:function(keyword, slideIndex){ //keyword: "back" or "forth"
		var slideshow=this, setting=this.setting
		clearTimeout(this.rotatetimer)
		if (!setting.displaymode.stoponclick && setting.displaymode.type!="manual"){ //if slider should continue auto rotating after nav buttons are clicked on
			this.curstep=(keyword=="back")? this.curstep-1 : this.curstep+1 //move curstep counter explicitly back or forth depending on direction of slide
			this.rotatetimer=setTimeout(function(){slideshow.rotate()}, setting.displaymode.pause)
		}
		var dir=(keyword=="back")? (setting.orientation=="h"? "right" : "down") : (setting.orientation=="h"? "left" : "up")	
		// var targetslide=(keyword=="back")? this.curslide-1 : this.curslide+1
		targetslide=(slideIndex<0)? this.$imageslides.length-1 : (slideIndex>this.$imageslides.length-1)? 0 : slideIndex //wrap around
		if (this.animation_isrunning==false)
			this.slide(targetslide, dir)
	},

	rotate:function(){
		var slideshow=this, setting=this.setting
		if (this.ismouseover){ //pause slideshow onmouseover
			this.rotatetimer=setTimeout(function(){slideshow.rotate()}, setting.displaymode.pause)
			return
		}
		var nextslide=(this.curslide<this.$imageslides.length-1)? this.curslide+1 : 0
		this.slide(nextslide, this.posprop) //go to next slide, either to the left or upwards depending on setting.orientation setting
		if (setting.displaymode.cycles==0 || this.curstep<this.maxsteps-1){
			this.rotatetimer=setTimeout(function(){slideshow.rotate()}, setting.displaymode.pause)
			this.curstep++
		}
	},

	init:function($){
		var slideshow=this, setting=this.setting
		setting.dimensions=[this.$wrapperdiv.width(), this.$wrapperdiv.height()]
		this.$wrapperdiv.css({position:'relative', visibility:'visible', overflow:'hidden', backgroundImage:'none'})
		if (this.$wrapperdiv.length==0){ //if no wrapper DIV found
			alert("Error: DIV with ID \""+setting.wrapperid+"\" not found on page.")
			return
		}
		this.$imageslides=this.$wrapperdiv.find('div.slide').hide()
		this.curslide=(this.curslide==null || this.curslide>this.$imageslides.length-1)? 0 : parseInt(this.curslide) //make sure curslide index is within bounds
		this.$imageslides.each(function(i){
			var $descs = $(this).find('div.desc').css({position:'absolute', left:0, top:0, width:'100%', height:'100%'})
			$(this).data({$descs: $descs}) // cache references to "desc" divs inside each slide
			var descduration = setting.slideduration
			$descs.each(function(i){ // add additional time to each desc's slide duration
				$(this).data({duration: descduration+=setting.delaybtwdesc})
			})
		})
		this.$imageslides.eq(this.curslide).show() // work on current slide to show
			.css(parallaxSlider.routines.createobj([this.posprop, 0])) //set current slide's CSS position (either "left" or "top") to 0
			.addClass(setting.activeslideclass)
			.stop()
			.find('div.desc').css({left: 0})

		var orientation=setting.orientation
		var controlpaths=(orientation=="h")? setting.navbuttons.slice(0, 2) : setting.navbuttons.slice(2)
		var $controls =  $('<img class="navbutton" src="'+controlpaths[1]+'" data-dir="forth" style="position:absolute; z-index:5; cursor:pointer; ' + (orientation=='v'? 'bottom:8px; left:46%' : 'top:46%; right:8px;') + '" />'
			+ '<img class="navbutton" src="'+controlpaths[0]+'" data-dir="back" style="position:absolute;z-index:5; cursor:pointer; ' + (orientation=='v'? 'top:8px; left:45%' : 'top:45%; left:8px;') + '" />'
		)
		.css({opacity:0})
		.click(function(){
			var keyword = this.getAttribute('data-dir')
			setting.curslide = (keyword == "right")? (setting.curslide == setting.content.length-1? 0 : setting.curslide + 1)
				: (setting.curslide == 0? setting.content.length-1 : setting.curslide - 1)
			slideshow.navigate(keyword)
		})
		this.$controls = $controls.appendTo(this.$wrapperdiv)
		this.positioncontrols(this.$controls)
		if (setting.displaymode.type=="auto"){ //auto slide mode?
			setting.displaymode.pause+=setting.slideduration
			this.maxsteps=setting.displaymode.cycles * this.$imageslides.length
			if (setting.displaymode.pauseonmouseover){
				this.$wrapperdiv.mouseenter(function(){slideshow.ismouseover=true})
				this.$wrapperdiv.mouseleave(function(){slideshow.ismouseover=false})
			}
			this.rotatetimer=setTimeout(function(){slideshow.rotate()}, setting.displaymode.pause)
		}

		var swipeOptions={ // swipe object variables
			triggerOnTouchEnd : true,
			triggerOnTouchLeave : true,
			fallbackToMouseEvents : true, // enable mouse emulation of swipe navigation in non touch devices?
			allowPageScroll: setting.orientation == 'h'? "vertical" : "horizontal",
			swipethreshold: setting.swipethreshold,
			excludedElements:[]
		}

		swipeOptions.swipeStatus = function(event, phase, direction, distance){
			if (phase == 'start' && event.target.tagName == 'A'){ // cancel A action when finger makes contact with element
				evtparent.onclick = function(){
					return false
				}
			}
			if (phase == 'cancel' && event.target.tagName == 'A'){ // if swipe action canceled (so no proper swipe), enable A action
				evtparent.onclick = function(){
					return true
				}
			}
			if (phase == 'end'){
				var navkeyword = /(right)|(down)/i.test(direction)? 'back' : 'forth'
				if ( (setting.orientation == 'h' && /(left)|(right)/i.test(direction)) || (setting.orientation == 'v' && /(up)|(down)/i.test(direction)) )
					slideshow.navigate(navkeyword)
			}
		}

		if (this.$wrapperdiv.swipe){
			this.$wrapperdiv.swipe(swipeOptions)
		}

		this.$wrapperdiv.bind('mouseenter click', function(){
			if (slideshow.$controls && slideshow.$controls.length == 2){
				slideshow.$controls.stop().velocity({opacity: 1})
			}
		})
	
		this.$wrapperdiv.bind('mouseleave', function(){
			if (slideshow.$controls && slideshow.$controls.length == 2){
				slideshow.$controls.stop().velocity({opacity: 0}, 'fast')
			}
		})
	}

}

parallaxSlider.routines={

	getSlideHTML:function(setting, imgref, w, h, posprop){
		var posstr=posprop+":"+((posprop=="left")? w : h)
		return '<div class="slide" style="background-image:url(' + imgref[0] + '); position:absolute;'+posstr+';width:'+w+'; height:'+h+'; z-index: 0">'
							+ ((imgref[1])? '<div class="desc">' + imgref[1] + '</div>\n' : '')
							+	'</div>'
	},

	getCookie:function(Name){ 
		var re=new RegExp(Name+"=[^;]+", "i"); //construct RE to search for target name/value pair
		if (document.cookie.match(re)) //if cookie found
			return document.cookie.match(re)[0].split("=")[1] //return its value
		return null
	},

	setCookie:function(name, value){
		document.cookie = name+"=" + value + ";path=/"
	},

	createobj:function(){
		var obj={}
		for (var i=0; i<arguments.length; i++){
			obj[arguments[i][0]]=arguments[i][1]
		}
		return obj
	}
}
/*! roundSlider v1.3.2 | (c) 2015-2018, Soundar | MIT license | http://roundsliderui.com/licence.html */
;(function($,window,undefined){"use strict";function RoundSlider(n,t){this.id=n.id,this.control=$(n),this.options=$.extend({},this.defaults,t)}function CreateRoundSlider(n,t){for(var i,r,u,f=0;f<this.length;f++)if(i=this[f],r=$.data(i,pluginName),r){if($.isPlainObject(n))typeof r.option=="function"?r.option(n):i.id&&window[i.id]&&typeof window[i.id].option=="function"&&window[i.id].option(n);else if(typeof n=="string"&&typeof r[n]=="function"){if((n==="option"||n.indexOf("get")===0)&&t[2]===undefined)return r[n](t[1]);r[n](t[1],t[2])}}else u=new RoundSlider(i,n),u._saveInstanceOnElement(),u._saveInstanceOnID(),u._raise("beforeCreate")!==!1?(u._init(),u._raise("create")):u._removeData();return this}var pluginName="roundSlider";$.fn[pluginName]=function(n){return CreateRoundSlider.call(this,n,arguments)},RoundSlider.prototype={pluginName:pluginName,version:"1.3.2",options:{},control:null,defaults:{min:0,max:100,step:1,value:null,radius:85,width:18,handleSize:"+0",startAngle:0,endAngle:"+360",animation:!0,showTooltip:!0,editableTooltip:!0,readOnly:!1,disabled:!1,keyboardAction:!0,mouseScrollAction:!1,lineCap:"square",sliderType:"default",circleShape:"full",handleShape:"round",beforeCreate:null,create:null,start:null,drag:null,change:null,stop:null,tooltipFormat:null},keys:{UP:38,DOWN:40,LEFT:37,RIGHT:39},_props:function(){return{numberType:["min","max","step","radius","width","startAngle"],booleanType:["animation","showTooltip","editableTooltip","readOnly","disabled","keyboardAction","mouseScrollAction"],stringType:["sliderType","circleShape","handleShape","lineCap"]}},_init:function(){var t,n;this._isBrowserSupport=this._isBrowserSupported(),this._isKO=!1,this._isAngular=!1,this.control.is("input")&&(this._isInputType=!0,this._hiddenField=this.control,this.control=this.$createElement("div"),this.control.insertAfter(this._hiddenField),this.options.value=this._hiddenField.val()||this.options.value,t=this,this._checkKO()&&setTimeout(function(){t._checkKO()},1),this._checkAngular()),this._bindOnDrag=!1,n=this._dataElement().attr("data-updateon"),typeof n=="string"?n=="drag"&&(this._bindOnDrag=!0):this._isAngular&&(this._bindOnDrag=!0),this._onInit()},_onInit:function(){this._initialize(),this._update(),this._render()},_initialize:function(){var n=this.browserName=this.getBrowserName();(n&&this.control.addClass("rs-"+n),this._isBrowserSupport)&&(this._isReadOnly=!1,this._checkDataType(),this._refreshCircleShape())},_render:function(){if(this.container=this.$createElement("div.rs-container"),this.innerContainer=this.$createElement("div.rs-inner-container"),this.block=this.$createElement("div.rs-block rs-outer rs-border"),this.container.append(this.innerContainer.append(this.block)),this.control.addClass("rs-control").empty().append(this.container),this._setRadius(),this._isBrowserSupport)this._createLayers(),this._setProperties(),this._setValue(),this._updateTooltipPos(),this._bindControlEvents("_bind");else{var n=this.$createElement("div.rs-msg");n.html(typeof this._throwError=="function"?this._throwError():this._throwError),this.control.empty().addClass("rs-error").append(n),this._isInputType&&this.control.append(this._dataElement())}},_update:function(){this._validateSliderType(),this._updateStartEnd(),this._validateStartEnd(),this._handle1=this._handle2=this._handleDefaults(),this._analyzeModelValue(),this._validateModelValue()},_createLayers:function(){var i=this.options.width,t=this._start,n;n=this.$createElement("div.rs-path rs-transition"),this._rangeSlider||this._showRange?(this.block1=n.clone().addClass("rs-range-color").rsRotate(t),this.block2=n.clone().addClass("rs-range-color").css("opacity","0").rsRotate(t),this.block3=n.clone().addClass("rs-path-color").rsRotate(t),this.block4=n.addClass("rs-path-color").css({opacity:"1","z-index":"1"}).rsRotate(t-180),this.block.append(this.block1,this.block2,this.block3,this.block4).addClass("rs-split")):this.block.append(n.addClass("rs-path-color")),this.lastBlock=this.$createElement("span.rs-block").css({padding:i}),this.innerBlock=this.$createElement("div.rs-inner rs-bg-color rs-border"),this.lastBlock.append(this.innerBlock),this.block.append(this.lastBlock),this._appendHandle(),this._appendOverlay(),this._appendHiddenField()},_setProperties:function(){this._updatePre(),this._setHandleShape(),this._addAnimation(),this._appendTooltip(),this.options.showTooltip||this._removeTooltip(),this.options.disabled?this.disable():this.options.readOnly&&this._readOnly(!0),this.options.mouseScrollAction&&this._bindScrollEvents("_bind")},_updatePre:function(){this._prechange=this._predrag=this.options.value},_setValue:function(){if(this._rangeSlider)this._setHandleValue(1),this._setHandleValue(2);else{this._showRange&&this._setHandleValue(1);var n=this.options.sliderType=="default"?this._active||1:parseFloat(this.bar.children().attr("index"));this._setHandleValue(n)}},_appendTooltip:function(){this.container.children(".rs-tooltip").length===0&&(this.tooltip=this.$createElement("span.rs-tooltip rs-tooltip-text"),this.container.append(this.tooltip),this._tooltipEditable(),this._updateTooltip())},_removeTooltip:function(){this.container.children(".rs-tooltip").length!=0&&this.tooltip&&this.tooltip.remove()},_tooltipEditable:function(){if(this.tooltip&&this.options.showTooltip){var n;this.options.editableTooltip?(this.tooltip.addClass("edit"),n="_bind"):(this.tooltip.removeClass("edit"),n="_unbind"),this[n](this.tooltip,"click",this._editTooltip)}},_editTooltip:function(){if(this.tooltip.hasClass("edit")&&!this._isReadOnly){var n=parseFloat(this.tooltip.css("border-left-width"))*2;this.input=this.$createElement("input.rs-input rs-tooltip-text").css({height:this.tooltip.outerHeight()-n,width:this.tooltip.outerWidth()-n}),this.tooltip.html(this.input).removeClass("edit").addClass("hover"),this.input.focus().val(this._getTooltipValue(!0)),this._bind(this.input,"blur",this._focusOut),this._bind(this.input,"change",this._focusOut)}},_focusOut:function(n){if(n.type=="change"){var t=this.input.val().replace("-",",");t[0]==","&&(t="-"+t.slice(1).replace("-",",")),this.options.value=t,this._analyzeModelValue(),this._validateModelValue(),this._setValue(),this.input.val(this._getTooltipValue(!0))}else this.tooltip.addClass("edit").removeClass("hover"),this._updateTooltip();this._raiseEvent("change")},_setHandleShape:function(){var n=this.options.handleShape;this._handles().removeClass("rs-handle-dot rs-handle-square"),n=="dot"?this._handles().addClass("rs-handle-dot"):n=="square"?this._handles().addClass("rs-handle-square"):this.options.handleShape=this.defaults.handleShape},_setHandleValue:function(n){this._active=n;var t=this["_handle"+n];this.options.sliderType!="min-range"&&(this.bar=this._activeHandleBar()),this._changeSliderValue(t.value,t.angle)},_setAnimation:function(){this.options.animation?this._addAnimation():this._removeAnimation()},_addAnimation:function(){this.options.animation&&this.control.addClass("rs-animation")},_removeAnimation:function(){this.control.removeClass("rs-animation")},_setRadius:function(){var t=this.options.radius,i=t*2,n=this.options.circleShape,r=i,u=i,f,e;if(this.container.removeClass().addClass("rs-container"),n.indexOf("half")===0){switch(n){case"half-top":case"half-bottom":r=t,u=i;break;case"half-left":case"half-right":r=i,u=t}this.container.addClass(n.replace("half-","")+" half")}else n.indexOf("quarter")===0?(r=u=t,f=n.split("-"),this.container.addClass(f[0]+" "+f[1]+" "+f[2])):this.container.addClass("full "+n);e={height:r,width:u},this.control.css(e),this.container.css(e)},_border:function(n){return n?parseFloat(this._startLine.children().css("border-bottom-width")):parseFloat(this.block.css("border-top-width"))*2},_appendHandle:function(){(this._rangeSlider||!this._showRange)&&this._createHandle(1),(this._rangeSlider||this._showRange)&&this._createHandle(2),this._startLine=this._addSeperator(this._start,"rs-start"),this._endLine=this._addSeperator(this._start+this._end,"rs-end"),this._refreshSeperator()},_addSeperator:function(n,t){var r=this.$createElement("span.rs-seperator rs-border"),u=this.options.width,f=this._border(),i=this.$createElement("span.rs-bar rs-transition "+t).append(r).rsRotate(n);return this.container.append(i),i},_refreshSeperator:function(){var i=this._startLine.add(this._endLine),r=i.children().removeAttr("style"),n=this.options,u=n.width,f=this._border(),t=u+f;n.lineCap=="round"&&n.circleShape!="full"?(i.addClass("rs-rounded"),r.css({width:t,height:t/2+1}),this._startLine.children().css("margin-top",-1).addClass(n.sliderType=="min-range"?"rs-range-color":"rs-path-color"),this._endLine.children().css("margin-top",t/-2).addClass("rs-path-color")):(i.removeClass("rs-rounded"),r.css({width:t,"margin-top":this._border(!0)/-2}).removeClass("rs-range-color rs-path-color"))},_updateSeperator:function(){this._startLine.rsRotate(this._start),this._endLine.rsRotate(this._start+this._end)},_createHandle:function(n){var t=this.$createElement("div.rs-handle rs-move"),u=this.options,f,r;(f=u.handleShape)!="round"&&t.addClass("rs-handle-"+f),t.attr({index:n,tabIndex:"0"});var i=this._dataElement()[0].id,i=i?i+"_":"",e=i+"handle"+(u.sliderType=="range"?"_"+(n==1?"start":"end"):"");return t.attr({role:"slider","aria-label":e}),r=this.$createElement("div.rs-bar rs-transition").css("z-index","7").append(t).rsRotate(this._start),r.addClass(u.sliderType=="range"&&n==2?"rs-second":"rs-first"),this.container.append(r),this._refreshHandle(),this.bar=r,this._active=n,n!=1&&n!=2&&(this["_handle"+n]=this._handleDefaults()),this._bind(t,"focus",this._handleFocus),this._bind(t,"blur",this._handleBlur),t},_refreshHandle:function(){var hSize=this.options.handleSize,h,w,isSquare=!0,isNumber=this.isNumber,s,diff;if(typeof hSize=="string"&&isNumber(hSize))if(hSize.charAt(0)==="+"||hSize.charAt(0)==="-")try{hSize=eval(this.options.width+hSize.charAt(0)+Math.abs(parseFloat(hSize)))}catch(e){console.warn(e)}else hSize.indexOf(",")&&(s=hSize.split(","),isNumber(s[0])&&isNumber(s[1])&&(w=parseFloat(s[0]),h=parseFloat(s[1]),isSquare=!1));isSquare&&(h=w=isNumber(hSize)?parseFloat(hSize):this.options.width),diff=(this.options.width+this._border()-w)/2,this._handles().css({height:h,width:w,margin:-h/2+"px 0 0 "+diff+"px"})},_handleDefaults:function(){return{angle:this._valueToAngle(this.options.min),value:this.options.min}},_handles:function(){return this.container.children("div.rs-bar").find(".rs-handle")},_activeHandleBar:function(n){return n=n!=undefined?n:this._active,$(this.container.children("div.rs-bar")[n-1])},_handleArgs:function(n){n=n!=undefined?n:this._active;var t=this["_handle"+n];return{element:this._activeHandleBar(n).children(),index:n,isActive:n==this._active,value:t?t.value:null,angle:t?t.angle:null}},_dataElement:function(){return this._isInputType?this._hiddenField:this.control},_raiseEvent:function(n){var t=this["_pre"+n];if(t!==this.options.value)return this["_pre"+n]=this.options.value,this._updateTooltip(),(n=="change"||this._bindOnDrag&&n=="drag")&&this._updateHidden(),this._raise(n,{value:this.options.value,preValue:t,handle:this._handleArgs()})},_elementDown:function(n){var i,r,u,t,f;if(!this._isReadOnly)if(i=$(n.target),i.hasClass("rs-handle"))this._handleDown(n);else{var e=this._getXY(n),o=this._getCenterPoint(),s=this._getDistance(e,o),h=this.block.outerWidth()/2,c=h-(this.options.width+this._border());s>=c&&s<=h&&(n.preventDefault(),r=this.control.find(".rs-handle.rs-focus"),this.control.attr("tabindex","0").focus().removeAttr("tabindex"),i.hasClass("rs-seperator")?(t=i.parent().hasClass("rs-start")?this.options.min:this.options.max,u=this._valueToAngle(t)):(f=this._getAngleValue(e,o),u=f.angle,t=f.value),this._rangeSlider&&(r=this.control.find(".rs-handle.rs-focus"),this._active=r.length==1?parseFloat(r.attr("index")):this._handle2.value-t<t-this._handle1.value?2:1,this.bar=this._activeHandleBar()),this._changeSliderValue(t,u),this._raiseEvent("change"))}},_handleDown:function(n){n.preventDefault();var t=$(n.target);t.focus(),this._removeAnimation(),this._bindMouseEvents("_bind"),this.bar=t.parent(),this._active=parseFloat(t.attr("index")),this._handles().removeClass("rs-move"),this._raise("start",{value:this.options.value,handle:this._handleArgs()})},_handleMove:function(n){n.preventDefault();var u=this._getXY(n),f=this._getCenterPoint(),t=this._getAngleValue(u,f,!0),i,r;i=t.angle,r=t.value,this._changeSliderValue(r,i),this._raiseEvent("drag")},_handleUp:function(){this._handles().addClass("rs-move"),this._bindMouseEvents("_unbind"),this._addAnimation(),this._raiseEvent("change"),this._raise("stop",{value:this.options.value,handle:this._handleArgs()})},_handleFocus:function(n){if(!this._isReadOnly){var t=$(n.target);this._handles().removeClass("rs-focus"),t.addClass("rs-focus"),this.bar=t.parent(),this._active=parseFloat(t.attr("index")),this.options.keyboardAction&&(this._bindKeyboardEvents("_unbind"),this._bindKeyboardEvents("_bind")),this.control.find("div.rs-bar").css("z-index","7"),this.bar.css("z-index","8")}},_handleBlur:function(){this._handles().removeClass("rs-focus"),this.options.keyboardAction&&this._bindKeyboardEvents("_unbind")},_handleKeyDown:function(n){var t,r,u,i,f;this._isReadOnly||(t=n.keyCode,r=this.keys,t==27&&this._handles().blur(),t>=35&&t<=40)&&(t>=37&&t<=40&&this._removeAnimation(),u=this["_handle"+this._active],n.preventDefault(),t==r.UP||t==r.RIGHT?i=this._round(this._limitValue(u.value+this.options.step)):t==r.DOWN||t==r.LEFT?i=this._round(this._limitValue(u.value-this._getMinusStep(u.value))):t==36?i=this._getKeyValue("Home"):t==35&&(i=this._getKeyValue("End")),f=this._valueToAngle(i),this._changeSliderValue(i,f),this._raiseEvent("change"))},_handleKeyUp:function(){this._addAnimation()},_getMinusStep:function(n){if(n==this.options.max){var t=(this.options.max-this.options.min)%this.options.step;return t==0?this.options.step:t}return this.options.step},_getKeyValue:function(n){return this._rangeSlider?n=="Home"?this._active==1?this.options.min:this._handle1.value:this._active==1?this._handle2.value:this.options.max:n=="Home"?this.options.min:this.options.max},_elementScroll:function(n){if(!this._isReadOnly){n.preventDefault();var i=n.originalEvent||n,r,t,f,u;(u=i.wheelDelta?i.wheelDelta/60:i.detail?-i.detail/2:0,u!=0)&&(this._updateActiveHandle(n),r=this["_handle"+this._active],t=r.value+(u>0?this.options.step:-this._getMinusStep(r.value)),t=this._limitValue(t),f=this._valueToAngle(t),this._removeAnimation(),this._changeSliderValue(t,f),this._raiseEvent("change"),this._addAnimation())}},_updateActiveHandle:function(n){var t=$(n.target);t.hasClass("rs-handle")&&t.parent().parent()[0]==this.control[0]&&(this.bar=t.parent(),this._active=parseFloat(t.attr("index"))),this.bar.find(".rs-handle").hasClass("rs-focus")||this.bar.find(".rs-handle").focus()},_bindControlEvents:function(n){this[n](this.control,"mousedown",this._elementDown),this[n](this.control,"touchstart",this._elementDown)},_bindScrollEvents:function(n){this[n](this.control,"mousewheel",this._elementScroll),this[n](this.control,"DOMMouseScroll",this._elementScroll)},_bindMouseEvents:function(n){this[n]($(document),"mousemove",this._handleMove),this[n]($(document),"mouseup",this._handleUp),this[n]($(document),"mouseleave",this._handleUp),this[n]($(document),"touchmove",this._handleMove),this[n]($(document),"touchend",this._handleUp),this[n]($(document),"touchcancel",this._handleUp)},_bindKeyboardEvents:function(n){this[n]($(document),"keydown",this._handleKeyDown),this[n]($(document),"keyup",this._handleKeyUp)},_changeSliderValue:function(n,t){var u=this._oriAngle(t),i=this._limitAngle(t);if(this._rangeSlider||this._showRange){if(this._active==1&&u<=this._oriAngle(this._handle2.angle)||this._active==2&&u>=this._oriAngle(this._handle1.angle)||this._invertRange){this["_handle"+this._active]={angle:t,value:n},this.options.value=this._rangeSlider?this._handle1.value+","+this._handle2.value:n,this.bar.rsRotate(i),this._updateARIA(n);var r=this._oriAngle(this._handle2.angle)-this._oriAngle(this._handle1.angle),f="1",e="0";r<=180&&!(r<0&&r>-180)&&(f="0",e="1"),this.block2.css("opacity",f),this.block3.css("opacity",e),(this._active==1?this.block4:this.block2).rsRotate(i-180),(this._active==1?this.block1:this.block3).rsRotate(i)}}else this["_handle"+this._active]={angle:t,value:n},this.options.value=n,this.bar.rsRotate(i),this._updateARIA(n)},_updateARIA:function(n){var i=this.options.min,r=this.options.max,t;this.bar.children().attr({"aria-valuenow":n}),this.options.sliderType=="range"?(t=this._handles(),t.eq(0).attr({"aria-valuemin":i}),t.eq(1).attr({"aria-valuemax":r}),this._active==1?t.eq(1).attr({"aria-valuemin":n}):t.eq(0).attr({"aria-valuemax":n})):this.bar.children().attr({"aria-valuemin":i,"aria-valuemax":r})},_checkKO:function(){var f=this._dataElement().data("bind"),t,i,r,n,u;if(typeof f=="string"&&typeof ko=="object"){if(t=ko.dataFor(this._dataElement()[0]),typeof t=="undefined")return!0;for(i=f.split(","),n=0;n<i.length;n++)if(u=i[n].split(":"),$.trim(u[0])=="value"){r=$.trim(u[1]);break}r&&(this._isKO=!0,ko.computed(function(){this.option("value",t[r]())},this))}},_checkAngular:function(){if(typeof angular=="object"&&typeof angular.element=="function"&&(this._ngName=this._dataElement().attr("ng-model"),typeof this._ngName=="string")){this._isAngular=!0;var n=this;this._scope().$watch(this._ngName,function(t){n.option("value",t)})}},_scope:function(){return angular.element(this._dataElement()).scope()},_getDistance:function(n,t){return Math.sqrt((n.x-t.x)*(n.x-t.x)+(n.y-t.y)*(n.y-t.y))},_getXY:function(n){return n.type.indexOf("mouse")==-1&&(n=(n.originalEvent||n).changedTouches[0]),{x:n.pageX,y:n.pageY}},_getCenterPoint:function(){var n=this.block.offset();return{x:n.left+this.block.outerWidth()/2,y:n.top+this.block.outerHeight()/2}},_getAngleValue:function(n,t,i){var u=Math.atan2(n.y-t.y,t.x-n.x),r=-u/(Math.PI/180);return r<this._start&&(r+=360),r=this._checkAngle(r,i),this._processStepByAngle(r)},_checkAngle:function(n,t){var f=this._oriAngle(n),i=this["_handle"+this._active].angle,r=this._oriAngle(i),u;if(f>this._end){if(!t)return i;n=this._start+(r<=this._end-r?0:this._end)}else if(t&&(u=this._handleDragDistance,this.isNumber(u)&&Math.abs(f-r)>u))return i;return n},_processStepByAngle:function(n){var t=this._angleToValue(n);return this._processStepByValue(t)},_processStepByValue:function(n){var r=this.options.step,e,t,u,f,i,o;return e=(n-this.options.min)%r,t=n-e,u=this._limitValue(t+r),f=this._limitValue(t-r),i=n>=t?n-t<u-n?t:u:t-n>n-f?t:f,i=this._round(i),o=this._valueToAngle(i),{value:i,angle:o}},_round:function(n){var t=this.options.step.toString().split(".");return t[1]?parseFloat(n.toFixed(t[1].length)):Math.round(n)},_oriAngle:function(n){var t=n-this._start;return t<0&&(t+=360),t},_limitAngle:function(n){return n>360+this._start&&(n-=360),n<this._start&&(n+=360),n},_limitValue:function(n){return n<this.options.min&&(n=this.options.min),n>this.options.max&&(n=this.options.max),n},_angleToValue:function(n){var t=this.options;return this._oriAngle(n)/this._end*(t.max-t.min)+t.min},_valueToAngle:function(n){var t=this.options;return(n-t.min)/(t.max-t.min)*this._end+this._start},_appendHiddenField:function(){this._hiddenField=this._hiddenField||this.$createElement("input"),this._hiddenField.attr({type:"hidden",name:this._dataElement()[0].id||""}),this.control.append(this._hiddenField),this._updateHidden()},_updateHidden:function(){var n=this.options.value;this._hiddenField.val(n),(this._isKO||this._isAngular)&&this._hiddenField.trigger("change"),this._isAngular&&(this._scope()[this._ngName]=n)},_updateTooltip:function(){this.tooltip&&!this.tooltip.hasClass("hover")&&this.tooltip.html(this._getTooltipValue()),this._updateTooltipPos()},_updateTooltipPos:function(){this.tooltip&&this.tooltip.css(this._getTooltipPos())},_getTooltipPos:function(){var n=this.options.circleShape,t;if(n=="full"||n=="pie"||n.indexOf("custom")===0)return{"margin-top":-this.tooltip.outerHeight()/2,"margin-left":-this.tooltip.outerWidth()/2};if(n.indexOf("half")!=-1){switch(n){case"half-top":case"half-bottom":t={"margin-left":-this.tooltip.outerWidth()/2};break;case"half-left":case"half-right":t={"margin-top":-this.tooltip.outerHeight()/2}}return t}return{}},_getTooltipValue:function(n){if(this._rangeSlider){var t=this.options.value.split(",");return n?t[0]+" - "+t[1]:this._tooltipValue(t[0],1)+" - "+this._tooltipValue(t[1],2)}return n?this.options.value:this._tooltipValue(this.options.value)},_tooltipValue:function(n,t){var i=this._raise("tooltipFormat",{value:n,handle:this._handleArgs(t)});return i!=null&&typeof i!="boolean"?i:n},_validateStartAngle:function(){var n=this.options.startAngle;return n=(this.isNumber(n)?parseFloat(n):0)%360,n<0&&(n+=360),this.options.startAngle=n,n},_validateEndAngle:function(){var end=this.options.endAngle;if(typeof end=="string"&&this.isNumber(end)&&(end.charAt(0)==="+"||end.charAt(0)==="-"))try{end=eval(this.options.startAngle+end.charAt(0)+Math.abs(parseFloat(end)))}catch(e){console.warn(e)}return end=(this.isNumber(end)?parseFloat(end):360)%360,end<=this.options.startAngle&&(end+=360),end},_refreshCircleShape:function(){var n=this.options.circleShape,i=["half-top","half-bottom","half-left","half-right","quarter-top-left","quarter-top-right","quarter-bottom-right","quarter-bottom-left","pie","custom-half","custom-quarter"],t;i.indexOf(n)==-1&&(t=["h1","h2","h3","h4","q1","q2","q3","q4","3/4","ch","cq"].indexOf(n),n=t!=-1?i[t]:n=="half"?"half-top":n=="quarter"?"quarter-top-left":"full"),this.options.circleShape=n},_appendOverlay:function(){var n=this.options.circleShape;n=="pie"?this._checkOverlay(".rs-overlay",270):(n=="custom-half"||n=="custom-quarter")&&(this._checkOverlay(".rs-overlay1",180),n=="custom-quarter"&&this._checkOverlay(".rs-overlay2",this._end))},_checkOverlay:function(n,t){var i=this.container.children(n);i.length==0&&(i=this.$createElement("div"+n+" rs-transition rs-bg-color"),this.container.append(i)),i.rsRotate(this._start+t)},_checkDataType:function(){var i=this.options,r,n,t,u=this._props();for(r in u.numberType)n=u.numberType[r],t=i[n],i[n]=this.isNumber(t)?parseFloat(t):this.defaults[n];for(r in u.booleanType)n=u.booleanType[r],t=i[n],i[n]=t=="false"?!1:!!t;for(r in u.stringType)n=u.stringType[r],t=i[n],i[n]=(""+t).toLowerCase()},_validateSliderType:function(){var n=this.options.sliderType.toLowerCase();this._rangeSlider=this._showRange=!1,n=="range"?this._rangeSlider=this._showRange=!0:n.indexOf("min")!=-1?(this._showRange=!0,n="min-range"):n="default",this.options.sliderType=n},_updateStartEnd:function(){var n=this.options.circleShape;n!="full"&&(n.indexOf("quarter")!=-1?this.options.endAngle="+90":n.indexOf("half")!=-1?this.options.endAngle="+180":n=="pie"&&(this.options.endAngle="+270"),n=="quarter-top-left"||n=="half-top"?this.options.startAngle=0:n=="quarter-top-right"||n=="half-right"?this.options.startAngle=90:n=="quarter-bottom-right"||n=="half-bottom"?this.options.startAngle=180:(n=="quarter-bottom-left"||n=="half-left")&&(this.options.startAngle=270))},_validateStartEnd:function(){this._start=this._validateStartAngle(),this._end=this._validateEndAngle();var n=this._start<this._end?0:360;this._end+=n-this._start},_analyzeModelValue:function(){var n=this.options.value,t=this.options.min,e=this.options.max,f,u,r=this.isNumber,i;n instanceof Array&&(n=n.toString()),i=typeof n=="string"?n.split(","):[n],this._rangeSlider?u=typeof n=="string"?i.length>=2?(r(i[0])?i[0]:t)+","+(r(i[1])?i[1]:e):r(i[0])?t+","+i[0]:t+","+t:r(n)?t+","+n:t+","+t:typeof n=="string"?(f=i.pop(),u=r(f)?parseFloat(f):t):u=r(n)?parseFloat(n):t,this.options.value=u},_validateModelValue:function(){var r=this.options.value,i;if(this._rangeSlider){var u=r.split(","),n=parseFloat(u[0]),t=parseFloat(u[1]);n=this._limitValue(n),t=this._limitValue(t),this._invertRange||n>t&&(t=n),this._handle1=this._processStepByValue(n),this._handle2=this._processStepByValue(t),this.options.value=this._handle1.value+","+this._handle2.value}else i=this._showRange?2:this._active||1,this["_handle"+i]=this._processStepByValue(this._limitValue(r)),this._showRange&&(this._handle1=this._handleDefaults()),this.options.value=this["_handle"+i].value},$createElement:function(n){var t=n.split(".");return $(document.createElement(t[0])).addClass(t[1]||"")},isNumber:function(n){return n=parseFloat(n),typeof n=="number"&&!isNaN(n)},getBrowserName:function(){var n="",t=window.navigator.userAgent;return!!window.opr&&!!opr.addons||!!window.opera||t.indexOf(" OPR/")>=0?n="opera":typeof InstallTrigger!="undefined"?n="firefox":/*@cc_on!@*/!1&&!document.documentMode?window.StyleMedia?n="edge":t.indexOf("Safari")!=-1&&t.indexOf("Chrome")==-1?n="safari":(!window.chrome||!window.chrome.webstore)&&t.indexOf("Chrome")==-1||(n="chrome"):n="ie",n},_isBrowserSupported:function(){for(var t=["borderRadius","WebkitBorderRadius","MozBorderRadius","OBorderRadius","msBorderRadius","KhtmlBorderRadius"],n=0;n<t.length;n++)if(document.body.style[t[n]]!==undefined)return!0},_throwError:function(){return"This browser doesn't support the border-radious property."},_raise:function(n,t){var u=this.options,i=u[n],r=!0;return t=t||{value:u.value},t.id=this.id,t.control=this.control,t.options=u,i&&(t.type=n,typeof i=="string"&&(i=window[i]),$.isFunction(i)&&(r=i.call(this,t),r=r===!1?!1:r)),this.control.trigger($.Event(n,t)),r},_bind:function(n,t,i){$(n).bind(t,$.proxy(i,this))},_unbind:function(n,t,i){$(n).unbind(t,$.proxy(i,this))},_getInstance:function(){return $.data(this._dataElement()[0],pluginName)},_saveInstanceOnElement:function(){$.data(this.control[0],pluginName,this)},_saveInstanceOnID:function(){var n=this.id;n&&typeof window[n]!="undefined"&&(window[n]=this)},_removeData:function(){var n=this._dataElement()[0];$.removeData&&$.removeData(n,pluginName),n.id&&typeof window[n.id]._init=="function"&&delete window[n.id]},_destroyControl:function(){this._isInputType&&this._dataElement().insertAfter(this.control).attr("type","text"),this.control.empty().removeClass("rs-control").height("").width(""),this._removeAnimation(),this._bindControlEvents("_unbind")},_updateWidth:function(){this.lastBlock.css("padding",this.options.width),this._refreshHandle()},_readOnly:function(n){this._isReadOnly=n,this.container.removeClass("rs-readonly"),n&&this.container.addClass("rs-readonly")},_get:function(n){return this.options[n]},_set:function(n,t){var i=this._props();if($.inArray(n,i.numberType)!=-1){if(!this.isNumber(t))return;t=parseFloat(t)}else $.inArray(n,i.booleanType)!=-1?t=t=="false"?!1:!!t:$.inArray(n,i.stringType)!=-1&&(t=t.toLowerCase());if(this.options[n]!=t){this.options[n]=t;switch(n){case"startAngle":case"endAngle":this._validateStartEnd(),this._updateSeperator(),this._appendOverlay();case"min":case"max":case"step":case"value":this._analyzeModelValue(),this._validateModelValue(),this._setValue(),this._updatePre(),this._updateHidden(),this._updateTooltip();break;case"radius":this._setRadius(),this._updateTooltipPos();break;case"width":this._removeAnimation(),this._updateWidth(),this._updateTooltipPos(),this._addAnimation(),this._refreshSeperator();break;case"handleSize":this._refreshHandle();break;case"handleShape":this._setHandleShape();break;case"animation":this._setAnimation();break;case"showTooltip":this.options.showTooltip?this._appendTooltip():this._removeTooltip();break;case"editableTooltip":this._tooltipEditable(),this._updateTooltipPos();break;case"disabled":this.options.disabled?this.disable():this.enable();break;case"readOnly":this.options.readOnly?this._readOnly(!0):!this.options.disabled&&this._readOnly(!1);break;case"mouseScrollAction":this._bindScrollEvents(this.options.mouseScrollAction?"_bind":"_unbind");break;case"lineCap":this._refreshSeperator();break;case"circleShape":this._refreshCircleShape(),this.options.circleShape=="full"&&(this.options.startAngle=0,this.options.endAngle="+360");case"sliderType":this._destroyControl(),this._onInit()}return this}},option:function(n,t){if(this._getInstance()&&this._isBrowserSupport){if($.isPlainObject(n)){(n.min!==undefined||n.max!==undefined)&&(n.min!==undefined&&(this.options.min=n.min,delete n.min),n.max!==undefined&&(this.options.max=n.max,delete n.max),n.value==undefined&&this._set("value",this.options.value));for(var i in n)this._set(i,n[i])}else if(n&&typeof n=="string"){if(t===undefined)return this._get(n);this._set(n,t)}return this}},getValue:function(n){if(this.options.sliderType=="range"&&this.isNumber(n)){var t=parseFloat(n);if(t==1||t==2)return this["_handle"+t].value}return this._get("value")},setValue:function(n,t){if(this.isNumber(n)){if(this.isNumber(t))if(this.options.sliderType=="range"){var i=parseFloat(t),r=parseFloat(n);i==1?n=r+","+this._handle2.value:i==2&&(n=this._handle1.value+","+r)}else this.options.sliderType=="default"&&(this._active=t);this._set("value",n)}},disable:function(){this.options.disabled=!0,this.container.addClass("rs-disabled"),this._readOnly(!0)},enable:function(){this.options.disabled=!1,this.container.removeClass("rs-disabled"),this.options.readOnly||this._readOnly(!1)},destroy:function(){this._getInstance()&&(this._destroyControl(),this._removeData(),this._isInputType&&this.control.remove())}},$.fn.rsRotate=function(n){var t=this;return t.css("-webkit-transform","rotate("+n+"deg)"),t.css("-moz-transform","rotate("+n+"deg)"),t.css("-ms-transform","rotate("+n+"deg)"),t.css("-o-transform","rotate("+n+"deg)"),t.css("transform","rotate("+n+"deg)"),t},$.fn[pluginName].prototype=RoundSlider.prototype})(jQuery,window);
/*! Select2 4.0.5 | https://github.com/select2/select2/blob/master/LICENSE.md */!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof module&&module.exports?module.exports=function(b,c){return void 0===c&&(c="undefined"!=typeof window?require("jquery"):require("jquery")(b)),a(c),c}:a(jQuery)}(function(a){var b=function(){if(a&&a.fn&&a.fn.select2&&a.fn.select2.amd)var b=a.fn.select2.amd;var b;return function(){if(!b||!b.requirejs){b?c=b:b={};var a,c,d;!function(b){function e(a,b){return v.call(a,b)}function f(a,b){var c,d,e,f,g,h,i,j,k,l,m,n,o=b&&b.split("/"),p=t.map,q=p&&p["*"]||{};if(a){for(a=a.split("/"),g=a.length-1,t.nodeIdCompat&&x.test(a[g])&&(a[g]=a[g].replace(x,"")),"."===a[0].charAt(0)&&o&&(n=o.slice(0,o.length-1),a=n.concat(a)),k=0;k<a.length;k++)if("."===(m=a[k]))a.splice(k,1),k-=1;else if(".."===m){if(0===k||1===k&&".."===a[2]||".."===a[k-1])continue;k>0&&(a.splice(k-1,2),k-=2)}a=a.join("/")}if((o||q)&&p){for(c=a.split("/"),k=c.length;k>0;k-=1){if(d=c.slice(0,k).join("/"),o)for(l=o.length;l>0;l-=1)if((e=p[o.slice(0,l).join("/")])&&(e=e[d])){f=e,h=k;break}if(f)break;!i&&q&&q[d]&&(i=q[d],j=k)}!f&&i&&(f=i,h=j),f&&(c.splice(0,h,f),a=c.join("/"))}return a}function g(a,c){return function(){var d=w.call(arguments,0);return"string"!=typeof d[0]&&1===d.length&&d.push(null),o.apply(b,d.concat([a,c]))}}function h(a){return function(b){return f(b,a)}}function i(a){return function(b){r[a]=b}}function j(a){if(e(s,a)){var c=s[a];delete s[a],u[a]=!0,n.apply(b,c)}if(!e(r,a)&&!e(u,a))throw new Error("No "+a);return r[a]}function k(a){var b,c=a?a.indexOf("!"):-1;return c>-1&&(b=a.substring(0,c),a=a.substring(c+1,a.length)),[b,a]}function l(a){return a?k(a):[]}function m(a){return function(){return t&&t.config&&t.config[a]||{}}}var n,o,p,q,r={},s={},t={},u={},v=Object.prototype.hasOwnProperty,w=[].slice,x=/\.js$/;p=function(a,b){var c,d=k(a),e=d[0],g=b[1];return a=d[1],e&&(e=f(e,g),c=j(e)),e?a=c&&c.normalize?c.normalize(a,h(g)):f(a,g):(a=f(a,g),d=k(a),e=d[0],a=d[1],e&&(c=j(e))),{f:e?e+"!"+a:a,n:a,pr:e,p:c}},q={require:function(a){return g(a)},exports:function(a){var b=r[a];return void 0!==b?b:r[a]={}},module:function(a){return{id:a,uri:"",exports:r[a],config:m(a)}}},n=function(a,c,d,f){var h,k,m,n,o,t,v,w=[],x=typeof d;if(f=f||a,t=l(f),"undefined"===x||"function"===x){for(c=!c.length&&d.length?["require","exports","module"]:c,o=0;o<c.length;o+=1)if(n=p(c[o],t),"require"===(k=n.f))w[o]=q.require(a);else if("exports"===k)w[o]=q.exports(a),v=!0;else if("module"===k)h=w[o]=q.module(a);else if(e(r,k)||e(s,k)||e(u,k))w[o]=j(k);else{if(!n.p)throw new Error(a+" missing "+k);n.p.load(n.n,g(f,!0),i(k),{}),w[o]=r[k]}m=d?d.apply(r[a],w):void 0,a&&(h&&h.exports!==b&&h.exports!==r[a]?r[a]=h.exports:m===b&&v||(r[a]=m))}else a&&(r[a]=d)},a=c=o=function(a,c,d,e,f){if("string"==typeof a)return q[a]?q[a](c):j(p(a,l(c)).f);if(!a.splice){if(t=a,t.deps&&o(t.deps,t.callback),!c)return;c.splice?(a=c,c=d,d=null):a=b}return c=c||function(){},"function"==typeof d&&(d=e,e=f),e?n(b,a,c,d):setTimeout(function(){n(b,a,c,d)},4),o},o.config=function(a){return o(a)},a._defined=r,d=function(a,b,c){if("string"!=typeof a)throw new Error("See almond README: incorrect module build, no module name");b.splice||(c=b,b=[]),e(r,a)||e(s,a)||(s[a]=[a,b,c])},d.amd={jQuery:!0}}(),b.requirejs=a,b.require=c,b.define=d}}(),b.define("almond",function(){}),b.define("jquery",[],function(){var b=a||$;return null==b&&console&&console.error&&console.error("Select2: An instance of jQuery or a jQuery-compatible library was not found. Make sure that you are including jQuery before Select2 on your web page."),b}),b.define("select2/utils",["jquery"],function(a){function b(a){var b=a.prototype,c=[];for(var d in b){"function"==typeof b[d]&&("constructor"!==d&&c.push(d))}return c}var c={};c.Extend=function(a,b){function c(){this.constructor=a}var d={}.hasOwnProperty;for(var e in b)d.call(b,e)&&(a[e]=b[e]);return c.prototype=b.prototype,a.prototype=new c,a.__super__=b.prototype,a},c.Decorate=function(a,c){function d(){var b=Array.prototype.unshift,d=c.prototype.constructor.length,e=a.prototype.constructor;d>0&&(b.call(arguments,a.prototype.constructor),e=c.prototype.constructor),e.apply(this,arguments)}function e(){this.constructor=d}var f=b(c),g=b(a);c.displayName=a.displayName,d.prototype=new e;for(var h=0;h<g.length;h++){var i=g[h];d.prototype[i]=a.prototype[i]}for(var j=(function(a){var b=function(){};a in d.prototype&&(b=d.prototype[a]);var e=c.prototype[a];return function(){return Array.prototype.unshift.call(arguments,b),e.apply(this,arguments)}}),k=0;k<f.length;k++){var l=f[k];d.prototype[l]=j(l)}return d};var d=function(){this.listeners={}};return d.prototype.on=function(a,b){this.listeners=this.listeners||{},a in this.listeners?this.listeners[a].push(b):this.listeners[a]=[b]},d.prototype.trigger=function(a){var b=Array.prototype.slice,c=b.call(arguments,1);this.listeners=this.listeners||{},null==c&&(c=[]),0===c.length&&c.push({}),c[0]._type=a,a in this.listeners&&this.invoke(this.listeners[a],b.call(arguments,1)),"*"in this.listeners&&this.invoke(this.listeners["*"],arguments)},d.prototype.invoke=function(a,b){for(var c=0,d=a.length;c<d;c++)a[c].apply(this,b)},c.Observable=d,c.generateChars=function(a){for(var b="",c=0;c<a;c++){b+=Math.floor(36*Math.random()).toString(36)}return b},c.bind=function(a,b){return function(){a.apply(b,arguments)}},c._convertData=function(a){for(var b in a){var c=b.split("-"),d=a;if(1!==c.length){for(var e=0;e<c.length;e++){var f=c[e];f=f.substring(0,1).toLowerCase()+f.substring(1),f in d||(d[f]={}),e==c.length-1&&(d[f]=a[b]),d=d[f]}delete a[b]}}return a},c.hasScroll=function(b,c){var d=a(c),e=c.style.overflowX,f=c.style.overflowY;return(e!==f||"hidden"!==f&&"visible"!==f)&&("scroll"===e||"scroll"===f||(d.innerHeight()<c.scrollHeight||d.innerWidth()<c.scrollWidth))},c.escapeMarkup=function(a){var b={"\\":"&#92;","&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#39;","/":"&#47;"};return"string"!=typeof a?a:String(a).replace(/[&<>"'\/\\]/g,function(a){return b[a]})},c.appendMany=function(b,c){if("1.7"===a.fn.jquery.substr(0,3)){var d=a();a.map(c,function(a){d=d.add(a)}),c=d}b.append(c)},c}),b.define("select2/results",["jquery","./utils"],function(a,b){function c(a,b,d){this.$element=a,this.data=d,this.options=b,c.__super__.constructor.call(this)}return b.Extend(c,b.Observable),c.prototype.render=function(){var b=a('<ul class="select2-results__options" role="tree"></ul>');return this.options.get("multiple")&&b.attr("aria-multiselectable","true"),this.$results=b,b},c.prototype.clear=function(){this.$results.empty()},c.prototype.displayMessage=function(b){var c=this.options.get("escapeMarkup");this.clear(),this.hideLoading();var d=a('<li role="treeitem" aria-live="assertive" class="select2-results__option"></li>'),e=this.options.get("translations").get(b.message);d.append(c(e(b.args))),d[0].className+=" select2-results__message",this.$results.append(d)},c.prototype.hideMessages=function(){this.$results.find(".select2-results__message").remove()},c.prototype.append=function(a){this.hideLoading();var b=[];if(null==a.results||0===a.results.length)return void(0===this.$results.children().length&&this.trigger("results:message",{message:"noResults"}));a.results=this.sort(a.results);for(var c=0;c<a.results.length;c++){var d=a.results[c],e=this.option(d);b.push(e)}this.$results.append(b)},c.prototype.position=function(a,b){b.find(".select2-results").append(a)},c.prototype.sort=function(a){return this.options.get("sorter")(a)},c.prototype.highlightFirstItem=function(){var a=this.$results.find(".select2-results__option[aria-selected]"),b=a.filter("[aria-selected=true]");b.length>0?b.first().trigger("mouseenter"):a.first().trigger("mouseenter"),this.ensureHighlightVisible()},c.prototype.setClasses=function(){var b=this;this.data.current(function(c){var d=a.map(c,function(a){return a.id.toString()});b.$results.find(".select2-results__option[aria-selected]").each(function(){var b=a(this),c=a.data(this,"data"),e=""+c.id;null!=c.element&&c.element.selected||null==c.element&&a.inArray(e,d)>-1?b.attr("aria-selected","true"):b.attr("aria-selected","false")})})},c.prototype.showLoading=function(a){this.hideLoading();var b=this.options.get("translations").get("searching"),c={disabled:!0,loading:!0,text:b(a)},d=this.option(c);d.className+=" loading-results",this.$results.prepend(d)},c.prototype.hideLoading=function(){this.$results.find(".loading-results").remove()},c.prototype.option=function(b){var c=document.createElement("li");c.className="select2-results__option";var d={role:"treeitem","aria-selected":"false"};b.disabled&&(delete d["aria-selected"],d["aria-disabled"]="true"),null==b.id&&delete d["aria-selected"],null!=b._resultId&&(c.id=b._resultId),b.title&&(c.title=b.title),b.children&&(d.role="group",d["aria-label"]=b.text,delete d["aria-selected"]);for(var e in d){var f=d[e];c.setAttribute(e,f)}if(b.children){var g=a(c),h=document.createElement("strong");h.className="select2-results__group";a(h);this.template(b,h);for(var i=[],j=0;j<b.children.length;j++){var k=b.children[j],l=this.option(k);i.push(l)}var m=a("<ul></ul>",{class:"select2-results__options select2-results__options--nested"});m.append(i),g.append(h),g.append(m)}else this.template(b,c);return a.data(c,"data",b),c},c.prototype.bind=function(b,c){var d=this,e=b.id+"-results";this.$results.attr("id",e),b.on("results:all",function(a){d.clear(),d.append(a.data),b.isOpen()&&(d.setClasses(),d.highlightFirstItem())}),b.on("results:append",function(a){d.append(a.data),b.isOpen()&&d.setClasses()}),b.on("query",function(a){d.hideMessages(),d.showLoading(a)}),b.on("select",function(){b.isOpen()&&(d.setClasses(),d.highlightFirstItem())}),b.on("unselect",function(){b.isOpen()&&(d.setClasses(),d.highlightFirstItem())}),b.on("open",function(){d.$results.attr("aria-expanded","true"),d.$results.attr("aria-hidden","false"),d.setClasses(),d.ensureHighlightVisible()}),b.on("close",function(){d.$results.attr("aria-expanded","false"),d.$results.attr("aria-hidden","true"),d.$results.removeAttr("aria-activedescendant")}),b.on("results:toggle",function(){var a=d.getHighlightedResults();0!==a.length&&a.trigger("mouseup")}),b.on("results:select",function(){var a=d.getHighlightedResults();if(0!==a.length){var b=a.data("data");"true"==a.attr("aria-selected")?d.trigger("close",{}):d.trigger("select",{data:b})}}),b.on("results:previous",function(){var a=d.getHighlightedResults(),b=d.$results.find("[aria-selected]"),c=b.index(a);if(0!==c){var e=c-1;0===a.length&&(e=0);var f=b.eq(e);f.trigger("mouseenter");var g=d.$results.offset().top,h=f.offset().top,i=d.$results.scrollTop()+(h-g);0===e?d.$results.scrollTop(0):h-g<0&&d.$results.scrollTop(i)}}),b.on("results:next",function(){var a=d.getHighlightedResults(),b=d.$results.find("[aria-selected]"),c=b.index(a),e=c+1;if(!(e>=b.length)){var f=b.eq(e);f.trigger("mouseenter");var g=d.$results.offset().top+d.$results.outerHeight(!1),h=f.offset().top+f.outerHeight(!1),i=d.$results.scrollTop()+h-g;0===e?d.$results.scrollTop(0):h>g&&d.$results.scrollTop(i)}}),b.on("results:focus",function(a){a.element.addClass("select2-results__option--highlighted")}),b.on("results:message",function(a){d.displayMessage(a)}),a.fn.mousewheel&&this.$results.on("mousewheel",function(a){var b=d.$results.scrollTop(),c=d.$results.get(0).scrollHeight-b+a.deltaY,e=a.deltaY>0&&b-a.deltaY<=0,f=a.deltaY<0&&c<=d.$results.height();e?(d.$results.scrollTop(0),a.preventDefault(),a.stopPropagation()):f&&(d.$results.scrollTop(d.$results.get(0).scrollHeight-d.$results.height()),a.preventDefault(),a.stopPropagation())}),this.$results.on("mouseup",".select2-results__option[aria-selected]",function(b){var c=a(this),e=c.data("data");if("true"===c.attr("aria-selected"))return void(d.options.get("multiple")?d.trigger("unselect",{originalEvent:b,data:e}):d.trigger("close",{}));d.trigger("select",{originalEvent:b,data:e})}),this.$results.on("mouseenter",".select2-results__option[aria-selected]",function(b){var c=a(this).data("data");d.getHighlightedResults().removeClass("select2-results__option--highlighted"),d.trigger("results:focus",{data:c,element:a(this)})})},c.prototype.getHighlightedResults=function(){return this.$results.find(".select2-results__option--highlighted")},c.prototype.destroy=function(){this.$results.remove()},c.prototype.ensureHighlightVisible=function(){var a=this.getHighlightedResults();if(0!==a.length){var b=this.$results.find("[aria-selected]"),c=b.index(a),d=this.$results.offset().top,e=a.offset().top,f=this.$results.scrollTop()+(e-d),g=e-d;f-=2*a.outerHeight(!1),c<=2?this.$results.scrollTop(0):(g>this.$results.outerHeight()||g<0)&&this.$results.scrollTop(f)}},c.prototype.template=function(b,c){var d=this.options.get("templateResult"),e=this.options.get("escapeMarkup"),f=d(b,c);null==f?c.style.display="none":"string"==typeof f?c.innerHTML=e(f):a(c).append(f)},c}),b.define("select2/keys",[],function(){return{BACKSPACE:8,TAB:9,ENTER:13,SHIFT:16,CTRL:17,ALT:18,ESC:27,SPACE:32,PAGE_UP:33,PAGE_DOWN:34,END:35,HOME:36,LEFT:37,UP:38,RIGHT:39,DOWN:40,DELETE:46}}),b.define("select2/selection/base",["jquery","../utils","../keys"],function(a,b,c){function d(a,b){this.$element=a,this.options=b,d.__super__.constructor.call(this)}return b.Extend(d,b.Observable),d.prototype.render=function(){var b=a('<span class="select2-selection" role="combobox"  aria-haspopup="true" aria-expanded="false"></span>');return this._tabindex=0,null!=this.$element.data("old-tabindex")?this._tabindex=this.$element.data("old-tabindex"):null!=this.$element.attr("tabindex")&&(this._tabindex=this.$element.attr("tabindex")),b.attr("title",this.$element.attr("title")),b.attr("tabindex",this._tabindex),this.$selection=b,b},d.prototype.bind=function(a,b){var d=this,e=(a.id,a.id+"-results");this.container=a,this.$selection.on("focus",function(a){d.trigger("focus",a)}),this.$selection.on("blur",function(a){d._handleBlur(a)}),this.$selection.on("keydown",function(a){d.trigger("keypress",a),a.which===c.SPACE&&a.preventDefault()}),a.on("results:focus",function(a){d.$selection.attr("aria-activedescendant",a.data._resultId)}),a.on("selection:update",function(a){d.update(a.data)}),a.on("open",function(){d.$selection.attr("aria-expanded","true"),d.$selection.attr("aria-owns",e),d._attachCloseHandler(a)}),a.on("close",function(){d.$selection.attr("aria-expanded","false"),d.$selection.removeAttr("aria-activedescendant"),d.$selection.removeAttr("aria-owns"),d.$selection.focus(),d._detachCloseHandler(a)}),a.on("enable",function(){d.$selection.attr("tabindex",d._tabindex)}),a.on("disable",function(){d.$selection.attr("tabindex","-1")})},d.prototype._handleBlur=function(b){var c=this;window.setTimeout(function(){document.activeElement==c.$selection[0]||a.contains(c.$selection[0],document.activeElement)||c.trigger("blur",b)},1)},d.prototype._attachCloseHandler=function(b){a(document.body).on("mousedown.select2."+b.id,function(b){var c=a(b.target),d=c.closest(".select2");a(".select2.select2-container--open").each(function(){var b=a(this);this!=d[0]&&b.data("element").select2("close")})})},d.prototype._detachCloseHandler=function(b){a(document.body).off("mousedown.select2."+b.id)},d.prototype.position=function(a,b){b.find(".selection").append(a)},d.prototype.destroy=function(){this._detachCloseHandler(this.container)},d.prototype.update=function(a){throw new Error("The `update` method must be defined in child classes.")},d}),b.define("select2/selection/single",["jquery","./base","../utils","../keys"],function(a,b,c,d){function e(){e.__super__.constructor.apply(this,arguments)}return c.Extend(e,b),e.prototype.render=function(){var a=e.__super__.render.call(this);return a.addClass("select2-selection--single"),a.html('<span class="select2-selection__rendered"></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>'),a},e.prototype.bind=function(a,b){var c=this;e.__super__.bind.apply(this,arguments);var d=a.id+"-container";this.$selection.find(".select2-selection__rendered").attr("id",d),this.$selection.attr("aria-labelledby",d),this.$selection.on("mousedown",function(a){1===a.which&&c.trigger("toggle",{originalEvent:a})}),this.$selection.on("focus",function(a){}),this.$selection.on("blur",function(a){}),a.on("focus",function(b){a.isOpen()||c.$selection.focus()}),a.on("selection:update",function(a){c.update(a.data)})},e.prototype.clear=function(){this.$selection.find(".select2-selection__rendered").empty()},e.prototype.display=function(a,b){var c=this.options.get("templateSelection");return this.options.get("escapeMarkup")(c(a,b))},e.prototype.selectionContainer=function(){return a("<span></span>")},e.prototype.update=function(a){if(0===a.length)return void this.clear();var b=a[0],c=this.$selection.find(".select2-selection__rendered"),d=this.display(b,c);c.empty().append(d),c.prop("title",b.title||b.text)},e}),b.define("select2/selection/multiple",["jquery","./base","../utils"],function(a,b,c){function d(a,b){d.__super__.constructor.apply(this,arguments)}return c.Extend(d,b),d.prototype.render=function(){var a=d.__super__.render.call(this);return a.addClass("select2-selection--multiple"),a.html('<ul class="select2-selection__rendered"></ul>'),a},d.prototype.bind=function(b,c){var e=this;d.__super__.bind.apply(this,arguments),this.$selection.on("click",function(a){e.trigger("toggle",{originalEvent:a})}),this.$selection.on("click",".select2-selection__choice__remove",function(b){if(!e.options.get("disabled")){var c=a(this),d=c.parent(),f=d.data("data");e.trigger("unselect",{originalEvent:b,data:f})}})},d.prototype.clear=function(){this.$selection.find(".select2-selection__rendered").empty()},d.prototype.display=function(a,b){var c=this.options.get("templateSelection");return this.options.get("escapeMarkup")(c(a,b))},d.prototype.selectionContainer=function(){return a('<li class="select2-selection__choice"><span class="select2-selection__choice__remove" role="presentation">&times;</span></li>')},d.prototype.update=function(a){if(this.clear(),0!==a.length){for(var b=[],d=0;d<a.length;d++){var e=a[d],f=this.selectionContainer(),g=this.display(e,f);f.append(g),f.prop("title",e.title||e.text),f.data("data",e),b.push(f)}var h=this.$selection.find(".select2-selection__rendered");c.appendMany(h,b)}},d}),b.define("select2/selection/placeholder",["../utils"],function(a){function b(a,b,c){this.placeholder=this.normalizePlaceholder(c.get("placeholder")),a.call(this,b,c)}return b.prototype.normalizePlaceholder=function(a,b){return"string"==typeof b&&(b={id:"",text:b}),b},b.prototype.createPlaceholder=function(a,b){var c=this.selectionContainer();return c.html(this.display(b)),c.addClass("select2-selection__placeholder").removeClass("select2-selection__choice"),c},b.prototype.update=function(a,b){var c=1==b.length&&b[0].id!=this.placeholder.id;if(b.length>1||c)return a.call(this,b);this.clear();var d=this.createPlaceholder(this.placeholder);this.$selection.find(".select2-selection__rendered").append(d)},b}),b.define("select2/selection/allowClear",["jquery","../keys"],function(a,b){function c(){}return c.prototype.bind=function(a,b,c){var d=this;a.call(this,b,c),null==this.placeholder&&this.options.get("debug")&&window.console&&console.error&&console.error("Select2: The `allowClear` option should be used in combination with the `placeholder` option."),this.$selection.on("mousedown",".select2-selection__clear",function(a){d._handleClear(a)}),b.on("keypress",function(a){d._handleKeyboardClear(a,b)})},c.prototype._handleClear=function(a,b){if(!this.options.get("disabled")){var c=this.$selection.find(".select2-selection__clear");if(0!==c.length){b.stopPropagation();for(var d=c.data("data"),e=0;e<d.length;e++){var f={data:d[e]};if(this.trigger("unselect",f),f.prevented)return}this.$element.val(this.placeholder.id).trigger("change"),this.trigger("toggle",{})}}},c.prototype._handleKeyboardClear=function(a,c,d){d.isOpen()||c.which!=b.DELETE&&c.which!=b.BACKSPACE||this._handleClear(c)},c.prototype.update=function(b,c){if(b.call(this,c),!(this.$selection.find(".select2-selection__placeholder").length>0||0===c.length)){var d=a('<span class="select2-selection__clear">&times;</span>');d.data("data",c),this.$selection.find(".select2-selection__rendered").prepend(d)}},c}),b.define("select2/selection/search",["jquery","../utils","../keys"],function(a,b,c){function d(a,b,c){a.call(this,b,c)}return d.prototype.render=function(b){var c=a('<li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="-1" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" aria-autocomplete="list" /></li>');this.$searchContainer=c,this.$search=c.find("input");var d=b.call(this);return this._transferTabIndex(),d},d.prototype.bind=function(a,b,d){var e=this;a.call(this,b,d),b.on("open",function(){e.$search.trigger("focus")}),b.on("close",function(){e.$search.val(""),e.$search.removeAttr("aria-activedescendant"),e.$search.trigger("focus")}),b.on("enable",function(){e.$search.prop("disabled",!1),e._transferTabIndex()}),b.on("disable",function(){e.$search.prop("disabled",!0)}),b.on("focus",function(a){e.$search.trigger("focus")}),b.on("results:focus",function(a){e.$search.attr("aria-activedescendant",a.id)}),this.$selection.on("focusin",".select2-search--inline",function(a){e.trigger("focus",a)}),this.$selection.on("focusout",".select2-search--inline",function(a){e._handleBlur(a)}),this.$selection.on("keydown",".select2-search--inline",function(a){if(a.stopPropagation(),e.trigger("keypress",a),e._keyUpPrevented=a.isDefaultPrevented(),a.which===c.BACKSPACE&&""===e.$search.val()){var b=e.$searchContainer.prev(".select2-selection__choice");if(b.length>0){var d=b.data("data");e.searchRemoveChoice(d),a.preventDefault()}}});var f=document.documentMode,g=f&&f<=11;this.$selection.on("input.searchcheck",".select2-search--inline",function(a){if(g)return void e.$selection.off("input.search input.searchcheck");e.$selection.off("keyup.search")}),this.$selection.on("keyup.search input.search",".select2-search--inline",function(a){if(g&&"input"===a.type)return void e.$selection.off("input.search input.searchcheck");var b=a.which;b!=c.SHIFT&&b!=c.CTRL&&b!=c.ALT&&b!=c.TAB&&e.handleSearch(a)})},d.prototype._transferTabIndex=function(a){this.$search.attr("tabindex",this.$selection.attr("tabindex")),this.$selection.attr("tabindex","-1")},d.prototype.createPlaceholder=function(a,b){this.$search.attr("placeholder",b.text)},d.prototype.update=function(a,b){var c=this.$search[0]==document.activeElement;this.$search.attr("placeholder",""),a.call(this,b),this.$selection.find(".select2-selection__rendered").append(this.$searchContainer),this.resizeSearch(),c&&this.$search.focus()},d.prototype.handleSearch=function(){if(this.resizeSearch(),!this._keyUpPrevented){var a=this.$search.val();this.trigger("query",{term:a})}this._keyUpPrevented=!1},d.prototype.searchRemoveChoice=function(a,b){this.trigger("unselect",{data:b}),this.$search.val(b.text),this.handleSearch()},d.prototype.resizeSearch=function(){this.$search.css("width","25px");var a="";if(""!==this.$search.attr("placeholder"))a=this.$selection.find(".select2-selection__rendered").innerWidth();else{a=.75*(this.$search.val().length+1)+"em"}this.$search.css("width",a)},d}),b.define("select2/selection/eventRelay",["jquery"],function(a){function b(){}return b.prototype.bind=function(b,c,d){var e=this,f=["open","opening","close","closing","select","selecting","unselect","unselecting"],g=["opening","closing","selecting","unselecting"];b.call(this,c,d),c.on("*",function(b,c){if(-1!==a.inArray(b,f)){c=c||{};var d=a.Event("select2:"+b,{params:c});e.$element.trigger(d),-1!==a.inArray(b,g)&&(c.prevented=d.isDefaultPrevented())}})},b}),b.define("select2/translation",["jquery","require"],function(a,b){function c(a){this.dict=a||{}}return c.prototype.all=function(){return this.dict},c.prototype.get=function(a){return this.dict[a]},c.prototype.extend=function(b){this.dict=a.extend({},b.all(),this.dict)},c._cache={},c.loadPath=function(a){if(!(a in c._cache)){var d=b(a);c._cache[a]=d}return new c(c._cache[a])},c}),b.define("select2/diacritics",[],function(){return{"Ⓐ":"A","Ａ":"A","À":"A","Á":"A","Â":"A","Ầ":"A","Ấ":"A","Ẫ":"A","Ẩ":"A","Ã":"A","Ā":"A","Ă":"A","Ằ":"A","Ắ":"A","Ẵ":"A","Ẳ":"A","Ȧ":"A","Ǡ":"A","Ä":"A","Ǟ":"A","Ả":"A","Å":"A","Ǻ":"A","Ǎ":"A","Ȁ":"A","Ȃ":"A","Ạ":"A","Ậ":"A","Ặ":"A","Ḁ":"A","Ą":"A","Ⱥ":"A","Ɐ":"A","Ꜳ":"AA","Æ":"AE","Ǽ":"AE","Ǣ":"AE","Ꜵ":"AO","Ꜷ":"AU","Ꜹ":"AV","Ꜻ":"AV","Ꜽ":"AY","Ⓑ":"B","Ｂ":"B","Ḃ":"B","Ḅ":"B","Ḇ":"B","Ƀ":"B","Ƃ":"B","Ɓ":"B","Ⓒ":"C","Ｃ":"C","Ć":"C","Ĉ":"C","Ċ":"C","Č":"C","Ç":"C","Ḉ":"C","Ƈ":"C","Ȼ":"C","Ꜿ":"C","Ⓓ":"D","Ｄ":"D","Ḋ":"D","Ď":"D","Ḍ":"D","Ḑ":"D","Ḓ":"D","Ḏ":"D","Đ":"D","Ƌ":"D","Ɗ":"D","Ɖ":"D","Ꝺ":"D","Ǳ":"DZ","Ǆ":"DZ","ǲ":"Dz","ǅ":"Dz","Ⓔ":"E","Ｅ":"E","È":"E","É":"E","Ê":"E","Ề":"E","Ế":"E","Ễ":"E","Ể":"E","Ẽ":"E","Ē":"E","Ḕ":"E","Ḗ":"E","Ĕ":"E","Ė":"E","Ë":"E","Ẻ":"E","Ě":"E","Ȅ":"E","Ȇ":"E","Ẹ":"E","Ệ":"E","Ȩ":"E","Ḝ":"E","Ę":"E","Ḙ":"E","Ḛ":"E","Ɛ":"E","Ǝ":"E","Ⓕ":"F","Ｆ":"F","Ḟ":"F","Ƒ":"F","Ꝼ":"F","Ⓖ":"G","Ｇ":"G","Ǵ":"G","Ĝ":"G","Ḡ":"G","Ğ":"G","Ġ":"G","Ǧ":"G","Ģ":"G","Ǥ":"G","Ɠ":"G","Ꞡ":"G","Ᵹ":"G","Ꝿ":"G","Ⓗ":"H","Ｈ":"H","Ĥ":"H","Ḣ":"H","Ḧ":"H","Ȟ":"H","Ḥ":"H","Ḩ":"H","Ḫ":"H","Ħ":"H","Ⱨ":"H","Ⱶ":"H","Ɥ":"H","Ⓘ":"I","Ｉ":"I","Ì":"I","Í":"I","Î":"I","Ĩ":"I","Ī":"I","Ĭ":"I","İ":"I","Ï":"I","Ḯ":"I","Ỉ":"I","Ǐ":"I","Ȉ":"I","Ȋ":"I","Ị":"I","Į":"I","Ḭ":"I","Ɨ":"I","Ⓙ":"J","Ｊ":"J","Ĵ":"J","Ɉ":"J","Ⓚ":"K","Ｋ":"K","Ḱ":"K","Ǩ":"K","Ḳ":"K","Ķ":"K","Ḵ":"K","Ƙ":"K","Ⱪ":"K","Ꝁ":"K","Ꝃ":"K","Ꝅ":"K","Ꞣ":"K","Ⓛ":"L","Ｌ":"L","Ŀ":"L","Ĺ":"L","Ľ":"L","Ḷ":"L","Ḹ":"L","Ļ":"L","Ḽ":"L","Ḻ":"L","Ł":"L","Ƚ":"L","Ɫ":"L","Ⱡ":"L","Ꝉ":"L","Ꝇ":"L","Ꞁ":"L","Ǉ":"LJ","ǈ":"Lj","Ⓜ":"M","Ｍ":"M","Ḿ":"M","Ṁ":"M","Ṃ":"M","Ɱ":"M","Ɯ":"M","Ⓝ":"N","Ｎ":"N","Ǹ":"N","Ń":"N","Ñ":"N","Ṅ":"N","Ň":"N","Ṇ":"N","Ņ":"N","Ṋ":"N","Ṉ":"N","Ƞ":"N","Ɲ":"N","Ꞑ":"N","Ꞥ":"N","Ǌ":"NJ","ǋ":"Nj","Ⓞ":"O","Ｏ":"O","Ò":"O","Ó":"O","Ô":"O","Ồ":"O","Ố":"O","Ỗ":"O","Ổ":"O","Õ":"O","Ṍ":"O","Ȭ":"O","Ṏ":"O","Ō":"O","Ṑ":"O","Ṓ":"O","Ŏ":"O","Ȯ":"O","Ȱ":"O","Ö":"O","Ȫ":"O","Ỏ":"O","Ő":"O","Ǒ":"O","Ȍ":"O","Ȏ":"O","Ơ":"O","Ờ":"O","Ớ":"O","Ỡ":"O","Ở":"O","Ợ":"O","Ọ":"O","Ộ":"O","Ǫ":"O","Ǭ":"O","Ø":"O","Ǿ":"O","Ɔ":"O","Ɵ":"O","Ꝋ":"O","Ꝍ":"O","Ƣ":"OI","Ꝏ":"OO","Ȣ":"OU","Ⓟ":"P","Ｐ":"P","Ṕ":"P","Ṗ":"P","Ƥ":"P","Ᵽ":"P","Ꝑ":"P","Ꝓ":"P","Ꝕ":"P","Ⓠ":"Q","Ｑ":"Q","Ꝗ":"Q","Ꝙ":"Q","Ɋ":"Q","Ⓡ":"R","Ｒ":"R","Ŕ":"R","Ṙ":"R","Ř":"R","Ȑ":"R","Ȓ":"R","Ṛ":"R","Ṝ":"R","Ŗ":"R","Ṟ":"R","Ɍ":"R","Ɽ":"R","Ꝛ":"R","Ꞧ":"R","Ꞃ":"R","Ⓢ":"S","Ｓ":"S","ẞ":"S","Ś":"S","Ṥ":"S","Ŝ":"S","Ṡ":"S","Š":"S","Ṧ":"S","Ṣ":"S","Ṩ":"S","Ș":"S","Ş":"S","Ȿ":"S","Ꞩ":"S","Ꞅ":"S","Ⓣ":"T","Ｔ":"T","Ṫ":"T","Ť":"T","Ṭ":"T","Ț":"T","Ţ":"T","Ṱ":"T","Ṯ":"T","Ŧ":"T","Ƭ":"T","Ʈ":"T","Ⱦ":"T","Ꞇ":"T","Ꜩ":"TZ","Ⓤ":"U","Ｕ":"U","Ù":"U","Ú":"U","Û":"U","Ũ":"U","Ṹ":"U","Ū":"U","Ṻ":"U","Ŭ":"U","Ü":"U","Ǜ":"U","Ǘ":"U","Ǖ":"U","Ǚ":"U","Ủ":"U","Ů":"U","Ű":"U","Ǔ":"U","Ȕ":"U","Ȗ":"U","Ư":"U","Ừ":"U","Ứ":"U","Ữ":"U","Ử":"U","Ự":"U","Ụ":"U","Ṳ":"U","Ų":"U","Ṷ":"U","Ṵ":"U","Ʉ":"U","Ⓥ":"V","Ｖ":"V","Ṽ":"V","Ṿ":"V","Ʋ":"V","Ꝟ":"V","Ʌ":"V","Ꝡ":"VY","Ⓦ":"W","Ｗ":"W","Ẁ":"W","Ẃ":"W","Ŵ":"W","Ẇ":"W","Ẅ":"W","Ẉ":"W","Ⱳ":"W","Ⓧ":"X","Ｘ":"X","Ẋ":"X","Ẍ":"X","Ⓨ":"Y","Ｙ":"Y","Ỳ":"Y","Ý":"Y","Ŷ":"Y","Ỹ":"Y","Ȳ":"Y","Ẏ":"Y","Ÿ":"Y","Ỷ":"Y","Ỵ":"Y","Ƴ":"Y","Ɏ":"Y","Ỿ":"Y","Ⓩ":"Z","Ｚ":"Z","Ź":"Z","Ẑ":"Z","Ż":"Z","Ž":"Z","Ẓ":"Z","Ẕ":"Z","Ƶ":"Z","Ȥ":"Z","Ɀ":"Z","Ⱬ":"Z","Ꝣ":"Z","ⓐ":"a","ａ":"a","ẚ":"a","à":"a","á":"a","â":"a","ầ":"a","ấ":"a","ẫ":"a","ẩ":"a","ã":"a","ā":"a","ă":"a","ằ":"a","ắ":"a","ẵ":"a","ẳ":"a","ȧ":"a","ǡ":"a","ä":"a","ǟ":"a","ả":"a","å":"a","ǻ":"a","ǎ":"a","ȁ":"a","ȃ":"a","ạ":"a","ậ":"a","ặ":"a","ḁ":"a","ą":"a","ⱥ":"a","ɐ":"a","ꜳ":"aa","æ":"ae","ǽ":"ae","ǣ":"ae","ꜵ":"ao","ꜷ":"au","ꜹ":"av","ꜻ":"av","ꜽ":"ay","ⓑ":"b","ｂ":"b","ḃ":"b","ḅ":"b","ḇ":"b","ƀ":"b","ƃ":"b","ɓ":"b","ⓒ":"c","ｃ":"c","ć":"c","ĉ":"c","ċ":"c","č":"c","ç":"c","ḉ":"c","ƈ":"c","ȼ":"c","ꜿ":"c","ↄ":"c","ⓓ":"d","ｄ":"d","ḋ":"d","ď":"d","ḍ":"d","ḑ":"d","ḓ":"d","ḏ":"d","đ":"d","ƌ":"d","ɖ":"d","ɗ":"d","ꝺ":"d","ǳ":"dz","ǆ":"dz","ⓔ":"e","ｅ":"e","è":"e","é":"e","ê":"e","ề":"e","ế":"e","ễ":"e","ể":"e","ẽ":"e","ē":"e","ḕ":"e","ḗ":"e","ĕ":"e","ė":"e","ë":"e","ẻ":"e","ě":"e","ȅ":"e","ȇ":"e","ẹ":"e","ệ":"e","ȩ":"e","ḝ":"e","ę":"e","ḙ":"e","ḛ":"e","ɇ":"e","ɛ":"e","ǝ":"e","ⓕ":"f","ｆ":"f","ḟ":"f","ƒ":"f","ꝼ":"f","ⓖ":"g","ｇ":"g","ǵ":"g","ĝ":"g","ḡ":"g","ğ":"g","ġ":"g","ǧ":"g","ģ":"g","ǥ":"g","ɠ":"g","ꞡ":"g","ᵹ":"g","ꝿ":"g","ⓗ":"h","ｈ":"h","ĥ":"h","ḣ":"h","ḧ":"h","ȟ":"h","ḥ":"h","ḩ":"h","ḫ":"h","ẖ":"h","ħ":"h","ⱨ":"h","ⱶ":"h","ɥ":"h","ƕ":"hv","ⓘ":"i","ｉ":"i","ì":"i","í":"i","î":"i","ĩ":"i","ī":"i","ĭ":"i","ï":"i","ḯ":"i","ỉ":"i","ǐ":"i","ȉ":"i","ȋ":"i","ị":"i","į":"i","ḭ":"i","ɨ":"i","ı":"i","ⓙ":"j","ｊ":"j","ĵ":"j","ǰ":"j","ɉ":"j","ⓚ":"k","ｋ":"k","ḱ":"k","ǩ":"k","ḳ":"k","ķ":"k","ḵ":"k","ƙ":"k","ⱪ":"k","ꝁ":"k","ꝃ":"k","ꝅ":"k","ꞣ":"k","ⓛ":"l","ｌ":"l","ŀ":"l","ĺ":"l","ľ":"l","ḷ":"l","ḹ":"l","ļ":"l","ḽ":"l","ḻ":"l","ſ":"l","ł":"l","ƚ":"l","ɫ":"l","ⱡ":"l","ꝉ":"l","ꞁ":"l","ꝇ":"l","ǉ":"lj","ⓜ":"m","ｍ":"m","ḿ":"m","ṁ":"m","ṃ":"m","ɱ":"m","ɯ":"m","ⓝ":"n","ｎ":"n","ǹ":"n","ń":"n","ñ":"n","ṅ":"n","ň":"n","ṇ":"n","ņ":"n","ṋ":"n","ṉ":"n","ƞ":"n","ɲ":"n","ŉ":"n","ꞑ":"n","ꞥ":"n","ǌ":"nj","ⓞ":"o","ｏ":"o","ò":"o","ó":"o","ô":"o","ồ":"o","ố":"o","ỗ":"o","ổ":"o","õ":"o","ṍ":"o","ȭ":"o","ṏ":"o","ō":"o","ṑ":"o","ṓ":"o","ŏ":"o","ȯ":"o","ȱ":"o","ö":"o","ȫ":"o","ỏ":"o","ő":"o","ǒ":"o","ȍ":"o","ȏ":"o","ơ":"o","ờ":"o","ớ":"o","ỡ":"o","ở":"o","ợ":"o","ọ":"o","ộ":"o","ǫ":"o","ǭ":"o","ø":"o","ǿ":"o","ɔ":"o","ꝋ":"o","ꝍ":"o","ɵ":"o","ƣ":"oi","ȣ":"ou","ꝏ":"oo","ⓟ":"p","ｐ":"p","ṕ":"p","ṗ":"p","ƥ":"p","ᵽ":"p","ꝑ":"p","ꝓ":"p","ꝕ":"p","ⓠ":"q","ｑ":"q","ɋ":"q","ꝗ":"q","ꝙ":"q","ⓡ":"r","ｒ":"r","ŕ":"r","ṙ":"r","ř":"r","ȑ":"r","ȓ":"r","ṛ":"r","ṝ":"r","ŗ":"r","ṟ":"r","ɍ":"r","ɽ":"r","ꝛ":"r","ꞧ":"r","ꞃ":"r","ⓢ":"s","ｓ":"s","ß":"s","ś":"s","ṥ":"s","ŝ":"s","ṡ":"s","š":"s","ṧ":"s","ṣ":"s","ṩ":"s","ș":"s","ş":"s","ȿ":"s","ꞩ":"s","ꞅ":"s","ẛ":"s","ⓣ":"t","ｔ":"t","ṫ":"t","ẗ":"t","ť":"t","ṭ":"t","ț":"t","ţ":"t","ṱ":"t","ṯ":"t","ŧ":"t","ƭ":"t","ʈ":"t","ⱦ":"t","ꞇ":"t","ꜩ":"tz","ⓤ":"u","ｕ":"u","ù":"u","ú":"u","û":"u","ũ":"u","ṹ":"u","ū":"u","ṻ":"u","ŭ":"u","ü":"u","ǜ":"u","ǘ":"u","ǖ":"u","ǚ":"u","ủ":"u","ů":"u","ű":"u","ǔ":"u","ȕ":"u","ȗ":"u","ư":"u","ừ":"u","ứ":"u","ữ":"u","ử":"u","ự":"u","ụ":"u","ṳ":"u","ų":"u","ṷ":"u","ṵ":"u","ʉ":"u","ⓥ":"v","ｖ":"v","ṽ":"v","ṿ":"v","ʋ":"v","ꝟ":"v","ʌ":"v","ꝡ":"vy","ⓦ":"w","ｗ":"w","ẁ":"w","ẃ":"w","ŵ":"w","ẇ":"w","ẅ":"w","ẘ":"w","ẉ":"w","ⱳ":"w","ⓧ":"x","ｘ":"x","ẋ":"x","ẍ":"x","ⓨ":"y","ｙ":"y","ỳ":"y","ý":"y","ŷ":"y","ỹ":"y","ȳ":"y","ẏ":"y","ÿ":"y","ỷ":"y","ẙ":"y","ỵ":"y","ƴ":"y","ɏ":"y","ỿ":"y","ⓩ":"z","ｚ":"z","ź":"z","ẑ":"z","ż":"z","ž":"z","ẓ":"z","ẕ":"z","ƶ":"z","ȥ":"z","ɀ":"z","ⱬ":"z","ꝣ":"z","Ά":"Α","Έ":"Ε","Ή":"Η","Ί":"Ι","Ϊ":"Ι","Ό":"Ο","Ύ":"Υ","Ϋ":"Υ","Ώ":"Ω","ά":"α","έ":"ε","ή":"η","ί":"ι","ϊ":"ι","ΐ":"ι","ό":"ο","ύ":"υ","ϋ":"υ","ΰ":"υ","ω":"ω","ς":"σ"}}),b.define("select2/data/base",["../utils"],function(a){function b(a,c){b.__super__.constructor.call(this)}return a.Extend(b,a.Observable),b.prototype.current=function(a){throw new Error("The `current` method must be defined in child classes.")},b.prototype.query=function(a,b){throw new Error("The `query` method must be defined in child classes.")},b.prototype.bind=function(a,b){},b.prototype.destroy=function(){},b.prototype.generateResultId=function(b,c){var d=b.id+"-result-";return d+=a.generateChars(4),null!=c.id?d+="-"+c.id.toString():d+="-"+a.generateChars(4),d},b}),b.define("select2/data/select",["./base","../utils","jquery"],function(a,b,c){function d(a,b){this.$element=a,this.options=b,d.__super__.constructor.call(this)}return b.Extend(d,a),d.prototype.current=function(a){var b=[],d=this;this.$element.find(":selected").each(function(){var a=c(this),e=d.item(a);b.push(e)}),a(b)},d.prototype.select=function(a){var b=this;if(a.selected=!0,c(a.element).is("option"))return a.element.selected=!0,void this.$element.trigger("change");if(this.$element.prop("multiple"))this.current(function(d){var e=[];a=[a],a.push.apply(a,d);for(var f=0;f<a.length;f++){var g=a[f].id;-1===c.inArray(g,e)&&e.push(g)}b.$element.val(e),b.$element.trigger("change")});else{var d=a.id;this.$element.val(d),this.$element.trigger("change")}},d.prototype.unselect=function(a){var b=this;if(this.$element.prop("multiple")){if(a.selected=!1,c(a.element).is("option"))return a.element.selected=!1,void this.$element.trigger("change");this.current(function(d){for(var e=[],f=0;f<d.length;f++){var g=d[f].id;g!==a.id&&-1===c.inArray(g,e)&&e.push(g)}b.$element.val(e),b.$element.trigger("change")})}},d.prototype.bind=function(a,b){var c=this;this.container=a,a.on("select",function(a){c.select(a.data)}),a.on("unselect",function(a){c.unselect(a.data)})},d.prototype.destroy=function(){this.$element.find("*").each(function(){c.removeData(this,"data")})},d.prototype.query=function(a,b){var d=[],e=this;this.$element.children().each(function(){var b=c(this);if(b.is("option")||b.is("optgroup")){var f=e.item(b),g=e.matches(a,f);null!==g&&d.push(g)}}),b({results:d})},d.prototype.addOptions=function(a){b.appendMany(this.$element,a)},d.prototype.option=function(a){var b;a.children?(b=document.createElement("optgroup"),b.label=a.text):(b=document.createElement("option"),void 0!==b.textContent?b.textContent=a.text:b.innerText=a.text),void 0!==a.id&&(b.value=a.id),a.disabled&&(b.disabled=!0),a.selected&&(b.selected=!0),a.title&&(b.title=a.title);var d=c(b),e=this._normalizeItem(a);return e.element=b,c.data(b,"data",e),d},d.prototype.item=function(a){var b={};if(null!=(b=c.data(a[0],"data")))return b;if(a.is("option"))b={id:a.val(),text:a.text(),disabled:a.prop("disabled"),selected:a.prop("selected"),title:a.prop("title")};else if(a.is("optgroup")){b={text:a.prop("label"),children:[],title:a.prop("title")};for(var d=a.children("option"),e=[],f=0;f<d.length;f++){var g=c(d[f]),h=this.item(g);e.push(h)}b.children=e}return b=this._normalizeItem(b),b.element=a[0],c.data(a[0],"data",b),b},d.prototype._normalizeItem=function(a){c.isPlainObject(a)||(a={id:a,text:a}),a=c.extend({},{text:""},a);var b={selected:!1,disabled:!1};return null!=a.id&&(a.id=a.id.toString()),null!=a.text&&(a.text=a.text.toString()),null==a._resultId&&a.id&&null!=this.container&&(a._resultId=this.generateResultId(this.container,a)),c.extend({},b,a)},d.prototype.matches=function(a,b){return this.options.get("matcher")(a,b)},d}),b.define("select2/data/array",["./select","../utils","jquery"],function(a,b,c){function d(a,b){var c=b.get("data")||[];d.__super__.constructor.call(this,a,b),this.addOptions(this.convertToOptions(c))}return b.Extend(d,a),d.prototype.select=function(a){var b=this.$element.find("option").filter(function(b,c){return c.value==a.id.toString()});0===b.length&&(b=this.option(a),this.addOptions(b)),d.__super__.select.call(this,a)},d.prototype.convertToOptions=function(a){function d(a){return function(){return c(this).val()==a.id}}for(var e=this,f=this.$element.find("option"),g=f.map(function(){return e.item(c(this)).id}).get(),h=[],i=0;i<a.length;i++){var j=this._normalizeItem(a[i]);if(c.inArray(j.id,g)>=0){var k=f.filter(d(j)),l=this.item(k),m=c.extend(!0,{},j,l),n=this.option(m);k.replaceWith(n)}else{var o=this.option(j);if(j.children){var p=this.convertToOptions(j.children);b.appendMany(o,p)}h.push(o)}}return h},d}),b.define("select2/data/ajax",["./array","../utils","jquery"],function(a,b,c){function d(a,b){this.ajaxOptions=this._applyDefaults(b.get("ajax")),null!=this.ajaxOptions.processResults&&(this.processResults=this.ajaxOptions.processResults),d.__super__.constructor.call(this,a,b)}return b.Extend(d,a),d.prototype._applyDefaults=function(a){var b={data:function(a){return c.extend({},a,{q:a.term})},transport:function(a,b,d){var e=c.ajax(a);return e.then(b),e.fail(d),e}};return c.extend({},b,a,!0)},d.prototype.processResults=function(a){return a},d.prototype.query=function(a,b){function d(){var d=f.transport(f,function(d){var f=e.processResults(d,a);e.options.get("debug")&&window.console&&console.error&&(f&&f.results&&c.isArray(f.results)||console.error("Select2: The AJAX results did not return an array in the `results` key of the response.")),b(f)},function(){d.status&&"0"===d.status||e.trigger("results:message",{message:"errorLoading"})});e._request=d}var e=this;null!=this._request&&(c.isFunction(this._request.abort)&&this._request.abort(),this._request=null);var f=c.extend({type:"GET"},this.ajaxOptions);"function"==typeof f.url&&(f.url=f.url.call(this.$element,a)),"function"==typeof f.data&&(f.data=f.data.call(this.$element,a)),this.ajaxOptions.delay&&null!=a.term?(this._queryTimeout&&window.clearTimeout(this._queryTimeout),this._queryTimeout=window.setTimeout(d,this.ajaxOptions.delay)):d()},d}),b.define("select2/data/tags",["jquery"],function(a){function b(b,c,d){var e=d.get("tags"),f=d.get("createTag");void 0!==f&&(this.createTag=f);var g=d.get("insertTag");if(void 0!==g&&(this.insertTag=g),b.call(this,c,d),a.isArray(e))for(var h=0;h<e.length;h++){var i=e[h],j=this._normalizeItem(i),k=this.option(j);this.$element.append(k)}}return b.prototype.query=function(a,b,c){function d(a,f){for(var g=a.results,h=0;h<g.length;h++){var i=g[h],j=null!=i.children&&!d({results:i.children},!0);if((i.text||"").toUpperCase()===(b.term||"").toUpperCase()||j)return!f&&(a.data=g,void c(a))}if(f)return!0;var k=e.createTag(b);if(null!=k){var l=e.option(k);l.attr("data-select2-tag",!0),e.addOptions([l]),e.insertTag(g,k)}a.results=g,c(a)}var e=this;if(this._removeOldTags(),null==b.term||null!=b.page)return void a.call(this,b,c);a.call(this,b,d)},b.prototype.createTag=function(b,c){var d=a.trim(c.term);return""===d?null:{id:d,text:d}},b.prototype.insertTag=function(a,b,c){b.unshift(c)},b.prototype._removeOldTags=function(b){this._lastTag;this.$element.find("option[data-select2-tag]").each(function(){this.selected||a(this).remove()})},b}),b.define("select2/data/tokenizer",["jquery"],function(a){function b(a,b,c){var d=c.get("tokenizer");void 0!==d&&(this.tokenizer=d),a.call(this,b,c)}return b.prototype.bind=function(a,b,c){a.call(this,b,c),this.$search=b.dropdown.$search||b.selection.$search||c.find(".select2-search__field")},b.prototype.query=function(b,c,d){function e(b){var c=g._normalizeItem(b);if(!g.$element.find("option").filter(function(){return a(this).val()===c.id}).length){var d=g.option(c);d.attr("data-select2-tag",!0),g._removeOldTags(),g.addOptions([d])}f(c)}function f(a){g.trigger("select",{data:a})}var g=this;c.term=c.term||"";var h=this.tokenizer(c,this.options,e);h.term!==c.term&&(this.$search.length&&(this.$search.val(h.term),this.$search.focus()),c.term=h.term),b.call(this,c,d)},b.prototype.tokenizer=function(b,c,d,e){for(var f=d.get("tokenSeparators")||[],g=c.term,h=0,i=this.createTag||function(a){return{id:a.term,text:a.term}};h<g.length;){var j=g[h];if(-1!==a.inArray(j,f)){var k=g.substr(0,h),l=a.extend({},c,{term:k}),m=i(l);null!=m?(e(m),g=g.substr(h+1)||"",h=0):h++}else h++}return{term:g}},b}),b.define("select2/data/minimumInputLength",[],function(){function a(a,b,c){this.minimumInputLength=c.get("minimumInputLength"),a.call(this,b,c)}return a.prototype.query=function(a,b,c){if(b.term=b.term||"",b.term.length<this.minimumInputLength)return void this.trigger("results:message",{message:"inputTooShort",args:{minimum:this.minimumInputLength,input:b.term,params:b}});a.call(this,b,c)},a}),b.define("select2/data/maximumInputLength",[],function(){function a(a,b,c){this.maximumInputLength=c.get("maximumInputLength"),a.call(this,b,c)}return a.prototype.query=function(a,b,c){if(b.term=b.term||"",this.maximumInputLength>0&&b.term.length>this.maximumInputLength)return void this.trigger("results:message",{message:"inputTooLong",args:{maximum:this.maximumInputLength,input:b.term,params:b}});a.call(this,b,c)},a}),b.define("select2/data/maximumSelectionLength",[],function(){function a(a,b,c){this.maximumSelectionLength=c.get("maximumSelectionLength"),a.call(this,b,c)}return a.prototype.query=function(a,b,c){var d=this;this.current(function(e){var f=null!=e?e.length:0;if(d.maximumSelectionLength>0&&f>=d.maximumSelectionLength)return void d.trigger("results:message",{message:"maximumSelected",args:{maximum:d.maximumSelectionLength}});a.call(d,b,c)})},a}),b.define("select2/dropdown",["jquery","./utils"],function(a,b){function c(a,b){this.$element=a,this.options=b,c.__super__.constructor.call(this)}return b.Extend(c,b.Observable),c.prototype.render=function(){var b=a('<span class="select2-dropdown"><span class="select2-results"></span></span>');return b.attr("dir",this.options.get("dir")),this.$dropdown=b,b},c.prototype.bind=function(){},c.prototype.position=function(a,b){},c.prototype.destroy=function(){this.$dropdown.remove()},c}),b.define("select2/dropdown/search",["jquery","../utils"],function(a,b){function c(){}return c.prototype.render=function(b){var c=b.call(this),d=a('<span class="select2-search select2-search--dropdown"><input class="select2-search__field" type="search" tabindex="-1" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" /></span>');return this.$searchContainer=d,this.$search=d.find("input"),c.prepend(d),c},c.prototype.bind=function(b,c,d){var e=this;b.call(this,c,d),this.$search.on("keydown",function(a){e.trigger("keypress",a),e._keyUpPrevented=a.isDefaultPrevented()}),this.$search.on("input",function(b){a(this).off("keyup")}),this.$search.on("keyup input",function(a){e.handleSearch(a)}),c.on("open",function(){e.$search.attr("tabindex",0),e.$search.focus(),window.setTimeout(function(){e.$search.focus()},0)}),c.on("close",function(){e.$search.attr("tabindex",-1),e.$search.val("")}),c.on("focus",function(){c.isOpen()||e.$search.focus()}),c.on("results:all",function(a){if(null==a.query.term||""===a.query.term){e.showSearch(a)?e.$searchContainer.removeClass("select2-search--hide"):e.$searchContainer.addClass("select2-search--hide")}})},c.prototype.handleSearch=function(a){if(!this._keyUpPrevented){var b=this.$search.val();this.trigger("query",{term:b})}this._keyUpPrevented=!1},c.prototype.showSearch=function(a,b){return!0},c}),b.define("select2/dropdown/hidePlaceholder",[],function(){function a(a,b,c,d){this.placeholder=this.normalizePlaceholder(c.get("placeholder")),a.call(this,b,c,d)}return a.prototype.append=function(a,b){b.results=this.removePlaceholder(b.results),a.call(this,b)},a.prototype.normalizePlaceholder=function(a,b){return"string"==typeof b&&(b={id:"",text:b}),b},a.prototype.removePlaceholder=function(a,b){for(var c=b.slice(0),d=b.length-1;d>=0;d--){var e=b[d];this.placeholder.id===e.id&&c.splice(d,1)}return c},a}),b.define("select2/dropdown/infiniteScroll",["jquery"],function(a){function b(a,b,c,d){this.lastParams={},a.call(this,b,c,d),this.$loadingMore=this.createLoadingMore(),this.loading=!1}return b.prototype.append=function(a,b){this.$loadingMore.remove(),this.loading=!1,a.call(this,b),this.showLoadingMore(b)&&this.$results.append(this.$loadingMore)},b.prototype.bind=function(b,c,d){var e=this;b.call(this,c,d),c.on("query",function(a){e.lastParams=a,e.loading=!0}),c.on("query:append",function(a){e.lastParams=a,e.loading=!0}),this.$results.on("scroll",function(){var b=a.contains(document.documentElement,e.$loadingMore[0]);if(!e.loading&&b){e.$results.offset().top+e.$results.outerHeight(!1)+50>=e.$loadingMore.offset().top+e.$loadingMore.outerHeight(!1)&&e.loadMore()}})},b.prototype.loadMore=function(){this.loading=!0;var b=a.extend({},{page:1},this.lastParams);b.page++,this.trigger("query:append",b)},b.prototype.showLoadingMore=function(a,b){return b.pagination&&b.pagination.more},b.prototype.createLoadingMore=function(){var b=a('<li class="select2-results__option select2-results__option--load-more"role="treeitem" aria-disabled="true"></li>'),c=this.options.get("translations").get("loadingMore");return b.html(c(this.lastParams)),b},b}),b.define("select2/dropdown/attachBody",["jquery","../utils"],function(a,b){function c(b,c,d){this.$dropdownParent=d.get("dropdownParent")||a(document.body),b.call(this,c,d)}return c.prototype.bind=function(a,b,c){var d=this,e=!1;a.call(this,b,c),b.on("open",function(){d._showDropdown(),d._attachPositioningHandler(b),e||(e=!0,b.on("results:all",function(){d._positionDropdown(),d._resizeDropdown()}),b.on("results:append",function(){d._positionDropdown(),d._resizeDropdown()}))}),b.on("close",function(){d._hideDropdown(),d._detachPositioningHandler(b)}),this.$dropdownContainer.on("mousedown",function(a){a.stopPropagation()})},c.prototype.destroy=function(a){a.call(this),this.$dropdownContainer.remove()},c.prototype.position=function(a,b,c){b.attr("class",c.attr("class")),b.removeClass("select2"),b.addClass("select2-container--open"),b.css({position:"absolute",top:-999999}),this.$container=c},c.prototype.render=function(b){var c=a("<span></span>"),d=b.call(this);return c.append(d),this.$dropdownContainer=c,c},c.prototype._hideDropdown=function(a){this.$dropdownContainer.detach()},c.prototype._attachPositioningHandler=function(c,d){var e=this,f="scroll.select2."+d.id,g="resize.select2."+d.id,h="orientationchange.select2."+d.id,i=this.$container.parents().filter(b.hasScroll);i.each(function(){a(this).data("select2-scroll-position",{x:a(this).scrollLeft(),y:a(this).scrollTop()})}),i.on(f,function(b){var c=a(this).data("select2-scroll-position");a(this).scrollTop(c.y)}),a(window).on(f+" "+g+" "+h,function(a){e._positionDropdown(),e._resizeDropdown()})},c.prototype._detachPositioningHandler=function(c,d){var e="scroll.select2."+d.id,f="resize.select2."+d.id,g="orientationchange.select2."+d.id;this.$container.parents().filter(b.hasScroll).off(e),a(window).off(e+" "+f+" "+g)},c.prototype._positionDropdown=function(){var b=a(window),c=this.$dropdown.hasClass("select2-dropdown--above"),d=this.$dropdown.hasClass("select2-dropdown--below"),e=null,f=this.$container.offset();f.bottom=f.top+this.$container.outerHeight(!1);var g={height:this.$container.outerHeight(!1)};g.top=f.top,g.bottom=f.top+g.height;var h={height:this.$dropdown.outerHeight(!1)},i={top:b.scrollTop(),bottom:b.scrollTop()+b.height()},j=i.top<f.top-h.height,k=i.bottom>f.bottom+h.height,l={left:f.left,top:g.bottom},m=this.$dropdownParent;"static"===m.css("position")&&(m=m.offsetParent());var n=m.offset();l.top-=n.top,l.left-=n.left,c||d||(e="below"),k||!j||c?!j&&k&&c&&(e="below"):e="above",("above"==e||c&&"below"!==e)&&(l.top=g.top-n.top-h.height),null!=e&&(this.$dropdown.removeClass("select2-dropdown--below select2-dropdown--above").addClass("select2-dropdown--"+e),this.$container.removeClass("select2-container--below select2-container--above").addClass("select2-container--"+e)),this.$dropdownContainer.css(l)},c.prototype._resizeDropdown=function(){var a={width:this.$container.outerWidth(!1)+"px"};this.options.get("dropdownAutoWidth")&&(a.minWidth=a.width,a.position="relative",a.width="auto"),this.$dropdown.css(a)},c.prototype._showDropdown=function(a){this.$dropdownContainer.appendTo(this.$dropdownParent),this._positionDropdown(),this._resizeDropdown()},c}),b.define("select2/dropdown/minimumResultsForSearch",[],function(){function a(b){for(var c=0,d=0;d<b.length;d++){var e=b[d];e.children?c+=a(e.children):c++}return c}function b(a,b,c,d){this.minimumResultsForSearch=c.get("minimumResultsForSearch"),this.minimumResultsForSearch<0&&(this.minimumResultsForSearch=1/0),a.call(this,b,c,d)}return b.prototype.showSearch=function(b,c){return!(a(c.data.results)<this.minimumResultsForSearch)&&b.call(this,c)},b}),b.define("select2/dropdown/selectOnClose",[],function(){function a(){}return a.prototype.bind=function(a,b,c){var d=this;a.call(this,b,c),b.on("close",function(a){d._handleSelectOnClose(a)})},a.prototype._handleSelectOnClose=function(a,b){if(b&&null!=b.originalSelect2Event){var c=b.originalSelect2Event;if("select"===c._type||"unselect"===c._type)return}var d=this.getHighlightedResults();if(!(d.length<1)){var e=d.data("data");null!=e.element&&e.element.selected||null==e.element&&e.selected||this.trigger("select",{data:e})}},a}),b.define("select2/dropdown/closeOnSelect",[],function(){function a(){}return a.prototype.bind=function(a,b,c){var d=this;a.call(this,b,c),b.on("select",function(a){d._selectTriggered(a)}),b.on("unselect",function(a){d._selectTriggered(a)})},a.prototype._selectTriggered=function(a,b){var c=b.originalEvent;c&&c.ctrlKey||this.trigger("close",{originalEvent:c,originalSelect2Event:b})},a}),b.define("select2/i18n/en",[],function(){return{errorLoading:function(){return"The results could not be loaded."},inputTooLong:function(a){var b=a.input.length-a.maximum,c="Please delete "+b+" character";return 1!=b&&(c+="s"),c},inputTooShort:function(a){return"Please enter "+(a.minimum-a.input.length)+" or more characters"},loadingMore:function(){return"Loading more results…"},maximumSelected:function(a){var b="You can only select "+a.maximum+" item";return 1!=a.maximum&&(b+="s"),b},noResults:function(){return"No results found"},searching:function(){return"Searching…"}}}),b.define("select2/defaults",["jquery","require","./results","./selection/single","./selection/multiple","./selection/placeholder","./selection/allowClear","./selection/search","./selection/eventRelay","./utils","./translation","./diacritics","./data/select","./data/array","./data/ajax","./data/tags","./data/tokenizer","./data/minimumInputLength","./data/maximumInputLength","./data/maximumSelectionLength","./dropdown","./dropdown/search","./dropdown/hidePlaceholder","./dropdown/infiniteScroll","./dropdown/attachBody","./dropdown/minimumResultsForSearch","./dropdown/selectOnClose","./dropdown/closeOnSelect","./i18n/en"],function(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C){function D(){this.reset()}return D.prototype.apply=function(l){if(l=a.extend(!0,{},this.defaults,l),null==l.dataAdapter){if(null!=l.ajax?l.dataAdapter=o:null!=l.data?l.dataAdapter=n:l.dataAdapter=m,l.minimumInputLength>0&&(l.dataAdapter=j.Decorate(l.dataAdapter,r)),l.maximumInputLength>0&&(l.dataAdapter=j.Decorate(l.dataAdapter,s)),l.maximumSelectionLength>0&&(l.dataAdapter=j.Decorate(l.dataAdapter,t)),l.tags&&(l.dataAdapter=j.Decorate(l.dataAdapter,p)),null==l.tokenSeparators&&null==l.tokenizer||(l.dataAdapter=j.Decorate(l.dataAdapter,q)),null!=l.query){var C=b(l.amdBase+"compat/query");l.dataAdapter=j.Decorate(l.dataAdapter,C)}if(null!=l.initSelection){var D=b(l.amdBase+"compat/initSelection");l.dataAdapter=j.Decorate(l.dataAdapter,D)}}if(null==l.resultsAdapter&&(l.resultsAdapter=c,null!=l.ajax&&(l.resultsAdapter=j.Decorate(l.resultsAdapter,x)),null!=l.placeholder&&(l.resultsAdapter=j.Decorate(l.resultsAdapter,w)),l.selectOnClose&&(l.resultsAdapter=j.Decorate(l.resultsAdapter,A))),null==l.dropdownAdapter){if(l.multiple)l.dropdownAdapter=u;else{var E=j.Decorate(u,v);l.dropdownAdapter=E}if(0!==l.minimumResultsForSearch&&(l.dropdownAdapter=j.Decorate(l.dropdownAdapter,z)),l.closeOnSelect&&(l.dropdownAdapter=j.Decorate(l.dropdownAdapter,B)),null!=l.dropdownCssClass||null!=l.dropdownCss||null!=l.adaptDropdownCssClass){var F=b(l.amdBase+"compat/dropdownCss");l.dropdownAdapter=j.Decorate(l.dropdownAdapter,F)}l.dropdownAdapter=j.Decorate(l.dropdownAdapter,y)}if(null==l.selectionAdapter){if(l.multiple?l.selectionAdapter=e:l.selectionAdapter=d,null!=l.placeholder&&(l.selectionAdapter=j.Decorate(l.selectionAdapter,f)),l.allowClear&&(l.selectionAdapter=j.Decorate(l.selectionAdapter,g)),l.multiple&&(l.selectionAdapter=j.Decorate(l.selectionAdapter,h)),null!=l.containerCssClass||null!=l.containerCss||null!=l.adaptContainerCssClass){var G=b(l.amdBase+"compat/containerCss");l.selectionAdapter=j.Decorate(l.selectionAdapter,G)}l.selectionAdapter=j.Decorate(l.selectionAdapter,i)}if("string"==typeof l.language)if(l.language.indexOf("-")>0){var H=l.language.split("-"),I=H[0];l.language=[l.language,I]}else l.language=[l.language];if(a.isArray(l.language)){var J=new k;l.language.push("en");for(var K=l.language,L=0;L<K.length;L++){var M=K[L],N={};try{N=k.loadPath(M)}catch(a){try{M=this.defaults.amdLanguageBase+M,N=k.loadPath(M)}catch(a){l.debug&&window.console&&console.warn&&console.warn('Select2: The language file for "'+M+'" could not be automatically loaded. A fallback will be used instead.');continue}}J.extend(N)}l.translations=J}else{var O=k.loadPath(this.defaults.amdLanguageBase+"en"),P=new k(l.language);P.extend(O),l.translations=P}return l},D.prototype.reset=function(){function b(a){function b(a){return l[a]||a}return a.replace(/[^\u0000-\u007E]/g,b)}function c(d,e){if(""===a.trim(d.term))return e;if(e.children&&e.children.length>0){for(var f=a.extend(!0,{},e),g=e.children.length-1;g>=0;g--){null==c(d,e.children[g])&&f.children.splice(g,1)}return f.children.length>0?f:c(d,f)}var h=b(e.text).toUpperCase(),i=b(d.term).toUpperCase();return h.indexOf(i)>-1?e:null}this.defaults={amdBase:"./",amdLanguageBase:"./i18n/",closeOnSelect:!0,debug:!1,dropdownAutoWidth:!1,escapeMarkup:j.escapeMarkup,language:C,matcher:c,minimumInputLength:0,maximumInputLength:0,maximumSelectionLength:0,minimumResultsForSearch:0,selectOnClose:!1,sorter:function(a){return a},templateResult:function(a){return a.text},templateSelection:function(a){return a.text},theme:"default",width:"resolve"}},D.prototype.set=function(b,c){var d=a.camelCase(b),e={};e[d]=c;var f=j._convertData(e);a.extend(this.defaults,f)},new D}),b.define("select2/options",["require","jquery","./defaults","./utils"],function(a,b,c,d){function e(b,e){if(this.options=b,null!=e&&this.fromElement(e),this.options=c.apply(this.options),e&&e.is("input")){var f=a(this.get("amdBase")+"compat/inputData");this.options.dataAdapter=d.Decorate(this.options.dataAdapter,f)}}return e.prototype.fromElement=function(a){var c=["select2"];null==this.options.multiple&&(this.options.multiple=a.prop("multiple")),null==this.options.disabled&&(this.options.disabled=a.prop("disabled")),null==this.options.language&&(a.prop("lang")?this.options.language=a.prop("lang").toLowerCase():a.closest("[lang]").prop("lang")&&(this.options.language=a.closest("[lang]").prop("lang"))),null==this.options.dir&&(a.prop("dir")?this.options.dir=a.prop("dir"):a.closest("[dir]").prop("dir")?this.options.dir=a.closest("[dir]").prop("dir"):this.options.dir="ltr"),a.prop("disabled",this.options.disabled),a.prop("multiple",this.options.multiple),a.data("select2Tags")&&(this.options.debug&&window.console&&console.warn&&console.warn('Select2: The `data-select2-tags` attribute has been changed to use the `data-data` and `data-tags="true"` attributes and will be removed in future versions of Select2.'),a.data("data",a.data("select2Tags")),a.data("tags",!0)),a.data("ajaxUrl")&&(this.options.debug&&window.console&&console.warn&&console.warn("Select2: The `data-ajax-url` attribute has been changed to `data-ajax--url` and support for the old attribute will be removed in future versions of Select2."),a.attr("ajax--url",a.data("ajaxUrl")),a.data("ajax--url",a.data("ajaxUrl")));var e={};e=b.fn.jquery&&"1."==b.fn.jquery.substr(0,2)&&a[0].dataset?b.extend(!0,{},a[0].dataset,a.data()):a.data();var f=b.extend(!0,{},e);f=d._convertData(f);for(var g in f)b.inArray(g,c)>-1||(b.isPlainObject(this.options[g])?b.extend(this.options[g],f[g]):this.options[g]=f[g]);return this},e.prototype.get=function(a){return this.options[a]},e.prototype.set=function(a,b){this.options[a]=b},e}),b.define("select2/core",["jquery","./options","./utils","./keys"],function(a,b,c,d){var e=function(a,c){null!=a.data("select2")&&a.data("select2").destroy(),this.$element=a,this.id=this._generateId(a),c=c||{},this.options=new b(c,a),e.__super__.constructor.call(this);var d=a.attr("tabindex")||0;a.data("old-tabindex",d),a.attr("tabindex","-1");var f=this.options.get("dataAdapter");this.dataAdapter=new f(a,this.options);var g=this.render();this._placeContainer(g);var h=this.options.get("selectionAdapter");this.selection=new h(a,this.options),this.$selection=this.selection.render(),this.selection.position(this.$selection,g);var i=this.options.get("dropdownAdapter");this.dropdown=new i(a,this.options),this.$dropdown=this.dropdown.render(),this.dropdown.position(this.$dropdown,g);var j=this.options.get("resultsAdapter");this.results=new j(a,this.options,this.dataAdapter),this.$results=this.results.render(),this.results.position(this.$results,this.$dropdown);var k=this;this._bindAdapters(),this._registerDomEvents(),this._registerDataEvents(),this._registerSelectionEvents(),this._registerDropdownEvents(),this._registerResultsEvents(),this._registerEvents(),this.dataAdapter.current(function(a){k.trigger("selection:update",{data:a})}),a.addClass("select2-hidden-accessible"),a.attr("aria-hidden","true"),this._syncAttributes(),a.data("select2",this)};return c.Extend(e,c.Observable),e.prototype._generateId=function(a){var b="";return b=null!=a.attr("id")?a.attr("id"):null!=a.attr("name")?a.attr("name")+"-"+c.generateChars(2):c.generateChars(4),b=b.replace(/(:|\.|\[|\]|,)/g,""),b="select2-"+b},e.prototype._placeContainer=function(a){a.insertAfter(this.$element);var b=this._resolveWidth(this.$element,this.options.get("width"));null!=b&&a.css("width",b)},e.prototype._resolveWidth=function(a,b){var c=/^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i;if("resolve"==b){var d=this._resolveWidth(a,"style");return null!=d?d:this._resolveWidth(a,"element")}if("element"==b){var e=a.outerWidth(!1);return e<=0?"auto":e+"px"}if("style"==b){var f=a.attr("style");if("string"!=typeof f)return null;for(var g=f.split(";"),h=0,i=g.length;h<i;h+=1){var j=g[h].replace(/\s/g,""),k=j.match(c);if(null!==k&&k.length>=1)return k[1]}return null}return b},e.prototype._bindAdapters=function(){this.dataAdapter.bind(this,this.$container),this.selection.bind(this,this.$container),this.dropdown.bind(this,this.$container),this.results.bind(this,this.$container)},e.prototype._registerDomEvents=function(){var b=this;this.$element.on("change.select2",function(){b.dataAdapter.current(function(a){b.trigger("selection:update",{data:a})})}),this.$element.on("focus.select2",function(a){b.trigger("focus",a)}),this._syncA=c.bind(this._syncAttributes,this),this._syncS=c.bind(this._syncSubtree,this),this.$element[0].attachEvent&&this.$element[0].attachEvent("onpropertychange",this._syncA);var d=window.MutationObserver||window.WebKitMutationObserver||window.MozMutationObserver;null!=d?(this._observer=new d(function(c){a.each(c,b._syncA),a.each(c,b._syncS)}),this._observer.observe(this.$element[0],{attributes:!0,childList:!0,subtree:!1})):this.$element[0].addEventListener&&(this.$element[0].addEventListener("DOMAttrModified",b._syncA,!1),this.$element[0].addEventListener("DOMNodeInserted",b._syncS,!1),this.$element[0].addEventListener("DOMNodeRemoved",b._syncS,!1))},e.prototype._registerDataEvents=function(){var a=this;this.dataAdapter.on("*",function(b,c){a.trigger(b,c)})},e.prototype._registerSelectionEvents=function(){var b=this,c=["toggle","focus"];this.selection.on("toggle",function(){b.toggleDropdown()}),this.selection.on("focus",function(a){b.focus(a)}),this.selection.on("*",function(d,e){-1===a.inArray(d,c)&&b.trigger(d,e)})},e.prototype._registerDropdownEvents=function(){var a=this;this.dropdown.on("*",function(b,c){a.trigger(b,c)})},e.prototype._registerResultsEvents=function(){var a=this;this.results.on("*",function(b,c){a.trigger(b,c)})},e.prototype._registerEvents=function(){var a=this;this.on("open",function(){a.$container.addClass("select2-container--open")}),this.on("close",function(){a.$container.removeClass("select2-container--open")}),this.on("enable",function(){a.$container.removeClass("select2-container--disabled")}),this.on("disable",function(){a.$container.addClass("select2-container--disabled")}),this.on("blur",function(){a.$container.removeClass("select2-container--focus")}),this.on("query",function(b){a.isOpen()||a.trigger("open",{}),this.dataAdapter.query(b,function(c){a.trigger("results:all",{data:c,query:b})})}),this.on("query:append",function(b){this.dataAdapter.query(b,function(c){a.trigger("results:append",{data:c,query:b})})}),this.on("keypress",function(b){var c=b.which;a.isOpen()?c===d.ESC||c===d.TAB||c===d.UP&&b.altKey?(a.close(),b.preventDefault()):c===d.ENTER?(a.trigger("results:select",{}),b.preventDefault()):c===d.SPACE&&b.ctrlKey?(a.trigger("results:toggle",{}),b.preventDefault()):c===d.UP?(a.trigger("results:previous",{}),b.preventDefault()):c===d.DOWN&&(a.trigger("results:next",{}),b.preventDefault()):(c===d.ENTER||c===d.SPACE||c===d.DOWN&&b.altKey)&&(a.open(),b.preventDefault())})},e.prototype._syncAttributes=function(){this.options.set("disabled",this.$element.prop("disabled")),this.options.get("disabled")?(this.isOpen()&&this.close(),this.trigger("disable",{})):this.trigger("enable",{})},e.prototype._syncSubtree=function(a,b){var c=!1,d=this;if(!a||!a.target||"OPTION"===a.target.nodeName||"OPTGROUP"===a.target.nodeName){if(b)if(b.addedNodes&&b.addedNodes.length>0)for(var e=0;e<b.addedNodes.length;e++){var f=b.addedNodes[e];f.selected&&(c=!0)}else b.removedNodes&&b.removedNodes.length>0&&(c=!0);else c=!0;c&&this.dataAdapter.current(function(a){d.trigger("selection:update",{data:a})})}},e.prototype.trigger=function(a,b){var c=e.__super__.trigger,d={open:"opening",close:"closing",select:"selecting",unselect:"unselecting"};if(void 0===b&&(b={}),a in d){var f=d[a],g={prevented:!1,name:a,args:b};if(c.call(this,f,g),g.prevented)return void(b.prevented=!0)}c.call(this,a,b)},e.prototype.toggleDropdown=function(){this.options.get("disabled")||(this.isOpen()?this.close():this.open())},e.prototype.open=function(){this.isOpen()||this.trigger("query",{})},e.prototype.close=function(){this.isOpen()&&this.trigger("close",{})},e.prototype.isOpen=function(){return this.$container.hasClass("select2-container--open")},e.prototype.hasFocus=function(){return this.$container.hasClass("select2-container--focus")},e.prototype.focus=function(a){this.hasFocus()||(this.$container.addClass("select2-container--focus"),this.trigger("focus",{}))},e.prototype.enable=function(a){this.options.get("debug")&&window.console&&console.warn&&console.warn('Select2: The `select2("enable")` method has been deprecated and will be removed in later Select2 versions. Use $element.prop("disabled") instead.'),null!=a&&0!==a.length||(a=[!0]);var b=!a[0];this.$element.prop("disabled",b)},e.prototype.data=function(){this.options.get("debug")&&arguments.length>0&&window.console&&console.warn&&console.warn('Select2: Data can no longer be set using `select2("data")`. You should consider setting the value instead using `$element.val()`.');var a=[];return this.dataAdapter.current(function(b){a=b}),a},e.prototype.val=function(b){if(this.options.get("debug")&&window.console&&console.warn&&console.warn('Select2: The `select2("val")` method has been deprecated and will be removed in later Select2 versions. Use $element.val() instead.'),null==b||0===b.length)return this.$element.val();var c=b[0];a.isArray(c)&&(c=a.map(c,function(a){return a.toString()})),this.$element.val(c).trigger("change")},e.prototype.destroy=function(){this.$container.remove(),this.$element[0].detachEvent&&this.$element[0].detachEvent("onpropertychange",this._syncA),null!=this._observer?(this._observer.disconnect(),this._observer=null):this.$element[0].removeEventListener&&(this.$element[0].removeEventListener("DOMAttrModified",this._syncA,!1),this.$element[0].removeEventListener("DOMNodeInserted",this._syncS,!1),this.$element[0].removeEventListener("DOMNodeRemoved",this._syncS,!1)),this._syncA=null,this._syncS=null,this.$element.off(".select2"),this.$element.attr("tabindex",this.$element.data("old-tabindex")),this.$element.removeClass("select2-hidden-accessible"),this.$element.attr("aria-hidden","false"),this.$element.removeData("select2"),this.dataAdapter.destroy(),this.selection.destroy(),this.dropdown.destroy(),this.results.destroy(),this.dataAdapter=null,this.selection=null,this.dropdown=null,this.results=null},e.prototype.render=function(){var b=a('<span class="select2 select2-container"><span class="selection"></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>');return b.attr("dir",this.options.get("dir")),this.$container=b,this.$container.addClass("select2-container--"+this.options.get("theme")),b.data("element",this.$element),b},e}),b.define("select2/compat/utils",["jquery"],function(a){function b(b,c,d){var e,f,g=[];e=a.trim(b.attr("class")),e&&(e=""+e,a(e.split(/\s+/)).each(function(){0===this.indexOf("select2-")&&g.push(this)})),e=a.trim(c.attr("class")),e&&(e=""+e,a(e.split(/\s+/)).each(function(){0!==this.indexOf("select2-")&&null!=(f=d(this))&&g.push(f)})),b.attr("class",g.join(" "))}return{syncCssClasses:b}}),b.define("select2/compat/containerCss",["jquery","./utils"],function(a,b){function c(a){return null}function d(){}return d.prototype.render=function(d){var e=d.call(this),f=this.options.get("containerCssClass")||"";a.isFunction(f)&&(f=f(this.$element));var g=this.options.get("adaptContainerCssClass");if(g=g||c,-1!==f.indexOf(":all:")){f=f.replace(":all:","");var h=g;g=function(a){var b=h(a);return null!=b?b+" "+a:a}}var i=this.options.get("containerCss")||{};return a.isFunction(i)&&(i=i(this.$element)),b.syncCssClasses(e,this.$element,g),e.css(i),e.addClass(f),e},d}),b.define("select2/compat/dropdownCss",["jquery","./utils"],function(a,b){function c(a){return null}function d(){}return d.prototype.render=function(d){var e=d.call(this),f=this.options.get("dropdownCssClass")||"";a.isFunction(f)&&(f=f(this.$element));var g=this.options.get("adaptDropdownCssClass");if(g=g||c,-1!==f.indexOf(":all:")){f=f.replace(":all:","");var h=g;g=function(a){var b=h(a);return null!=b?b+" "+a:a}}var i=this.options.get("dropdownCss")||{};return a.isFunction(i)&&(i=i(this.$element)),b.syncCssClasses(e,this.$element,g),e.css(i),e.addClass(f),e},d}),b.define("select2/compat/initSelection",["jquery"],function(a){function b(a,b,c){c.get("debug")&&window.console&&console.warn&&console.warn("Select2: The `initSelection` option has been deprecated in favor of a custom data adapter that overrides the `current` method. This method is now called multiple times instead of a single time when the instance is initialized. Support will be removed for the `initSelection` option in future versions of Select2"),this.initSelection=c.get("initSelection"),this._isInitialized=!1,a.call(this,b,c)}return b.prototype.current=function(b,c){var d=this;if(this._isInitialized)return void b.call(this,c);this.initSelection.call(null,this.$element,function(b){d._isInitialized=!0,a.isArray(b)||(b=[b]),c(b)})},b}),b.define("select2/compat/inputData",["jquery"],function(a){function b(a,b,c){this._currentData=[],this._valueSeparator=c.get("valueSeparator")||",","hidden"===b.prop("type")&&c.get("debug")&&console&&console.warn&&console.warn("Select2: Using a hidden input with Select2 is no longer supported and may stop working in the future. It is recommended to use a `<select>` element instead."),a.call(this,b,c)}return b.prototype.current=function(b,c){function d(b,c){var e=[];return b.selected||-1!==a.inArray(b.id,c)?(b.selected=!0,e.push(b)):b.selected=!1,b.children&&e.push.apply(e,d(b.children,c)),e}for(var e=[],f=0;f<this._currentData.length;f++){var g=this._currentData[f];e.push.apply(e,d(g,this.$element.val().split(this._valueSeparator)))}c(e)},b.prototype.select=function(b,c){if(this.options.get("multiple")){var d=this.$element.val();d+=this._valueSeparator+c.id,this.$element.val(d),this.$element.trigger("change")}else this.current(function(b){a.map(b,function(a){a.selected=!1})}),this.$element.val(c.id),this.$element.trigger("change")},b.prototype.unselect=function(a,b){var c=this;b.selected=!1,this.current(function(a){for(var d=[],e=0;e<a.length;e++){var f=a[e];b.id!=f.id&&d.push(f.id)}c.$element.val(d.join(c._valueSeparator)),c.$element.trigger("change")})},b.prototype.query=function(a,b,c){for(var d=[],e=0;e<this._currentData.length;e++){var f=this._currentData[e],g=this.matches(b,f);null!==g&&d.push(g)}c({results:d})},b.prototype.addOptions=function(b,c){var d=a.map(c,function(b){return a.data(b[0],"data")});this._currentData.push.apply(this._currentData,d)},b}),b.define("select2/compat/matcher",["jquery"],function(a){function b(b){function c(c,d){var e=a.extend(!0,{},d);if(null==c.term||""===a.trim(c.term))return e;if(d.children){for(var f=d.children.length-1;f>=0;f--){var g=d.children[f];b(c.term,g.text,g)||e.children.splice(f,1)}if(e.children.length>0)return e}return b(c.term,d.text,d)?e:null}return c}return b}),b.define("select2/compat/query",[],function(){function a(a,b,c){c.get("debug")&&window.console&&console.warn&&console.warn("Select2: The `query` option has been deprecated in favor of a custom data adapter that overrides the `query` method. Support will be removed for the `query` option in future versions of Select2."),a.call(this,b,c)}return a.prototype.query=function(a,b,c){b.callback=c,this.options.get("query").call(null,b)},a}),b.define("select2/dropdown/attachContainer",[],function(){function a(a,b,c){a.call(this,b,c)}return a.prototype.position=function(a,b,c){c.find(".dropdown-wrapper").append(b),b.addClass("select2-dropdown--below"),c.addClass("select2-container--below")},a}),b.define("select2/dropdown/stopPropagation",[],function(){function a(){}return a.prototype.bind=function(a,b,c){a.call(this,b,c);var d=["blur","change","click","dblclick","focus","focusin","focusout","input","keydown","keyup","keypress","mousedown","mouseenter","mouseleave","mousemove","mouseover","mouseup","search","touchend","touchstart"];this.$dropdown.on(d.join(" "),function(a){a.stopPropagation()})},a}),b.define("select2/selection/stopPropagation",[],function(){function a(){}return a.prototype.bind=function(a,b,c){a.call(this,b,c);var d=["blur","change","click","dblclick","focus","focusin","focusout","input","keydown","keyup","keypress","mousedown","mouseenter","mouseleave","mousemove","mouseover","mouseup","search","touchend","touchstart"];this.$selection.on(d.join(" "),function(a){a.stopPropagation()})},a}),function(c){"function"==typeof b.define&&b.define.amd?b.define("jquery-mousewheel",["jquery"],c):"object"==typeof exports?module.exports=c:c(a)}(function(a){function b(b){var g=b||window.event,h=i.call(arguments,1),j=0,l=0,m=0,n=0,o=0,p=0;if(b=a.event.fix(g),b.type="mousewheel","detail"in g&&(m=-1*g.detail),"wheelDelta"in g&&(m=g.wheelDelta),"wheelDeltaY"in g&&(m=g.wheelDeltaY),"wheelDeltaX"in g&&(l=-1*g.wheelDeltaX),"axis"in g&&g.axis===g.HORIZONTAL_AXIS&&(l=-1*m,m=0),j=0===m?l:m,"deltaY"in g&&(m=-1*g.deltaY,j=m),"deltaX"in g&&(l=g.deltaX,0===m&&(j=-1*l)),0!==m||0!==l){if(1===g.deltaMode){var q=a.data(this,"mousewheel-line-height");j*=q,m*=q,l*=q}else if(2===g.deltaMode){var r=a.data(this,"mousewheel-page-height");j*=r,m*=r,l*=r}if(n=Math.max(Math.abs(m),Math.abs(l)),(!f||n<f)&&(f=n,d(g,n)&&(f/=40)),d(g,n)&&(j/=40,l/=40,m/=40),j=Math[j>=1?"floor":"ceil"](j/f),l=Math[l>=1?"floor":"ceil"](l/f),m=Math[m>=1?"floor":"ceil"](m/f),k.settings.normalizeOffset&&this.getBoundingClientRect){var s=this.getBoundingClientRect();o=b.clientX-s.left,p=b.clientY-s.top}return b.deltaX=l,b.deltaY=m,b.deltaFactor=f,b.offsetX=o,b.offsetY=p,b.deltaMode=0,h.unshift(b,j,l,m),e&&clearTimeout(e),e=setTimeout(c,200),(a.event.dispatch||a.event.handle).apply(this,h)}}function c(){f=null}function d(a,b){return k.settings.adjustOldDeltas&&"mousewheel"===a.type&&b%120==0}var e,f,g=["wheel","mousewheel","DOMMouseScroll","MozMousePixelScroll"],h="onwheel"in document||document.documentMode>=9?["wheel"]:["mousewheel","DomMouseScroll","MozMousePixelScroll"],i=Array.prototype.slice;if(a.event.fixHooks)for(var j=g.length;j;)a.event.fixHooks[g[--j]]=a.event.mouseHooks;var k=a.event.special.mousewheel={version:"3.1.12",setup:function(){if(this.addEventListener)for(var c=h.length;c;)this.addEventListener(h[--c],b,!1);else this.onmousewheel=b;a.data(this,"mousewheel-line-height",k.getLineHeight(this)),a.data(this,"mousewheel-page-height",k.getPageHeight(this))},teardown:function(){if(this.removeEventListener)for(var c=h.length;c;)this.removeEventListener(h[--c],b,!1);else this.onmousewheel=null;a.removeData(this,"mousewheel-line-height"),a.removeData(this,"mousewheel-page-height")},getLineHeight:function(b){var c=a(b),d=c["offsetParent"in a.fn?"offsetParent":"parent"]();return d.length||(d=a("body")),parseInt(d.css("fontSize"),10)||parseInt(c.css("fontSize"),10)||16},getPageHeight:function(b){return a(b).height()},settings:{adjustOldDeltas:!0,normalizeOffset:!0}};a.fn.extend({mousewheel:function(a){return a?this.bind("mousewheel",a):this.trigger("mousewheel")},unmousewheel:function(a){return this.unbind("mousewheel",a)}})}),b.define("jquery.select2",["jquery","jquery-mousewheel","./select2/core","./select2/defaults"],function(a,b,c,d){if(null==a.fn.select2){var e=["open","close","destroy"];a.fn.select2=function(b){if("object"==typeof(b=b||{}))return this.each(function(){var d=a.extend(!0,{},b);new c(a(this),d)}),this;if("string"==typeof b){var d,f=Array.prototype.slice.call(arguments,1);return this.each(function(){var c=a(this).data("select2");null==c&&window.console&&console.error&&console.error("The select2('"+b+"') method was called on an element that is not using Select2."),d=c[b].apply(c,f)}),a.inArray(b,e)>-1?this:d}throw new Error("Invalid arguments for Select2: "+b)}}return null==a.fn.select2.defaults&&(a.fn.select2.defaults=d),c}),{define:b.define,require:b.require}}(),c=b.require("jquery.select2");return a.fn.select2.amd=b,c});
/*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/

 Version: 1.9.0
  Author: Ken Wheeler
 Website: http://kenwheeler.github.io
    Docs: http://kenwheeler.github.io/slick
    Repo: http://github.com/kenwheeler/slick
  Issues: http://github.com/kenwheeler/slick/issues

 */
(function(i){"use strict";"function"==typeof define&&define.amd?define(["jquery"],i):"undefined"!=typeof exports?module.exports=i(require("jquery")):i(jQuery)})(function(i){"use strict";var e=window.Slick||{};e=function(){function e(e,o){var s,n=this;n.defaults={accessibility:!0,adaptiveHeight:!1,appendArrows:i(e),appendDots:i(e),arrows:!0,asNavFor:null,prevArrow:'<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',nextArrow:'<button class="slick-next" aria-label="Next" type="button">Next</button>',autoplay:!1,autoplaySpeed:3e3,centerMode:!1,centerPadding:"50px",cssEase:"ease",customPaging:function(e,t){return i('<button type="button" />').text(t+1)},dots:!1,dotsClass:"slick-dots",draggable:!0,easing:"linear",edgeFriction:.35,fade:!1,focusOnSelect:!1,focusOnChange:!1,infinite:!0,initialSlide:0,lazyLoad:"ondemand",mobileFirst:!1,pauseOnHover:!0,pauseOnFocus:!0,pauseOnDotsHover:!1,respondTo:"window",responsive:null,rows:1,rtl:!1,slide:"",slidesPerRow:1,slidesToShow:1,slidesToScroll:1,speed:500,swipe:!0,swipeToSlide:!1,touchMove:!0,touchThreshold:5,useCSS:!0,useTransform:!0,variableWidth:!1,vertical:!1,verticalSwiping:!1,waitForAnimate:!0,zIndex:1e3},n.initials={animating:!1,dragging:!1,autoPlayTimer:null,currentDirection:0,currentLeft:null,currentSlide:0,direction:1,$dots:null,listWidth:null,listHeight:null,loadIndex:0,$nextArrow:null,$prevArrow:null,scrolling:!1,slideCount:null,slideWidth:null,$slideTrack:null,$slides:null,sliding:!1,slideOffset:0,swipeLeft:null,swiping:!1,$list:null,touchObject:{},transformsEnabled:!1,unslicked:!1},i.extend(n,n.initials),n.activeBreakpoint=null,n.animType=null,n.animProp=null,n.breakpoints=[],n.breakpointSettings=[],n.cssTransitions=!1,n.focussed=!1,n.interrupted=!1,n.hidden="hidden",n.paused=!0,n.positionProp=null,n.respondTo=null,n.rowCount=1,n.shouldClick=!0,n.$slider=i(e),n.$slidesCache=null,n.transformType=null,n.transitionType=null,n.visibilityChange="visibilitychange",n.windowWidth=0,n.windowTimer=null,s=i(e).data("slick")||{},n.options=i.extend({},n.defaults,o,s),n.currentSlide=n.options.initialSlide,n.originalSettings=n.options,"undefined"!=typeof document.mozHidden?(n.hidden="mozHidden",n.visibilityChange="mozvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(n.hidden="webkitHidden",n.visibilityChange="webkitvisibilitychange"),n.autoPlay=i.proxy(n.autoPlay,n),n.autoPlayClear=i.proxy(n.autoPlayClear,n),n.autoPlayIterator=i.proxy(n.autoPlayIterator,n),n.changeSlide=i.proxy(n.changeSlide,n),n.clickHandler=i.proxy(n.clickHandler,n),n.selectHandler=i.proxy(n.selectHandler,n),n.setPosition=i.proxy(n.setPosition,n),n.swipeHandler=i.proxy(n.swipeHandler,n),n.dragHandler=i.proxy(n.dragHandler,n),n.keyHandler=i.proxy(n.keyHandler,n),n.instanceUid=t++,n.htmlExpr=/^(?:\s*(<[\w\W]+>)[^>]*)$/,n.registerBreakpoints(),n.init(!0)}var t=0;return e}(),e.prototype.activateADA=function(){var i=this;i.$slideTrack.find(".slick-active").attr({"aria-hidden":"false"}).find("a, input, button, select").attr({tabindex:"0"})},e.prototype.addSlide=e.prototype.slickAdd=function(e,t,o){var s=this;if("boolean"==typeof t)o=t,t=null;else if(t<0||t>=s.slideCount)return!1;s.unload(),"number"==typeof t?0===t&&0===s.$slides.length?i(e).appendTo(s.$slideTrack):o?i(e).insertBefore(s.$slides.eq(t)):i(e).insertAfter(s.$slides.eq(t)):o===!0?i(e).prependTo(s.$slideTrack):i(e).appendTo(s.$slideTrack),s.$slides=s.$slideTrack.children(this.options.slide),s.$slideTrack.children(this.options.slide).detach(),s.$slideTrack.append(s.$slides),s.$slides.each(function(e,t){i(t).attr("data-slick-index",e)}),s.$slidesCache=s.$slides,s.reinit()},e.prototype.animateHeight=function(){var i=this;if(1===i.options.slidesToShow&&i.options.adaptiveHeight===!0&&i.options.vertical===!1){var e=i.$slides.eq(i.currentSlide).outerHeight(!0);i.$list.animate({height:e},i.options.speed)}},e.prototype.animateSlide=function(e,t){var o={},s=this;s.animateHeight(),s.options.rtl===!0&&s.options.vertical===!1&&(e=-e),s.transformsEnabled===!1?s.options.vertical===!1?s.$slideTrack.animate({left:e},s.options.speed,s.options.easing,t):s.$slideTrack.animate({top:e},s.options.speed,s.options.easing,t):s.cssTransitions===!1?(s.options.rtl===!0&&(s.currentLeft=-s.currentLeft),i({animStart:s.currentLeft}).animate({animStart:e},{duration:s.options.speed,easing:s.options.easing,step:function(i){i=Math.ceil(i),s.options.vertical===!1?(o[s.animType]="translate("+i+"px, 0px)",s.$slideTrack.css(o)):(o[s.animType]="translate(0px,"+i+"px)",s.$slideTrack.css(o))},complete:function(){t&&t.call()}})):(s.applyTransition(),e=Math.ceil(e),s.options.vertical===!1?o[s.animType]="translate3d("+e+"px, 0px, 0px)":o[s.animType]="translate3d(0px,"+e+"px, 0px)",s.$slideTrack.css(o),t&&setTimeout(function(){s.disableTransition(),t.call()},s.options.speed))},e.prototype.getNavTarget=function(){var e=this,t=e.options.asNavFor;return t&&null!==t&&(t=i(t).not(e.$slider)),t},e.prototype.asNavFor=function(e){var t=this,o=t.getNavTarget();null!==o&&"object"==typeof o&&o.each(function(){var t=i(this).slick("getSlick");t.unslicked||t.slideHandler(e,!0)})},e.prototype.applyTransition=function(i){var e=this,t={};e.options.fade===!1?t[e.transitionType]=e.transformType+" "+e.options.speed+"ms "+e.options.cssEase:t[e.transitionType]="opacity "+e.options.speed+"ms "+e.options.cssEase,e.options.fade===!1?e.$slideTrack.css(t):e.$slides.eq(i).css(t)},e.prototype.autoPlay=function(){var i=this;i.autoPlayClear(),i.slideCount>i.options.slidesToShow&&(i.autoPlayTimer=setInterval(i.autoPlayIterator,i.options.autoplaySpeed))},e.prototype.autoPlayClear=function(){var i=this;i.autoPlayTimer&&clearInterval(i.autoPlayTimer)},e.prototype.autoPlayIterator=function(){var i=this,e=i.currentSlide+i.options.slidesToScroll;i.paused||i.interrupted||i.focussed||(i.options.infinite===!1&&(1===i.direction&&i.currentSlide+1===i.slideCount-1?i.direction=0:0===i.direction&&(e=i.currentSlide-i.options.slidesToScroll,i.currentSlide-1===0&&(i.direction=1))),i.slideHandler(e))},e.prototype.buildArrows=function(){var e=this;e.options.arrows===!0&&(e.$prevArrow=i(e.options.prevArrow).addClass("slick-arrow"),e.$nextArrow=i(e.options.nextArrow).addClass("slick-arrow"),e.slideCount>e.options.slidesToShow?(e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),e.htmlExpr.test(e.options.prevArrow)&&e.$prevArrow.prependTo(e.options.appendArrows),e.htmlExpr.test(e.options.nextArrow)&&e.$nextArrow.appendTo(e.options.appendArrows),e.options.infinite!==!0&&e.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true")):e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({"aria-disabled":"true",tabindex:"-1"}))},e.prototype.buildDots=function(){var e,t,o=this;if(o.options.dots===!0&&o.slideCount>o.options.slidesToShow){for(o.$slider.addClass("slick-dotted"),t=i("<ul />").addClass(o.options.dotsClass),e=0;e<=o.getDotCount();e+=1)t.append(i("<li />").append(o.options.customPaging.call(this,o,e)));o.$dots=t.appendTo(o.options.appendDots),o.$dots.find("li").first().addClass("slick-active")}},e.prototype.buildOut=function(){var e=this;e.$slides=e.$slider.children(e.options.slide+":not(.slick-cloned)").addClass("slick-slide"),e.slideCount=e.$slides.length,e.$slides.each(function(e,t){i(t).attr("data-slick-index",e).data("originalStyling",i(t).attr("style")||"")}),e.$slider.addClass("slick-slider"),e.$slideTrack=0===e.slideCount?i('<div class="slick-track"/>').appendTo(e.$slider):e.$slides.wrapAll('<div class="slick-track"/>').parent(),e.$list=e.$slideTrack.wrap('<div class="slick-list"/>').parent(),e.$slideTrack.css("opacity",0),e.options.centerMode!==!0&&e.options.swipeToSlide!==!0||(e.options.slidesToScroll=1),i("img[data-lazy]",e.$slider).not("[src]").addClass("slick-loading"),e.setupInfinite(),e.buildArrows(),e.buildDots(),e.updateDots(),e.setSlideClasses("number"==typeof e.currentSlide?e.currentSlide:0),e.options.draggable===!0&&e.$list.addClass("draggable")},e.prototype.buildRows=function(){var i,e,t,o,s,n,r,l=this;if(o=document.createDocumentFragment(),n=l.$slider.children(),l.options.rows>0){for(r=l.options.slidesPerRow*l.options.rows,s=Math.ceil(n.length/r),i=0;i<s;i++){var d=document.createElement("div");for(e=0;e<l.options.rows;e++){var a=document.createElement("div");for(t=0;t<l.options.slidesPerRow;t++){var c=i*r+(e*l.options.slidesPerRow+t);n.get(c)&&a.appendChild(n.get(c))}d.appendChild(a)}o.appendChild(d)}l.$slider.empty().append(o),l.$slider.children().children().children().css({width:100/l.options.slidesPerRow+"%",display:"inline-block"})}},e.prototype.checkResponsive=function(e,t){var o,s,n,r=this,l=!1,d=r.$slider.width(),a=window.innerWidth||i(window).width();if("window"===r.respondTo?n=a:"slider"===r.respondTo?n=d:"min"===r.respondTo&&(n=Math.min(a,d)),r.options.responsive&&r.options.responsive.length&&null!==r.options.responsive){s=null;for(o in r.breakpoints)r.breakpoints.hasOwnProperty(o)&&(r.originalSettings.mobileFirst===!1?n<r.breakpoints[o]&&(s=r.breakpoints[o]):n>r.breakpoints[o]&&(s=r.breakpoints[o]));null!==s?null!==r.activeBreakpoint?(s!==r.activeBreakpoint||t)&&(r.activeBreakpoint=s,"unslick"===r.breakpointSettings[s]?r.unslick(s):(r.options=i.extend({},r.originalSettings,r.breakpointSettings[s]),e===!0&&(r.currentSlide=r.options.initialSlide),r.refresh(e)),l=s):(r.activeBreakpoint=s,"unslick"===r.breakpointSettings[s]?r.unslick(s):(r.options=i.extend({},r.originalSettings,r.breakpointSettings[s]),e===!0&&(r.currentSlide=r.options.initialSlide),r.refresh(e)),l=s):null!==r.activeBreakpoint&&(r.activeBreakpoint=null,r.options=r.originalSettings,e===!0&&(r.currentSlide=r.options.initialSlide),r.refresh(e),l=s),e||l===!1||r.$slider.trigger("breakpoint",[r,l])}},e.prototype.changeSlide=function(e,t){var o,s,n,r=this,l=i(e.currentTarget);switch(l.is("a")&&e.preventDefault(),l.is("li")||(l=l.closest("li")),n=r.slideCount%r.options.slidesToScroll!==0,o=n?0:(r.slideCount-r.currentSlide)%r.options.slidesToScroll,e.data.message){case"previous":s=0===o?r.options.slidesToScroll:r.options.slidesToShow-o,r.slideCount>r.options.slidesToShow&&r.slideHandler(r.currentSlide-s,!1,t);break;case"next":s=0===o?r.options.slidesToScroll:o,r.slideCount>r.options.slidesToShow&&r.slideHandler(r.currentSlide+s,!1,t);break;case"index":var d=0===e.data.index?0:e.data.index||l.index()*r.options.slidesToScroll;r.slideHandler(r.checkNavigable(d),!1,t),l.children().trigger("focus");break;default:return}},e.prototype.checkNavigable=function(i){var e,t,o=this;if(e=o.getNavigableIndexes(),t=0,i>e[e.length-1])i=e[e.length-1];else for(var s in e){if(i<e[s]){i=t;break}t=e[s]}return i},e.prototype.cleanUpEvents=function(){var e=this;e.options.dots&&null!==e.$dots&&(i("li",e.$dots).off("click.slick",e.changeSlide).off("mouseenter.slick",i.proxy(e.interrupt,e,!0)).off("mouseleave.slick",i.proxy(e.interrupt,e,!1)),e.options.accessibility===!0&&e.$dots.off("keydown.slick",e.keyHandler)),e.$slider.off("focus.slick blur.slick"),e.options.arrows===!0&&e.slideCount>e.options.slidesToShow&&(e.$prevArrow&&e.$prevArrow.off("click.slick",e.changeSlide),e.$nextArrow&&e.$nextArrow.off("click.slick",e.changeSlide),e.options.accessibility===!0&&(e.$prevArrow&&e.$prevArrow.off("keydown.slick",e.keyHandler),e.$nextArrow&&e.$nextArrow.off("keydown.slick",e.keyHandler))),e.$list.off("touchstart.slick mousedown.slick",e.swipeHandler),e.$list.off("touchmove.slick mousemove.slick",e.swipeHandler),e.$list.off("touchend.slick mouseup.slick",e.swipeHandler),e.$list.off("touchcancel.slick mouseleave.slick",e.swipeHandler),e.$list.off("click.slick",e.clickHandler),i(document).off(e.visibilityChange,e.visibility),e.cleanUpSlideEvents(),e.options.accessibility===!0&&e.$list.off("keydown.slick",e.keyHandler),e.options.focusOnSelect===!0&&i(e.$slideTrack).children().off("click.slick",e.selectHandler),i(window).off("orientationchange.slick.slick-"+e.instanceUid,e.orientationChange),i(window).off("resize.slick.slick-"+e.instanceUid,e.resize),i("[draggable!=true]",e.$slideTrack).off("dragstart",e.preventDefault),i(window).off("load.slick.slick-"+e.instanceUid,e.setPosition)},e.prototype.cleanUpSlideEvents=function(){var e=this;e.$list.off("mouseenter.slick",i.proxy(e.interrupt,e,!0)),e.$list.off("mouseleave.slick",i.proxy(e.interrupt,e,!1))},e.prototype.cleanUpRows=function(){var i,e=this;e.options.rows>0&&(i=e.$slides.children().children(),i.removeAttr("style"),e.$slider.empty().append(i))},e.prototype.clickHandler=function(i){var e=this;e.shouldClick===!1&&(i.stopImmediatePropagation(),i.stopPropagation(),i.preventDefault())},e.prototype.destroy=function(e){var t=this;t.autoPlayClear(),t.touchObject={},t.cleanUpEvents(),i(".slick-cloned",t.$slider).detach(),t.$dots&&t.$dots.remove(),t.$prevArrow&&t.$prevArrow.length&&(t.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),t.htmlExpr.test(t.options.prevArrow)&&t.$prevArrow.remove()),t.$nextArrow&&t.$nextArrow.length&&(t.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),t.htmlExpr.test(t.options.nextArrow)&&t.$nextArrow.remove()),t.$slides&&(t.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function(){i(this).attr("style",i(this).data("originalStyling"))}),t.$slideTrack.children(this.options.slide).detach(),t.$slideTrack.detach(),t.$list.detach(),t.$slider.append(t.$slides)),t.cleanUpRows(),t.$slider.removeClass("slick-slider"),t.$slider.removeClass("slick-initialized"),t.$slider.removeClass("slick-dotted"),t.unslicked=!0,e||t.$slider.trigger("destroy",[t])},e.prototype.disableTransition=function(i){var e=this,t={};t[e.transitionType]="",e.options.fade===!1?e.$slideTrack.css(t):e.$slides.eq(i).css(t)},e.prototype.fadeSlide=function(i,e){var t=this;t.cssTransitions===!1?(t.$slides.eq(i).css({zIndex:t.options.zIndex}),t.$slides.eq(i).animate({opacity:1},t.options.speed,t.options.easing,e)):(t.applyTransition(i),t.$slides.eq(i).css({opacity:1,zIndex:t.options.zIndex}),e&&setTimeout(function(){t.disableTransition(i),e.call()},t.options.speed))},e.prototype.fadeSlideOut=function(i){var e=this;e.cssTransitions===!1?e.$slides.eq(i).animate({opacity:0,zIndex:e.options.zIndex-2},e.options.speed,e.options.easing):(e.applyTransition(i),e.$slides.eq(i).css({opacity:0,zIndex:e.options.zIndex-2}))},e.prototype.filterSlides=e.prototype.slickFilter=function(i){var e=this;null!==i&&(e.$slidesCache=e.$slides,e.unload(),e.$slideTrack.children(this.options.slide).detach(),e.$slidesCache.filter(i).appendTo(e.$slideTrack),e.reinit())},e.prototype.focusHandler=function(){var e=this;e.$slider.off("focus.slick blur.slick").on("focus.slick","*",function(t){var o=i(this);setTimeout(function(){e.options.pauseOnFocus&&o.is(":focus")&&(e.focussed=!0,e.autoPlay())},0)}).on("blur.slick","*",function(t){i(this);e.options.pauseOnFocus&&(e.focussed=!1,e.autoPlay())})},e.prototype.getCurrent=e.prototype.slickCurrentSlide=function(){var i=this;return i.currentSlide},e.prototype.getDotCount=function(){var i=this,e=0,t=0,o=0;if(i.options.infinite===!0)if(i.slideCount<=i.options.slidesToShow)++o;else for(;e<i.slideCount;)++o,e=t+i.options.slidesToScroll,t+=i.options.slidesToScroll<=i.options.slidesToShow?i.options.slidesToScroll:i.options.slidesToShow;else if(i.options.centerMode===!0)o=i.slideCount;else if(i.options.asNavFor)for(;e<i.slideCount;)++o,e=t+i.options.slidesToScroll,t+=i.options.slidesToScroll<=i.options.slidesToShow?i.options.slidesToScroll:i.options.slidesToShow;else o=1+Math.ceil((i.slideCount-i.options.slidesToShow)/i.options.slidesToScroll);return o-1},e.prototype.getLeft=function(i){var e,t,o,s,n=this,r=0;return n.slideOffset=0,t=n.$slides.first().outerHeight(!0),n.options.infinite===!0?(n.slideCount>n.options.slidesToShow&&(n.slideOffset=n.slideWidth*n.options.slidesToShow*-1,s=-1,n.options.vertical===!0&&n.options.centerMode===!0&&(2===n.options.slidesToShow?s=-1.5:1===n.options.slidesToShow&&(s=-2)),r=t*n.options.slidesToShow*s),n.slideCount%n.options.slidesToScroll!==0&&i+n.options.slidesToScroll>n.slideCount&&n.slideCount>n.options.slidesToShow&&(i>n.slideCount?(n.slideOffset=(n.options.slidesToShow-(i-n.slideCount))*n.slideWidth*-1,r=(n.options.slidesToShow-(i-n.slideCount))*t*-1):(n.slideOffset=n.slideCount%n.options.slidesToScroll*n.slideWidth*-1,r=n.slideCount%n.options.slidesToScroll*t*-1))):i+n.options.slidesToShow>n.slideCount&&(n.slideOffset=(i+n.options.slidesToShow-n.slideCount)*n.slideWidth,r=(i+n.options.slidesToShow-n.slideCount)*t),n.slideCount<=n.options.slidesToShow&&(n.slideOffset=0,r=0),n.options.centerMode===!0&&n.slideCount<=n.options.slidesToShow?n.slideOffset=n.slideWidth*Math.floor(n.options.slidesToShow)/2-n.slideWidth*n.slideCount/2:n.options.centerMode===!0&&n.options.infinite===!0?n.slideOffset+=n.slideWidth*Math.floor(n.options.slidesToShow/2)-n.slideWidth:n.options.centerMode===!0&&(n.slideOffset=0,n.slideOffset+=n.slideWidth*Math.floor(n.options.slidesToShow/2)),e=n.options.vertical===!1?i*n.slideWidth*-1+n.slideOffset:i*t*-1+r,n.options.variableWidth===!0&&(o=n.slideCount<=n.options.slidesToShow||n.options.infinite===!1?n.$slideTrack.children(".slick-slide").eq(i):n.$slideTrack.children(".slick-slide").eq(i+n.options.slidesToShow),e=n.options.rtl===!0?o[0]?(n.$slideTrack.width()-o[0].offsetLeft-o.width())*-1:0:o[0]?o[0].offsetLeft*-1:0,n.options.centerMode===!0&&(o=n.slideCount<=n.options.slidesToShow||n.options.infinite===!1?n.$slideTrack.children(".slick-slide").eq(i):n.$slideTrack.children(".slick-slide").eq(i+n.options.slidesToShow+1),e=n.options.rtl===!0?o[0]?(n.$slideTrack.width()-o[0].offsetLeft-o.width())*-1:0:o[0]?o[0].offsetLeft*-1:0,e+=(n.$list.width()-o.outerWidth())/2)),e},e.prototype.getOption=e.prototype.slickGetOption=function(i){var e=this;return e.options[i]},e.prototype.getNavigableIndexes=function(){var i,e=this,t=0,o=0,s=[];for(e.options.infinite===!1?i=e.slideCount:(t=e.options.slidesToScroll*-1,o=e.options.slidesToScroll*-1,i=2*e.slideCount);t<i;)s.push(t),t=o+e.options.slidesToScroll,o+=e.options.slidesToScroll<=e.options.slidesToShow?e.options.slidesToScroll:e.options.slidesToShow;return s},e.prototype.getSlick=function(){return this},e.prototype.getSlideCount=function(){var e,t,o,s,n=this;return s=n.options.centerMode===!0?Math.floor(n.$list.width()/2):0,o=n.swipeLeft*-1+s,n.options.swipeToSlide===!0?(n.$slideTrack.find(".slick-slide").each(function(e,s){var r,l,d;if(r=i(s).outerWidth(),l=s.offsetLeft,n.options.centerMode!==!0&&(l+=r/2),d=l+r,o<d)return t=s,!1}),e=Math.abs(i(t).attr("data-slick-index")-n.currentSlide)||1):n.options.slidesToScroll},e.prototype.goTo=e.prototype.slickGoTo=function(i,e){var t=this;t.changeSlide({data:{message:"index",index:parseInt(i)}},e)},e.prototype.init=function(e){var t=this;i(t.$slider).hasClass("slick-initialized")||(i(t.$slider).addClass("slick-initialized"),t.buildRows(),t.buildOut(),t.setProps(),t.startLoad(),t.loadSlider(),t.initializeEvents(),t.updateArrows(),t.updateDots(),t.checkResponsive(!0),t.focusHandler()),e&&t.$slider.trigger("init",[t]),t.options.accessibility===!0&&t.initADA(),t.options.autoplay&&(t.paused=!1,t.autoPlay())},e.prototype.initADA=function(){var e=this,t=Math.ceil(e.slideCount/e.options.slidesToShow),o=e.getNavigableIndexes().filter(function(i){return i>=0&&i<e.slideCount});e.$slides.add(e.$slideTrack.find(".slick-cloned")).attr({"aria-hidden":"true",tabindex:"-1"}).find("a, input, button, select").attr({tabindex:"-1"}),null!==e.$dots&&(e.$slides.not(e.$slideTrack.find(".slick-cloned")).each(function(t){var s=o.indexOf(t);if(i(this).attr({role:"tabpanel",id:"slick-slide"+e.instanceUid+t,tabindex:-1}),s!==-1){var n="slick-slide-control"+e.instanceUid+s;i("#"+n).length&&i(this).attr({"aria-describedby":n})}}),e.$dots.attr("role","tablist").find("li").each(function(s){var n=o[s];i(this).attr({role:"presentation"}),i(this).find("button").first().attr({role:"tab",id:"slick-slide-control"+e.instanceUid+s,"aria-controls":"slick-slide"+e.instanceUid+n,"aria-label":s+1+" of "+t,"aria-selected":null,tabindex:"-1"})}).eq(e.currentSlide).find("button").attr({"aria-selected":"true",tabindex:"0"}).end());for(var s=e.currentSlide,n=s+e.options.slidesToShow;s<n;s++)e.options.focusOnChange?e.$slides.eq(s).attr({tabindex:"0"}):e.$slides.eq(s).removeAttr("tabindex");e.activateADA()},e.prototype.initArrowEvents=function(){var i=this;i.options.arrows===!0&&i.slideCount>i.options.slidesToShow&&(i.$prevArrow.off("click.slick").on("click.slick",{message:"previous"},i.changeSlide),i.$nextArrow.off("click.slick").on("click.slick",{message:"next"},i.changeSlide),i.options.accessibility===!0&&(i.$prevArrow.on("keydown.slick",i.keyHandler),i.$nextArrow.on("keydown.slick",i.keyHandler)))},e.prototype.initDotEvents=function(){var e=this;e.options.dots===!0&&e.slideCount>e.options.slidesToShow&&(i("li",e.$dots).on("click.slick",{message:"index"},e.changeSlide),e.options.accessibility===!0&&e.$dots.on("keydown.slick",e.keyHandler)),e.options.dots===!0&&e.options.pauseOnDotsHover===!0&&e.slideCount>e.options.slidesToShow&&i("li",e.$dots).on("mouseenter.slick",i.proxy(e.interrupt,e,!0)).on("mouseleave.slick",i.proxy(e.interrupt,e,!1))},e.prototype.initSlideEvents=function(){var e=this;e.options.pauseOnHover&&(e.$list.on("mouseenter.slick",i.proxy(e.interrupt,e,!0)),e.$list.on("mouseleave.slick",i.proxy(e.interrupt,e,!1)))},e.prototype.initializeEvents=function(){var e=this;e.initArrowEvents(),e.initDotEvents(),e.initSlideEvents(),e.$list.on("touchstart.slick mousedown.slick",{action:"start"},e.swipeHandler),e.$list.on("touchmove.slick mousemove.slick",{action:"move"},e.swipeHandler),e.$list.on("touchend.slick mouseup.slick",{action:"end"},e.swipeHandler),e.$list.on("touchcancel.slick mouseleave.slick",{action:"end"},e.swipeHandler),e.$list.on("click.slick",e.clickHandler),i(document).on(e.visibilityChange,i.proxy(e.visibility,e)),e.options.accessibility===!0&&e.$list.on("keydown.slick",e.keyHandler),e.options.focusOnSelect===!0&&i(e.$slideTrack).children().on("click.slick",e.selectHandler),i(window).on("orientationchange.slick.slick-"+e.instanceUid,i.proxy(e.orientationChange,e)),i(window).on("resize.slick.slick-"+e.instanceUid,i.proxy(e.resize,e)),i("[draggable!=true]",e.$slideTrack).on("dragstart",e.preventDefault),i(window).on("load.slick.slick-"+e.instanceUid,e.setPosition),i(e.setPosition)},e.prototype.initUI=function(){var i=this;i.options.arrows===!0&&i.slideCount>i.options.slidesToShow&&(i.$prevArrow.show(),i.$nextArrow.show()),i.options.dots===!0&&i.slideCount>i.options.slidesToShow&&i.$dots.show()},e.prototype.keyHandler=function(i){var e=this;i.target.tagName.match("TEXTAREA|INPUT|SELECT")||(37===i.keyCode&&e.options.accessibility===!0?e.changeSlide({data:{message:e.options.rtl===!0?"next":"previous"}}):39===i.keyCode&&e.options.accessibility===!0&&e.changeSlide({data:{message:e.options.rtl===!0?"previous":"next"}}))},e.prototype.lazyLoad=function(){function e(e){i("img[data-lazy]",e).each(function(){var e=i(this),t=i(this).attr("data-lazy"),o=i(this).attr("data-srcset"),s=i(this).attr("data-sizes")||r.$slider.attr("data-sizes"),n=document.createElement("img");n.onload=function(){e.animate({opacity:0},100,function(){o&&(e.attr("srcset",o),s&&e.attr("sizes",s)),e.attr("src",t).animate({opacity:1},200,function(){e.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading")}),r.$slider.trigger("lazyLoaded",[r,e,t])})},n.onerror=function(){e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),r.$slider.trigger("lazyLoadError",[r,e,t])},n.src=t})}var t,o,s,n,r=this;if(r.options.centerMode===!0?r.options.infinite===!0?(s=r.currentSlide+(r.options.slidesToShow/2+1),n=s+r.options.slidesToShow+2):(s=Math.max(0,r.currentSlide-(r.options.slidesToShow/2+1)),n=2+(r.options.slidesToShow/2+1)+r.currentSlide):(s=r.options.infinite?r.options.slidesToShow+r.currentSlide:r.currentSlide,n=Math.ceil(s+r.options.slidesToShow),r.options.fade===!0&&(s>0&&s--,n<=r.slideCount&&n++)),t=r.$slider.find(".slick-slide").slice(s,n),"anticipated"===r.options.lazyLoad)for(var l=s-1,d=n,a=r.$slider.find(".slick-slide"),c=0;c<r.options.slidesToScroll;c++)l<0&&(l=r.slideCount-1),t=t.add(a.eq(l)),t=t.add(a.eq(d)),l--,d++;e(t),r.slideCount<=r.options.slidesToShow?(o=r.$slider.find(".slick-slide"),e(o)):r.currentSlide>=r.slideCount-r.options.slidesToShow?(o=r.$slider.find(".slick-cloned").slice(0,r.options.slidesToShow),e(o)):0===r.currentSlide&&(o=r.$slider.find(".slick-cloned").slice(r.options.slidesToShow*-1),e(o))},e.prototype.loadSlider=function(){var i=this;i.setPosition(),i.$slideTrack.css({opacity:1}),i.$slider.removeClass("slick-loading"),i.initUI(),"progressive"===i.options.lazyLoad&&i.progressiveLazyLoad()},e.prototype.next=e.prototype.slickNext=function(){var i=this;i.changeSlide({data:{message:"next"}})},e.prototype.orientationChange=function(){var i=this;i.checkResponsive(),i.setPosition()},e.prototype.pause=e.prototype.slickPause=function(){var i=this;i.autoPlayClear(),i.paused=!0},e.prototype.play=e.prototype.slickPlay=function(){var i=this;i.autoPlay(),i.options.autoplay=!0,i.paused=!1,i.focussed=!1,i.interrupted=!1},e.prototype.postSlide=function(e){var t=this;if(!t.unslicked&&(t.$slider.trigger("afterChange",[t,e]),t.animating=!1,t.slideCount>t.options.slidesToShow&&t.setPosition(),t.swipeLeft=null,t.options.autoplay&&t.autoPlay(),t.options.accessibility===!0&&(t.initADA(),t.options.focusOnChange))){var o=i(t.$slides.get(t.currentSlide));o.attr("tabindex",0).focus()}},e.prototype.prev=e.prototype.slickPrev=function(){var i=this;i.changeSlide({data:{message:"previous"}})},e.prototype.preventDefault=function(i){i.preventDefault()},e.prototype.progressiveLazyLoad=function(e){e=e||1;var t,o,s,n,r,l=this,d=i("img[data-lazy]",l.$slider);d.length?(t=d.first(),o=t.attr("data-lazy"),s=t.attr("data-srcset"),n=t.attr("data-sizes")||l.$slider.attr("data-sizes"),r=document.createElement("img"),r.onload=function(){s&&(t.attr("srcset",s),n&&t.attr("sizes",n)),t.attr("src",o).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"),l.options.adaptiveHeight===!0&&l.setPosition(),l.$slider.trigger("lazyLoaded",[l,t,o]),l.progressiveLazyLoad()},r.onerror=function(){e<3?setTimeout(function(){l.progressiveLazyLoad(e+1)},500):(t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),l.$slider.trigger("lazyLoadError",[l,t,o]),l.progressiveLazyLoad())},r.src=o):l.$slider.trigger("allImagesLoaded",[l])},e.prototype.refresh=function(e){var t,o,s=this;o=s.slideCount-s.options.slidesToShow,!s.options.infinite&&s.currentSlide>o&&(s.currentSlide=o),s.slideCount<=s.options.slidesToShow&&(s.currentSlide=0),t=s.currentSlide,s.destroy(!0),i.extend(s,s.initials,{currentSlide:t}),s.init(),e||s.changeSlide({data:{message:"index",index:t}},!1)},e.prototype.registerBreakpoints=function(){var e,t,o,s=this,n=s.options.responsive||null;if("array"===i.type(n)&&n.length){s.respondTo=s.options.respondTo||"window";for(e in n)if(o=s.breakpoints.length-1,n.hasOwnProperty(e)){for(t=n[e].breakpoint;o>=0;)s.breakpoints[o]&&s.breakpoints[o]===t&&s.breakpoints.splice(o,1),o--;s.breakpoints.push(t),s.breakpointSettings[t]=n[e].settings}s.breakpoints.sort(function(i,e){return s.options.mobileFirst?i-e:e-i})}},e.prototype.reinit=function(){var e=this;e.$slides=e.$slideTrack.children(e.options.slide).addClass("slick-slide"),e.slideCount=e.$slides.length,e.currentSlide>=e.slideCount&&0!==e.currentSlide&&(e.currentSlide=e.currentSlide-e.options.slidesToScroll),e.slideCount<=e.options.slidesToShow&&(e.currentSlide=0),e.registerBreakpoints(),e.setProps(),e.setupInfinite(),e.buildArrows(),e.updateArrows(),e.initArrowEvents(),e.buildDots(),e.updateDots(),e.initDotEvents(),e.cleanUpSlideEvents(),e.initSlideEvents(),e.checkResponsive(!1,!0),e.options.focusOnSelect===!0&&i(e.$slideTrack).children().on("click.slick",e.selectHandler),e.setSlideClasses("number"==typeof e.currentSlide?e.currentSlide:0),e.setPosition(),e.focusHandler(),e.paused=!e.options.autoplay,e.autoPlay(),e.$slider.trigger("reInit",[e])},e.prototype.resize=function(){var e=this;i(window).width()!==e.windowWidth&&(clearTimeout(e.windowDelay),e.windowDelay=window.setTimeout(function(){e.windowWidth=i(window).width(),e.checkResponsive(),e.unslicked||e.setPosition()},50))},e.prototype.removeSlide=e.prototype.slickRemove=function(i,e,t){var o=this;return"boolean"==typeof i?(e=i,i=e===!0?0:o.slideCount-1):i=e===!0?--i:i,!(o.slideCount<1||i<0||i>o.slideCount-1)&&(o.unload(),t===!0?o.$slideTrack.children().remove():o.$slideTrack.children(this.options.slide).eq(i).remove(),o.$slides=o.$slideTrack.children(this.options.slide),o.$slideTrack.children(this.options.slide).detach(),o.$slideTrack.append(o.$slides),o.$slidesCache=o.$slides,void o.reinit())},e.prototype.setCSS=function(i){var e,t,o=this,s={};o.options.rtl===!0&&(i=-i),e="left"==o.positionProp?Math.ceil(i)+"px":"0px",t="top"==o.positionProp?Math.ceil(i)+"px":"0px",s[o.positionProp]=i,o.transformsEnabled===!1?o.$slideTrack.css(s):(s={},o.cssTransitions===!1?(s[o.animType]="translate("+e+", "+t+")",o.$slideTrack.css(s)):(s[o.animType]="translate3d("+e+", "+t+", 0px)",o.$slideTrack.css(s)))},e.prototype.setDimensions=function(){var i=this;i.options.vertical===!1?i.options.centerMode===!0&&i.$list.css({padding:"0px "+i.options.centerPadding}):(i.$list.height(i.$slides.first().outerHeight(!0)*i.options.slidesToShow),i.options.centerMode===!0&&i.$list.css({padding:i.options.centerPadding+" 0px"})),i.listWidth=i.$list.width(),i.listHeight=i.$list.height(),i.options.vertical===!1&&i.options.variableWidth===!1?(i.slideWidth=Math.ceil(i.listWidth/i.options.slidesToShow),i.$slideTrack.width(Math.ceil(i.slideWidth*i.$slideTrack.children(".slick-slide").length))):i.options.variableWidth===!0?i.$slideTrack.width(5e3*i.slideCount):(i.slideWidth=Math.ceil(i.listWidth),i.$slideTrack.height(Math.ceil(i.$slides.first().outerHeight(!0)*i.$slideTrack.children(".slick-slide").length)));var e=i.$slides.first().outerWidth(!0)-i.$slides.first().width();i.options.variableWidth===!1&&i.$slideTrack.children(".slick-slide").width(i.slideWidth-e)},e.prototype.setFade=function(){var e,t=this;t.$slides.each(function(o,s){e=t.slideWidth*o*-1,t.options.rtl===!0?i(s).css({position:"relative",right:e,top:0,zIndex:t.options.zIndex-2,opacity:0}):i(s).css({position:"relative",left:e,top:0,zIndex:t.options.zIndex-2,opacity:0})}),t.$slides.eq(t.currentSlide).css({zIndex:t.options.zIndex-1,opacity:1})},e.prototype.setHeight=function(){var i=this;if(1===i.options.slidesToShow&&i.options.adaptiveHeight===!0&&i.options.vertical===!1){var e=i.$slides.eq(i.currentSlide).outerHeight(!0);i.$list.css("height",e)}},e.prototype.setOption=e.prototype.slickSetOption=function(){var e,t,o,s,n,r=this,l=!1;if("object"===i.type(arguments[0])?(o=arguments[0],l=arguments[1],n="multiple"):"string"===i.type(arguments[0])&&(o=arguments[0],s=arguments[1],l=arguments[2],"responsive"===arguments[0]&&"array"===i.type(arguments[1])?n="responsive":"undefined"!=typeof arguments[1]&&(n="single")),"single"===n)r.options[o]=s;else if("multiple"===n)i.each(o,function(i,e){r.options[i]=e});else if("responsive"===n)for(t in s)if("array"!==i.type(r.options.responsive))r.options.responsive=[s[t]];else{for(e=r.options.responsive.length-1;e>=0;)r.options.responsive[e].breakpoint===s[t].breakpoint&&r.options.responsive.splice(e,1),e--;r.options.responsive.push(s[t])}l&&(r.unload(),r.reinit())},e.prototype.setPosition=function(){var i=this;i.setDimensions(),i.setHeight(),i.options.fade===!1?i.setCSS(i.getLeft(i.currentSlide)):i.setFade(),i.$slider.trigger("setPosition",[i])},e.prototype.setProps=function(){var i=this,e=document.body.style;i.positionProp=i.options.vertical===!0?"top":"left",
"top"===i.positionProp?i.$slider.addClass("slick-vertical"):i.$slider.removeClass("slick-vertical"),void 0===e.WebkitTransition&&void 0===e.MozTransition&&void 0===e.msTransition||i.options.useCSS===!0&&(i.cssTransitions=!0),i.options.fade&&("number"==typeof i.options.zIndex?i.options.zIndex<3&&(i.options.zIndex=3):i.options.zIndex=i.defaults.zIndex),void 0!==e.OTransform&&(i.animType="OTransform",i.transformType="-o-transform",i.transitionType="OTransition",void 0===e.perspectiveProperty&&void 0===e.webkitPerspective&&(i.animType=!1)),void 0!==e.MozTransform&&(i.animType="MozTransform",i.transformType="-moz-transform",i.transitionType="MozTransition",void 0===e.perspectiveProperty&&void 0===e.MozPerspective&&(i.animType=!1)),void 0!==e.webkitTransform&&(i.animType="webkitTransform",i.transformType="-webkit-transform",i.transitionType="webkitTransition",void 0===e.perspectiveProperty&&void 0===e.webkitPerspective&&(i.animType=!1)),void 0!==e.msTransform&&(i.animType="msTransform",i.transformType="-ms-transform",i.transitionType="msTransition",void 0===e.msTransform&&(i.animType=!1)),void 0!==e.transform&&i.animType!==!1&&(i.animType="transform",i.transformType="transform",i.transitionType="transition"),i.transformsEnabled=i.options.useTransform&&null!==i.animType&&i.animType!==!1},e.prototype.setSlideClasses=function(i){var e,t,o,s,n=this;if(t=n.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden","true"),n.$slides.eq(i).addClass("slick-current"),n.options.centerMode===!0){var r=n.options.slidesToShow%2===0?1:0;e=Math.floor(n.options.slidesToShow/2),n.options.infinite===!0&&(i>=e&&i<=n.slideCount-1-e?n.$slides.slice(i-e+r,i+e+1).addClass("slick-active").attr("aria-hidden","false"):(o=n.options.slidesToShow+i,t.slice(o-e+1+r,o+e+2).addClass("slick-active").attr("aria-hidden","false")),0===i?t.eq(t.length-1-n.options.slidesToShow).addClass("slick-center"):i===n.slideCount-1&&t.eq(n.options.slidesToShow).addClass("slick-center")),n.$slides.eq(i).addClass("slick-center")}else i>=0&&i<=n.slideCount-n.options.slidesToShow?n.$slides.slice(i,i+n.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false"):t.length<=n.options.slidesToShow?t.addClass("slick-active").attr("aria-hidden","false"):(s=n.slideCount%n.options.slidesToShow,o=n.options.infinite===!0?n.options.slidesToShow+i:i,n.options.slidesToShow==n.options.slidesToScroll&&n.slideCount-i<n.options.slidesToShow?t.slice(o-(n.options.slidesToShow-s),o+s).addClass("slick-active").attr("aria-hidden","false"):t.slice(o,o+n.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false"));"ondemand"!==n.options.lazyLoad&&"anticipated"!==n.options.lazyLoad||n.lazyLoad()},e.prototype.setupInfinite=function(){var e,t,o,s=this;if(s.options.fade===!0&&(s.options.centerMode=!1),s.options.infinite===!0&&s.options.fade===!1&&(t=null,s.slideCount>s.options.slidesToShow)){for(o=s.options.centerMode===!0?s.options.slidesToShow+1:s.options.slidesToShow,e=s.slideCount;e>s.slideCount-o;e-=1)t=e-1,i(s.$slides[t]).clone(!0).attr("id","").attr("data-slick-index",t-s.slideCount).prependTo(s.$slideTrack).addClass("slick-cloned");for(e=0;e<o+s.slideCount;e+=1)t=e,i(s.$slides[t]).clone(!0).attr("id","").attr("data-slick-index",t+s.slideCount).appendTo(s.$slideTrack).addClass("slick-cloned");s.$slideTrack.find(".slick-cloned").find("[id]").each(function(){i(this).attr("id","")})}},e.prototype.interrupt=function(i){var e=this;i||e.autoPlay(),e.interrupted=i},e.prototype.selectHandler=function(e){var t=this,o=i(e.target).is(".slick-slide")?i(e.target):i(e.target).parents(".slick-slide"),s=parseInt(o.attr("data-slick-index"));return s||(s=0),t.slideCount<=t.options.slidesToShow?void t.slideHandler(s,!1,!0):void t.slideHandler(s)},e.prototype.slideHandler=function(i,e,t){var o,s,n,r,l,d=null,a=this;if(e=e||!1,!(a.animating===!0&&a.options.waitForAnimate===!0||a.options.fade===!0&&a.currentSlide===i))return e===!1&&a.asNavFor(i),o=i,d=a.getLeft(o),r=a.getLeft(a.currentSlide),a.currentLeft=null===a.swipeLeft?r:a.swipeLeft,a.options.infinite===!1&&a.options.centerMode===!1&&(i<0||i>a.getDotCount()*a.options.slidesToScroll)?void(a.options.fade===!1&&(o=a.currentSlide,t!==!0&&a.slideCount>a.options.slidesToShow?a.animateSlide(r,function(){a.postSlide(o)}):a.postSlide(o))):a.options.infinite===!1&&a.options.centerMode===!0&&(i<0||i>a.slideCount-a.options.slidesToScroll)?void(a.options.fade===!1&&(o=a.currentSlide,t!==!0&&a.slideCount>a.options.slidesToShow?a.animateSlide(r,function(){a.postSlide(o)}):a.postSlide(o))):(a.options.autoplay&&clearInterval(a.autoPlayTimer),s=o<0?a.slideCount%a.options.slidesToScroll!==0?a.slideCount-a.slideCount%a.options.slidesToScroll:a.slideCount+o:o>=a.slideCount?a.slideCount%a.options.slidesToScroll!==0?0:o-a.slideCount:o,a.animating=!0,a.$slider.trigger("beforeChange",[a,a.currentSlide,s]),n=a.currentSlide,a.currentSlide=s,a.setSlideClasses(a.currentSlide),a.options.asNavFor&&(l=a.getNavTarget(),l=l.slick("getSlick"),l.slideCount<=l.options.slidesToShow&&l.setSlideClasses(a.currentSlide)),a.updateDots(),a.updateArrows(),a.options.fade===!0?(t!==!0?(a.fadeSlideOut(n),a.fadeSlide(s,function(){a.postSlide(s)})):a.postSlide(s),void a.animateHeight()):void(t!==!0&&a.slideCount>a.options.slidesToShow?a.animateSlide(d,function(){a.postSlide(s)}):a.postSlide(s)))},e.prototype.startLoad=function(){var i=this;i.options.arrows===!0&&i.slideCount>i.options.slidesToShow&&(i.$prevArrow.hide(),i.$nextArrow.hide()),i.options.dots===!0&&i.slideCount>i.options.slidesToShow&&i.$dots.hide(),i.$slider.addClass("slick-loading")},e.prototype.swipeDirection=function(){var i,e,t,o,s=this;return i=s.touchObject.startX-s.touchObject.curX,e=s.touchObject.startY-s.touchObject.curY,t=Math.atan2(e,i),o=Math.round(180*t/Math.PI),o<0&&(o=360-Math.abs(o)),o<=45&&o>=0?s.options.rtl===!1?"left":"right":o<=360&&o>=315?s.options.rtl===!1?"left":"right":o>=135&&o<=225?s.options.rtl===!1?"right":"left":s.options.verticalSwiping===!0?o>=35&&o<=135?"down":"up":"vertical"},e.prototype.swipeEnd=function(i){var e,t,o=this;if(o.dragging=!1,o.swiping=!1,o.scrolling)return o.scrolling=!1,!1;if(o.interrupted=!1,o.shouldClick=!(o.touchObject.swipeLength>10),void 0===o.touchObject.curX)return!1;if(o.touchObject.edgeHit===!0&&o.$slider.trigger("edge",[o,o.swipeDirection()]),o.touchObject.swipeLength>=o.touchObject.minSwipe){switch(t=o.swipeDirection()){case"left":case"down":e=o.options.swipeToSlide?o.checkNavigable(o.currentSlide+o.getSlideCount()):o.currentSlide+o.getSlideCount(),o.currentDirection=0;break;case"right":case"up":e=o.options.swipeToSlide?o.checkNavigable(o.currentSlide-o.getSlideCount()):o.currentSlide-o.getSlideCount(),o.currentDirection=1}"vertical"!=t&&(o.slideHandler(e),o.touchObject={},o.$slider.trigger("swipe",[o,t]))}else o.touchObject.startX!==o.touchObject.curX&&(o.slideHandler(o.currentSlide),o.touchObject={})},e.prototype.swipeHandler=function(i){var e=this;if(!(e.options.swipe===!1||"ontouchend"in document&&e.options.swipe===!1||e.options.draggable===!1&&i.type.indexOf("mouse")!==-1))switch(e.touchObject.fingerCount=i.originalEvent&&void 0!==i.originalEvent.touches?i.originalEvent.touches.length:1,e.touchObject.minSwipe=e.listWidth/e.options.touchThreshold,e.options.verticalSwiping===!0&&(e.touchObject.minSwipe=e.listHeight/e.options.touchThreshold),i.data.action){case"start":e.swipeStart(i);break;case"move":e.swipeMove(i);break;case"end":e.swipeEnd(i)}},e.prototype.swipeMove=function(i){var e,t,o,s,n,r,l=this;return n=void 0!==i.originalEvent?i.originalEvent.touches:null,!(!l.dragging||l.scrolling||n&&1!==n.length)&&(e=l.getLeft(l.currentSlide),l.touchObject.curX=void 0!==n?n[0].pageX:i.clientX,l.touchObject.curY=void 0!==n?n[0].pageY:i.clientY,l.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(l.touchObject.curX-l.touchObject.startX,2))),r=Math.round(Math.sqrt(Math.pow(l.touchObject.curY-l.touchObject.startY,2))),!l.options.verticalSwiping&&!l.swiping&&r>4?(l.scrolling=!0,!1):(l.options.verticalSwiping===!0&&(l.touchObject.swipeLength=r),t=l.swipeDirection(),void 0!==i.originalEvent&&l.touchObject.swipeLength>4&&(l.swiping=!0,i.preventDefault()),s=(l.options.rtl===!1?1:-1)*(l.touchObject.curX>l.touchObject.startX?1:-1),l.options.verticalSwiping===!0&&(s=l.touchObject.curY>l.touchObject.startY?1:-1),o=l.touchObject.swipeLength,l.touchObject.edgeHit=!1,l.options.infinite===!1&&(0===l.currentSlide&&"right"===t||l.currentSlide>=l.getDotCount()&&"left"===t)&&(o=l.touchObject.swipeLength*l.options.edgeFriction,l.touchObject.edgeHit=!0),l.options.vertical===!1?l.swipeLeft=e+o*s:l.swipeLeft=e+o*(l.$list.height()/l.listWidth)*s,l.options.verticalSwiping===!0&&(l.swipeLeft=e+o*s),l.options.fade!==!0&&l.options.touchMove!==!1&&(l.animating===!0?(l.swipeLeft=null,!1):void l.setCSS(l.swipeLeft))))},e.prototype.swipeStart=function(i){var e,t=this;return t.interrupted=!0,1!==t.touchObject.fingerCount||t.slideCount<=t.options.slidesToShow?(t.touchObject={},!1):(void 0!==i.originalEvent&&void 0!==i.originalEvent.touches&&(e=i.originalEvent.touches[0]),t.touchObject.startX=t.touchObject.curX=void 0!==e?e.pageX:i.clientX,t.touchObject.startY=t.touchObject.curY=void 0!==e?e.pageY:i.clientY,void(t.dragging=!0))},e.prototype.unfilterSlides=e.prototype.slickUnfilter=function(){var i=this;null!==i.$slidesCache&&(i.unload(),i.$slideTrack.children(this.options.slide).detach(),i.$slidesCache.appendTo(i.$slideTrack),i.reinit())},e.prototype.unload=function(){var e=this;i(".slick-cloned",e.$slider).remove(),e.$dots&&e.$dots.remove(),e.$prevArrow&&e.htmlExpr.test(e.options.prevArrow)&&e.$prevArrow.remove(),e.$nextArrow&&e.htmlExpr.test(e.options.nextArrow)&&e.$nextArrow.remove(),e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden","true").css("width","")},e.prototype.unslick=function(i){var e=this;e.$slider.trigger("unslick",[e,i]),e.destroy()},e.prototype.updateArrows=function(){var i,e=this;i=Math.floor(e.options.slidesToShow/2),e.options.arrows===!0&&e.slideCount>e.options.slidesToShow&&!e.options.infinite&&(e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false"),e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false"),0===e.currentSlide?(e.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true"),e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false")):e.currentSlide>=e.slideCount-e.options.slidesToShow&&e.options.centerMode===!1?(e.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")):e.currentSlide>=e.slideCount-1&&e.options.centerMode===!0&&(e.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")))},e.prototype.updateDots=function(){var i=this;null!==i.$dots&&(i.$dots.find("li").removeClass("slick-active").end(),i.$dots.find("li").eq(Math.floor(i.currentSlide/i.options.slidesToScroll)).addClass("slick-active"))},e.prototype.visibility=function(){var i=this;i.options.autoplay&&(document[i.hidden]?i.interrupted=!0:i.interrupted=!1)},i.fn.slick=function(){var i,t,o=this,s=arguments[0],n=Array.prototype.slice.call(arguments,1),r=o.length;for(i=0;i<r;i++)if("object"==typeof s||"undefined"==typeof s?o[i].slick=new e(o[i],s):t=o[i].slick[s].apply(o[i].slick,n),"undefined"!=typeof t)return t;return o}});
$.fn.datepicker.dates['id'] = {
  days: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"],
  daysShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
  daysMin: ["Mi", "Sn", "Se", "Ra", "Ka", "Ju", "Sa"],
  months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
  monthsShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
  today: "Hari ini",
  clear: "Hapus",
  format: 'dd MM yyyy',
  titleFormat: "MM yyyy",
  weekStart: 0,
};

function HelperFormatRupiah(angka, prefix){
  var number_string = String(angka).replace(/[^,\d]/g, '').toString(),
  split = number_string.split(','),
  sisa = split[0].length % 3,
  rupiah = split[0].substr(0, sisa),
  ribuan = split[0].substr(sisa).match(/\d{3}/gi),
  separator = '';

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}
var
$whiteSpace = /^ *$/,
$numReg = /^[0-9]+$/,
$onlyOneName = /^[a-zA-Z'-]+$/,
$fullName = /^(?:[\u00c0-\u01ffa-zA-Z-\s\.']){3,}(?:[\u00c0-\u01ffa-zA-Z-\s\.']{3,})+$/i,
$email =  /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
$monthsName = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

// Mobile Check
var $isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    webOS: function() {
        return navigator.userAgent.match(/webOS/i);
    },
    any: function() {
        return ($isMobile.Android() || $isMobile.BlackBerry() || $isMobile.iOS() || $isMobile.Opera() || $isMobile.Windows() || $isMobile.webOS());
    },
};
function HelperGetAge(dateString) {
  var today = new Date();
  var birthDate = new Date(dateString);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
  }
  return age;
};
var Accordion = {

  default: function() {
    if ( $('.js-accordion-items').length ) {

      $('.c-accordion-item').on('click', '.question', function(){
        $(this).parent('.c-accordion-item').toggleClass('opened');
      });

    }

    if ( $('.main-footer .link-container .casual').length ) {
      $('.main-footer .link-container .casual li:first-child').on('click', function(){
        $(this).toggleClass('active');
      });
    }
  },

  init: function() {
    Accordion.default();
  }

};
var AktivasiMelaluiEmail = {

  kodeKado: function() {
    $('.aktivasi-kode-kado input[type="text"]').on('input', function() {
      if (!$whiteSpace.test($(this).val())) {
        $('.aktivasi-kode-kado button').removeClass('disabled').addClass('primary');
      } else {
        $('.aktivasi-kode-kado button').removeClass('primary').addClass('disabled');
      }
    });

    $('.js-aktivasi-before-step1').on('click', function(){
      if ($(this).hasClass('primary')) {
        $('#giftCodeInput').fadeOut('fast', function() {
          $('#giftActiveNow').fadeIn('slow');
        });
        setTimeout(function() {
          $('#giftCodeInput').remove();
        }, 300);
      }
    });
  },

  step0: function() {
    $('.aktivasi-step-0').addClass('active');

    setTimeout(function() {
      $('.aktivasi-step-0').addClass('show-content');
    }, 250);

    $('.js-aktivasi-to-step-1').on('click', function(e) {
      e.preventDefault();

      AktivasiMelaluiEmail.switch('.aktivasi-step-0', '.aktivasi-step-1');
    });
  },

  step1: function() {

    $('.js-my-name').on('input', function() {
      AktivasiMelaluiEmail.validateDataUpdate.step1();
    });

    $('.js-gender-dropdown, .js-birth-date, .js-input-file').on('change', function() {
      AktivasiMelaluiEmail.validateDataUpdate.step1();
    });

    $('.js-id-card').on('input', function() {
      AktivasiMelaluiEmail.validateDataUpdate.step1();
    });

    $('.js-next-member-data').on('click', function() {
      var _this = $(this);
      if (_this.hasClass('primary')) {
        Popup.show('existingMemberData');
      }
    });

    $('.js-email, .js-phone-number, .js-address, .js-pos-code').on('input', function() {
      AktivasiMelaluiEmail.validateDataUpdate.step2();
    });

    $('.js-province').on('change', function() {
      AktivasiMelaluiEmail.validateDataUpdate.step2();
    });

    $('.js-aktivasi-kado-selesai').on('click', function() {
      AktivasiMelaluiEmail.switch('.aktivasi-step-1', '.aktivasi-step-selesai');
    });

    $('.js-use-existing-data').on('click', function(event) {
      Popup.hide('existingMemberData');
      $('.js-next-member-data').parents('.next-button').fadeOut('fast', function(){
        $('.agreement, .aktivasi-kado-selesai-button').fadeIn('fast');
      });
    });

    $('.js-use-new-data').on('click', function(event) {
      Popup.hide('existingMemberData');
      AktivasiMelaluiEmail.validateDataUpdate.step2();
      $('.js-next-member-data').parents('.next-button').fadeOut('fast', function(){
        $('.form-input-phone-number').fadeIn('fast');
        $("html, body").animate({scrollTop: $('.form-input-email').offset().top }, 500);
      });
    });
  },

  switch: function(prev, next){

    $(prev).removeClass('show-content');
    setTimeout(function() {
      $(prev).removeClass('active');
      $(next).addClass('active');
      $("html, body").animate({scrollTop: 0 }, 500);
    }, 350);

    setTimeout(function() {
      $(next).addClass('show-content');
    }, 400);

  },

  validateDataUpdate: {

    step1: function(){

      var _valMyName = $('.js-my-name').val(),
          _valMyNameIsValid = false;
      if (_valMyName.length > 2 && ($onlyOneName.test(_valMyName) || $fullName.test(_valMyName))) {
        _valMyNameIsValid = true;
      }

      var _gender = !$whiteSpace.test($('.js-gender-dropdown').val()),
      _birthDate = !$whiteSpace.test($('.js-birth-date').val());

      setTimeout(function() {
        var _valIDCard = $('.js-id-card').val(),
            _valIDCardLength = _valIDCard.length,
            _valInputFile = $('.js-input-file').val();

        if (_valIDCardLength == 17) {
          _valIDCardLength = 16;
        }

        $('.js-agreement-checkbox').prop('checked', false);

        $('.form-input-email, .form-input-phone-number, .form-input-address, .agreement, .aktivasi-kado-selesai-button').fadeOut(0, function(){
          $('.js-next-member-data').parents('.next-button').fadeIn('fast');
        });

        if (!$whiteSpace.test(_valIDCard) && _valIDCardLength == 16 && !$whiteSpace.test(_valInputFile) && _valMyNameIsValid && _gender && _birthDate) {
          $('.js-next-member-data').removeClass('disabled').addClass('primary');
        } else {
          $('.js-next-member-data').removeClass('primary').addClass('disabled');
        }
      }, 150);

    },

    step2: function(){

      var _phoneNumber = $('.js-phone-number'),
          _valPhoneNumber = _phoneNumber.val(),
          _valPhoneNumberIsValid = false;

      if (_valPhoneNumber.length >= 8) {
        if ($(window).width() >= 768) {
          if (!$('.form-input-email').hasClass('has-show')) {
            setTimeout(function() {
              $("html, body").animate({scrollTop: $('.form-input-email').offset().top }, 500);
            }, 100);
          }
        }
        $('.form-input-email').fadeIn('fast').addClass('has-show');
        _valPhoneNumberIsValid = true;
      } else {
          $('.form-input-email').fadeOut('fast').removeClass('has-show');
      }

      var _email = $('.js-email'),
          _valEmail = _email.val(),
          _valEmailIsValid = false;

      if ($email.test(_valEmail) && !$whiteSpace.test(_valEmail)) {
        if ($(window).width() >= 768) {
          if (!$('.form-input-address').hasClass('has-show')) {
            setTimeout(function() {
              $("html, body").animate({scrollTop: $('.form-input-address').offset().top }, 500);
            }, 100);
          }
        }
        $('.form-input-address').fadeIn('fast').addClass('has-show');
        _valEmailIsValid = true;
      } else {
        $('.form-input-address').fadeOut('fast').removeClass('has-show');
      }

      var _address = $('.js-address'),
          _valAddress = _address.val(),
          _valAddressIsValid = false;

      if (!$whiteSpace.test(_valAddress)) {
        _valAddressIsValid = true;
      }

      var _province = $('.js-province'),
          _valProvince = _province.val(),
          _valProvinceIsValid = false;

      if (!$whiteSpace.test(_valProvince)) {
        _valProvinceIsValid = true;
      }

      var _posCode = $('.js-pos-code'),
          _valPosCode = _posCode.val(),
          _valPosCodeLength = _valPosCode.length,
          _valPosCodeIsValid = false;

      if (_valPosCodeLength == 6) {
        _valPosCodeLength = 5;
      }

      if (!$whiteSpace.test(_valPosCode) && _valPosCodeLength == 5) {
        _valPosCodeIsValid = true;
        setTimeout(function() {
          $("html, body").animate({scrollTop: $('.aktivasi-kado-selesai-button').offset().top }, 500);
        }, 100);
      }

      if (_valPhoneNumberIsValid && _valEmailIsValid && _valAddressIsValid && _valProvinceIsValid && _valPosCodeIsValid) {
        $('.agreement, .aktivasi-kado-selesai-button').fadeIn('fast');
      } else {
        $('.agreement, .aktivasi-kado-selesai-button').fadeOut('fast');
      }

    },

  },

  init: function() {

    if ( $('.js-aktivasi-kado-selesai').length ) {
      AktivasiMelaluiEmail.step0();
      AktivasiMelaluiEmail.step1();
      AktivasiMelaluiEmail.kodeKado();
    }
  }

};
var BackToTop = {

  event: function(){

    $('.js-back-to-top').on('click', function(){
      $("html, body").animate({scrollTop: 0 }, 500);
    });

  },

  init: function() {
    if ($('.js-back-to-top').length) {
      BackToTop.event();
    }
  }

};
var CPEditProfile = {

  toggleInput: function() {
    $('.js-trigger-edit-profile').on('click', function(e){
      e.preventDefault();
      // $('.js-trigger-edit-profile').stop().fadeIn('fast');
      // $('.input-this-detail').stop().slideUp('fast');
      // $(this).stop().fadeOut('fast');
      // $(this).closest('tr').find('.input-this-detail').slideDown('fast');
    });
  },

  init: function() {

    if ( $('.js-trigger-edit-profile').length ) {
      CPEditProfile.toggleInput();
    }

  }

};
var Carousel = {

  windowResize: function (){

    $(window).resize(function() {
      Carousel.ourPartner();
    });

  },

  cardPromo: function(){
    $('.js-nav-0').addClass('active');

    if ($('.js-card-promo-list').length) {
      $('.js-card-promo-list').slick({
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        speed: 500,
        arrows: false,
        swipeToSlide: true,
        touchMove: true,
        swipe: true
      });
      $('.js-card-promo-list').on('beforeChange', function(e, slick, current){
        $('.navigator').removeClass('active');
        $('.navigator').removeClass('current');
        switch (current) {
          case 0:
            $('.js-nav-0').addClass('current');
            $('.js-nav-1').addClass('active');
            break;
          case 1:
            $('.js-nav-0').addClass('current');
            $('.js-nav-1').addClass('current');
            $('.js-nav-2').addClass('active');
            break;
          case 2:
            $('.js-nav-0').addClass('active');
            break;
        }
      });
    }
    $('.c-promo').addClass('show-promo');
  },

  blogPromo: function(){
    function NavAction(type, totalList){
      var current = parseInt($('.js-target-current').text());
      var currentMobile = parseInt($('.js-target-current-mobile').text());
      if(type === 'next'){
        if(current < totalList / 2){
          $('.js-target-current').text(current + 1);
        }
        if(currentMobile < totalList){
          $('.js-target-current-mobile').text(currentMobile + 1);
        }
      } else if( type === 'back'){
        if(current > 1){
          $('.js-target-current').text(current - 1);
        }
        if(currentMobile > 1){
          $('.js-target-current-mobile').text(currentMobile - 1);
        }
      }
    }

    var totalList = $('.js-blog-promo-list').children().length;

    $('.js-target-blog-length').text(totalList/2);
    $('.js-target-blog-length-mobile').text(totalList);

    $('.js-next').click(function(){
      $('.js-blog-promo-list').slick('slickNext');
      NavAction('next', totalList);
    });

    $('.js-back').click(function(){
      $('.js-blog-promo-list').slick('slickPrev');
      NavAction('back', totalList);
    });

    if($('.js-blog-promo-list').length){
      $('.js-blog-promo-list').slick({
        autoplay: false,
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: false,
        speed: 500,
        arrows: false,
        adaptiveHeight: true,
        responsive: [
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ],
      });
    }
  },

  ourPartner: function(){

    if ($('.js-our-partner-list').length) {

      var _slideToShow = 5;
      if ($(window).width() < 768) {
        _slideToShow = 2;
      }

      if ($('.js-our-partner-list .item').length > _slideToShow) {

        if ($('.js-our-partner-list .slick-list').length) {
          $('.js-our-partner-list').slick('unslick');
        }

        $('.js-our-partner-list').slick({
          autoplay: false,
          slidesToShow: 5,
          slidesToScroll: 1,
          infinite: false,
          speed: 500,
          arrows: true,
          responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
            }
          }
        ],
        });

      }

      $('.js-our-partner-list').removeClass('show-transition');
    }

  },

  storiesLovelife: function(){
    if ($('.js-stories-lovelife').length) {
      $('.js-stories-lovelife').slick({
        autoplay: true,
        slidesToScroll: 1,
        infinite: true,
        speed: 650,
        arrows: true,
        autoplaySpeed: 7500,
      });
      setTimeout(function() {
        $('.js-stories-lovelife').removeClass('show-transition');
      }, 1500);
    }
  },

  productLovelife: function(){

    if ($('.js-product-lovelife').length) {

      var _slideToShow = 3;
      if ($(window).width() < 768) {
        _slideToShow = 1;
      }

      if ($('.js-product-lovelife .item').length > _slideToShow) {

        if ($('.js-product-lovelife .slick-list').length) {
          $('.js-product-lovelife').slick('unslick');
        }

        $('.js-product-lovelife').slick({
          autoplay: false,
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: false,
          speed: 500,
          arrows: true,
          responsive: [
            {
              breakpoint: 1025,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 1,
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
              }
            }
          ]
        });

      }


      $('.js-product-lovelife').removeClass('show-transition');
    }

  },

  specialProduct: function(){

    if ($('.js-special-product').length) {

      if ($(window).width() < 768) {

        if ($('.js-special-product .item').length > 1) {

          if ($('.js-special-product .slick-list').length) {
            $('.js-special-product').slick('unslick');
          }

          $('.js-special-product').slick({
            autoplay: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            speed: 500,
            arrows: true,
          });

        }

      }

      $('.js-special-product').removeClass('show-transition');
    }

  },

  listTemaKado: function() {
    if ( $('.js-list-tema-kado').length ) {
      var temp;

      if ($('.js-list-tema-kado ul .slick-list').length) {
        $('.js-list-tema-kado ul').slick('unslick');
      }

      $('.js-list-tema-kado ul').slick({
        slidesToShow: 5,
        slidesToScroll: 3,
        infinite: false,
        speed: 650,
        arrows: true,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1
            }
          }
        ]
      });

      $('.js-list-tema-kado [data-kado]').on('click', function(e) {
        e.preventDefault();

        temp = $(this).data('kado');

        $('.js-list-tema-kado [data-kado]').removeClass('active');
        $(this).addClass('active');

        Carousel.listDesainKado(temp);

        if ($('.form-input-email-gift').length && !$('.form-input-email-gift').hasClass('has-show')) {
          setTimeout(function() {
            $("html, body").animate({scrollTop: $('.form-input-email-gift').offset().top }, 500);
          }, 100);
        }
        $('.form-input-email-gift').fadeIn('fast').addClass('has-show');

      });


    }
  },

  listDesainKado: function(element) {
    var color;

    $('.list-desain-kado').addClass('active');

    $('.js-desain-kado.slick-slider').slick('unslick');
    $('.js-desain-kado').removeClass('active');

    $('.js-desain-kado[data-kado-view="' + element + '"]').addClass('active').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: false,
      speed: 650,
      arrows: true
    });

    color = $('.js-desain-kado[data-kado-view="' + element + '"] .slick-current .greeting-card').data('color');
    $('.desain-kado-container .copytext').attr('class', 'copytext ' + color);

    $('.js-desain-kado[data-kado-view="' + element + '"]').on('afterChange', function(slick, currentSlide) {
      color = $('.js-desain-kado[data-kado-view="' + element + '"] .slick-current .greeting-card').data('color');
      $('.desain-kado-container .copytext').attr('class', 'copytext ' + color);
    });

    $('.list-desain-kado').on('click', '.ubah-pesanmu', function(){
      $('.desain-kado-container .copytext .input textarea').focus();
    });
  },

  init: function() {
    Carousel.windowResize();
    Carousel.ourPartner();
    Carousel.blogPromo();
    Carousel.cardPromo();
    Carousel.storiesLovelife();
    Carousel.productLovelife();
    Carousel.listTemaKado();
    Carousel.specialProduct();
  }

};
var CircularSlider = {
  slider: null,
  max: null,
  min: null,
  value: null,
  init: function() {
    var _this = this;
    var wp = $('.c-circular-slider');
    var el = $('.circular-slider');
    var containerWidth = wp.innerWidth();

    if (wp.length) {
      var max = parseInt(el.attr('data-max-value'));
      var min = parseInt(el.attr('data-min-value'));
      var value = parseInt(el.attr('data-value'));

      _this.max = parseInt(max);
      _this.min = parseInt(min);
      _this.value = parseInt(value);

      _this.generate(max, min, value);

      $(window).resize(function() {
        _this.reload();
      });

      $('.circular-label-input').keyup(function(e) {
        _this.generate(_this.max, _this.min, e.target.value);
      });

      $('.circular-label-input').blur(function(e) {
        var nil = parseInt(e.target.value);

        if (typeof nil == 'number') {
          if (nil < _this.min) {
            _this.value = _this.min;
          } else if (nil > _this.max) {
            _this.value = _this.max;
          } else {
            _this.value = nil;
          }
        } else {
          _this.value = _this.min;
        }

        $('.circular-label-input').val(_this.value);
        CircularSlider.sliderChange($('.circular-label-input'), _this.value);
      });
    }
  },
  generate: function(max, min, value) {
    var el = $('.circular-slider');

    var obj = el.roundSlider({
      radius: 200,
      width: 7,
      startAngle: 90,
      handleSize: "40,40",
      handleShape: "round",
      sliderType: "min-range",
      value: value,
      max: max,
      min: min,
      create: function(e) {
        $('.circular-label-input').val(e.value);
      },
      drag: function(e) {
        el.find('.circularValue').val(e.value);
        $('.circular-label-input').val(e.value);
        CircularSlider.sliderChange(el, e.value);
      },
      change: function(e) {
        el.find('.circularValue').val(e.value);
        $('.circular-label-input').val(e.value);
        CircularSlider.sliderChange(el, e.value);
      }
    });

    this.slider = obj;
  },
  reload: function() {
    var el = $('.circular-slider');
    el.roundSlider('destroy');
    this.generate(this.max, this.min, this.value);
  },
  sliderChange: function(element, value) {
    if ( $('.js-from-age').length && $('.js-to-age').length ) {

      element.closest('.js-from-age').each(function(){
        var toAgeSlider = $(this).next('.js-to-age').find('.the-range-slider');

        toAgeSlider.data('ionRangeSlider').update({
          from: (value + 1),
          from_min: (value + 1)
        });
      });

    }
  }
};
var Clipboard = {

  event: function(){

    var clipboard = new ClipboardJS('.js-copy-clipboard');

    clipboard.on('success', function(e) {
        var _textMessage = $('.js-copy-clipboard').attr('data-clipboard-success');
        Clipboard.tooltip.show(_textMessage);
    });

    clipboard.on('error', function(e) {
        var _textMessage = $('.js-copy-clipboard').attr('data-clipboard-error');
        Clipboard.tooltip.show(_textMessage);
    });

  },

  tooltip: {
    create: function(){
      $('body').append('<div class="clipboard-tooltip"><p>Copied Succefully</p></div>');
    },
    show: function(text){
      $('.clipboard-tooltip p').text(text);
      $('.clipboard-tooltip').fadeIn('250');
      setTimeout(function() {
        $('.clipboard-tooltip').fadeOut('250');
      }, 2000);
    }
  },

  init: function() {
    if ($('.js-copy-clipboard').length) {
      Clipboard.event();
      Clipboard.tooltip.create();
    }
  }

};
var CustomDropdown = {

  default: function() {
    if ( $('.js-custom-dropdown').length ) {

      $('.js-custom-dropdown').select2({
        theme: 'astra-life-dropdown',
        minimumResultsForSearch: Infinity
      });

    }
  },

  init: function() {
    CustomDropdown.default();
  }

};
var CustomUpload = {

  benefitRecipientsProfilePicture: {
    path: '',
    alt: '',
  },

  event: function(){

    $('.js-custom-upload .c-custom-upload').on('click', function() {
      if (!$(this).hasClass('disabled')) {
        Popup.show('benefitRecipients');
        CustomUpload.resetBenefitRecipients($(this));
      }
    });

    $('.js-fullname-benefit-recipients').on('input', function() {
      CustomUpload.validateBenefitRecipients();
    });

    $('.js-relationship-benefit-recipients').on('change', function() {
      CustomUpload.validateBenefitRecipients();
    });

    $('.js-profil-picture-benefit-recipients').on('change', function(e) {
      CustomUpload.setProfilPicture(e, $(this));
      CustomUpload.validateBenefitRecipients();
    });

    $('.js-add-benefit-recipients').on('click', function() {
      if ($(this).hasClass('primary')) {
        CustomUpload.setBenefitRecipients();
        $('.js-custom-upload .c-custom-upload .irs.irs--range-slider-custom > *').on('click', function(e){
          e.stopPropagation();
        });
      }
    });


  },

  rangeSlider: function(){

    if ($('.js-range-slider-custom-upload').length) {
      $('.js-range-slider-custom-upload').each(function(index, el) {
        var _this = $(this),
          _target = _this.siblings('.percent-number').find('span');

        _this.ionRangeSlider({
          skin: 'range-slider-custom',
          min: 0,
          max: 100,
          from: 0,
          postfix: '',
          onStart: function (data) {
            _target.text(data.from);
          },
          onChange: function (data) {
            _target.text(data.from);
          },
          onFinish: function(data){
            CustomUpload.maxInput.set();
          }
        });

      });
    }

  },

  validateBenefitRecipients: function(){

    var _valName = $('.js-fullname-benefit-recipients').val(),
        _valNameIsValid = false;

    if (_valName.length > 2 && $onlyOneName.test(_valName) || $fullName.test(_valName)) {
        _valNameIsValid = true; 
    }

    var _valRelationshipIsValid = !$whiteSpace.test($('.js-relationship-benefit-recipients').val());

    if (_valNameIsValid && _valRelationshipIsValid) {
      $('.js-add-benefit-recipients').removeClass('disabled').addClass('primary');
    } else {
      $('.js-add-benefit-recipients').removeClass('primary').addClass('disabled');
    }

  },

  setBenefitRecipients: function(){
    var _valName = $('.js-fullname-benefit-recipients').val(),
        _valRelationship = $('.js-relationship-benefit-recipients option:selected').text(),
        _ammountPercentage = $('.js-ammount-percentage-benefit-recipients').val(),
        _targetIndex = $('.js-add-benefit-recipients').attr('data-target'),
        _src = '/img/icon/default-profile.png',
        _alt = 'Default Image',
        _imageUploaded = 'default',
        _target;

    if (_targetIndex != 'new') {
      _target = $('.js-custom-upload .c-custom-upload:nth-child('+(parseInt(_targetIndex) + 1)+')');
    } else {
      _target = $('.js-custom-upload .c-custom-upload:nth-child('+($('.js-custom-upload .c-custom-upload.added').length + 1)+')');
    }

    _target.find('.name').text(_valName);
    _target.find('.relationship').text(_valRelationship);
    _target.find('.percent-number span').text(_ammountPercentage);
    _target.find('.js-range-slider-custom-upload').val(_ammountPercentage);

    var _isRangeSlider = _target.find('.js-range-slider-custom-upload').data("ionRangeSlider");

    _isRangeSlider.update({
        from: _ammountPercentage
    });

    CustomUpload.maxInput.set();

    if (!$whiteSpace.test(CustomUpload.benefitRecipientsProfilePicture.path)) {
      _src = CustomUpload.benefitRecipientsProfilePicture.path;
      _alt = CustomUpload.benefitRecipientsProfilePicture.alt;
      _imageUploaded = 'file upload';
    }

    _target.find('.avatar img').attr({
      'src': _src,
      'alt': _alt,
      'data-file-upload': _imageUploaded
    });

    _target.removeClass('disabled').addClass('added');
    _target.find('.is-added').addClass('show-content');
    _target.find('.not-added').addClass('hide-content');

    Popup.hide('benefitRecipients');

  },

  resetBenefitRecipients: function(selector){

    var _fullName = '',
      _relationship = '',
      _picturePath = '',
      _pictureAlt = '',
      _fromValuePercentage = '',
      _placeholder = '',
      _maxInput = '',
      _ammountPercentage = $('.js-ammount-percentage-benefit-recipients').parents('.range-slider-percentage'),
      _input = _ammountPercentage.find('h2 input'),
      _isRangeSlider = _ammountPercentage.find('.the-range-slider').data("ionRangeSlider"),
      _fileUploaded = selector.find('.avatar img').attr('data-file-upload');

    var _maxInputGet = CustomUpload.maxInput.get();
    if (selector.hasClass('added')) {

      _fullName = selector.find('.name').text();
      _relationship = $('.js-relationship-benefit-recipients option').filter(function () { return $(this).text() === selector.find('.relationship').text(); }).val();
      if (selector.find('.avatar img').attr('data-file-upload') != 'default') {
        _picturePath = selector.find('.avatar img').attr('src');
        _pictureAlt = selector.find('.avatar img').attr('alt');
      }
      _fromValuePercentage = parseInt(selector.find('.percent-number span').text());
      _maxInput = _fromValuePercentage + _maxInputGet;
      $('.js-add-benefit-recipients').attr('data-target', selector.index());

    } else {

      if (!$whiteSpace.test($('.js-profil-picture-benefit-recipients').val())) {
        $('.js-profil-picture-benefit-recipients').val('').trigger('change');
      }
      $('.js-add-benefit-recipients').attr('data-target', 'new');
      _fromValuePercentage = _ammountPercentage.find('.the-range-slider').attr('data-from');
      _maxInput = CustomUpload.maxInput.get();

    }

    if (_fileUploaded != 'default' && _fileUploaded != undefined) {
      _placeholder = _pictureAlt;
    } else {
      _placeholder = $('.js-profil-picture-benefit-recipients').parents('.input-file').find('button').attr('data-placeholder');
    }

    $('.js-fullname-benefit-recipients').val(_fullName);
    $('.js-relationship-benefit-recipients').val(_relationship).trigger('change');

    CustomUpload.benefitRecipientsProfilePicture.path = _picturePath;
    CustomUpload.benefitRecipientsProfilePicture.alt = _pictureAlt;

    if (!$whiteSpace.test($('.js-profil-picture-benefit-recipients').val())) {
      $('.js-profil-picture-benefit-recipients').val('').trigger('change');
    }

    $('.js-profil-picture-benefit-recipients').parents('.input-file').find('div span').text(_placeholder);


    _input.val(_fromValuePercentage);
    _isRangeSlider.update({
        from: _fromValuePercentage,
        from_max: _maxInput
    });

  },

  setProfilPicture: function(evt, selector){
    var _this = selector,
      _val = _this.val();

    var isIE = (navigator.appName=="Microsoft Internet Explorer"),
      _path  = _this.val();

     if(!$whiteSpace.test(_val)){

        if(isIE) {
            var _pathImage = _this.attr('data-pathimage');
            if (_path == _pathImage) {
                _path = _pathImage;
            }
            var _filename = _path.match(/[^\/\\]+$/);

            CustomUpload.benefitRecipientsProfilePicture.path = _path;
            CustomUpload.benefitRecipientsProfilePicture.alt = _filename;

        } else { 
            var _files = evt.target.files;
            var reader = new FileReader();
            reader.onload = (function() {
                return function(e) {
                    var _filename = _files[0].name,
                        _path = e.target.result;
                    CustomUpload.benefitRecipientsProfilePicture.path = _path;
                    CustomUpload.benefitRecipientsProfilePicture.alt = _filename;
                };
            })(_files);
            reader.readAsDataURL(_files[0]);
        }
     }
  },

  maxInput: {

    set: function(){
        var _maxInput = CustomUpload.maxInput.get();

        if (_maxInput == 0) {
          $('.js-next-step-5').removeClass('disabled').addClass('primary');
          $('.js-custom-upload .c-custom-upload').not('.added').addClass('disabled');
        } else {
          $('.js-next-step-5').removeClass('primary').addClass('disabled');
          $('.js-custom-upload .c-custom-upload').not('.added').removeClass('disabled');
        }

        $('.js-custom-upload .c-custom-upload').each(function(index, el) {
          var _thisIsRangeSlider = $(this).find('.js-range-slider-custom-upload').data("ionRangeSlider");
          _thisIsRangeSlider.update({
              from_max: parseInt($(this).find('.percent-number span').text()) + _maxInput
          });
        });
    },

    get: function(){

        var _totalAmmountPercentage = 0,
          _maxInput = 100;
        $('.js-custom-upload .c-custom-upload').each(function(index, el) {
          _totalAmmountPercentage += parseInt($(this).find('.percent-number span').text());
        });
        _maxInput -= _totalAmmountPercentage;
        return _maxInput;
    }
  },

  init: function() {

    CustomUpload.event();
    CustomUpload.rangeSlider();

  },
};
var DatePicker = {
  month: null,
  year: null,

  default: function() {
    if ( $('.js-datepicker').length ) {
      $('.js-datepicker').each(function(){
        var _this = $(this),
          _defaultDate = _this.attr('data-default-date'),
          _options = {
            templates: {
              leftArrow: '<img src="img/icon/arrow-right-gray.svg">',
              rightArrow: '<img src="img/icon/arrow-right-gray.svg">'
            },
            autoclose: true,
            language: 'id'
          };

        if (_defaultDate != undefined) {
          var _year = _defaultDate.split('/')[0],
          _month = parseInt(_defaultDate.split('/')[1]) - 1,
          _day = parseInt(_defaultDate.split('/')[2]);

          _options.defaultViewDate = { year: _year, month: _month, day: _day };
        }
        _this.datepicker(_options);
      });
    }
  },



  init: function() {
    if ( $('.js-datepicker').length ) {
      DatePicker.default();
    }
  }

};
var Editable = {
  init: function() {
    if ($('.js_edit_text').length) { 
      $('.js_edit_text').on('click', function(event) {
        $('.js_editable').toggleClass('active');
      });
    }
  }
};
var FlowCekStatusKado = {
  step0: function() {
    setTimeout(
      function() {
        $('.flow-cek-status-kado.step-0')
          .css('opacity', 0)
          .slideDown('fast')
          .animate(
            { opacity: 1 },
            { queue: false, duration: 'fast' }
          );
      }, 300);

    $('.flow-cek-status-kado.step-0').on('click', '.js-cek-status-kado', function() {
      if ($('.flow-cek-status-kado.step-0 input').val() !== '') {
        $('.flow-cek-status-kado.step-1')
          .css('opacity', 0)
          .slideDown('fast')
          .animate(
            { opacity: 1 },
            { queue: false, duration: 'fast' }
          );

        setTimeout(function() {
          $("html, body").animate({ scrollTop: $(document).height() }, 500);
        }, 250);
      }
    });
  },

  init: () => {
    FlowCekStatusKado.step0();
  }

};
var Form = {

  inputNumber: function(){

    $('body').on('focus', 'input[type=number]', function (e) {

      $(this).on('mousewheel.disableScroll', function (e) {
        e.preventDefault();
      });

    }).on('blur', 'input[type=number]', function (e) {

      $(this).off('mousewheel.disableScroll');

    }).on('input', 'input[type=number]', function (e) {

      var _this = $(this),
        _valLength = _this.val().length,
        _maxLength = parseInt(_this.attr('maxlength'));

      if (_valLength > _maxLength) {
        _this.val(_this.val().slice(0, _maxLength));
      }

    });

  },

  init: function() {
    Form.inputNumber();
  }

};
var Global = {

  cpNav: function() {
    $('.js-nav-trigger').on('click', function(){
      $(this).toggleClass('open');
      $(this).next('ul').slideToggle('fast');
    });

    $(window).resize(function(e) {
      if ( $(window).width() >= 768 ) {
        $('.js-nav-trigger').removeClass('open');
        $('.js-nav-trigger').next('ul').show();
      }
    });
  },

  doYouKnow: function() {
    $('.floating-do-you-know .close-button').on('click', function(e) {
      $('.floating-do-you-know').removeClass('show').show(0).fadeOut('fast');
      e.preventDefault();
    });
  },

  basicPageTab: function(){

    $('.js-basic-page-tab .column-tab-title').on('click', function(e) {
      var _this = $(this),
      _parent = _this.parents('.column-tab-item');

      if (!_parent.hasClass('active')) {
        $('.column-tab-item').removeClass('active');
        _parent.addClass('active');
      }
    });

  },

  tinggalkanPesan: function() {
    $('.js-tinggalkan-pesan input, .js-tinggalkan-pesan textarea').on('input', function() {
      if ( !$('.js-tinggalkan-pesan input').val() || !$('.js-tinggalkan-pesan textarea').val() ) {
        $('.js-tinggalkan-pesan button').prop('disabled', true);
      } else {
        $('.js-tinggalkan-pesan button').prop('disabled', false);
      }
    });
  },


  backStep: function(){
    var _scrollTop = $(window).scrollTop();
    var _space = 59;
    if ($(window).width() <= 320) {
      _space = 55;
    }
    if ($('.registration-pane.show-content .action-back-step').length) {
      if (_scrollTop > $('.registration-pane.show-content .action-back-step-placeholder').offset().top - _space) {
        $('.registration-pane.show-content .action-back-step').addClass('scroll-fixed');
      } else {
        $('.registration-pane.show-content .action-back-step').removeClass('scroll-fixed');
      }
    }
  },

  init: function() {

    if ( $('.js-nav-trigger').length ) {
      Global.cpNav();
    }

    if ( $('.floating-do-you-know').length ) {
      Global.doYouKnow();
    }

    if ( $('.js-basic-page-tab').length ) {
      Global.basicPageTab();
    }

    if ( $('.js-tinggalkan-pesan').length ) {
      Global.tinggalkanPesan();
    }

    if ($('.action-back-step').length) {
      $('.action-back-step').each(function(){
        $(this).after('<div class="action-back-step-placeholder"></div>');
      });
      $(window).scroll(function(){
        Global.backStep();
      });
    }
  }

};
var InspirasiKado = {
  tempDate: null,
  tempMonth: null,

  inputCheck: function() {
    $('#kado-untuk').on('input', function() {
      InspirasiKado.buttonEnable();
    });

    $('#tanggal-spesial').datepicker()
      .on('hide', function(e) {
        InspirasiKado.tempDate = e.date.getDate();
        InspirasiKado.tempMonth = e.date.getMonth();

        InspirasiKado.buttonEnable();
      });
  },

  // zodiacOutput: function(date, month) {

  // },

  buttonEnable: function() {
    var personZodiac,
        personName,
        zodiacSigns = ['Aquarius', 'Pisces', 'Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo', 'Libra', 'Scorpio', 'Sagittarius', 'Capricorn'];

      $('[data-zodiac]').removeClass('active');
      $('.zodiac-container').removeClass('active');



    if ( $('#kado-untuk').val() && $('#tanggal-spesial').val() ) {
      $('.kado-untuk-container button').attr('disabled', false);
    } else {
      $('.kado-untuk-container button').attr('disabled', true);
    }

    $('.kado-untuk-container button').on('click', function() {

      switch(InspirasiKado.tempMonth) {
        // January
        case 0:
          if (InspirasiKado.tempDate >= 20 ) {
            personZodiac = zodiacSigns[0];
          } else {
            personZodiac = zodiacSigns[11];
          }
          break;

        // February
        case 1:
          if (InspirasiKado.tempDate >= 19 ) {
            personZodiac = zodiacSigns[1];
          } else {
            personZodiac = zodiacSigns[0];
          }
          break;

        // March
        case 2:
          if (InspirasiKado.tempDate >= 21 ) {
            personZodiac = zodiacSigns[2];
          } else {
            personZodiac = zodiacSigns[1];
          }
          break;

        // April
        case 3:
          if (InspirasiKado.tempDate >= 20 ) {
            personZodiac = zodiacSigns[3];
          } else {
            personZodiac = zodiacSigns[2];
          }
          break;

        // May
        case 4:
          if (InspirasiKado.tempDate >= 21 ) {
            personZodiac = zodiacSigns[4];
          } else {
            personZodiac = zodiacSigns[3];
          }
          break;

        // June
        case 5:
          if (InspirasiKado.tempDate >= 21 ) {
            personZodiac = zodiacSigns[5];
          } else {
            personZodiac = zodiacSigns[4];
          }
          break;

        // July
        case 6:
          if (InspirasiKado.tempDate >= 23 ) {
            personZodiac = zodiacSigns[6];
          } else {
            personZodiac = zodiacSigns[5];
          }
          break;

        // August
        case 7:
          if (InspirasiKado.tempDate >= 23 ) {
            personZodiac = zodiacSigns[7];
          } else {
            personZodiac = zodiacSigns[6];
          }
          break;

        // September
        case 8:
          if (InspirasiKado.tempDate >= 23 ) {
            personZodiac = zodiacSigns[8];
          } else {
            personZodiac = zodiacSigns[7];
          }
          break;

        // October
        case 9:
          if (InspirasiKado.tempDate >= 23 ) {
            personZodiac = zodiacSigns[9];
          } else {
            personZodiac = zodiacSigns[8];
          }
          break;

        // November
        case 10:
          if (InspirasiKado.tempDate >= 22 ) {
            personZodiac = zodiacSigns[10];
          } else {
            personZodiac = zodiacSigns[9];
          }
          break;

        // December
        case 11:
          if (InspirasiKado.tempDate >= 21 ) {
            personZodiac = zodiacSigns[11];
          } else {
            personZodiac = zodiacSigns[10];
          }
          break;
      }

      personName = $('#kado-untuk').val();

      $('.zodiac-container').addClass('active');

      $('[data-zodiac=' + personZodiac + ']').addClass('active');

      $('#person-zodiac').text(personZodiac);
      $('#person-name').text(personName);
    });
  },

  init: () => {
    if ( $('.c-inspirasi-kado').length ) {
      InspirasiKado.inputCheck();
    }
  }

};
var InputFile = {
  handleChange: function() {

    $('.input-file').each(function(index, el) {
      $(this).find('button').attr('data-placeholder', $(this).find('div span').text());
    });

    $('.input-file button').on('click', function(){
      var label = $(this).closest('.input-file').find('div span'),
      placeholder = $(this).attr('data-placeholder'),
      filename;

      $(this).closest('.input-file').find('input[type="file"]').click();

      $('.input-file input[type="file"]').on('change', function(event){
        var _val = $(this).val(),
          _isValid = false;

        if ($whiteSpace.test(_val)) {
          label.text(placeholder).removeClass('has-input');
        } else {
          filename = event.target.files[0].name;
          _isValid = InputFile.validate(event, $(this));
          if (_isValid) {
            label.text(filename).addClass('has-input');
          }
        }

      });

    });
  },

  validate: function(evt, selector){
    var _this = selector;

    var isIE  = (navigator.appName=="Microsoft Internet Explorer"),
      _path = _this.val(),
      _ext  = _path.substring(_path.lastIndexOf('.') + 1).toLowerCase(),
      _format = _this.attr('data-format'),
      _maxUpload = parseInt(_this.attr('data-maxupload')) * 1024,
      _message = _this.parents('.input-file').find('.error-message'),
      _errFormat = _message.attr('data-err-format'),
      _errMaxUpload = _message.attr('data-err-maxupload');

    _message.hide();

     if(_format.indexOf(_ext) != -1){

        if(isIE) {
            var _pathImage = _this.attr('data-pathimage');
            if (_path == _pathImage) {
                _path = _pathImage;
            }
            var _myFSO = new ActiveXObject("Scripting.FileSystemObject"),
                _thefile = _myFSO.getFile(_path),
                _size = _thefile.size;

            if (_size > _maxUpload) {
                _this.val('');
                _message.text(_errMaxUpload).show();
                return false;
            } else {
              return true;
            }

        } else { 
            var _files = evt.target.files,
                _sizeupload = _files[0].size;

            if (_sizeupload > _maxUpload) {
                _this.val('');
                _message.text(_errMaxUpload).show();
                return false;
            } else {
              return true;
            }

        }

    } else {
      _message.text(_errFormat).show();
      _this.val('');
      return false;
    }

  },

  init: function() {
    if ( $('.input-file').length ) {
      InputFile.handleChange();
    }
  }
};
var MainHeader = {
  dropDown: function () {
    if ($('[data-header]').length) {

      var menu = ["life", "sports", "giving"];
      var urutan = 0;

      $('.main-header .up-down-arrow a:nth-of-type(1)').on('click', function (e) {

        e.preventDefault();

        if (urutan < 2) {
          urutan++;
        } else {
          urutan = 0;
        }

        $('[data-header]').removeClass('active-banner');

        $('[data-header=' + menu[urutan] + ']').addClass('active-banner');

      });

      $('.main-header .up-down-arrow a:nth-of-type(2)').on('click', function (e) {

        e.preventDefault();

        if (urutan > 0) {
          urutan--;
        } else {
          urutan = 2;
        }

        $('[data-header]').removeClass('active-banner');

        $('[data-header=' + menu[urutan] + ']').addClass('active-banner');

      });

    }
  },

  init: function () {
    MainHeader.dropDown();
  }
};
var MobileMenu = {

  event: function(){
    $('.js-mobile-menu').on('click', function(){
      var _this = $(this);

      if (_this.hasClass('close')) {
        $('.c-header').removeClass('show');
        $('.c-live-chat').removeClass('show-navigation-menu');
        _this.removeClass('close');
        MobileMenu.scroll.add();
      } else {
        $('.c-header').addClass('show');
        $('.c-live-chat').addClass('show-navigation-menu');
        _this.addClass('close');
        MobileMenu.scroll.remove();
      }

    });
  },

  scroll: {

    remove: function(){
      if ($isMobile.iOS()) {
          $('body').addClass('remove-scroll-ios');
      } else{
          $('body').addClass('remove-scroll');
      }
    },

    add: function(){
      $('body').removeClass('remove-scroll-ios remove-scroll');
    },

  },

  init: function() {
    MobileMenu.event();
  }

};
var NearestEvent = {

  event: function(){

    $(document).on('click', function(){
      if ($('.js-nearest-event').hasClass('show')) {
        $('.js-nearest-event, .ne-tooltip').removeClass('show');
      }
    });

    $('.nearest-event').on('click', function(e){
      e.stopPropagation();
    });

    $('.js-nearest-event').on('click', function(){
      if ($(this).hasClass('show')) {
        $('.ne-tooltip').removeClass('show');
        $(this).removeClass('show');
      } else {
        $('.ne-tooltip').addClass('show');
        $(this).addClass('show');
      }
    });

    $('.js-nearest-event-navigation > div').on('click', function(){
      var _this = $(this),
        _target = parseInt(_this.attr('data-target')),
        _length = $('.ne-section').length;

      if (_this.hasClass('prev')) {
        if (_target == 0) {
          return false;
        } else {
          _target -= 1;
          NearestEvent.nearestEventNavigation(_target);
        }
      } else {
        if (_target == _length) {
          return false;
        } else {
          NearestEvent.nearestEventNavigation(_target);
        }
      }
    });

  },

  nearestEventNavigation: function(target){
    $('.js-nearest-event-navigation .prev').attr('data-target', target);
    $('.js-nearest-event-navigation .next').attr('data-target', (target+1));
    $('.ne-section').removeClass('active');
    $('.ne-section[data-event="'+target+'"]').addClass('active');
  },

  init: function() {
    NearestEvent.event();
  },

};
var ParallaxSlider = {

  // set parallax
  set: function(){
    var oldSlide = 0;
    var currSlide = 0;
    var dataValues = [];

    // Init Parallax Slider
    var pSlider = new parallaxSlider({
      wrapperid: 'myparallaxslider', //ID of DIV on page to house slider
      displaymode: {type:'auto', pause: 5000, cycles: 0, stoponclick: false, pauseonmouseover: true},
      delaybtwdesc:  500, // delay in milliseconds between the revealing of each description layer inside a slide
      navbuttons: ['left.png', 'right.png', 'up.png', 'down.png'], // path to nav images
      activeslideclass: 'selectedslide', // CSS class that gets added to currently shown DIV slide
      orientation: 'h', //Valid values: "h" or "v"
      persist: false, //remember last viewed slide and recall within same session?
      slideduration: 1000,
      onUpdateSlide: function(currentSlide){

        ParallaxSlider.counter($('#myparallaxslider .slide:nth-child('+(currentSlide+1)+')'));

        setTimeout(function() {
          $('.irs-grid-text').removeClass('show-irs');
          $('.js-grid-text-'+currentSlide).addClass('show-irs');
        }, 150);

        var _isRangeSlider = $('.parallaxsliderNavigation').data("ionRangeSlider");
        _isRangeSlider.update({
            from: currentSlide
        });

      }
    });

    // Push Pagination Values
    $('#myparallaxslider .slide').each(function(){
      var _this = $(this);
      _titlePagination = _this.attr('data-pagination');
      dataValues.push(_titlePagination);
    });

    // Pagination Range Slider
    $(".parallaxsliderNavigation").ionRangeSlider({
      skin: 'parallax-rangeslider',
      grid: true,
      from: 0,
      to: 4,
      values: dataValues,
      onFinish: function(n) {
        oldSlide = currSlide;
        currSlide = n.from;

        ParallaxSlider.counter($('#myparallaxslider .slide:nth-child('+(currSlide+1)+')'));

        var keyword = (currSlide > oldSlide) ? "forth" : "back";
        pSlider.gotoSlide(keyword, currSlide);

      }
    });

    // Pagination Event
    $(document).on('click', '#myparallaxslider + .pagination .irs--parallax-rangeslider .irs + .irs-grid > .irs-grid-pol', function(){

      var _index = $(this).index(),
        _currentSlide = 0;
      oldSlide = currSlide;

      currSlide = _index / 6;

      var keyword = (currSlide > oldSlide) ? "forth" : "back";
      pSlider.gotoSlide(keyword, currSlide);

      ParallaxSlider.counter($('#myparallaxslider .slide:nth-child('+(currSlide+1)+')'));

      var _isRangeSlider = $('.parallaxsliderNavigation').data("ionRangeSlider");
      _isRangeSlider.update({
          from: currSlide
      });

    });

    var _lengthGridText = $('.irs-grid-text').length - 1;

    // Add Class show-irs
    setTimeout(function() {
      $('.js-grid-text-0, .js-grid-text-' + _lengthGridText).addClass('show-irs');
    }, 150);

  },

  // counter
  counter: function(parentSelector){

    var i = 0,
    maxNumber = parseInt(parentSelector.find('.js-counter-currency-number').attr('data-number')),
    counter = parseInt(parentSelector.find('.js-counter-currency-number').attr('data-counter'));

    var inv = setInterval(function() {
      if(i < maxNumber){
        i += counter;
        parentSelector.find('.js-counter-currency-number').text(HelperFormatRupiah(i));
      }
      else{
        parentSelector.find('.js-counter-currency-number').text(HelperFormatRupiah(maxNumber));
        clearInterval(inv);
      }
    }, 1);

  },

  // init
  init: function() {
    ParallaxSlider.set();
  }
};
var Payment = {

  paymentAgree: function(){
    if ($('#payment_agree').length) {
      $('#payment_agree').click(function() {
        if ($(this).prop('checked')) {
          if ($('.js-aktivasi-kado-selesai').length) {
            $('.js-aktivasi-kado-selesai').removeClass('disabled').addClass('primary');
          }
          if ($('.btn-bayar').length) {
            $('.btn-bayar').removeClass('disabled');
          }
          console.log(1);
        } else {
          console.log(0);
          if ($('.js-aktivasi-kado-selesai').length) {
            $('.js-aktivasi-kado-selesai').removeClass('primary').addClass('disabled');
          }
          if ($('.btn-bayar').length) {
            $('.btn-bayar').addClass('disabled');
          }
        }
      });
    }
  },

  bankList: function(){
    if ($('.bank-list').length) {
      $('.bank-list').click(function() {
        var _this = $(this);
        if (!_this.parent().hasClass('active')) {
          $('.bank').removeClass('active');
          _this.parent().addClass('active');
        }
      });
    }
  },

  checkAgreement: function(element_id) {
    $('#' + element_id)[0].checked = true;
    Popup.hide('paymentTnc');
    if ($('.js-aktivasi-kado-selesai').length) {
      $('.js-aktivasi-kado-selesai').removeClass('disabled').addClass('primary');
    }
    if ($('.btn-bayar').length) {
      $('.btn-bayar').removeClass('disabled');
    }
  },

  checkboxAgreement: function() {
    $(".js-agreement-checkbox").on('click', function() {
      if($(this).prop('checked')) {
        Popup.show('paymentTnc');
      }
    });
  },

  init: function() {
    Payment.paymentAgree();
    Payment.bankList();
    Payment.checkboxAgreement();
  },
};
var PolicyBenefit = {

  event: function(){

    $('.js-addtional-policy-benefit input').on('click', function(){
      var _this = $(this),
      _checked = _this.is(':checked'),
      _parents = _this.parents('.bp-a-checkbox-item'),
      _target = _parents.find('.bp-a-checkbox-value-number'),
      _dataValue = _target.attr('data-value'),
      _dataDefault = _target.attr('data-default');

      if (_checked) {
        _target.text(_dataValue);
      } else {
        _target.text(_dataDefault);
      }

    });

  },

  init: function() {
    PolicyBenefit.event();
  },

};
var Popup = {

  global: function(){
    $(document).on('click', '.c-popup .close', function() {
      var _this = $(this);
      if (_this.hasClass('tnc-authentication')) {
          Popup.show('authentication');
      } else {
        $('.c-popup').fadeOut('fast', function() {
          if ($isMobile.iOS()) {
            $('body').removeClass('remove-scroll-ios');
          } else {
            $('body').removeClass('remove-scroll');
          }
        });
      }
    });
  },

  show: function(popup_id) {
    $('.c-popup').fadeOut('fast');
    $('#' + popup_id).fadeIn('fast');
    if ($isMobile.iOS()) {
      setTimeout(function() {
        $('body').addClass('remove-scroll-ios');
      }, 750);
    } else {
      $('body').addClass('remove-scroll');
    }
  },

  hide: function(popup_id) {
    $('#' + popup_id).fadeOut('fast');
    if ($isMobile.iOS()) {
      $('body').removeClass('remove-scroll-ios');
    } else {
      $('body').removeClass('remove-scroll');
    }
  },

  category: function(){
    $('.js-popup-category > .popup-category-col > h4').on('click', function() {
      if ($(this).hasClass('active')) {
        return false;
      } else {
        $('.js-popup-category h4').removeClass('active');
        $(this).addClass('active');
      }
    });
  },

  authentication: {

    event: function() {

      // auth pane
      $(document).on('click','.js-auth-pane', function() {

        var _target = $(this).attr('data-target'),
          _before = '#' + $(this).parents('.authentication-pane').attr('id');

        $('.authentication-pane').removeClass('active');
        $(_target).addClass('active');
        if ($(_target).find('.js-auth-back').attr('data-target') == undefined) {
          $(_target).find('.js-auth-back').attr('data-target', _before);
        }

      // auth back
      }).on('click','.js-auth-back', function() {

        var _target = $(this).attr('data-target');

        $('.authentication-pane').removeClass('active');
        $(_target).addClass('active');
        if ($(_target).hasClass('authentication-login')) {
          $('.js-auth-back').removeAttr('data-target');
        }
      });

      // login input
      $(document).on('input','.js-auth-login-i-username, .js-auth-login-i-password', function() {

        Popup.authentication.login();

      // login aggreement
      }).on('click','.js-auth-login-aggreement', function() {

        Popup.authentication.login();

      // tnc auth aggreement
      }).on('click','.js-aggreement-tnc-authentication', function() {
        if (!$('.js-auth-login-aggreement').prop('checked')) {
          $('.js-auth-login-aggreement').trigger('click');
        }
        Popup.show('authentication');
      });

      // forgot username
      $(document).on('input','.js-auth-forgot-username-i-id-number, .js-auth-forgot-username-i-policy-number', function() {

        Popup.authentication.forgotUsername();

      }).on('click', '.js-auth-forgot-username', function(e){

        if ($(this).hasClass('primary')) {
          $('.js-auth-forgot-username-i-id-number, .js-auth-forgot-username-i-policy-number').val('');
          $('.authentication-pane').removeClass('active');
          $('#requestProcessedUsernamePane').addClass('active');
          $('#requestProcessedUsernamePane').find('.js-auth-back').attr('data-target', '#loginPane');
          e.preventDefault();
        }

      }).on('click', '.js-done-forgot-username', function(e){

        $('.js-auth-forgot-username-i-id-number, .js-auth-forgot-username-i-policy-number').val('');
        $('#requestProcessedUsernamePane').find('.js-auth-back').trigger('click');

      });


      // forgot password
      $(document).on('input','.js-auth-forgot-password-i-username, .js-auth-forgot-password-i-id-number', function() {

        Popup.authentication.forgotPassword();

      }).on('click', '.js-auth-forgot-password', function(e){

        if ($(this).hasClass('primary')) {
          $('.js-auth-forgot-password-i-username, .js-auth-forgot-password-i-id-number').val('');
          $('.authentication-pane').removeClass('active');
          $('#requestProcessedPasswordPane').addClass('active');
          $('#requestProcessedPasswordPane').find('.js-auth-back').attr('data-target', '#loginPane');
          e.preventDefault();
        }

      }).on('click', '.js-done-forgot-password', function(e){

        $('.js-auth-forgot-password-i-username, .js-auth-forgot-password-i-id-number').val('');
        $('#requestProcessedPasswordPane').find('.js-auth-back').trigger('click');

      });


    },

    // login
    login: function(){
      var _username = $('.js-auth-login-i-username').val(),
      _validUsername = false,
      _password = $('.js-auth-login-i-password').val(),
      _validPassword = false,
      _aggreement = $('.js-auth-login-aggreement').prop('checked');

      if (!$whiteSpace.test(_username) && _username.length > 2) {
        _validUsername = true;
      }

      if (!$whiteSpace.test(_password) && _password.length > 2) {
        _validPassword = true;
      }

      if (_validUsername && _validPassword && _aggreement) {
        $('.authentication-login').find('.js-auth-login').removeClass('disabled').addClass('primary');
      } else {
        $('.authentication-login').find('.js-auth-login').removeClass('primary').addClass('disabled');
      }

    },

    // forgot username
    forgotUsername: function(){
      var _idNumber = $('.js-auth-forgot-username-i-id-number').val(),
      _validIdNumber = false,
      _policyNumber = $('.js-auth-forgot-username-i-policy-number').val(),
      _validPolicyNumber = false;

      if (!$whiteSpace.test(_idNumber) && _idNumber.length == 16) {
        _validIdNumber = true;
      }

      if (!$whiteSpace.test(_policyNumber) && _policyNumber.length > 2) {
        _validPolicyNumber = true;
      }

      if (_validIdNumber && _validPolicyNumber) {
        $('.authentication-forgot-username').find('.js-auth-forgot-username').removeClass('disabled').addClass('primary');
      } else {
        $('.authentication-forgot-username').find('.js-auth-forgot-username').removeClass('primary').addClass('disabled');
      }
    },

    // forgot password
    forgotPassword: function(){
      var _username = $('.js-auth-forgot-password-i-username').val(),
      _validUsername = false,
      _idNumber = $('.js-auth-forgot-password-i-id-number').val(),
      _validIdNumber = false;

      if (!$whiteSpace.test(_username) && _username.length > 2) {
        _validUsername = true;
      }

      if (!$whiteSpace.test(_idNumber) && _idNumber.length == 16) {
        _validIdNumber = true;
      }

      if (_validUsername && _validIdNumber) {
        $('.authentication-forgot-password').find('.js-auth-forgot-password').removeClass('disabled').addClass('primary');
      } else {
        $('.authentication-forgot-password').find('.js-auth-forgot-password').removeClass('primary').addClass('disabled');
      }
    }


  },

  resetPassword: function(){

    $(document).on('input','.js-reset-password-i-new-password, .js-reset-password-i-confirm-new-password', function() {

      var _password = $('.js-reset-password-i-new-password').val(),
      _confirmPassword = $('.js-reset-password-i-confirm-new-password').val();

    if (!$whiteSpace.test(_password) && _password == _confirmPassword) {
      $('.authentication-reset-password').find('.js-reset-password').removeClass('disabled').addClass('primary');
    } else {
      $('.authentication-reset-password').find('.js-reset-password').removeClass('primary').addClass('disabled');
    }

    }).on('click', '.js-reset-password', function(e){

      if ($(this).hasClass('primary')) {
        $('.popup-reset-password .authentication-pane').removeClass('active');
        $('#resetPasswordSuccessPane').addClass('active');
        e.preventDefault();
      }

    }).on('click', '.js-back-login-reset-password', function(e){

      $('#loginPane').addClass('active');
      Popup.show('authentication');
      setTimeout(function() {
        $('.popup-reset-password .authentication-pane').removeClass('active');
        $('#resetPasswordPane').addClass('active').find('input').val('');
      }, 750);

    });

  },

  newPassword: function(){

    $(document).on('input','.js-new-password-i-new-password, .js-new-password-i-confirm-new-password', function() {

      var _password = $('.js-new-password-i-new-password').val(),
      _confirmPassword = $('.js-new-password-i-confirm-new-password').val();

    if (!$whiteSpace.test(_password) && _password == _confirmPassword) {
      $('.authentication-new-password').find('.js-new-password').removeClass('disabled').addClass('primary');
    } else {
      $('.authentication-new-password').find('.js-new-password').removeClass('primary').addClass('disabled');
    }

    }).on('click', '.js-new-password', function(e){

      if ($(this).hasClass('primary')) {
        $('.popup-new-password .authentication-pane').removeClass('active');
        $('#newPasswordSuccessPane').addClass('active');
        e.preventDefault();
      }

    }).on('click', '.js-back-login-new-password', function(e){

      $('#loginPane').addClass('active');
      Popup.show('authentication');
      setTimeout(function() {
        $('.popup-new-password .authentication-pane').removeClass('active');
        $('#newPasswordPane').addClass('active').find('input').val('');
      }, 750);

    });

  },

  init: function(trigger_id) {
    if ($('.c-popup').length) {
      $('.c-popup').each(function(){
        var _this = $(this);
        _this.find('.popup-wrapper').wrap('<div class="popup-centering"></div>').wrap('<div class="popup-vertical-middle"></div>');
      });
    }
    Popup.global();
    Popup.category();
    Popup.authentication.event();
    Popup.resetPassword();
    Popup.newPassword();

  }
};
var RangeSlider = {

  event: function() {
    if ( $('.js-range-slider').length ) {
      $('.js-range-slider').each(function(){

        var wrapperRangeSlider = $(this);
        var h2input, _maxLength, _val;
        var jsRangeSlider = wrapperRangeSlider.find('.the-range-slider');
        var maxValue = parseInt(jsRangeSlider.data('max'));
        var minValue = parseInt(jsRangeSlider.data('min'));

        if ( wrapperRangeSlider.find('h2 input').length ) {
          h2input = wrapperRangeSlider.find('h2 input');

          // only number
          h2input.on('keypress', function(e) {
            var _key = parseInt(e.key);
            if (!$numReg.test(_key)) return false;
          });

          h2input.on('input', function() {
            var _this = $(this);
            _maxLength = _this.attr('maxlength');
            _val = _this.val();

            if (_maxLength != undefined) {
              _maxLength = parseInt(_maxLength);
              if (_val.length > _maxLength) {
                _val =  _val.slice(0, _maxLength);
              }
            }

            // range-slider-money without delimiter
            if (wrapperRangeSlider.hasClass('range-slider-money')) {
              _val = parseInt(_val.split('.').join(''));
            }

            jsRangeSlider.data('ionRangeSlider').update({
              from: _val
            });

            // range-slider-money with delimiter
            if (wrapperRangeSlider.hasClass('range-slider-money')) {
              _this.val(HelperFormatRupiah(_val));
            } else {
              _this.val(_val);
            }

          });

          h2input.on('blur', function(){
            var _this = $(this);
            _val = _this.val();

            // range-slider-money without delimiter
            if (wrapperRangeSlider.hasClass('range-slider-money')) {
              if (!$whiteSpace.test(_val)) {
                _val = parseInt(_val.split('.').join(''));
              }
            }

            if (_val > maxValue) {
              // range-slider-money with delimiter
              if (wrapperRangeSlider.hasClass('range-slider-money')) {
                h2input.val(HelperFormatRupiah(maxValue));
              }
              else {
                h2input.val(maxValue);
              }
            } else if (_val < minValue) {
              // range-slider-money with delimiter
              if (wrapperRangeSlider.hasClass('range-slider-money')) {
                h2input.val(HelperFormatRupiah(minValue));
              }
              else {
                h2input.val(minValue);
              }
            }
          });

          jsRangeSlider.ionRangeSlider({
            onStart: function (data) {
              h2input.val(HelperFormatRupiah(data.from));
              setTimeout(function() {
                if ($(window).width() < 768) {
                  if (data.from_percent < 17) {
                    wrapperRangeSlider.find('.irs--astra-life .irs-single').css({
                      'left': data.from_percent + '%',
                      'right': 'auto'
                    });
                  } else if (data.from_percent > 75) {
                    wrapperRangeSlider.find('.irs--astra-life .irs-single').css({
                      'right': (100 - data.from_percent) + '%',
                      'left': 'auto'
                    });
                  } else {
                    wrapperRangeSlider.find('.irs--astra-life .irs-single').css({
                      'left': (data.from_percent - 20) + '%',
                      'right': 'auto'
                    });
                  }
                }
                if ( wrapperRangeSlider.closest('.the-input-field').length ) {
                  wrapperRangeSlider.closest('.the-input-field').find('.js-ajukan-button').prop('disabled', false);
                }
              }, 1000);
            },
            onChange: function (data) {
              h2input.val(HelperFormatRupiah(data.from));
              if ($(window).width() < 768) {
                if (data.from_percent < 17) {
                  wrapperRangeSlider.find('.irs--astra-life .irs-single').css({
                    'left': data.from_percent + '%',
                    'right': 'auto'
                  });
                } else if (data.from_percent > 75) {
                  wrapperRangeSlider.find('.irs--astra-life .irs-single').css({
                    'right': (100 - data.from_percent) + '%',
                    'left': 'auto'
                  });
                } else {
                  wrapperRangeSlider.find('.irs--astra-life .irs-single').css({
                    'left': (data.from_percent - 20) + '%',
                    'right': 'auto'
                  });
                }
              }
              if ( wrapperRangeSlider.closest('.the-input-field').length ) {
                wrapperRangeSlider.closest('.the-input-field').find('.js-ajukan-button').prop('disabled', false);
              }
            },
            onUpdate: function (data) {
              h2input.val(HelperFormatRupiah(data.from));
            }
          });
        } else {
          jsRangeSlider.ionRangeSlider();
        }

      });
    }
  },

  init: function() {
    RangeSlider.event();
  }

};
var RegistrationGift = {

  alertEndSession: function(){

    $('.header-container a.logo, .header-navigation a').on('click', function(e) {
      var _href = $(this).attr('href');
      if (!$('.registration-pane.registration-step-0').hasClass('show-content')) {
        Popup.show('alertEndSession');
        $('.js-end-registration').attr('href', _href);
        e.preventDefault();
      }
    });

    $('.js-continue-registration').on('click', function(e) {
      Popup.hide('alertEndSession');
      e.preventDefault();
    });

    $('.js-end-registration').on('click', function(e) {
      Popup.hide('alertEndSession');
    });

  },

  backStep: function(){
    $('.js-back-step').on('click', function(event) {
      var _this = $(this),
      _target = '.' + _this.attr('data-target'),
      _targetInt = parseInt(_target.split('.registration-step-')[1]);

      RegistrationGift.switch(".registration-step-"+(_targetInt+1), ".registration-step-"+_targetInt);
      if (_targetInt == 2) {
        $('.parent-container').removeClass('p-payment-summary payment-summary-isport');
      }

    });
  },

  tabs: function(){
    $('.js-registration-tabs .item').on('click', function(event) {
      var _this = $(this),
        _target = '.' + _this.attr('data-target'),
      _targetInt = parseInt(_target.split('.registration-step-')[1]);

      $('.js-registration-tabs .item').removeClass('current');
      _this.addClass('current');

      $('.registration-pane').removeClass('show-content');

      setTimeout(function() {
        $('.registration-pane').removeClass('active');
        $(_target).addClass('active');
        $("html, body").animate({scrollTop: 0 }, 500);
      }, 350);

      setTimeout(function() {
        $(_target).addClass('show-content');
        if (_targetInt == 3) {
          $('.parent-container').addClass('p-payment-summary payment-summary-gift');
        } else {
          $('.parent-container').removeClass('p-payment-summary payment-summary-gift');
        }
      }, 400);

    });
  },


  step0: function(){

    $('.registration-step-0').addClass('active');

    setTimeout(function() {
      $('.registration-step-0').addClass('show-content');
    }, 250);

    // start registration next step 1
    $('.js-start-registration-gift').on('click', function(e) {

      RegistrationGift.switch(".registration-step-0", ".registration-step-1");
      setTimeout(function() {
        $('.js-registration-tabs').addClass('show-content');
      }, 400);
      e.preventDefault();

    });

  },

  step1: function(){

    $('.js-gift-name-for, .js-phone-number-gift, .js-email-gift').on('input',  function() {
      RegistrationGift.validateBuyGift();
    });

    $('.js-gift-price-dropdown, .js-send-gift-date').on('change',  function() {
      RegistrationGift.validateBuyGift();
    });

    $('.js-next-step-2').on('click',  function() {
      RegistrationGift.switch(".registration-step-1", ".registration-step-2");
    });


    $('.js-next-step-2').on('click', function() {
      var _this = $(this);
      if (_this.hasClass('primary')) {
        RegistrationiSport.switch(".registration-step-1", ".registration-step-2");
      }
    });

    $('#personalizationGift').on('change', function(){
      if ( !$('#personalizationGift:checked').length ) {
        $('.c-pilih-tema-kado').stop().fadeOut('fast');
      } else {
        $('.c-pilih-tema-kado').stop().fadeIn('fast');
      }
    });

  },

  step2: function(){

      $('.js-my-name, .js-phone-number, .js-email').on('input', function() {
        RegistrationGift.validateDataUpdate();
      });

      $('.js-next-step3').on('click', function() {
        var _this = $(this);
        if (_this.hasClass('primary')) {
          RegistrationGift.switch(".registration-step-2", ".registration-step-3");
        }
      });

  },

  step3: function(){

    $('.js-next-payment').on('click', function() {

      $('.registration-pane').removeClass('show-content');
      setTimeout(function() {
        $('.registration-pane').removeClass('active');
        $('.registration-payment-options').addClass('active');
        $("html, body").animate({scrollTop: 0 }, 500);
      }, 350);

      setTimeout(function() {
        $('.registration-payment-options').addClass('show-content');
        $('.parent-container').removeClass('p-payment-summary payment-summary-gift');
      }, 400);

    });

    $('.js-payment-success').on('click', function(e) {

      $('.registration-pane').removeClass('show-content');
      setTimeout(function() {
        $('.registration-pane').removeClass('active');
        $('.registration-payment-success').addClass('active');
        $("html, body").animate({scrollTop: 0 }, 500);
      }, 350);

      setTimeout(function() {
        $('.registration-payment-success').addClass('show-content');
      }, 400);

      e.preventDefault();

    });

    $('.js-detail-virtual-account').on('click', function(e) {
      Popup.show('virtualAccountConfirmation');
      e.preventDefault();
    });

    $('.js-cancel-use-virtual-account').on('click', function(e) {
      Popup.hide('virtualAccountConfirmation');
      e.preventDefault();
    });

    $('.js-confirm-use-virtual-account').on('click', function(e) {

      Popup.hide('virtualAccountConfirmation');
      $('.registration-pane').removeClass('show-content');
      setTimeout(function() {
        $('.registration-pane').removeClass('active');
        $('.registration-detail-virtual-account').addClass('active');
        $("html, body").animate({scrollTop: 0 }, 500);
      }, 350);

      setTimeout(function() {
        $('.registration-detail-virtual-account').addClass('show-content');
      }, 400);

      e.preventDefault();

    });

  },

  switch: function(prev, next){

    $(prev).removeClass('show-content');
    setTimeout(function() {
      $(prev).removeClass('active');
      $(next).addClass('active');
      $("html, body").animate({scrollTop: 0 }, 500);
    }, 350);

    setTimeout(function() {
      $(next).addClass('show-content');
    }, 400);

    setTimeout(function() {
      var _prevInt = parseInt(prev.split('.registration-step-')[1]),
          _nextInt = parseInt(next.split('.registration-step-')[1]);
      if (_nextInt > 1) {
        $('.js-registration-tabs .item[data-target="registration-step-'+_prevInt+'"]').removeClass('current').addClass('completed');
        $('.js-registration-tabs .item[data-target="registration-step-'+_nextInt+'"]').addClass('current');
        if (_nextInt == 3) {
          $('.parent-container').addClass('p-payment-summary payment-summary-gift');
        }
      } else {
        $('.js-registration-tabs .item[data-target="registration-step-'+_nextInt+'"]').addClass('current');
      }
    }, 750);

  },

  resetPremium: function(){

    $("html, body").animate({scrollTop: 0 }, 500);
    $('.js-assurance-for-dropdown, .js-sport-list, .js-assurance-period-dropdown, .js-assurance-start, .js-total-premium-dropdown').val('').trigger('change');
    $('.sport-extreme-note').hide(0);
    $('.js-sport-category').text('');
    $('.js-community-code').val('');
    $('.calculate-premium-result, .form-input-wrapper-community-code').fadeOut('fast');
    $('.js-addtional-policy-benefit input[type="checkbox"]').prop('checked', false);
    $('.bp-a-checkbox-item').each(function(){
      var _target = $(this).find('.bp-a-checkbox-value-number'),
      _dataDefault = _target.attr('data-default');
      _target.text(_dataDefault);
    });

  },

  validateBuyGift: function(){

    var _giftNameFor = $('.js-gift-name-for').val(),
        _giftNameForIsValid = false;

    if (_giftNameFor.length > 2) {
      if ($onlyOneName.test(_giftNameFor) || $fullName.test(_giftNameFor)) {
        if (!$('.form-input-gift-price').hasClass('has-show')) {
          setTimeout(function() {
            $("html, body").animate({scrollTop: $('.form-input-gift-price').offset().top }, 500);
          }, 100);
        }
        $('.form-input-gift-price').fadeIn('fast').addClass('has-show');
        _giftNameForIsValid = true;
      } else {
        $('.form-input-gift-price').fadeOut('fast').removeClass('has-show');
      }
    } else {
        $('.form-input-gift-price').fadeOut('fast').removeClass('has-show');
    }

    var _giftPrice = $('.js-gift-price-dropdown').val(),
    _giftPriceIsValid = false;

    if (!$whiteSpace.test(_giftPrice)) {
      if (!$('.form-input-benefit-gift').hasClass('has-show')) {
        setTimeout(function() {
          $("html, body").animate({scrollTop: $('.form-input-benefit-gift').offset().top }, 500);
        }, 100);
      }
      $('.form-input-benefit-gift').fadeIn('fast').addClass('has-show');
      _giftPriceIsValid = true;
      Carousel.listTemaKado();
    } else {
      $('.form-input-benefit-gift').fadeOut('fast').removeClass('has-show');
    }

    var _giftEmail = $('.js-email-gift').val(),
    _giftEmailIsValid = false;

    if ($email.test(_giftEmail) && !$whiteSpace.test(_giftEmail)) {
      if ($(window).width() >= 768) {
        if (!$('.form-input-phone-number-gift').hasClass('has-show')) {
          setTimeout(function() {
            $("html, body").animate({scrollTop: $('.form-input-phone-number-gift').offset().top }, 500);
          }, 100);
        }
      }
      $('.form-input-phone-number-gift').fadeIn('fast').addClass('has-show');
      _giftEmailIsValid = true;
    } else {
      $('.form-input-phone-number-gift').fadeOut('fast').removeClass('has-show');
    }

    var _phoneNumberGift = $('.js-phone-number-gift').val(),
    _phoneNumberGiftIsValid = false;

    if (_phoneNumberGift.length >= 8) {
      if ($(window).width() >= 768) {
        if (!$('.form-input-email').hasClass('has-show')) {
          setTimeout(function() {
            $("html, body").animate({scrollTop: $('.form-input-send-gift-date').offset().top }, 500);
          }, 100);
        }
      }
      $('.form-input-send-gift-date, .form-input-whatsapp-gift').fadeIn('fast').addClass('has-show');
      _phoneNumberGiftIsValid = true;
    } else {
        $('.form-input-send-gift-date, .form-input-whatsapp-gift').fadeOut('fast').removeClass('has-show');
    }

    var _sendGiftDate = !$whiteSpace.test($('.js-send-gift-date').val());

    if (_giftNameForIsValid && _giftPriceIsValid && _giftEmailIsValid && _phoneNumberGiftIsValid && _sendGiftDate) {
      $('.js-next-step-2').removeClass('disabled').addClass('primary');
    } else {
      $('.js-next-step-2').removeClass('primary').addClass('disabled');
    }

  },

  validateDataUpdate: function(){
    var _myName = $('.js-my-name').val(),
    _myNameIsValid = false;

    if (_myName.length > 2) {
      if ($onlyOneName.test(_myName) || $fullName.test(_myName)) {
        _myNameIsValid = true;
      }
    }

    var _phoneNumber = $('.js-phone-number').val(),
    _phoneNumberIsValid = false;

    if (_phoneNumber.length >= 8) {
      _phoneNumberIsValid = true;
    }

    var _email = $('.js-email').val(),
    _emailIsValid = false;

    if ($email.test(_email) && !$whiteSpace.test(_email)) {
      _emailIsValid = true;
    }

    if (_myNameIsValid && _phoneNumberIsValid && _emailIsValid) {
      $('.js-next-step3').removeClass('disabled').addClass('primary');
    } else {
      $('.js-next-step3').removeClass('primary').addClass('disabled');
    }

  },

  init: function() {
    if ($('.js-start-registration-gift').length) {
      RegistrationiSport.alertEndSession();
      RegistrationGift.backStep();
      RegistrationGift.tabs();
      RegistrationGift.step0();
      RegistrationGift.step1();
      RegistrationGift.step2();
      RegistrationGift.step3();
    }
  },

};
var RegistrationLife = {

  defaultAgeProtection: 29,

  alertEndSession: function(){

    $('.header-container a.logo, .header-navigation a').on('click', function(e) {
      var _href = $(this).attr('href');
      if (!$('.registration-pane.registration-step-0').hasClass('show-content')) {
        Popup.show('alertEndSession');
        $('.js-end-registration').attr('href', _href);
        e.preventDefault();
      }
    });

    $('.js-continue-registration').on('click', function(e) {
      Popup.hide('alertEndSession');
      e.preventDefault();
    });

    $('.js-end-registration').on('click', function(e) {
      Popup.hide('alertEndSession');
    });

  },

  backStep: function(){
    $('.js-back-step').on('click', function(event) {
      var _this = $(this),
      _target = '.' + _this.attr('data-target'),
      _targetInt = parseInt(_target.split('.registration-step-')[1]);

      RegistrationLife.switch(".registration-step-"+(_targetInt+1), ".registration-step-"+_targetInt);
      if (_targetInt == 4) {
        $('.parent-container').removeClass('p-payment-summary');
      }

    });
  },

  tabs: function(){
    $('.js-registration-tabs .item').on('click', function(event) {
      var _this = $(this),
        _target = '.' + _this.attr('data-target'),
      _targetInt = parseInt(_target.split('.registration-step-')[1]);

      $('.js-registration-tabs .item').removeClass('current');
      _this.addClass('current');

      $('.registration-pane').removeClass('show-content');

      setTimeout(function() {
        $('.registration-pane').removeClass('active');
        $(_target).addClass('active');
        $("html, body").animate({scrollTop: 0 }, 500);
      }, 350);

      setTimeout(function() {
        $(_target).addClass('show-content');
        if (_targetInt == 5) {
          $('.parent-container').addClass('p-payment-summary');
        } else {
          $('.parent-container').removeClass('p-payment-summary');
        }
      }, 400);

    });
  },

  step0: function(){

    $('.registration-step-0').addClass('active');

    setTimeout(function() {
      $('.registration-step-0').addClass('show-content');
    }, 250);

    // start registration next step 1
    $('.js-start-registration').on('click', function(e) {

      RegistrationLife.switch(".registration-step-0", ".registration-step-1");
      setTimeout(function() {
        $('.js-registration-tabs').addClass('show-content');
      }, 400);
      e.preventDefault();

    });

  },

  step1: function(){

    // question health 1
    $('.js-health-input1').on('change', function(event) {
      var _this = $(this),
          _val = _this.val();
      if (_val == 1) {
        RegistrationLife.notAllowed();
      } else {
        $('.question-health-2').fadeIn(400);
        $("html, body").animate({scrollTop: $('.js-health-input2').offset().top }, 500);
      }
    });

    // question health 2
    $('.js-health-input2').on('change', function(event) {
      var _this = $(this),
          _val = _this.val();
      if (_val == 1) {
        RegistrationLife.notAllowed();
      } else {
        $('.registration-step-1 .next-button').fadeIn(400);
        $("html, body").animate({scrollTop: $('.registration-step-1 .next-button').offset().top }, 500);
      }
    });

    // next step 2
    $('.js-next-step-2').on('click', function(e) {
      RegistrationLife.switch(".registration-step-1", ".registration-step-2");
      e.preventDefault();
    });

  },

  step2: function(){

    var wrapperRangeSlider = $('.js-range-protection-slider');
    var h2input, _maxLength, _val;
    var jsRangeSlider = wrapperRangeSlider.find('.the-range-slider');
    var maxValue = parseInt(jsRangeSlider.data('max'));
    var minValue = parseInt(jsRangeSlider.data('min'));

    h2input = wrapperRangeSlider.find('h2 input');

    // only number
    h2input.on('keypress', function(e) {
      var _key = parseInt(e.key);
      if (!$numReg.test(_key)) return false;
    });

    h2input.on('input', function() {
      var _this = $(this);
      _maxLength = _this.attr('maxlength');
      _val = _this.val();

      if (_maxLength != undefined) {
        _maxLength = parseInt(_maxLength);
        if (_val.length > _maxLength) {
          _val =  _val.slice(0, _maxLength);
        }
      }

      jsRangeSlider.data('ionRangeSlider').update({
        from: _val
      });

      _this.val(_val);

    });

    h2input.on('blur', function(){
      var _this = $(this);
      _val = _this.val();

      if (_val > maxValue) {
        h2input.val(maxValue);
      } else if (_val < minValue) { 
        h2input.val(minValue);
      }
    });

    jsRangeSlider.ionRangeSlider({
      onStart: function (data) {
        h2input.val(HelperFormatRupiah(data.from));
      },
      onChange: function (data) {
        h2input.val(HelperFormatRupiah(data.from));
        if ($(window).width() < 768) {
          if (data.from_percent < 17) {
            wrapperRangeSlider.find('.irs--astra-life .irs-single').css({
              'left': data.from_percent + '%',
              'right': 'auto'
            });
          } else if (data.from_percent > 75) {
            wrapperRangeSlider.find('.irs--astra-life .irs-single').css({
              'right': (100 - data.from_percent) + '%',
              'left': 'auto'
            });
          } else {
            wrapperRangeSlider.find('.irs--astra-life .irs-single').css({
              'left': (data.from_percent - 20) + '%',
              'right': 'auto'
            });
          }
        }

        if (data.from > RegistrationLife.defaultAgeProtection) {
          $('.default-protection-copy').addClass('hide-note');
          $('.drag-protection-copy').removeClass('hide-note');
        } else {
          $('.drag-protection-copy').addClass('hide-note');
          $('.default-protection-copy').removeClass('hide-note');
        }
      },
      onUpdate: function (data) {
        h2input.val(HelperFormatRupiah(data.from));
      }
    });

    $('.js-gender-dropdown, .js-installment-premium').on('change',  function() {
      RegistrationLife.validatePremium();
    });

    $('.js-birth-date').on('change',  function() {
      var _getBirthDate = $(this).val();
      _getBirthDate = _getBirthDate.split(' ');

      var _getDay = _getBirthDate[0],
      _getMonth = $monthsName.indexOf(_getBirthDate[1]) + 1,
      _getYear = _getBirthDate[2];

      if (_getMonth < 10) {
        _getMonth = '0'+_getMonth;
      }
      var _ageDataFormat = _getYear + '/' + _getMonth + '/' + _getDay;
      var _getAge = HelperGetAge(_ageDataFormat);

      if (_getAge < 29 ) {
        RegistrationLife.defaultAgeProtection = 29;
        jsRangeSlider.data('ionRangeSlider').update({
          from: 29
        });
      } else if (_getAge >= 29 && _getAge < 85){
        RegistrationLife.defaultAgeProtection = _getAge + 1;
        jsRangeSlider.data('ionRangeSlider').update({
          from: _getAge + 1
        });
      } else {
        RegistrationLife.defaultAgeProtection = 85;
        jsRangeSlider.data('ionRangeSlider').update({
          from: 85
        });
      }

      RegistrationLife.validatePremium();
    });

    $('.registration-step-2 .the-range-slider').each(function(){
        $(this).ionRangeSlider({
          onFinish: function(data){
            RegistrationLife.validatePremium();
          }
        });
    });

    $('.registration-step-2 .the-range-slider').on('input',  function(e) {

    });

    $('.js-body-weight, .js-body-height, .registration-step-2 input').on('input',  function(e) {
      var _this = $(this),
        _val = _this.val();
        if (_val == '0') {
          _this.val('');
        } else {
          RegistrationLife.validatePremium();
        }
    });

    $('.js-calculate-premium').on('click',  function() {
      var _this = $(this);
      if (_this.hasClass('primary')) {
        $("html, body").animate({scrollTop: $('.js-calculate-premium').offset().top + 160 }, 500);
        $('.calculate-loader').fadeIn(400);
        setTimeout(function() {
          $('.about-user-premium, .calculate-loader').fadeOut(0);
          _this.parents('.next-button').fadeOut(0, function(){
            $('.calculate-loader').fadeOut(400, function(){
              $('.calculate-premium-result').fadeIn(400);
              $("html, body").animate({scrollTop: $('.js-get-premium').offset().top }, 500);
            });
          });
        }, 1250);
      }
    });

    $('.js-get-premium').on('click',  function() {
      RegistrationLife.switch(".registration-step-2", ".registration-step-3");
    });

    $('.js-recalculate-premium').on('click',  function() {
      RegistrationLife.resetPremium();
    });

  },

  step3: function(){

      $('.js-my-name').on('input', function() {
        RegistrationLife.validateDataUpdate.step1();
      });

      $('.js-id-card').on('input', function() {
          RegistrationLife.validateDataUpdate.step1();
      });

      $('.js-input-file').on('change', function() {
        RegistrationLife.validateDataUpdate.step1();
      });

      $('.js-next-member-data-step1').on('click', function() {
        var _this = $(this);
        if (_this.hasClass('primary')) {
          Popup.show('existingMemberData');
        }
      });

      $('.js-use-existing-data').on('click', function(event) {
        Popup.hide('existingMemberData');
        RegistrationLife.switch(".registration-step-3", ".registration-step-4");
      });

      $('.js-use-new-data').on('click', function(event) {
        Popup.hide('existingMemberData');
        RegistrationLife.validateDataUpdate.step2();
        $('.js-next-member-data-step1').parents('.next-button').fadeOut('fast', function(){
          $('.form-input-phone-number').fadeIn('fast');
          $("html, body").animate({scrollTop: $('.form-input-phone-number').offset().top }, 500);
        });
      });

      $('.js-phone-number').on('input', function() {
          RegistrationLife.validateDataUpdate.step2();
      });

      $('.js-email').on('input', function() {
        RegistrationLife.validateDataUpdate.step2();
      });

      $('.js-address').on('input', function() {
        RegistrationLife.validateDataUpdate.step2();
      });

      $('.js-province').on('change', function() {
        RegistrationLife.validateDataUpdate.step2();
      });

      $('.js-pos-code').on('input', function() {
        RegistrationLife.validateDataUpdate.step2();
      });

      $('.js-next-member-data-step2').on('click', function() {
        var _this = $(this);
        if (_this.hasClass('primary')) {
          RegistrationLife.switch(".registration-step-3", ".registration-step-4");
        }
      });

  },

  step4: function(){

    $('.js-next-step-5').on('click', function() {
      var _this = $(this);
      if (_this.hasClass('primary')) {
        RegistrationLife.switch(".registration-step-4", ".registration-step-5");
      }
    });

  },

  step5: function(){

    $('.js-next-payment').on('click', function() {

      $('.registration-pane').removeClass('show-content');
      setTimeout(function() {
        $('.registration-pane').removeClass('active');
        $('.registration-payment-options').addClass('active');
        $("html, body").animate({scrollTop: 0 }, 500);
      }, 350);

      setTimeout(function() {
        $('.registration-payment-options').addClass('show-content');
        $('.parent-container').removeClass('p-payment-summary');
      }, 400);

    });

    $('.js-payment-success').on('click', function(e) {

      $('.registration-pane').removeClass('show-content');
      setTimeout(function() {
        $('.registration-pane').removeClass('active');
        $('.registration-payment-success').addClass('active');
        $("html, body").animate({scrollTop: 0 }, 500);
      }, 350);

      setTimeout(function() {
        $('.registration-payment-success').addClass('show-content');
      }, 400);

      e.preventDefault();

    });

    $('.js-detail-virtual-account').on('click', function(e) {
      Popup.show('virtualAccountConfirmation');
      e.preventDefault();
    });

    $('.js-cancel-use-virtual-account').on('click', function(e) {
      Popup.hide('virtualAccountConfirmation');
      e.preventDefault();
    });

    $('.js-confirm-use-virtual-account').on('click', function(e) {

      Popup.hide('virtualAccountConfirmation');
      $('.registration-pane').removeClass('show-content');
      setTimeout(function() {
        $('.registration-pane').removeClass('active');
        $('.registration-detail-virtual-account').addClass('active');
        $("html, body").animate({scrollTop: 0 }, 500);
      }, 350);

      setTimeout(function() {
        $('.registration-detail-virtual-account').addClass('show-content');
      }, 400);

      e.preventDefault();

    });

  },

  switch: function(prev, next){

    $(prev).removeClass('show-content');
    setTimeout(function() {
      $(prev).removeClass('active');
      $(next).addClass('active');
      $("html, body").animate({scrollTop: 0 }, 500);
    }, 350);

    setTimeout(function() {
      $(next).addClass('show-content');
    }, 400);

    setTimeout(function() {
      var _prevInt = parseInt(prev.split('.registration-step-')[1]),
          _nextInt = parseInt(next.split('.registration-step-')[1]);
      if (_nextInt > 1) {
        $('.js-registration-tabs .item[data-target="registration-step-'+_prevInt+'"]').removeClass('current').addClass('completed');
        $('.js-registration-tabs .item[data-target="registration-step-'+_nextInt+'"]').addClass('current');
        if (_nextInt == 5) {
          $('.parent-container').addClass('p-payment-summary');
        }
      } else {
        $('.js-registration-tabs .item[data-target="registration-step-'+_nextInt+'"]').addClass('current');
      }
    }, 750);

  },

  notAllowed: function(){
    RegistrationLife.switch(".registration-step-1", ".registration-not-allowed");
    $('.js-registration-tabs').removeClass('show-content');
  },

  validatePremium: function(){

    var _gender = !$whiteSpace.test($('.js-gender-dropdown').val()),
        _birthDate = !$whiteSpace.test($('.js-birth-date').val()),
        _weight = !$whiteSpace.test($('.js-body-weight').val()),
        _height = !$whiteSpace.test($('.js-body-height').val());
        _installmentPremium = !$whiteSpace.test($('.js-installment-premium').val());

    $('.js-premium-periode').text($('.js-installment-premium').find('option:selected').text());

    $('.calculate-premium-result').fadeOut(0, function(){
      $('.about-user-premium').fadeIn(400);
      $('.js-calculate-premium').parents('.next-button').fadeIn(400);
    });

    if (_gender && _birthDate && _weight && _height && _installmentPremium) {
      $('.js-calculate-premium').removeClass('disabled').addClass('primary');
    } else {
      $('.about-user-premium').fadeIn(400);
      $('.js-calculate-premium').removeClass('primary').addClass('disabled');
    }

  },

  resetPremium: function(){

    $("html, body").animate({scrollTop: 0 }, 500);
    $('.js-gender-dropdown, .js-birth-date, .js-installment-premium').val('').trigger('change');
    $('.js-body-weight, .js-body-height').val('');
    $('.calculate-premium-result').fadeOut('fast', function(){
      $('.about-user-premium').fadeIn(400);
      $('.js-calculate-premium').parents('.next-button').fadeIn(400);
    });

    $('.registration-step-2 .js-range-slider').each(function(index, el) {
      var _this = $(this),
          _input = _this.find('h2 input'),
          _isRangeSlider = _this.find('.the-range-slider').data("ionRangeSlider"),
          _fromValue = _this.find('.the-range-slider').attr('data-from');
          _input.val(_fromValue);
          _isRangeSlider.update({
              from: _fromValue
          });
    });

  },

  validateDataUpdate: {
    step1: function(){

      var _valMyName = $('.js-my-name').val(),
          _valMyNameIsValid = false;
      if (_valMyName.length > 2) {
        if ($onlyOneName.test(_valMyName) || $fullName.test(_valMyName)) {
          if ($(window).width() >= 768) {
            if (!$('.form-input-id-card-and-file').hasClass('has-show')) {
              setTimeout(function() {
                $("html, body").animate({scrollTop: $('.form-input-id-card-and-file').offset().top }, 500);
              }, 100);
            }
          }
          $('.form-input-id-card-and-file').fadeIn('fast').addClass('has-show');
          _valMyNameIsValid = true;
        } else {
          $('.form-input-id-card-and-file').fadeOut('fast').removeClass('has-show');
        }
      } else {
          $('.form-input-id-card-and-file').fadeOut('fast').removeClass('has-show');
      }

      setTimeout(function() {
        var _valIDCard = $('.js-id-card').val(),
            _valIDCardLength = _valIDCard.length,
            _valInputFile = $('.js-input-file').val();

        if (_valIDCardLength == 17) {
          _valIDCardLength = 16;
        }

        $('.form-input-phone-number, .form-input-email, .form-input-address').fadeOut(0, function(){
          $('.js-next-member-data-step1').parents('.next-button').fadeIn('fast');
        });

        if (!$whiteSpace.test(_valIDCard) && _valIDCardLength == 16 && !$whiteSpace.test(_valInputFile) && _valMyNameIsValid) {
          $('.js-next-member-data-step1').removeClass('disabled').addClass('primary');
        } else {
          $('.js-next-member-data-step1').removeClass('primary').addClass('disabled');
        }
      }, 150);

    },

    step2: function(){
      var _phoneNumber = $('.js-phone-number'),
          _valPhoneNumber = _phoneNumber.val(),
          _valPhoneNumberIsValid = false;

      if (_valPhoneNumber.length >= 8) {
        if ($(window).width() >= 768) {
          if (!$('.form-input-email').hasClass('has-show')) {
            setTimeout(function() {
              $("html, body").animate({scrollTop: $('.form-input-email').offset().top }, 500);
            }, 100);
          }
        }
        $('.form-input-email').fadeIn('fast').addClass('has-show');
        _valPhoneNumberIsValid = true;
      } else {
          $('.form-input-email').fadeOut('fast').removeClass('has-show');
      }

      var _email = $('.js-email'),
          _valEmail = _email.val(),
          _valEmailIsValid = false;

      if ($email.test(_valEmail) && !$whiteSpace.test(_valEmail)) {
        if ($(window).width() >= 768) {
          if (!$('.form-input-address').hasClass('has-show')) {
            setTimeout(function() {
              $("html, body").animate({scrollTop: $('.form-input-address').offset().top }, 500);
            }, 100);
          }
        }
        $('.form-input-address').fadeIn('fast').addClass('has-show');
        _valEmailIsValid = true;
      } else {
        $('.form-input-address').fadeOut('fast').removeClass('has-show');
      }

      var _address = $('.js-address'),
          _valAddress = _address.val(),
          _valAddressIsValid = false;

      if (!$whiteSpace.test(_valAddress)) {
        _valAddressIsValid = true;
      }

      var _province = $('.js-province'),
          _valProvince = _province.val(),
          _valProvinceIsValid = false;

      if (!$whiteSpace.test(_valProvince)) {
        _valProvinceIsValid = true;
      }

      var _posCode = $('.js-pos-code'),
          _valPosCode = _posCode.val(),
          _valPosCodeLength = _valPosCode.length,
          _valPosCodeIsValid = false;

      if (_valPosCodeLength == 6) {
        _valPosCodeLength = 5;
      }

      if (!$whiteSpace.test(_valPosCode) && _valPosCodeLength == 5) {
        _valPosCodeIsValid = true;
      }

      if (_valPhoneNumberIsValid && _valEmailIsValid && _valAddressIsValid && _valProvinceIsValid && _valPosCodeIsValid) {
        $('.js-next-member-data-step2').removeClass('disabled').addClass('primary');
      } else {
        $('.js-next-member-data-step2').removeClass('primary').addClass('disabled');
      }

    },
  },

  init: function() {
    if ($('.js-start-registration').length) {
      RegistrationLife.alertEndSession();
      RegistrationLife.backStep();
      RegistrationLife.tabs();
      RegistrationLife.step0();
      RegistrationLife.step1();
      RegistrationLife.step2();
      RegistrationLife.step3();
      RegistrationLife.step4();
      RegistrationLife.step5();
    }
  },

};
var RegistrationiSport = {

  alertEndSession: function(){

    $('.header-container a.logo, .header-navigation a').on('click', function(e) {
      var _href = $(this).attr('href');
      if (!$('.registration-pane.registration-step-0').hasClass('show-content')) {
        Popup.show('alertEndSession');
        $('.js-end-registration').attr('href', _href);
        e.preventDefault();
      }
    });

    $('.js-continue-registration').on('click', function(e) {
      Popup.hide('alertEndSession');
      e.preventDefault();
    });

    $('.js-end-registration').on('click', function(e) {
      Popup.hide('alertEndSession');
    });

  },

  backStep: function(){
    $('.js-back-step').on('click', function(event) {
      var _this = $(this),
      _target = '.' + _this.attr('data-target'),
      _targetInt = parseInt(_target.split('.registration-step-')[1]);

      RegistrationiSport.switch(".registration-step-"+(_targetInt+1), ".registration-step-"+_targetInt);
      if (_targetInt == 2) {
        $('.parent-container').removeClass('p-payment-summary payment-summary-isport');
      }

    });
  },

  tabs: function(){
    $('.js-registration-tabs .item').on('click', function(event) {
      var _this = $(this),
        _target = '.' + _this.attr('data-target'),
      _targetInt = parseInt(_target.split('.registration-step-')[1]);

      $('.js-registration-tabs .item').removeClass('current');
      _this.addClass('current');

      $('.registration-pane').removeClass('show-content');

      setTimeout(function() {
        $('.registration-pane').removeClass('active');
        $(_target).addClass('active');
        $("html, body").animate({scrollTop: 0 }, 500);
      }, 350);

      setTimeout(function() {
        $(_target).addClass('show-content');
        if (_targetInt == 3) {
          $('.parent-container').addClass('p-payment-summary payment-summary-isport');
        } else {
          $('.parent-container').removeClass('p-payment-summary payment-summary-isport');
        }
      }, 400);

    });
  },


  step0: function(){

    $('.registration-step-0').addClass('active');

    setTimeout(function() {
      $('.registration-step-0').addClass('show-content');
    }, 250);

    // start registration next step 1
    $('.js-start-registration-isport').on('click', function(e) {

      RegistrationiSport.switch(".registration-step-0", ".registration-step-1");
      setTimeout(function() {
        $('.js-registration-tabs').addClass('show-content');
      }, 400);
      e.preventDefault();

    });

  },

  step1: function(){

    $('.js-assurance-for-dropdown, .js-sport-list, .js-assurance-period-dropdown, .js-assurance-start, .js-total-premium-dropdown').on('change',  function() {
      RegistrationiSport.validatePremium.step1();
    });

    $('.js-calculate-premium').on('click',  function() {
      var _this = $(this);
      if (_this.hasClass('primary')) {
        $("html, body").animate({scrollTop: $('.js-calculate-premium').offset().top + 160 }, 500);
        $('.calculate-loader').fadeIn(400);
        setTimeout(function() {
          $('.calculate-loader').fadeOut(0);
          _this.parents('.next-button-isport').fadeOut(0, function(){
            $('.calculate-loader').fadeOut(400, function(){
              $('.calculate-premium-result').fadeIn(400);
              $("html, body").animate({scrollTop: $('.js-get-premium').offset().top }, 500);
            });
          });
        }, 1250);
      }
    });

    $('.js-get-premium').on('click',  function() {
      $(this).fadeOut(0, function(){
        $('.form-input-wrapper-community-code').fadeIn('fast');
      });
    });

    $('.js-have-community-code-dropdown').on('change',  function() {
      RegistrationiSport.validatePremium.step2();
    });

    $('.js-community-code').on('keyup',  function() {
      RegistrationiSport.validatePremium.step2();
    });

    $('.js-next-step-2').on('click',  function() {
      RegistrationiSport.switch(".registration-step-1", ".registration-step-2");
    });

    $('.js-recalculate-premium').on('click',  function() {
      RegistrationiSport.resetPremium();
    });

  },

  step2: function(){

      $('.js-my-name').on('input', function() {
        RegistrationiSport.validateDataUpdate.step1();
      });

      $('.js-gender-dropdown, .js-birth-date, .js-input-file').on('change', function() {
        RegistrationiSport.validateDataUpdate.step1();
      });

      $('.js-id-card').on('input', function() {
          RegistrationiSport.validateDataUpdate.step1();
      });

      $('.js-next-member-data-step1').on('click', function() {
        var _this = $(this);
        if (_this.hasClass('primary')) {
          Popup.show('existingMemberData');
        }
      });

      $('.js-use-existing-data').on('click', function(event) {
        Popup.hide('existingMemberData');
        var _checkAssuranceForSelected = $('.js-assurance-for-dropdown option:selected').attr('data-selected');
        if (_checkAssuranceForSelected == 'child') {
          $('.js-next-member-data-step1').parents('.next-button').hide(0);
          $('.form-input-child, .next-button-isport-step3').show(0);
          $("html, body").animate({scrollTop: $('.form-input-child').offset().top }, 500);
        } else {
          RegistrationiSport.switch(".registration-step-2", ".registration-step-3");
        }
      });

      $('.js-use-new-data').on('click', function(event) {
        Popup.hide('existingMemberData');
        RegistrationiSport.validateDataUpdate.step2();
        $('.js-next-member-data-step1').parents('.next-button').fadeOut('fast', function(){
          $('.form-input-phone-number').fadeIn('fast');
          $("html, body").animate({scrollTop: $('.form-input-phone-number').offset().top }, 500);
        });
      });

      $('.js-phone-number').on('input', function() {
          RegistrationiSport.validateDataUpdate.step2();
      });

      $('.js-email').on('input', function() {
        RegistrationiSport.validateDataUpdate.step2();
      });

      $('.js-address').on('input', function() {
        RegistrationiSport.validateDataUpdate.step2();
      });

      $('.js-province').on('change', function() {
        RegistrationiSport.validateDataUpdate.step2();
      });

      $('.js-pos-code').on('input', function() {
        RegistrationiSport.validateDataUpdate.step2();
      });

      $('.js-my-child-name').on('input', function() {
        RegistrationiSport.validateDataUpdate.step3();
      });

      $('.js-child-gender-dropdown').on('change', function() {
        RegistrationiSport.validateDataUpdate.step3();
      });

      $('.js-child-birth-date').on('change', function() {
        RegistrationiSport.validateDataUpdate.step3();
      });

      $('.js-next-member-data-step2').on('click', function() {
        var _this = $(this);
        if (_this.hasClass('primary')) {
          RegistrationiSport.switch(".registration-step-2", ".registration-step-3");
        }
      });

  },

  step3: function(){

    $('.js-next-payment').on('click', function() {

      $('.registration-pane').removeClass('show-content');
      setTimeout(function() {
        $('.registration-pane').removeClass('active');
        $('.registration-payment-options').addClass('active');
        $("html, body").animate({scrollTop: 0 }, 500);
      }, 350);

      setTimeout(function() {
        $('.registration-payment-options').addClass('show-content');
        $('.parent-container').removeClass('p-payment-summary payment-summary-isport');
      }, 400);

    });

    $('.js-payment-success').on('click', function(e) {

      $('.registration-pane').removeClass('show-content');
      setTimeout(function() {
        $('.registration-pane').removeClass('active');
        $('.registration-payment-success').addClass('active');
        $("html, body").animate({scrollTop: 0 }, 500);
      }, 350);

      setTimeout(function() {
        $('.registration-payment-success').addClass('show-content');
      }, 400);

      e.preventDefault();

    });

    $('.js-detail-virtual-account').on('click', function(e) {
      Popup.show('virtualAccountConfirmation');
      e.preventDefault();
    });

    $('.js-cancel-use-virtual-account').on('click', function(e) {
      Popup.hide('virtualAccountConfirmation');
      e.preventDefault();
    });

    $('.js-confirm-use-virtual-account').on('click', function(e) {

      Popup.hide('virtualAccountConfirmation');
      $('.registration-pane').removeClass('show-content');
      setTimeout(function() {
        $('.registration-pane').removeClass('active');
        $('.registration-detail-virtual-account').addClass('active');
        $("html, body").animate({scrollTop: 0 }, 500);
      }, 350);

      setTimeout(function() {
        $('.registration-detail-virtual-account').addClass('show-content');
      }, 400);

      e.preventDefault();

    });

  },

  switch: function(prev, next){

    $(prev).removeClass('show-content');
    setTimeout(function() {
      $(prev).removeClass('active');
      $(next).addClass('active');
      $("html, body").animate({scrollTop: 0 }, 500);
    }, 350);

    setTimeout(function() {
      $(next).addClass('show-content');
    }, 400);

    setTimeout(function() {
      var _prevInt = parseInt(prev.split('.registration-step-')[1]),
          _nextInt = parseInt(next.split('.registration-step-')[1]);
      if (_nextInt > 1) {
        $('.js-registration-tabs .item[data-target="registration-step-'+_prevInt+'"]').removeClass('current').addClass('completed');
        $('.js-registration-tabs .item[data-target="registration-step-'+_nextInt+'"]').addClass('current');
        if (_nextInt == 3) {
          $('.parent-container').addClass('p-payment-summary payment-summary-isport');
        }
      } else {
        $('.js-registration-tabs .item[data-target="registration-step-'+_nextInt+'"]').addClass('current');
      }
    }, 750);

  },

  resetPremium: function(){

    $("html, body").animate({scrollTop: 0 }, 500);
    $('.js-assurance-for-dropdown, .js-sport-list, .js-assurance-period-dropdown, .js-assurance-start, .js-total-premium-dropdown').val('').trigger('change');
    $('.sport-extreme-note').hide(0);
    $('.js-sport-category').text('');
    $('.js-community-code').val('');
    $('.calculate-premium-result, .form-input-wrapper-community-code').fadeOut('fast');
    $('.js-addtional-policy-benefit input[type="checkbox"], .form-input-assurance-start input[type="checkbox"]').prop('checked', false);
    $('.bp-a-checkbox-item').each(function(){
      var _target = $(this).find('.bp-a-checkbox-value-number'),
      _dataDefault = _target.attr('data-default');
      _target.text(_dataDefault);
    });

  },

  validatePremium: {

    step1: function(){
      var _assuranceFor = $('.js-assurance-for-dropdown').val(),
          _assuranceForSelected = $('.js-assurance-for-dropdown option:selected').attr('data-selected'),
          _valAssuranceFor = false;

      if (_assuranceForSelected == 'child') {
        $('.child-selected').addClass('is-selected');
      } else {
        $('.child-selected').removeClass('is-selected');
      }

      if (!$whiteSpace.test(_assuranceFor)) {
        if (!$('.form-input-sport-list').hasClass('has-show')) {
          setTimeout(function() {
            $("html, body").animate({scrollTop: $('.form-input-sport-list').offset().top }, 500);
          }, 100);
        }
        $('.form-input-sport-list').fadeIn('fast').addClass('has-show');
        _valAssuranceFor = true;
      } else {
        $('.form-input-sport-list').fadeOut('fast').removeClass('has-show');
      }

      var _sport = $('.js-sport-list').val(),
          _valSport = false;

      if (!$whiteSpace.test(_sport)) {
        if (!$('.form-input-assurance-period').hasClass('has-show')) {
          setTimeout(function() {
            $("html, body").animate({scrollTop: $('.form-input-assurance-period').offset().top }, 500);
          }, 100);
        }
        $('.form-input-assurance-period').fadeIn('fast').addClass('has-show');
        _valSport = true;
      } else {
        $('.form-input-assurance-period').fadeOut('fast').removeClass('has-show');
      }

      var _assurancePeriod = $('.js-assurance-period-dropdown').val(),
          _valAssurancePeriod = false;

      if (!$whiteSpace.test(_assurancePeriod)) {
        if (!$('.form-input-assurance-start').hasClass('has-show')) {
          setTimeout(function() {
            $("html, body").animate({scrollTop: $('.form-input-assurance-start').offset().top }, 500);
          }, 100);
        }
        $('.form-input-assurance-start').fadeIn('fast').addClass('has-show');
        $('.registration-step-1').addClass('add-more-p-bottom');
        _valAssurancePeriod = true;
      } else {
        $('.form-input-assurance-start').fadeOut('fast').removeClass('has-show');
        $('.registration-step-1').removeClass('add-more-p-bottom');
      }

      var _assuranceStart = $('.js-assurance-start').val(),
          _valAssuranceStart = false;

      if (!$whiteSpace.test(_assuranceStart)) {
        if (!$('.form-input-total-premium').hasClass('has-show')) {
          setTimeout(function() {
            $("html, body").animate({scrollTop: $('.form-input-total-premium').offset().top }, 500);
          }, 100);
        }
        $('.form-input-total-premium').fadeIn('fast').addClass('has-show');
        _valAssuranceStart = true;
      } else {
        $('.form-input-total-premium').fadeOut('fast').removeClass('has-show');
      }

      var _totalPremium = $('.js-total-premium-dropdown').val(),
          _valTotalPremium = false;

      if (!$whiteSpace.test(_totalPremium)) {
        if (!$('.form-input-benefit-policy').hasClass('has-show')) {
          setTimeout(function() {
            $("html, body").animate({scrollTop: $('.form-input-benefit-policy').offset().top }, 500);
          }, 100);
        }
        $('.registration-step-1').removeClass('add-more-p-bottom');
        $('.form-input-benefit-policy, .next-button-isport').fadeIn('fast').addClass('has-show');
        $('.js-calculate-premium').removeClass('disabled').addClass('primary');
        _valTotalPremium = true;
      } else {
        $('.form-input-benefit-policy, .next-button-isport').fadeOut('fast').removeClass('has-show');
        $('.js-calculate-premium').removeClass('primary').addClass('disabled');
      }

    },

    step2: function(){
      var _communityCode = $('.js-community-code').val(),
        _valcommunityCode = false;

      if (_communityCode.length > 5) {
        $('.js-next-step-2').removeClass('disabled').addClass('primary');
        _valcommunityCode = true;
      } else {
        $('.js-next-step-2').removeClass('primary').addClass('disabled');
      }

      var _haveCommunityCode = $('.js-have-community-code-dropdown').val(),
          _valHaveCommunityCode = false;

      if (_haveCommunityCode == "1") {
        if (_valcommunityCode) {
          $('.js-next-step-2').removeClass('disabled').addClass('primary');
        } else {
          $('.js-next-step-2').removeClass('primary').addClass('disabled');
          $('.form-input-community-code').fadeIn('fast');
        }
      } else {
        $('.js-next-step-2').removeClass('disabled').addClass('primary');
        $('.form-input-community-code').fadeOut(100);
      }
      $("html, body").animate({scrollTop: $('body').height() }, 500);
    }

  },

  validateDataUpdate: {
    step1: function(){

      var _valMyName = $('.js-my-name').val(),
          _valMyNameIsValid = false;
      if (_valMyName.length > 2 && ($onlyOneName.test(_valMyName) || $fullName.test(_valMyName))) {
        _valMyNameIsValid = true;
      }

      var _gender = !$whiteSpace.test($('.js-gender-dropdown').val()),
      _birthDate = !$whiteSpace.test($('.js-birth-date').val());

      setTimeout(function() {
        var _valIDCard = $('.js-id-card').val(),
            _valIDCardLength = _valIDCard.length,
            _valInputFile = $('.js-input-file').val();

        if (_valIDCardLength == 17) {
          _valIDCardLength = 16;
        }
        
        // TEST
        $('.form-input-phone-number, .form-input-email, .form-input-address, .next-button-isport-step3').fadeOut(0, function(){
          $('.js-next-member-data-step1').parents('.next-button').fadeIn('fast');
        });

        if (!$whiteSpace.test(_valIDCard) && _valIDCardLength == 16 && !$whiteSpace.test(_valInputFile) && _valMyNameIsValid && _gender && _birthDate) {
          $('.js-next-member-data-step1').removeClass('disabled').addClass('primary');
        } else {
          $('.js-next-member-data-step1').removeClass('primary').addClass('disabled');
        }
      }, 150);

    },

    step2: function(){
      var _phoneNumber = $('.js-phone-number'),
          _valPhoneNumber = _phoneNumber.val(),
          _valPhoneNumberIsValid = false;

      if (_valPhoneNumber.length >= 8) {
        if ($(window).width() >= 768) {
          if (!$('.form-input-email').hasClass('has-show')) {
            setTimeout(function() {
              $("html, body").animate({scrollTop: $('.form-input-email').offset().top }, 500);
            }, 100);
          }
        }
        $('.form-input-email').fadeIn('fast').addClass('has-show');
        _valPhoneNumberIsValid = true;
      } else {
          $('.form-input-email').fadeOut('fast').removeClass('has-show');
      }

      var _email = $('.js-email'),
          _valEmail = _email.val(),
          _valEmailIsValid = false;

      if ($email.test(_valEmail) && !$whiteSpace.test(_valEmail)) {
        if ($(window).width() >= 768) {
          if (!$('.form-input-address').hasClass('has-show')) {
            setTimeout(function() {
              $("html, body").animate({scrollTop: $('.form-input-address').offset().top }, 500);
            }, 100);
          }
        }
        $('.form-input-address').fadeIn('fast').addClass('has-show');
        _valEmailIsValid = true;
      } else {
        $('.form-input-address').fadeOut('fast').removeClass('has-show');
      }

      var _address = $('.js-address'),
          _valAddress = _address.val(),
          _valAddressIsValid = false;

      if (!$whiteSpace.test(_valAddress)) {
        _valAddressIsValid = true;
      }

      var _province = $('.js-province'),
          _valProvince = _province.val(),
          _valProvinceIsValid = false;

      if (!$whiteSpace.test(_valProvince)) {
        _valProvinceIsValid = true;
      }

      var _posCode = $('.js-pos-code'),
          _valPosCode = _posCode.val(),
          _valPosCodeLength = _valPosCode.length,
          _valPosCodeIsValid = false;

      if (_valPosCodeLength == 6) {
        _valPosCodeLength = 5;
      }

      if (!$whiteSpace.test(_valPosCode) && _valPosCodeLength == 5) {
        _valPosCodeIsValid = true;
      }

      var _checkAssuranceForSelected = $('.js-assurance-for-dropdown option:selected').attr('data-selected');
      if (_checkAssuranceForSelected == 'child') {
        if (_valPhoneNumberIsValid && _valEmailIsValid && _valAddressIsValid && _valProvinceIsValid && _valPosCodeIsValid) {
          $('.form-input-child, .next-button-isport-step3').show(0);
          $("html, body").animate({scrollTop: $('.form-input-child').offset().top }, 500);
        } else {
          $('.form-input-child, .next-button-isport-step3').hide(0);
        }
      } else {
        if (_valPhoneNumberIsValid && _valEmailIsValid && _valAddressIsValid && _valProvinceIsValid && _valPosCodeIsValid) {
          $('.next-button-isport-step3').show(0);
          $('.js-next-member-data-step2').removeClass('disabled').addClass('primary');
        } else {
          $('.next-button-isport-step3').hide(0);
          $('.js-next-member-data-step2').removeClass('primary').addClass('disabled');
        }
      }

    },

    step3: function(){
      var _valMyChildName = $('.js-my-child-name').val(),
          _valMyChildNameIsValid = false;
      if (_valMyChildName.length > 2 && ($onlyOneName.test(_valMyChildName) || $fullName.test(_valMyChildName))) {
        _valMyChildNameIsValid = true;
      }

      var _genderChild = !$whiteSpace.test($('.js-child-gender-dropdown').val()),
      _birthDateChild = !$whiteSpace.test($('.js-child-birth-date').val());

      if (_birthDateChild) {
        var _getBirthDate = $('.js-child-birth-date').val();
        _getBirthDate = _getBirthDate.split(' ');

        var _getDay = _getBirthDate[0],
        _getMonth = $monthsName.indexOf(_getBirthDate[1]) + 1,
        _getYear = _getBirthDate[2];

        if (_getMonth < 10) {
          _getMonth = '0'+_getMonth;
        }

        var _ageDataFormat = _getYear + '/' + _getMonth + '/' + _getDay;

        var _getAge = HelperGetAge(_ageDataFormat);

        if (_getAge > 17) {
          Popup.show('policyChild');
        }
      }

      if (_valMyChildName && _genderChild && _birthDateChild) {
        $('.js-next-member-data-step2').removeClass('disabled').addClass('primary');
      } else {
        $('.js-next-member-data-step2').removeClass('primary').addClass('disabled');
      }

    }
  },

  init: function() {
    if ($('.js-start-registration-isport').length) {
      RegistrationiSport.alertEndSession();
      RegistrationiSport.backStep();
      RegistrationiSport.tabs();
      RegistrationiSport.step0();
      RegistrationiSport.step1();
      RegistrationiSport.step2();
      RegistrationiSport.step3();
    }
  },

};
var RiskMeter = {
  pilih: function() {

    $('.js-riskmeter-dropdown').select2({
      theme: 'astra-life-dropdown',
      language: {
        noResults: function() {
          return "Olahraga tidak ditemukan";
        }
      }
    });

    $('.js-riskmeter-dropdown').on('change', function() {

      var selectValue = $(this).val(),
          jsResiko = $(this).parent('.form-input').next('.js-resiko'),
          jsResikoSlider = $(this).parent('.form-input').nextAll('.js-resiko-slider');

      // show the risk
      jsResiko.fadeIn('fast');
      jsResikoSlider.fadeIn('fast');

      // find risk list with selected value
      jsResiko.find('.list-resiko').each(function() {
        $(this).stop().hide();
        if ( $(this).data('olahraga') == selectValue ) {
          $(this).stop().fadeIn('fast');
        }
      });

      // ummm, slider?
      jsResikoSlider.find('.slider-container').each(function() {
        $(this).stop().hide();

        if ( $(this).find('ul').hasClass('slick-initialized') ) {
          $(this).find('ul').slick('unslick');
        }

        if ( $(this).data('olahraga') == selectValue ) {
          $(this).stop().show();
          RiskMeter.slider($(this).find('ul'));
        }
      });

    });

  },

  slider: function(element) {
    element.slick({
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: false,
      swipe: true
    });
  },

  init: function() {
    if ( $('.c-riskmeter').length ) {
      RiskMeter.pilih();
    }
  }
};
var SEOHider = {

  event: function() {
    $('.js-seo-hider').on('click', function(e) {
      var _this = $(this),
      _textMore = _this.attr('data-text-more'),
      _textLess = _this.attr('data-text-less');

      if (_this.hasClass('expanded')) {
        $('.c-seo-catcher').removeClass('expanded');
        _this.removeClass('expanded');
        _this.text(_textMore);
      } else {
        $('.c-seo-catcher').addClass('expanded');
        _this.addClass('expanded');
        _this.text(_textLess);
      }
      e.preventDefault();
    });
  },

  init: function() {
      SEOHider.event();
  }
};
var SportCategories = {

  set: function(){

    if ( $('.js-sport-list').length ) {
      $('.js-sport-list').select2({
        theme: 'astra-life-dropdown',
        language: {
          noResults: function() {
            return "Olahraga tidak ditemukan";
          }
        }
      }).on("change", function (e) {

        var _this = $(this),
          _val = _this.val(),
          _category = _this.find('option:selected').attr('data-category'),
          _text = "";

          if (_category == "0") {
            _text = "Umum";
            $('.sport-extreme-note').hide(0);
          } else {
            _text = "Ekstrem";
            $('.sport-extreme-note').show(0);
          }

          $('.js-sport-category').text(_text);

      });
    }

  },

  init: function() {
    SportCategories.set();
  },

};
var SportsEvents = {

  sportsEvents: function() {
    var temp = new Date();

    $('.js-sports-events-datepicker').datepicker({
      templates: {
        leftArrow: '<img src="img/icon/arrow-right-gray.svg">',
        rightArrow: '<img src="img/icon/arrow-right-gray.svg">'
      },
      maxViewMode: 0,
      startDate: temp,
      ignoreReadonly: true,
    })
    .on('changeMonth', function(e) {
      SportsEvents.month = new Date(e.date).getMonth() + 1;
      SportsEvents.year = new Date(e.date).getFullYear();
      SportsEvents.updateEvents();
    });

    $('.datepicker .datepicker-days tbody').on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();
    });


    SportsEvents.month = temp.getMonth() + 1;
    SportsEvents.year = temp.getFullYear();
    SportsEvents.updateEvents();

  },

  updateEvents: function() {
    var datepicker = $('.js-sports-events-datepicker'),
        eventsdates = [],
        fulleventsdates = [];

    $('.event-list').each(function() {
      $(this).stop().hide();

      if ( $(this).data('month') == SportsEvents.month & $(this).data('year') == SportsEvents.year ) {
        $(this).stop().fadeIn('fast');

        $(this).find('li').each(function(){
          eventsdates.push($(this).data('date'));
        });

        for (var date in eventsdates) {
          fulleventsdates.push(new Date(SportsEvents.year, (SportsEvents.month - 1), eventsdates[date]));
        }

        $('.js-sports-events-datepicker').datepicker('setDates', fulleventsdates);
      }
    });

  },

  showEventDetail: function() {
    $('[data-detail]').on('click', function(e) {
      e.preventDefault();
      var temp = $(this).data('detail');

      if ( $('[data-detail-view=' + temp + ']').length ) {
        $('.sports-event-container').addClass('hide-me');
        $('[data-detail-view=' + temp + ']').addClass('active');
      }
    });

    $('.sports-event-detail .back').on('click', function(e) {
      e.preventDefault();
      $('.sports-event-container').removeClass('hide-me');
      $('[data-detail-view]').removeClass('active');
    });
  },

  init: () => {
    if ( $('.js-sports-events-datepicker').length ) {
      SportsEvents.sportsEvents();
      SportsEvents.showEventDetail();
    }
  }

};
var VideoPopup = {

  videoUrl: '',
  init: function() {
    var el = $('.js-video-popup-trigger');
    this.videoUrl = el.attr('data-video-url');
    var _this = this;

    if (el.length) {
      $(document).on('click', '.c-modal-video .bg, .c-modal-video .close', function() {
        _this.hide();
      });

      el.click(function() {
        _this.show();
      });
    }	
  },
  show: function() {
    $('body').append('<div class="c-modal-video"><div class="bg"></div><div class="popup"><div class="close"></div><div class="videoWrapper"><iframe width="832" height="468" src="'+this.videoUrl+'" frameborder="0" allowfullscreen></iframe></div></div></div>');
    setTimeout(function() {
      $('.c-modal-video').fadeIn('fast');
      if ($isMobile.iOS()) {
          $('body').addClass('remove-scroll-ios');
      } else{
          $('body').addClass('remove-scroll');
      }
    }, 100);
  },
  hide: function() {
    $('.c-modal-video').fadeOut('fast', function() {
      $('.c-modal-video').remove();
      $('body').removeClass('remove-scroll-ios remove-scroll');
    });
  }
};
var WindowScroll = {

  // hasCounter
  hasCounter: false,

  // backToTop
  backToTop: function(){

    var _scrollTop = $(window).scrollTop(),
      _showButton = $('.main-footer').offset().top - ($('.main-footer').outerHeight() * 3.5);

    if (_scrollTop > _showButton){
      $('.js-back-to-top').addClass('show');
    } else {
      $('.js-back-to-top').removeClass('show');
    }

  },

  // startCounterParallaxSlider
  startCounterParallaxSlider: function(){
    var _scrollTop = $(window).scrollTop(),
    _startCounter = $('#myparallaxslider').offset().top - ($('#myparallaxslider').outerHeight() * 1.25);

    if (_scrollTop > _startCounter && !WindowScroll.hasCounter){
      WindowScroll.hasCounter = true;
      ParallaxSlider.counter($('#myparallaxslider .slide:nth-child(1)'));
    }
  },

  // init
  init: function() {

    if ($('.js-back-to-top').length) {
      $(window).scroll(function(){
        WindowScroll.backToTop();
      });
    }

    if ($('#myparallaxslider').length) {
      $(window).scroll(function(){
        WindowScroll.startCounterParallaxSlider();
      });
      WindowScroll.startCounterParallaxSlider();
    }

  }

};