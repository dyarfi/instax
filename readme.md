
## About This Laravel Apanel CMS

- ```$ composer install```
- ```$ php artisan migrate```
- ```$ php artisan serve```

## License

### Tech Requirements

- PHP: >=5.5.9
- Laravel: 5.2.*
- MySQL
- Docker
- Docker Compose



### Quick Start

This project comes with `docker-compose.yml`, so you can run this project in the docker enviroment.  Follow steps bellow to run this application on your local environment:

- Clone this repository
- `cd` to the project directory: `cd instax`
- Run `docker`containers for the application: `docker-compose up -d`
- Install composer dependencies inside docker: `docker-compose exec app composer install -vvv `
- Duplicate `.env.example`to `.env`
- Generate Laravel Application Key: `docker-compose exec app php artisan key:generate`
- Change database configuration in the `.env`file:  
  - `DB_HOST=localhost`
  - `DB_DATABASE=instax`
  - `DB_USERNAME=root`
  - `DB_PASSWORD=instaxpassword`
- Run database migration and seeder: `docker-compose exec app php artisan migrate --seed`
- Allow write permission on cache and storage directory:  
  - `sudo chmod 777 -R storage`
  - `sudo chmod 777 -R bootstrap/cache`
- Your application will run at port:  
  - MySQL: 33060
  - Nginx: 8000
  - PHP-FPM: 9000
- Now you can access the app by visiting `http://localhost:8000` url in browser.


The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
