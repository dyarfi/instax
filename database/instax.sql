CREATE DATABASE  IF NOT EXISTS `instax` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `instax`;
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: instax
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activations`
--

DROP TABLE IF EXISTS activations;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE activations (
  id int(10) unsigned NOT NULL,
  user_id int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  completed tinyint(1) NOT NULL DEFAULT '0',
  completed_at timestamp NULL DEFAULT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activations`
--

INSERT INTO activations VALUES (1,1,'4etSQNqXt5SSmbX3VfUgrPfmbj6MwCz0',1,'2016-06-21 02:51:50','2016-06-21 02:51:50','2016-06-21 02:51:50',NULL);
INSERT INTO activations VALUES (2,48,'kDe4cSVHlsdZmxVHFjMJ99ZSEibimqcD',1,'2016-06-21 02:54:24','2016-06-21 02:54:24','2016-06-21 02:54:24',NULL);
INSERT INTO activations VALUES (3,49,'ABcpvQ4KRyPZLDIYjNTg3dOvUGSLoUl3',1,'2016-06-21 02:56:58','2016-06-21 02:56:58','2016-06-21 02:56:58',NULL);
INSERT INTO activations VALUES (4,50,'w0uRcpILn5sjjVATjqOwdc3GngrUKlHp',1,'2016-06-21 02:59:37','2016-06-21 02:59:37','2016-06-21 02:59:37',NULL);
INSERT INTO activations VALUES (5,51,'QS6uzbOfYO0V89sE2g8KeThUKF0JUCq1',1,'2016-06-21 03:04:03','2016-06-21 03:04:03','2016-06-21 03:04:03',NULL);
INSERT INTO activations VALUES (6,52,'luRhYJdpJxRbYpGeFxAhowyXCVlS4Bre',1,'2016-06-21 03:08:26','2016-06-21 03:08:26','2016-06-21 03:08:26',NULL);
INSERT INTO activations VALUES (7,53,'Mc7yKxUVJWP13rncwOQa648yDgYQmDhM',1,'2016-06-21 03:10:56','2016-06-21 03:10:56','2016-06-21 03:10:56',NULL);
INSERT INTO activations VALUES (8,54,'4vywLMK9AxYaUKeRqPYRHrlolHNyUFtK',1,'2016-06-21 04:00:33','2016-06-21 04:00:33','2016-06-21 04:00:33',NULL);
INSERT INTO activations VALUES (9,55,'zvQHtu4CMbJOI1PvhplJzqPJrcKWQxZ7',1,'2016-06-21 04:24:28','2016-06-21 04:24:28','2016-06-21 04:24:28',NULL);
INSERT INTO activations VALUES (10,83,'a072lFx2n2BBKIWWlZ8U4tb0cm1Y1ySm',1,'2017-07-11 22:58:46','2017-07-11 22:58:46','2017-07-11 22:58:46',NULL);
INSERT INTO activations VALUES (11,84,'qXl8vfEpWynvQJCJAH4e6W9AXEV2JlSe',1,'2017-07-11 22:59:56','2017-07-11 22:59:56','2017-07-11 22:59:56',NULL);
INSERT INTO activations VALUES (12,85,'Wrp2eNRgzD1snwyb8koXtFrAW5DjTJ8y',1,'2017-07-11 23:36:30','2017-07-11 23:36:30','2017-07-11 23:36:30',NULL);
INSERT INTO activations VALUES (13,86,'Ox504M3HpkV4j2aIne07S2SyHABpMC9i',1,'2017-07-11 23:57:38','2017-07-11 23:57:38','2017-07-11 23:57:38',NULL);
INSERT INTO activations VALUES (14,87,'P2JodogxUNpCJXkaGm5eOQ2lAJiZuHfD',1,'2017-07-12 00:15:24','2017-07-12 00:15:24','2017-07-12 00:15:24',NULL);
INSERT INTO activations VALUES (15,89,'rW1YRk2untQ8N9Nfzwnq1dKUWs4RbIAB',1,'2017-07-12 00:18:16','2017-07-12 00:18:16','2017-07-12 00:18:16',NULL);
INSERT INTO activations VALUES (16,91,'VydUJDCSIV5EFZdQD60JVGd9iQTvQ8bd',1,'2017-07-12 00:18:51','2017-07-12 00:18:51','2017-07-12 00:18:51',NULL);
INSERT INTO activations VALUES (17,93,'21X85Xe0o4i53voIPLn0XxgGPbj6f24x',1,'2017-07-12 00:19:24','2017-07-12 00:19:24','2017-07-12 00:19:24',NULL);
INSERT INTO activations VALUES (18,95,'F8NXf2dSOySXNfVIc0bnzHZ853jvYdxs',1,'2017-07-12 00:21:17','2017-07-12 00:21:17','2017-07-12 00:21:17',NULL);
INSERT INTO activations VALUES (19,96,'SdfWI7nbF8aUmGluQrzvS2Yj1lhAVZhP',1,'2017-07-12 00:25:08','2017-07-12 00:25:08','2017-07-12 00:25:08',NULL);
INSERT INTO activations VALUES (20,97,'GsxHB2EBMEte2N8tIDjZS3UNvsM4OTul',1,'2017-07-12 00:26:48','2017-07-12 00:26:48','2017-07-12 00:26:48',NULL);
INSERT INTO activations VALUES (21,98,'DJV7I1EmGHFRh2vtAckzzNM8InmeUHHk',1,'2017-07-12 00:31:44','2017-07-12 00:31:44','2017-07-12 00:31:44',NULL);

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS images;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE images (
  id int(10) unsigned NOT NULL,
  participant_id int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  url varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  title varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  file_name varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  attribute varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  count int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  deleted_at timestamp NULL DEFAULT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

INSERT INTO images VALUES (1,1,'canvas',NULL,'Test Test test','19-05-10-5cd5675ca4e78.jpg',NULL,NULL,1,NULL,'2019-05-10 11:58:21','2019-05-10 11:58:21');
INSERT INTO images VALUES (2,1,'canvas',NULL,'Test Test','19-05-10-5cd567f9b8e97.jpg',NULL,NULL,1,NULL,'2019-05-10 12:00:58','2019-05-10 12:00:58');
INSERT INTO images VALUES (3,2,'canvas',NULL,'Test Test Test','19-05-10-5cd56cb2c7e9f.jpg',NULL,NULL,1,NULL,'2019-05-10 12:21:07','2019-05-10 12:21:07');
INSERT INTO images VALUES (4,2,'canvas',NULL,'TEst Tes Tes Test','19-05-13-5cd998db49eb3.jpg',NULL,NULL,1,NULL,'2019-05-13 16:18:37','2019-05-13 16:18:37');

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS logs;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  id int(10) unsigned NOT NULL,
  user_id int(11) NOT NULL,
  description varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  request text COLLATE utf8mb4_unicode_ci NOT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

INSERT INTO logs VALUES (1,20,'change','{\"url\":\"http:\\/\\/127.0.0.1:8000\\/admin-panel\\/setting\\/change\",\"method\":\"POST\",\"query\":\"http:\\/\\/127.0.0.1:8000\\/admin-panel\\/setting\\/change\",\"secure\":false,\"client_ip\":\"127.0.0.1\",\"payload\":{\"_token\":\"5JBogt2ximeQXf3AmWQu2mzFg33ijSj2vb2Arkzf\",\"setting_form\":\"bzRTNkdrZURRWmN6S2c0WGM3TDE2VkR2NVhtU3VOb0RkVnZ0cGVQcA==\",\"email-contact\":\"contact@apanel.app\",\"email-info\":\"info@apanel.app\",\"email-administrator\":\"administrator@apanel.app\",\"smtp-server\":\"127.0.0.1\",\"site-name\":\"Lotte Choco Pie - Video Vote\",\"maintenance-mode\":\"No\",\"site-locale\":\"en\",\"site-theme\":\"default\",\"site-admin-theme\":\"ace-admin\",\"site-tagline\":\"Award winning digital Agency in Indonesia\",\"site-timezone\":\"Asia\\/Jakarta\",\"site-country\":\"Indonesia\",\"facebook\":\"https:\\/\\/facebook.com\\/lottechocopieindonesia\",\"twitter\":\"https:\\/\\/twitter.com\\/lottechocopieindonesia\",\"youtube\":\"https:\\/\\/www.youtube.com\\/channel\\/UCTDHjqWFKK5eM0pr-MEbXdw\",\"instagram\":\"https:\\/\\/instagram.com\\/lottechocopie.id\",\"thumbnail-size\":\"380x420px\",\"image-size\":\"600x740px\",\"meta-robots\":\"index all, follow all\",\"meta-keywords\":\"Website cool meta keywords\",\"meta-description\":\"Website cool meta description\",\"meta-generator\":\"apanel 1.0\",\"address\":\"Menara Sentraya, Floor 36\\r\\nJl. Iskandarsyah Raya No.1A, Jakarta 12130\",\"phone\":\"(62) 21 304 5632\",\"fax\":\"(62) 21 4752 1433\",\"email\":\"company@d3-app.dev\",\"company-name\":\"Fuji Film Instax\"}}','2019-04-25 06:06:38','2019-04-25 06:06:38',NULL);
INSERT INTO logs VALUES (2,20,'change','{\"url\":\"http:\\/\\/127.0.0.1:8000\\/admin-panel\\/setting\\/change\",\"method\":\"POST\",\"query\":\"http:\\/\\/127.0.0.1:8000\\/admin-panel\\/setting\\/change\",\"secure\":false,\"client_ip\":\"127.0.0.1\",\"payload\":{\"_token\":\"5JBogt2ximeQXf3AmWQu2mzFg33ijSj2vb2Arkzf\",\"setting_form\":\"UEdSbHg3aHBFZW4xWk1PNjRzMmZKNUVlZzdWeWg5RTZ2VDNUSEJDZA==\",\"email-contact\":\"contact@apanel.app\",\"email-info\":\"info@apanel.app\",\"email-administrator\":\"administrator@apanel.app\",\"smtp-server\":\"127.0.0.1\",\"site-name\":\"Fuji Film Instax\",\"maintenance-mode\":\"No\",\"site-locale\":\"en\",\"site-theme\":\"default\",\"site-admin-theme\":\"ace-admin\",\"site-tagline\":\"Award winning digital Agency in Indonesia\",\"site-timezone\":\"Asia\\/Jakarta\",\"site-country\":\"Indonesia\",\"facebook\":\"https:\\/\\/facebook.com\\/lottechocopieindonesia\",\"twitter\":\"https:\\/\\/twitter.com\\/lottechocopieindonesia\",\"youtube\":\"https:\\/\\/www.youtube.com\\/channel\\/UCTDHjqWFKK5eM0pr-MEbXdw\",\"instagram\":\"https:\\/\\/instagram.com\\/lottechocopie.id\",\"thumbnail-size\":\"380x420px\",\"image-size\":\"600x740px\",\"meta-robots\":\"index all, follow all\",\"meta-keywords\":\"Website cool meta keywords\",\"meta-description\":\"Website cool meta description\",\"meta-generator\":\"apanel 1.0\",\"address\":\"Menara Sentraya, Floor 36\\r\\nJl. Iskandarsyah Raya No.1A, Jakarta 12130\",\"phone\":\"(62) 21 304 5632\",\"fax\":\"(62) 21 4752 1433\",\"email\":\"company@d3-app.dev\",\"company-name\":\"Fuji Film Instax\"}}','2019-04-25 06:13:07','2019-04-25 06:13:07',NULL);
INSERT INTO logs VALUES (3,20,'processForm','{\"url\":\"http:\\/\\/127.0.0.1:8000\\/admin-panel\\/menu\\/create\",\"method\":\"POST\",\"query\":\"http:\\/\\/127.0.0.1:8000\\/admin-panel\\/menu\\/create\",\"secure\":false,\"client_ip\":\"127.0.0.1\",\"payload\":{\"_token\":\"PZJQDBFOtx3SjKzCVmEHKZt3zAGjC3kMe5U0iOzJ\",\"name\":\"Home\",\"description\":\"<p>Home<\\/p>\",\"index\":\"1\",\"status\":\"1\"}}','2019-04-26 11:27:28','2019-04-26 11:27:28',NULL);
INSERT INTO logs VALUES (4,20,'processForm','{\"url\":\"http:\\/\\/127.0.0.1:8000\\/admin-panel\\/menu\\/create\",\"method\":\"POST\",\"query\":\"http:\\/\\/127.0.0.1:8000\\/admin-panel\\/menu\\/create\",\"secure\":false,\"client_ip\":\"127.0.0.1\",\"payload\":{\"_token\":\"PZJQDBFOtx3SjKzCVmEHKZt3zAGjC3kMe5U0iOzJ\",\"name\":\"Home\",\"description\":\"<p>Home<\\/p>\",\"index\":\"1\",\"status\":\"1\",\"image\":{},\"image_mobile\":{}}}','2019-04-26 11:48:51','2019-04-26 11:48:51',NULL);
INSERT INTO logs VALUES (5,20,'processForm','{\"url\":\"http:\\/\\/127.0.0.1:8000\\/admin-panel\\/menu\\/create\",\"method\":\"POST\",\"query\":\"http:\\/\\/127.0.0.1:8000\\/admin-panel\\/menu\\/create\",\"secure\":false,\"client_ip\":\"127.0.0.1\",\"payload\":{\"_token\":\"PZJQDBFOtx3SjKzCVmEHKZt3zAGjC3kMe5U0iOzJ\",\"name\":\"Home\",\"description\":\"<p>Home<\\/p>\",\"index\":\"1\",\"status\":\"1\",\"image\":{},\"image_mobile\":{}}}','2019-04-26 11:49:15','2019-04-26 11:49:15',NULL);
INSERT INTO logs VALUES (6,20,'processForm','{\"url\":\"http:\\/\\/127.0.0.1:8000\\/admin-panel\\/menu\\/create\",\"method\":\"POST\",\"query\":\"http:\\/\\/127.0.0.1:8000\\/admin-panel\\/menu\\/create\",\"secure\":false,\"client_ip\":\"127.0.0.1\",\"payload\":{\"_token\":\"PZJQDBFOtx3SjKzCVmEHKZt3zAGjC3kMe5U0iOzJ\",\"name\":\"About\",\"description\":\"<p>About<\\/p>\",\"index\":\"2\",\"status\":\"1\",\"image\":{},\"image_mobile\":{}}}','2019-04-26 12:00:30','2019-04-26 12:00:30',NULL);
INSERT INTO logs VALUES (7,20,'processForm','{\"url\":\"http:\\/\\/127.0.0.1:8000\\/admin-panel\\/menu\\/create\",\"method\":\"POST\",\"query\":\"http:\\/\\/127.0.0.1:8000\\/admin-panel\\/menu\\/create\",\"secure\":false,\"client_ip\":\"127.0.0.1\",\"payload\":{\"_token\":\"PZJQDBFOtx3SjKzCVmEHKZt3zAGjC3kMe5U0iOzJ\",\"name\":\"Activity\",\"description\":\"<p>Activity<\\/p>\",\"index\":\"3\",\"status\":\"1\",\"image\":{},\"image_mobile\":{}}}','2019-04-26 12:02:07','2019-04-26 12:02:07',NULL);

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS menus;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE menus (
  id int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  slug varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  description text COLLATE utf8mb4_unicode_ci,
  image varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  image_mobile varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `index` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

INSERT INTO menus VALUES (1,'Home','home','<p>Home</p>','menu_40535.jpg','menu_mobile_87192.jpg',1,1,'2019-04-26 11:49:15','2019-04-26 11:49:15',NULL);
INSERT INTO menus VALUES (2,'About','about','<p>About</p>','menu_83937.jpg','menu_mobile_64103.jpg',2,1,'2019-04-26 12:00:30','2019-04-26 12:00:30',NULL);
INSERT INTO menus VALUES (3,'Activity','activity','<p>Activity</p>','menu_99362.jpg','menu_mobile_53789.jpg',3,1,'2019-04-26 12:02:07','2019-04-26 12:02:07',NULL);

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS migrations;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE migrations (
  id int(10) unsigned NOT NULL,
  migration varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  batch int(11) NOT NULL,
  PRIMARY KEY (id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

INSERT INTO migrations VALUES (1,'2014_10_29_202547_migration_cartalyst_tags_create_tables',1);
INSERT INTO migrations VALUES (2,'2015_11_24_142038_migration_cartalyst_sentinel',1);
INSERT INTO migrations VALUES (4,'2016_06_28_122418_create_images_table',1);
INSERT INTO migrations VALUES (5,'2017_04_07_210036_create_logs_table',1);
INSERT INTO migrations VALUES (6,'2017_06_16_134354_create_mediable_tables',1);
INSERT INTO migrations VALUES (7,'2017_08_10_174039_create_sessions_table',1);
INSERT INTO migrations VALUES (8,'2019_04_24_090247_create_table_settings',1);
INSERT INTO migrations VALUES (9,'2015_12_01_223306_create_pages_table',2);
INSERT INTO migrations VALUES (10,'2019_04_25_062740_create_menus_table',3);
INSERT INTO migrations VALUES (11,'2019_04_25_064254_create_participants_table',4);

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS pages;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE pages (
  id int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  slug varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  description text COLLATE utf8mb4_unicode_ci,
  `index` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--


--
-- Table structure for table `participants`
--

DROP TABLE IF EXISTS participants;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE participants (
  id int(10) unsigned NOT NULL,
  provider_id varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  provider varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'email',
  profile_url varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  photo_url varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  first_name varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  last_name varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  username varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  email varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  avatar varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  about varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  phone varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  phone_number varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  phone_home varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  address varchar(214) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  region varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  province varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  urban_district varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  sub_urban varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  zip_code varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  website varchar(72) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  gender varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  age tinyint(3) unsigned DEFAULT NULL,
  nationality varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  id_number varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  file_name varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  verify varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  completed tinyint(3) unsigned DEFAULT NULL,
  logged_in tinyint(3) unsigned DEFAULT NULL,
  last_login int(10) unsigned DEFAULT NULL,
  session_id varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  join_date timestamp NULL DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  remember_token varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY participants_provider_id_unique (provider_id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participants`
--

INSERT INTO participants VALUES (1,NULL,'email',NULL,NULL,'Participant','One',NULL,NULL,'participant1@yahoo.com','$2y$10$CpJjdOsSH2v5d8AiwrQstOJO.e6ZonBArX3WVvUpTgmAMDdsxmWyG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'YKswpwSZ0LKSFKeSYJZwGEmWjvtNxupHNW9SVLh3qgAHvFPVncUKqYkRZBNS','2019-05-10 11:53:46','2019-05-10 11:53:46',NULL);
INSERT INTO participants VALUES (2,NULL,'email',NULL,NULL,'Particpant','Two',NULL,NULL,'participant2@gmail.com','$2y$10$.WaVkQhYBGXlIWdpmhAoJOa2Hh3NLaOoWxwdmwifYRYZ6FRVkPwOe',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'slFlZGPvLxvUHyq7DRfSq9iUIWQ8crac48JYyWzMrDQ9oxQhqmAKni72l2xI','2019-05-10 12:08:58','2019-05-10 12:08:58',NULL);

--
-- Table structure for table `persistences`
--

DROP TABLE IF EXISTS persistences;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE persistences (
  id int(10) unsigned NOT NULL,
  user_id int(10) unsigned NOT NULL,
  `code` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY persistences_code_unique (`code`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persistences`
--

--
-- Table structure for table `reminders`
--

DROP TABLE IF EXISTS reminders;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE reminders (
  id int(10) unsigned NOT NULL,
  user_id int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  completed tinyint(1) NOT NULL DEFAULT '0',
  completed_at timestamp NULL DEFAULT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reminders`
--


--
-- Table structure for table `role_users`
--

DROP TABLE IF EXISTS role_users;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE role_users (
  user_id int(10) unsigned NOT NULL,
  role_id int(10) unsigned NOT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (user_id,role_id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_users`
--

INSERT INTO role_users VALUES (20,1,'2015-11-27 23:34:56','2015-11-27 23:34:56',NULL);
INSERT INTO role_users VALUES (21,3,'2015-11-28 00:45:56','2015-11-28 00:45:56',NULL);
INSERT INTO role_users VALUES (22,3,'2015-12-01 23:08:03','2015-12-01 23:08:03',NULL);
INSERT INTO role_users VALUES (23,2,'2015-12-02 19:33:04','2015-12-02 19:33:04',NULL);
INSERT INTO role_users VALUES (24,4,'2015-12-02 19:32:54','2015-12-02 19:32:54',NULL);
INSERT INTO role_users VALUES (41,1,'2015-12-11 00:43:54','2015-12-11 00:43:54',NULL);
INSERT INTO role_users VALUES (42,1,'2015-12-11 00:50:56','2015-12-11 00:50:56',NULL);
INSERT INTO role_users VALUES (43,1,'2015-12-11 00:52:27','2015-12-11 00:52:27',NULL);
INSERT INTO role_users VALUES (98,2,'2017-07-12 00:43:55','2017-07-12 00:43:55',NULL);
INSERT INTO role_users VALUES (99,1,'2017-07-18 21:09:30','2017-07-18 21:09:30',NULL);

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS roles;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE roles (
  id int(10) unsigned NOT NULL,
  slug varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  permissions text COLLATE utf8mb4_unicode_ci,
  description text COLLATE utf8mb4_unicode_ci,
  deleted_at timestamp NULL DEFAULT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY roles_slug_unique (slug)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

INSERT INTO roles VALUES (1,'admin','Admin','{\"admin\":true,\"banner\":true,\"campaign\":true,\"news\":true,\"page\":true,\"participant\":true}',NULL,NULL,'2015-11-24 21:30:56','2018-04-05 11:24:01');
INSERT INTO roles VALUES (2,'publisher','Publisher','{\"admin\":true,\"page\":true}',NULL,NULL,'2015-11-24 21:59:15','2016-01-01 18:19:09');
INSERT INTO roles VALUES (3,'mechanic','Mechanic','{\"admin\":false}',NULL,NULL,'2015-11-26 02:22:27','2017-07-03 20:26:17');
INSERT INTO roles VALUES (4,'supervisor','Supervisor','{\"admin\":false,\"page\":true,\"participant\":true,\"task\":true}',NULL,NULL,'2015-11-26 02:25:18','2016-01-01 18:37:10');
INSERT INTO roles VALUES (6,'country-admin','Country Admin','{\"admin\":false}',NULL,NULL,'2015-11-28 00:56:06','2015-12-25 05:49:09');
INSERT INTO roles VALUES (7,'country-manager','Country Manager','{\"admin\":false,\"page\":true,\"participant\":true,\"task\":true}',NULL,NULL,'2015-12-25 05:51:05','2016-01-05 23:03:48');

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS sessions;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE sessions (
  id varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  user_id int(11) DEFAULT NULL,
  ip_address varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  user_agent text COLLATE utf8mb4_unicode_ci,
  payload text COLLATE utf8mb4_unicode_ci NOT NULL,
  last_activity int(11) NOT NULL,
  UNIQUE KEY sessions_id_unique (id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--


--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS settings;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE settings (
  id int(10) unsigned NOT NULL,
  `group` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  slug varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  description text COLLATE utf8mb4_unicode_ci,
  `value` text COLLATE utf8mb4_unicode_ci,
  help_text text COLLATE utf8mb4_unicode_ci,
  input_type varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  editable int(10) unsigned NOT NULL,
  weight int(10) unsigned NOT NULL,
  attributes text COLLATE utf8mb4_unicode_ci,
  `status` int(10) unsigned NOT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

INSERT INTO settings VALUES (1,'email','contact','Website Email Contact','email-contact','Official Email Contact for the Company','contact@apanel.app','This email must be a valid company contact','text',1,0,NULL,1,NULL,'2015-11-24 19:54:41','2019-04-25 06:13:07');
INSERT INTO settings VALUES (2,'email','info','Website Email Info','email-info','Email for the company information','info@apanel.app','Valid Email for the company information','text',1,0,NULL,1,NULL,'2015-11-24 19:55:36','2019-04-25 06:13:07');
INSERT INTO settings VALUES (3,'site','name','Site Name','site-name','Auto Wapps Website','Fuji Film Instax','Apanel','text',1,0,NULL,1,NULL,'2015-11-24 20:12:18','2019-04-25 06:13:07');
INSERT INTO settings VALUES (4,'site','maintenance','Maintenance Mode','maintenance-mode','Maintenance Mode','No','Maintenance Mode','text',1,0,NULL,1,NULL,'2015-11-25 05:14:19','2019-04-25 06:13:07');
INSERT INTO settings VALUES (5,'site','locale','Site Locale','site-locale','Site Default Locale Language','en','Site Locale Language','text',1,0,NULL,1,NULL,'2015-11-26 02:26:44','2019-04-25 06:13:07');
INSERT INTO settings VALUES (6,'email','administrator','Website Email Administrator','email-administrator','Default Email Administrator','administrator@apanel.app','This is used for contacting Email Administrator','text',1,0,NULL,1,NULL,'2015-12-02 19:30:14','2019-04-25 06:13:07');
INSERT INTO settings VALUES (7,'socmed','facebook','Facebook','facebook','Social Media link for Facebook Company','https://facebook.com/lottechocopieindonesia','This is the link for facebook page','text',1,0,NULL,1,NULL,'2015-12-07 19:57:37','2019-04-25 06:13:07');
INSERT INTO settings VALUES (8,'socmed','twitter','Twitter','twitter','The Social media link for the Twitter account company','https://twitter.com/lottechocopieindonesia','This is the link for Twitter account','text',1,0,NULL,0,NULL,'2015-12-07 19:58:46','2019-04-25 06:13:07');
INSERT INTO settings VALUES (9,'email','smtp.server','SMTP Server','smtp-server','SMTP server setting for sending email from website server','127.0.0.1','Default setting for SMTP','text',1,0,NULL,1,NULL,'2015-12-16 07:13:54','2019-04-25 06:13:07');
INSERT INTO settings VALUES (10,'image','logo','Image Logo','image-logo','Image logo for the Company Profiling','logo.png',NULL,'file',1,0,NULL,1,NULL,'2015-12-28 08:33:23','2018-03-21 22:07:02');
INSERT INTO settings VALUES (11,'meta','robots','Meta Robots','meta-robots','Meta Robots','index all, follow all',NULL,'text',1,0,NULL,1,NULL,'2016-01-04 20:28:39','2019-04-25 06:13:07');
INSERT INTO settings VALUES (12,'meta','keywords','Meta Keywords','meta-keywords','Meta keywords for the website','Website cool meta keywords',NULL,'textarea',1,0,NULL,1,NULL,'2016-01-04 20:29:14','2019-04-25 06:13:07');
INSERT INTO settings VALUES (13,'meta','description','Meta Description','meta-description','Meta Description for Website','Website cool meta description',NULL,'textarea',1,0,NULL,1,NULL,'2016-01-04 20:30:02','2019-04-25 06:13:07');
INSERT INTO settings VALUES (14,'meta','generator','Meta Generator','meta-generator','Meta Generator for the website','apanel 1.0',NULL,'text',1,0,NULL,1,NULL,'2016-01-04 20:30:51','2019-04-25 06:13:07');
INSERT INTO settings VALUES (15,'site','default.theme','Site Theme','site-theme','Site Theme default','default',NULL,'text',1,0,NULL,1,NULL,'2016-01-04 20:34:27','2019-04-25 06:13:07');
INSERT INTO settings VALUES (16,'site','admin.theme','Site Admin Theme','site-admin-theme','Site Admin Theme','ace-admin',NULL,'text',1,0,NULL,1,NULL,'2016-01-04 20:34:59','2019-04-25 06:13:07');
INSERT INTO settings VALUES (17,'site','tagline','Site Tagline','site-tagline','Site Tagline','Award winning digital Agency in Indonesia',NULL,'text',1,0,NULL,1,NULL,'2016-01-04 20:40:47','2019-04-25 06:13:07');
INSERT INTO settings VALUES (18,'site','timezone','Site Timezone','site-timezone','Timezone for website, related with content publishing','Asia/Jakarta','Timezone for website, related with content publishing','text',1,0,NULL,1,NULL,'2016-01-05 22:57:33','2019-04-25 06:13:07');
INSERT INTO settings VALUES (19,'site','country','Site Country','site-country','Website country','Indonesia',NULL,'text',1,0,NULL,1,NULL,'2016-01-05 23:38:53','2019-04-25 06:13:07');
INSERT INTO settings VALUES (20,'image','thumbnail_size','Thumbnail Size','thumbnail-size','Thumbnail Size for image website thumbnail','380x420px',NULL,'text',1,0,NULL,1,NULL,'2015-12-28 08:33:23','2019-04-25 06:13:07');
INSERT INTO settings VALUES (21,'image','image_size','Image Size','image-size','Image Size for image website images','600x740px',NULL,'text',1,0,NULL,1,NULL,'2015-12-28 08:33:23','2019-04-25 06:13:07');
INSERT INTO settings VALUES (22,'socmed','youtube','YouTube','youtube','YouTube Channel','https://www.youtube.com/channel/UCTDHjqWFKK5eM0pr-MEbXdw','Company YouTube Channel','text',1,0,NULL,1,NULL,'2017-06-22 20:37:56','2019-04-25 06:13:07');
INSERT INTO settings VALUES (23,'company','address','Address','address','Company Address','Menara Sentraya, Floor 36\r\nJl. Iskandarsyah Raya No.1A, Jakarta 12130','Company Address','textarea',1,0,NULL,1,NULL,'2017-06-22 20:37:56','2019-04-25 06:13:07');
INSERT INTO settings VALUES (24,'company','phone','Phone','phone','Company Phone Number','(62) 21 304 5632','Company Phone','text',1,0,NULL,1,NULL,'2017-07-04 23:35:27','2019-04-25 06:13:07');
INSERT INTO settings VALUES (25,'company','fax','Fax','fax','Company Fax Number','(62) 21 4752 1433','Company Fax Number','text',1,0,NULL,1,NULL,'2017-07-04 23:43:50','2019-04-25 06:13:07');
INSERT INTO settings VALUES (26,'company','email','Email','email','Company Email','company@d3-app.dev','Company Email','text',1,0,NULL,1,NULL,'2017-07-05 00:36:41','2019-04-25 06:13:07');
INSERT INTO settings VALUES (27,'socmed','instagram','Instagram','instagram','Company Instagram Account','https://instagram.com/lottechocopie.id','Company Instagram Account','text',1,0,NULL,1,NULL,'2017-07-05 18:54:09','2019-04-25 06:13:07');
INSERT INTO settings VALUES (28,'company','name','Name','company-name','Company Name','Fuji Film Instax','Client Company Name','text',1,0,NULL,1,NULL,'2017-07-05 00:36:41','2019-04-25 06:13:07');

--
-- Table structure for table `tagged`
--

DROP TABLE IF EXISTS tagged;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE tagged (
  id int(10) unsigned NOT NULL,
  taggable_type varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  taggable_id int(10) unsigned NOT NULL,
  tag_id int(10) unsigned NOT NULL,
  PRIMARY KEY (id),
  KEY tagged_taggable_type_taggable_id_index (taggable_type,taggable_id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tagged`
--


--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS tags;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE tags (
  id int(10) unsigned NOT NULL,
  namespace varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  slug varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  count int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--


--
-- Table structure for table `throttle`
--

DROP TABLE IF EXISTS throttle;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE throttle (
  id int(10) unsigned NOT NULL,
  user_id int(10) unsigned DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  ip varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id),
  KEY throttle_user_id_index (user_id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `throttle`
--


--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS users;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE users (
  id int(10) unsigned NOT NULL,
  username varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  email varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  permissions text COLLATE utf8mb4_unicode_ci,
  last_login timestamp NULL DEFAULT NULL,
  first_name varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  last_name varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  avatar varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  image varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  provider varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'email',
  provider_id varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  about varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  attributes varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  current_team_id int(11) NOT NULL,
  remember_token varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  deleted_at timestamp NULL DEFAULT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY users_email_unique (email),
  UNIQUE KEY users_provider_id_unique (provider_id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

INSERT INTO users VALUES (20,'dyarfi','defrian.yarfi@gmail.com','$2y$10$MgLIFp.aln//QNCuWKA98.DghURQqOtBLgLQF22MEJemlfRz5e9iq','{\"users.index\":true,\"users.edit\":true,\"users.update\":true,\"users.change\":true,\"users.create\":true,\"users.trash\":true,\"users.delete\":true,\"users.restored\":true,\"users.store\":true,\"users.show\":true,\"users.export\":true,\"users.dashboard\":true,\"roles.index\":true,\"roles.edit\":true,\"roles.update\":true,\"roles.change\":true,\"roles.create\":true,\"roles.trash\":true,\"roles.delete\":true,\"roles.restored\":true,\"roles.store\":true,\"roles.show\":true,\"teams.index\":true,\"teams.edit\":true,\"teams.update\":true,\"teams.change\":true,\"teams.create\":true,\"teams.delete\":true,\"teams.restored\":true,\"teams.trash\":true,\"teams.store\":true,\"teams.show\":true,\"teams.invite\":true,\"teams.invitation\":true,\"teams.attach\":true,\"teams.detach\":true,\"permissions.index\":true,\"permissions.edit\":true,\"permissions.create\":true,\"permissions.store\":true,\"permissions.change\":true,\"permissions.show\":true,\"settings.index\":true,\"settings.edit\":true,\"settings.update\":true,\"settings.create\":true,\"settings.store\":true,\"settings.trash\":true,\"settings.delete\":true,\"settings.restored\":true,\"settings.show\":true,\"settings.change\":true,\"logs.index\":true,\"logs.edit\":true,\"logs.create\":true,\"logs.store\":true,\"logs.export\":true,\"logs.show\":true,\"pages.index\":true,\"pages.edit\":true,\"pages.update\":true,\"pages.change\":true,\"pages.create\":true,\"pages.store\":true,\"pages.show\":true,\"menus.index\":true,\"menus.edit\":true,\"menus.update\":true,\"menus.change\":true,\"menus.create\":true,\"menus.store\":true,\"menus.show\":true,\"banners.index\":true,\"banners.edit\":true,\"banners.update\":true,\"banners.change\":true,\"banners.create\":true,\"banners.store\":true,\"banners.trash\":true,\"banners.delete\":true,\"banners.restored\":true,\"banners.show\":true,\"banners.datatable\":true,\"banners.export\":true,\"news.index\":true,\"news.edit\":true,\"news.update\":true,\"news.change\":true,\"news.create\":true,\"news.store\":true,\"news.trash\":true,\"news.delete\":true,\"news.restored\":true,\"news.show\":true,\"news.tags\":true,\"news.tags.show\":true,\"news.datatable\":true,\"news.export\":true,\"newscategories.index\":true,\"newscategories.edit\":true,\"newscategories.update\":true,\"newscategories.change\":true,\"newscategories.create\":true,\"newscategories.store\":true,\"newscategories.trash\":true,\"newscategories.delete\":true,\"newscategories.restored\":true,\"newscategories.show\":true,\"newscategories.datatable\":true,\"newscategories.export\":true,\"campaigns.index\":true,\"campaigns.edit\":true,\"campaigns.update\":true,\"campaigns.change\":true,\"campaigns.create\":true,\"campaigns.store\":true,\"campaigns.trash\":true,\"campaigns.delete\":true,\"campaigns.restored\":true,\"campaigns.show\":true,\"campaigns.datatable\":true,\"campaigns.export\":true,\"videos.index\":true,\"videos.edit\":true,\"videos.update\":true,\"videos.change\":true,\"videos.create\":true,\"videos.store\":true,\"videos.trash\":true,\"videos.delete\":true,\"videos.restored\":true,\"videos.show\":true,\"videos.datatable\":true,\"videos.export\":true,\"ambassadors.index\":true,\"ambassadors.edit\":true,\"ambassadors.update\":true,\"ambassadors.change\":true,\"ambassadors.create\":true,\"ambassadors.store\":true,\"ambassadors.trash\":true,\"ambassadors.delete\":true,\"ambassadors.restored\":true,\"ambassadors.show\":true,\"ambassadors.datatable\":true,\"ambassadors.export\":true,\"galleries.index\":true,\"galleries.edit\":true,\"galleries.update\":true,\"galleries.change\":true,\"galleries.create\":true,\"galleries.store\":true,\"galleries.trash\":true,\"galleries.delete\":true,\"galleries.restored\":true,\"galleries.show\":true,\"galleries.datatable\":true,\"galleries.export\":true,\"winners.index\":true,\"winners.edit\":true,\"winners.update\":true,\"winners.change\":true,\"winners.create\":true,\"winners.store\":true,\"winners.trash\":true,\"winners.delete\":true,\"winners.restored\":true,\"winners.show\":true,\"winners.datatable\":true,\"winners.export\":true,\"participants.index\":true,\"participants.edit\":true,\"participants.update\":true,\"participants.change\":true,\"participants.create\":true,\"participants.store\":true,\"participants.trash\":true,\"participants.delete\":true,\"participants.restored\":true,\"participants.show\":true,\"participants.datatable\":true,\"participants.export\":true,\"images.index\":true,\"images.edit\":true,\"images.update\":true,\"images.change\":true,\"images.create\":true,\"images.store\":true,\"images.trash\":true,\"images.delete\":true,\"images.restored\":true,\"images.show\":true,\"images.datatable\":true,\"images.export\":true,\"votes.index\":true,\"votes.edit\":true,\"votes.update\":true,\"votes.change\":true,\"votes.create\":true,\"votes.store\":true,\"votes.trash\":true,\"votes.delete\":true,\"votes.restored\":true,\"votes.show\":true}','2019-05-08 09:10:42','Defrian','Yarfi','http://gravatar.com/dyarfi','usr-34678.jpg','email',NULL,'Web developer, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Empire','{\"show_profile\":\"0\",\"show_profile_image\":\"0\",\"width\":100,\"height\":100,\"crop_x\":\"30\",\"crop_y\":\"48\",\"crop_w\":\"180\",\"crop_h\":\"180\"}',0,'9gs01qjBgox4eWdFxntXsUKgb60WpPo9vCIh4aYaql1T5bolXsEw9EY7XewG',1,NULL,'2015-11-27 23:34:56','2019-05-08 09:10:42');
INSERT INTO users VALUES (21,'dyarfi20','dyarfi20@gmail.com','$2y$10$PWBLmg2GWhV5WD6PCw94YeY0GW5ox5yfMXIWvSfz6yayKR8l.jaAa',NULL,'2017-10-25 19:52:08','Nairfed','Ifray','https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50','','google','114777686552144649876','Web Development and E-commerce and Application Programmer','{\"skins\":\"#438EB9\"}',0,'N2obQFeYALvqMaDAJiqie5vdarMM5nPh4K9HVyKxA1Qq5iPSYpWJsG2B0K8L',1,NULL,'2015-11-28 00:45:56','2017-10-25 19:52:08');
INSERT INTO users VALUES (22,'Emer','defrian.yarfi@yahoo.com','$2y$10$EwfWl7QpS9rM/eE/0ihc0OsH8l3DcXGuQjCzZeqU8KchlPqvQaBz2',NULL,'2017-07-12 21:29:43','Emeraldi','Octavian',NULL,'usr-80594.jpg','email',NULL,'Baby of the house','{\"skins\":\"#438EB9\",\"crop_x\":null,\"crop_y\":null,\"width\":100,\"height\":100,\"crop_w\":\"180\",\"crop_h\":\"180\"}',0,'SyprnvoUIHMUrkqpEh43w0AQIOG7Aeh7fsa2hmeXBsPKbX4Anot7reBd5Lx7',1,NULL,'2015-12-01 23:08:03','2017-07-12 21:29:43');
INSERT INTO users VALUES (23,'zmael','deffsidefry@ymail.com','$2y$10$zmSx2zI1lONqDAVMc8VXR.D3BAkVTlqbF3bkR2yA6DugqqclQC556',NULL,'2017-07-12 00:23:36','Zmael','Milajovic',NULL,'','email',NULL,'Test Driven Application','',0,'Adb3SrELAh0wDtEx2b0icwT4W85GHz5gYygjoAw7dwSDYcIJzOmMymPoJgMP',1,NULL,'2015-12-02 19:32:20','2017-07-12 00:23:36');
INSERT INTO users VALUES (24,'Defrian Dentsu','defrian.yarfi@d3.dentsu.co.id','$2y$10$DGxOR9TQVX2RRxFXUK.LKON4iX3x4uYjLICnluVfNDIMoUTGTvO.W',NULL,'2017-10-20 17:30:18','Yudhay','Kendricks',NULL,'','email',NULL,'Web Developer Senior defrian.yarfi@d3.dentsu.co.id','',0,'WhdvWGTRoJOsMKPY19J6o3ysWFTuUoa0N2z2K3ebrPhQdEiGt0bjSUfKGz9A',1,NULL,'2015-12-02 19:32:54','2017-10-20 17:30:18');
INSERT INTO users VALUES (43,'Valents','defrian.yarfi@facebook.com','$2y$10$Ky4GJjugVDkm5T8Oth//.Oc.DUrO7bWMR7xFH0mR3H.b677Nbqn8m','{\"users.index\":true,\"users.edit\":true,\"users.update\":true,\"users.change\":true,\"users.create\":true,\"users.trash\":true,\"users.delete\":true,\"users.restored\":true,\"users.store\":true,\"users.show\":true,\"users.export\":true,\"users.dashboard\":true,\"roles.index\":true,\"roles.edit\":true,\"roles.update\":true,\"roles.change\":true,\"roles.create\":true,\"roles.trash\":true,\"roles.delete\":true,\"roles.restored\":true,\"roles.store\":true,\"roles.show\":true,\"teams.index\":true,\"teams.edit\":true,\"teams.update\":true,\"teams.change\":true,\"teams.create\":true,\"teams.delete\":true,\"teams.restored\":true,\"teams.trash\":true,\"teams.store\":true,\"teams.show\":true,\"teams.invite\":true,\"teams.invitation\":true,\"teams.attach\":true,\"teams.detach\":true,\"permissions.index\":true,\"permissions.edit\":true,\"permissions.create\":true,\"permissions.store\":true,\"permissions.change\":true,\"permissions.show\":true,\"settings.index\":true,\"settings.edit\":true,\"settings.update\":true,\"settings.create\":true,\"settings.store\":true,\"settings.trash\":true,\"settings.delete\":true,\"settings.restored\":true,\"settings.show\":true,\"settings.change\":true,\"logs.index\":true,\"logs.edit\":true,\"logs.create\":true,\"logs.store\":true,\"logs.show\":true,\"pages.index\":true,\"pages.edit\":true,\"pages.update\":true,\"pages.change\":true,\"pages.create\":true,\"pages.store\":true,\"pages.show\":true,\"menus.index\":true,\"menus.edit\":true,\"menus.update\":true,\"menus.change\":true,\"menus.create\":true,\"menus.store\":true,\"menus.show\":true,\"banners.index\":true,\"banners.edit\":true,\"banners.update\":true,\"banners.change\":true,\"banners.create\":true,\"banners.store\":true,\"banners.trash\":true,\"banners.delete\":true,\"banners.restored\":true,\"banners.show\":true,\"banners.datatable\":true,\"banners.export\":true,\"blogs.index\":true,\"blogs.edit\":true,\"blogs.update\":true,\"blogs.change\":true,\"blogs.create\":true,\"blogs.store\":true,\"blogs.trash\":true,\"blogs.delete\":true,\"blogs.restored\":true,\"blogs.show\":true,\"blogs.tags\":true,\"blogs.tags.show\":true,\"blogs.datatable\":true,\"blogs.export\":true,\"blogcategories.index\":true,\"blogcategories.edit\":true,\"blogcategories.update\":true,\"blogcategories.change\":true,\"blogcategories.create\":true,\"blogcategories.store\":true,\"blogcategories.trash\":true,\"blogcategories.delete\":true,\"blogcategories.restored\":true,\"blogcategories.show\":true,\"blogcategories.datatable\":true,\"blogcategories.export\":true,\"tasks.index\":true,\"tasks.edit\":true,\"tasks.update\":true,\"tasks.change\":true,\"tasks.create\":true,\"tasks.store\":true,\"tasks.trash\":true,\"tasks.delete\":true,\"tasks.restored\":true,\"tasks.show\":true,\"careers.index\":true,\"careers.edit\":true,\"careers.update\":true,\"careers.change\":true,\"careers.create\":true,\"careers.store\":true,\"careers.trash\":true,\"careers.delete\":true,\"careers.restored\":true,\"careers.show\":true,\"divisions.index\":true,\"divisions.edit\":true,\"divisions.update\":true,\"divisions.change\":true,\"divisions.create\":true,\"divisions.store\":true,\"divisions.trash\":true,\"divisions.delete\":true,\"divisions.restored\":true,\"divisions.show\":true,\"applicants.index\":true,\"applicants.edit\":true,\"applicants.update\":true,\"applicants.change\":true,\"applicants.create\":true,\"applicants.store\":true,\"applicants.trash\":true,\"applicants.delete\":true,\"applicants.restored\":true,\"applicants.show\":true,\"contacts.index\":true,\"contacts.edit\":true,\"contacts.update\":true,\"contacts.change\":true,\"contacts.create\":true,\"contacts.store\":true,\"contacts.trash\":true,\"contacts.delete\":true,\"contacts.restored\":true,\"contacts.show\":true,\"contacts.datatable\":true,\"contacts.export\":true,\"participants.index\":true,\"participants.edit\":true,\"participants.update\":true,\"participants.change\":true,\"participants.create\":true,\"participants.store\":true,\"participants.trash\":true,\"participants.delete\":true,\"participants.restored\":true,\"participants.show\":true,\"participants.datatable\":true,\"participants.export\":true,\"images.index\":true,\"images.edit\":true,\"images.update\":true,\"images.change\":true,\"images.create\":true,\"images.store\":true,\"images.trash\":true,\"images.delete\":true,\"images.restored\":true,\"images.show\":true,\"clients.index\":true,\"clients.edit\":true,\"clients.update\":true,\"clients.change\":true,\"clients.create\":true,\"clients.store\":true,\"clients.trash\":true,\"clients.delete\":true,\"clients.restored\":true,\"clients.show\":true,\"clients.datatable\":true,\"clients.export\":true,\"projects.index\":true,\"projects.edit\":true,\"projects.update\":true,\"projects.change\":true,\"projects.create\":true,\"projects.store\":true,\"projects.trash\":true,\"projects.delete\":true,\"projects.restored\":true,\"projects.show\":true,\"projects.datatable\":true,\"projects.export\":true,\"portfolios.index\":true,\"portfolios.edit\":true,\"portfolios.update\":true,\"portfolios.change\":true,\"portfolios.create\":true,\"portfolios.store\":true,\"portfolios.trash\":true,\"portfolios.delete\":true,\"portfolios.restored\":true,\"portfolios.show\":true,\"portfolios.tags\":true,\"portfolios.tags.show\":true,\"portfolios.datatable\":true,\"portfolios.export\":true}','2017-10-25 19:53:00','Valent','Schemaichel',NULL,'usr-62496.jpg','email',NULL,'Speaker of the Computer','{\"crop_x\":\"90\",\"crop_y\":\"65\",\"crop_w\":\"90\",\"crop_h\":\"90\",\"skins\":\"#438EB9\",\"show_profile\":\"0\",\"show_profile_image\":\"0\"}',0,'fQhcgNZyp3xdVvUdP7cRt8y44FYLAAqCInUSlTptS5HBkebZIgQzqG1rfEdC',1,NULL,'2015-12-11 00:52:27','2017-10-25 19:53:00');
INSERT INTO users VALUES (44,'Defrian','admin@admin.com','$2y$10$CNsYxdwKHVD3ijfCJv8yS.X/RI9Vcnw0Wg12mONbcEHPkEMHe.Ybq',NULL,NULL,NULL,NULL,NULL,'','email',NULL,'Host of the House','',0,'xmzy19SuAbDW0OZ7q2FZKDRY2AkzJw0qrh6wbH6PuonX7lCKS1L57QMamGbz',1,NULL,'2015-12-31 05:19:23','2016-07-01 02:56:45');
INSERT INTO users VALUES (55,'dyarfi','-','$2y$10$1sJUeCFz1ED/tojSjXXZcOh8RF4fUZLXvgCxIxmmZbIeWd2C6/gee',NULL,'2016-06-21 15:46:01','Defrian','Yarfi','http://pbs.twimg.com/profile_images/417721509696634880/tKSK06gY_normal.jpeg','','twitter','300187659','Web Developer','',0,'Ak3S1VuS1fh1BoHXsLVkeFUJfwxiID4mqIKVtkYtS8yfnWckeI3QrCHxGND4',2,NULL,'2016-06-21 04:24:28','2017-10-25 19:14:50');
INSERT INTO users VALUES (96,NULL,'defrian.yarfi@gmail.co.id','$2y$10$VU4m.i9eybAgvqY8SwgwOuD7n6bmy6bV/F8LJFAYryyxWCAL1BISC',NULL,'2017-07-12 00:25:08',NULL,NULL,NULL,NULL,'email',NULL,NULL,'',0,NULL,1,NULL,'2017-07-12 00:25:08','2017-07-12 00:25:08');
INSERT INTO users VALUES (97,NULL,'defrian.yarfi@facebook.co.id','$2y$10$gE4x4MOOUpSFbbccNS8i2eo/NPsw0fvTSKlkvgi04Ngf2H7ASPfFy',NULL,'2017-07-12 00:26:48',NULL,NULL,NULL,NULL,'email',NULL,NULL,'',0,NULL,1,NULL,'2017-07-12 00:26:48','2017-07-12 00:26:48');
INSERT INTO users VALUES (98,NULL,'deffsidefry@ymail.co.id','$2y$10$OzjRyvfixamHrK7q5dwmh.Nt2sbwG.1.A58v21anN84tdnUD.rzKu','{\"users.index\":true,\"users.edit\":true,\"users.update\":true,\"users.change\":true,\"users.create\":true,\"users.trash\":true,\"users.delete\":true,\"users.restored\":true,\"users.store\":true,\"users.show\":true,\"users.export\":true,\"users.dashboard\":true,\"roles.index\":true,\"roles.edit\":true,\"roles.update\":true,\"roles.change\":true,\"roles.create\":true,\"roles.trash\":true,\"roles.delete\":true,\"roles.restored\":true,\"roles.store\":true,\"roles.show\":true,\"teams.index\":true,\"teams.edit\":true,\"teams.update\":true,\"teams.change\":true,\"teams.create\":true,\"teams.delete\":true,\"teams.restored\":true,\"teams.trash\":true,\"teams.store\":true,\"teams.show\":true,\"teams.invite\":true,\"teams.invitation\":true,\"teams.attach\":true,\"teams.detach\":true,\"permissions.index\":true,\"permissions.edit\":true,\"permissions.create\":true,\"permissions.store\":true,\"permissions.change\":true,\"permissions.show\":true,\"settings.index\":true,\"settings.edit\":true,\"settings.update\":true,\"settings.create\":true,\"settings.store\":true,\"settings.trash\":true,\"settings.delete\":true,\"settings.restored\":true,\"settings.show\":true,\"settings.change\":true,\"logs.index\":true,\"logs.edit\":true,\"logs.create\":true,\"logs.store\":true,\"logs.show\":true,\"pages.index\":true,\"pages.edit\":true,\"pages.update\":true,\"pages.change\":true,\"pages.create\":true,\"pages.store\":true,\"pages.show\":true,\"menus.index\":true,\"menus.edit\":true,\"menus.update\":true,\"menus.change\":true,\"menus.create\":true,\"menus.store\":true,\"menus.show\":true}','2017-07-12 00:47:07',NULL,NULL,NULL,NULL,'email',NULL,NULL,'',0,NULL,1,NULL,'2017-07-12 00:31:44','2017-07-12 00:47:25');
INSERT INTO users VALUES (99,NULL,'ezflkr@gmail.com','$2y$10$RpbFGFNL/FnGOwWYzruEuuDRLhEJklJkRt9lfEMlldAytvpRBlI2u',NULL,NULL,'Eik','Zulfikar',NULL,NULL,'email',NULL,NULL,'',0,NULL,1,NULL,'2017-07-18 21:09:30','2017-07-18 21:09:30');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
