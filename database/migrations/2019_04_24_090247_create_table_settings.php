<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('group', 32)->nullable();
            $table->string('key', 64)->nullable();
            $table->string('name', 64)->nullable();
            $table->string('slug', 64)->nullable();
            $table->text('description')->nullable();
            $table->text('value')->nullable();
            $table->text('help_text')->nullable();
            $table->string('input_type',64)->nullable();
            $table->integer('editable')->unsigned();
            $table->integer('weight')->nullable();
            $table->text('attributes')->nullable();
            $table->integer('status')->unsigned();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
