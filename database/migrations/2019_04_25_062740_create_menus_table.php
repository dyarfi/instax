<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('menus', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('slug')->nullable();            
        $table->text('description')->nullable();
        $table->string('image')->nullable();
        $table->string('image_mobile')->nullable();
        $table->integer('index')->unsigned();
        $table->boolean('status')->default(1);
        $table->nullableTimestamps();
        $table->softDeletes();            
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {       
	    Schema::drop('menus');
    }
}
