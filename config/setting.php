<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Administrator panel settings
	|--------------------------------------------------------------------------
	|
	| Change this settings to desire name and defaults
	| this will be the url for administration CMS
	|
	|
	*/

	// Company name default
	'company_name'	=> 'Company Name',

	// Administrator label name
	'admin_app' => 'Admin Panel',

	// Administrator url
	'admin_url' => 'admin-panel',

	/*
	 |--------------------------------------------------------------------------
	 | Access Controller Lists in administrator panel modules
	 |--------------------------------------------------------------------------
	 |
	 | Modify or add new configuration
	 | Always add new array [action],[controller] for registering new controller
	 |
	 */

	'modules' => [
				// Admin modules will be in the App Modules directory
				['Admin' => [
						//------ Admin users controller @ see ModuleServiceProviders for flagged User Module
						['Users' => [
								// Action for first index
								'action' => ['users.index'],
								// Controller method list
								'method' => ['users.index','users.edit','users.update','users.change','users.create','users.trash','users.delete','users.restored','users.store','users.show','users.export','users.dashboard'],
							]
						],
						//------ Admin roles controller
						['Roles' => [
								// Action for first index
								'action' => ['roles.index'],
								// Controller method list
								'method' => ['roles.index','roles.edit','roles.update','roles.change','roles.create','roles.trash','roles.delete','roles.restored','roles.store','roles.show']
							]
						],
						//------ User teams controller
						// ['Teams' => [
						// 		// Action for first index
						// 		'action' => ['teams.index'],
						// 		// Controller method list
						// 		'method' => ['teams.index','teams.edit','teams.update','teams.change','teams.create','teams.delete','teams.restored','teams.trash','teams.store','teams.show','teams.invite','teams.invitation','teams.attach','teams.detach']
						// 	]
						// ],
						//------ Admin permissions controller
						['Permissions' => [
								// Action for first index
								'action' => ['permissions.index'],
								// Controller method list
								'method' => ['permissions.index','permissions.edit','permissions.create','permissions.store','permissions.change','permissions.show']
							]
						],
						//------ Admin settings controller
						['Settings' => [
								// Action for first index
								'action' => ['settings.index'],
								// Controller method list
								'method' => ['settings.index','settings.edit','settings.update','settings.create','settings.store','settings.trash','settings.delete','settings.restored','settings.show','settings.change']
							]
						],
						//------ Admin logs controller
						['Logs' => [
								// Action for first index
								'action' => ['logs.index'],
								// Controller method list
								'method' => ['logs.index','logs.edit','logs.create','logs.store','logs.export','logs.show']
							]
						]
					]
				],
				// Pages modules will be in the App Modules directory
				['Page' => [
						//------ Pages controller
						['Pages' => [
								// Action for first index
								'action' => ['pages.index'],
								// Controller method list
								'method' => ['pages.index','pages.edit','pages.update','pages.change','pages.create','pages.store','pages.show'],
							]
						],
						//------ Menus controller
						['Menus' => [
								// Action for first index
								'action' => ['menus.index'],
								// Controller method list
								'method' => ['menus.index','menus.edit','menus.update','menus.change','menus.create','menus.store','menus.show'],
							]
						],
					]
				],
				// Banner modules will be in the App Modules directory
				['Banner' => [
						//------ Banners controller
						['Banners' => [
								// Action for first index
								'action' => ['banners.index'],
								// Controller method list
								'method' => ['banners.index','banners.edit','banners.update','banners.change','banners.create','banners.store','banners.trash','banners.delete','banners.restored','banners.show','banners.datatable','banners.export']
							]
						]
					]
				],
				// Blog modules will be in the App Modules directory
				// ['Blog' => [
				// 		//------ Blog controller
				// 		['Blogs' => [
				// 				// Action for first index
				// 				'action' => ['blogs.index'],
				// 				// Controller method list
				// 				'method' => ['blogs.index','blogs.edit','blogs.update','blogs.change','blogs.create','blogs.store','blogs.trash','blogs.delete','blogs.restored','blogs.show','blogs.tags','blogs.tags.show','blogs.datatable','blogs.export']
				// 			]
				// 		],
				// 		//------ Blog Category controller
				// 		['Category' => [
				// 				// Action for first index
				// 				'action' => ['blogcategories.index'],
				// 				// Controller method list
				// 				'method' => ['blogcategories.index','blogcategories.edit','blogcategories.update','blogcategories.change','blogcategories.create','blogcategories.store','blogcategories.trash','blogcategories.delete','blogcategories.restored','blogcategories.show','blogcategories.datatable','blogcategories.export']
				// 			]
				// 		]
				// 	]
				// ],
				// News modules will be in the App Modules directory
				// ['News' => [
				// 		//------ News controller
				// 		['News' => [
				// 				// Action for first index
				// 				'action' => ['news.index'],
				// 				// Controller method list
				// 				'method' => ['news.index','news.edit','news.update','news.change','news.create','news.store','news.trash','news.delete','news.restored','news.show','news.tags','news.tags.show','news.datatable','news.export']
				// 			]
				// 		],
				// 		//------ News Category controller
				// 		['Category' => [
				// 				// Action for first index
				// 				'action' => ['newscategories.index'],
				// 				// Controller method list
				// 				'method' => ['newscategories.index','newscategories.edit','newscategories.update','newscategories.change','newscategories.create','newscategories.store','newscategories.trash','newscategories.delete','newscategories.restored','newscategories.show','newscategories.datatable','newscategories.export']
				// 			]
				// 		]
				// 	]
				// ],
				// Tasks modules will be in the App Modules directory
				// ['Task' => [
				// 		//------ Tasks controller
				// 		['Tasks' => [
				// 				// Action for first index
				// 				'action' => ['tasks.index'],
				// 				// Controller method list
				// 				'method' => ['tasks.index','tasks.edit','tasks.update','tasks.change','tasks.create','tasks.store','tasks.trash','tasks.delete','tasks.restored','tasks.show']
				// 			]
				// 		]
				// 	]
				// ],
				// Campaign modules will be in the App Modules directory
				// ['Campaign' => [
				// 			//------ Campaigns controller
				// 			['Campaigns' => [
				// 				// Action for first index
				// 				'action' => ['campaigns.index'],
				// 				// Controller method list
				// 				'method' => ['campaigns.index','campaigns.edit','campaigns.update','campaigns.change','campaigns.create','campaigns.store','campaigns.trash','campaigns.delete','campaigns.restored','campaigns.show','campaigns.datatable','campaigns.export']
				// 			]
				// 		],
				// 		//------ Campaign controller
				// 		['Videos' => [
				// 				// Action for first index
				// 				'action' => ['videos.index'],
				// 				// Controller method list
				// 				'method' => ['videos.index','videos.edit','videos.update','videos.change','videos.create','videos.store','videos.trash','videos.delete','videos.restored','videos.show','videos.datatable','videos.export']
				// 			]
				// 		],
				// 		//------ Ambassador controller
				// 		['Ambassadors' => [
				// 				// Action for first index
				// 				'action' => ['ambassadors.index'],
				// 				// Controller method list
				// 				'method' => ['ambassadors.index','ambassadors.edit','ambassadors.update','ambassadors.change','ambassadors.create','ambassadors.store','ambassadors.trash','ambassadors.delete','ambassadors.restored','ambassadors.show','ambassadors.datatable','ambassadors.export']
				// 			]
				// 		],
						/*
						//------ Event controller
						['Events' => [
								// Action for first index
								'action' => ['events.index'],
								// Controller method list
								'method' => ['events.index','events.edit','events.update','events.change','events.create','events.store','events.trash','events.delete','events.restored','events.show','events.datatable','events.export']
							]
						],
						*/
						//------ Gallery controller
				// 		['Galleries' => [
				// 				// Action for first index
				// 				'action' => ['galleries.index'],
				// 				// Controller method list
				// 				'method' => ['galleries.index','galleries.edit','galleries.update','galleries.change','galleries.create','galleries.store','galleries.trash','galleries.delete','galleries.restored','galleries.show','galleries.datatable','galleries.export']
				// 			]
        //                 ],                        
				// 		//------ Winners controller
				// 		['Winners' => [
        //                         // Action for first index
        //                         'action' => ['winners.index'],
        //                         // Controller method list
        //                         'method' => ['winners.index','winners.edit','winners.update','winners.change','winners.create','winners.store','winners.trash','winners.delete','winners.restored','winners.show','winners.datatable','winners.export']
        //                     ]
        //                 ],
				// 	]
				// ],
				// // Career modules will be in the App Modules directory
				// ['Career' => [
				// 		//------ Career controller
				// 		['Careers' => [
				// 				// Action for first index
				// 				'action' => ['careers.index'],
				// 				// Controller method list
				// 				'method' => ['careers.index','careers.edit','careers.update','careers.change','careers.create','careers.store','careers.trash','careers.delete','careers.restored','careers.show']
				// 			]
				// 		],
				// 		//------ Division controller
				// 		['Divisions' => [
				// 				// Action for first index
				// 				'action' => ['divisions.index'],
				// 				// Controller method list
				// 				'method' => ['divisions.index','divisions.edit','divisions.update','divisions.change','divisions.create','divisions.store','divisions.trash','divisions.delete','divisions.restored','divisions.show']
				// 			]
				// 		],
				// 		//------ Applicant controller
				// 		['Applicants' => [
				// 				// Action for first index
				// 				'action' => ['applicants.index'],
				// 				// Controller method list
				// 				'method' => ['applicants.index','applicants.edit','applicants.update','applicants.change','applicants.create','applicants.store','applicants.trash','applicants.delete','applicants.restored','applicants.show']
				// 			]
				// 		]
				// 	]
				// ],
				// // Contact modules will be in the App Modules directory
				// ['Contact' => [
				// 		//------ Contact controller
				// 		['Contacts' => [
				// 				// Action for first index
				// 				'action' => ['contacts.index'],
				// 				// Controller method list
				// 				'method' => ['contacts.index','contacts.edit','contacts.update','contacts.change','contacts.create','contacts.store','contacts.trash','contacts.delete','contacts.restored','contacts.show','contacts.datatable','contacts.export']
				// 			]
				// 		]
				// 	]
				// ],
				// Participant modules will be in the App Modules directory
				['Participant' => [
						//------ Participants controller
						['Participants' => [
								// Action for first index
								'action' => ['participants.index'],
								// Controller method list
								'method' => ['participants.index','participants.edit','participants.update','participants.change','participants.create','participants.store','participants.trash','participants.delete','participants.restored','participants.show','participants.datatable','participants.export']
							]
						],
						//------ Participant Images controller
						['Images' => [
								// Action for first index
								'action' => ['images.index'],
								// Controller method list
								'method' => ['images.index','images.edit','images.update','images.change','images.create','images.store','images.trash','images.delete','images.restored','images.show','images.datatable','images.export']
							]
						],
						//------ Participant Votes controller
						// ['Votes' => [
						// 		// Action for first index
						// 		'action' => ['votes.index'],
						// 		// Controller method list
						// 		'method' => ['votes.index','votes.edit','votes.update','votes.change','votes.create','votes.store','votes.trash','votes.delete','votes.restored','votes.show']
						// 	]
						// ]
					]
				],
				// Portfolio modules will be in the App Modules directory
				// ['Portfolio' => [
				// 		//------ Clients controller
				// 		['Clients' => [
				// 				// Action for first index
				// 				'action' => ['clients.index'],
				// 				// Controller method list
				// 				'method' => ['clients.index','clients.edit','clients.update','clients.change','clients.create','clients.store','clients.trash','clients.delete','clients.restored','clients.show','clients.datatable','clients.export']
				// 			]
				// 		],
				// 		//------ Projects controller
				// 		['Projects' => [
				// 				// Action for first index
				// 				'action' => ['projects.index'],
				// 				// Controller method list
				// 				'method' => ['projects.index','projects.edit','projects.update','projects.change','projects.create','projects.store','projects.trash','projects.delete','projects.restored','projects.show','projects.datatable','projects.export']
				// 			]
				// 		],
				// 		//------ Portfolios controller
				// 		['Portfolios' => [
				// 				// Action for first index
				// 				'action' => ['portfolios.index'],
				// 				// Controller method list
				// 				'method' => ['portfolios.index','portfolios.edit','portfolios.update','portfolios.change','portfolios.create','portfolios.store','portfolios.trash','portfolios.delete','portfolios.restored','portfolios.show','portfolios.tags','portfolios.tags.show','portfolios.datatable','portfolios.export','portfolios.medialist']
				// 			]
				// 		]
				// 	]
				// ]

	 ],


 	/*
	 |--------------------------------------------------------------------------
	 | Users attributes default in administrator panel
	 |--------------------------------------------------------------------------
	 |
	 | Modify or add new configuration
	 | Always add new array [attribute],[value] for registering new attribute
	 |
	 */

	 'attributes' => [
		['skins' =>
			['#438EB9' => true, '#222A2D' => false, '#C6487E' => false, '#D0D0D0' => false]
	 	],
	 	['show_email' =>
	 		['Yes' => 1, 'No' => 0]
	 	],
	 	['show_profile' =>
	 		['Yes' => 1, 'No' => 0]
	 	],
	 	['show_profile_image' =>
	 		['Yes' => 1, 'No' => 0]
	 	],
	],

	/*
	 |--------------------------------------------------------------------------
	 | Table status inactive or active attributes default in administrator panel
	 |--------------------------------------------------------------------------
	 |
	 | Modify or add new configuration
	 | Always add new array [attribute],[value] for registering new attribute
	 |
	 */
    'status' =>
		[
			'0' => 'Unsigned',
			'1' => 'Active',
			'2' => 'Inactive'
        ],
    'winner' =>
		[
			'0' => 'No',
			'1' => 'Yes'
		],   

	'configure'	=> ['install' => true,'safe_mode'=> 'http://your-site.com/']
];
